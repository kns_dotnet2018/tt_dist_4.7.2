﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using DistributorSystemBL;
using DistributorSystemBO;
using System.Collections;

namespace DistributorSystem
{
    public partial class SMDetails : System.Web.UI.Page
    {
        SMDetailsBO objSMBO;
        SMDetailsBL objSMBL;
        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDistributors = new DataTable();
                if (!IsPostBack)
                {
                    objSMBL = new SMDetailsBL();
                    objSMBO = new SMDetailsBO();
                    objSMBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                    dtDistributors = objSMBL.getSM_Mapping_DetailsBL(objSMBO);
                    Session["dtDistributors"] = dtDistributors;
                    Session["ID"] = null;
                    if (dtDistributors.Rows.Count > 0)
                    {
                        grdSMDetails.DataSource = dtDistributors;
                        grdSMDetails.DataBind();
                        int id = Convert.ToInt32(dtDistributors.Rows[0]["ID"]);
                        ////Session["Clicked_ID"] = ID;
                        DataRow drfirst = selectedRow(id);
                        LoadEditPanel(drfirst);
                    }
                    else
                    {
                        grdSMDetails.DataSource = null;
                        grdSMDetails.DataBind();
                        populate_DropDown();
                        dlcpname.SelectedValue = Convert.ToString(Session["DistributorNumber"]);
                        populate_listBox();
                    }
                }
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
          // populate_DropDown();

           //if (objSMBO != null)
           //{
           //    if (!"Admin".Equals(objSMBO.CP_Number))
           //    {
           //        //document.getElementById("dlcpname").disabled=true;
           //        //    dlcpname.DataValueField.
           //        dlcpname.Attributes.Add("disabled", "disabled");
           //    }
           //}
           
        }

        protected void dlcpname_SelectedIndexChanged(object sender, EventArgs e)
        {
   
            lstSelectCusName.Items.Clear();
            lstSelectedCusName.Items.Clear();
            lstSelectCusName.DataBind();
            lstSelectCusName.ToolTip = lstSelectCusName.Text;
            lstSelectedCusName.DataBind();
            lstSelectedCusName.ToolTip = lstSelectedCusName.Text;
            populate_listBox();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                objSMBO = new SMDetailsBO();
                objSMBL = new SMDetailsBL();
                if (Session["ID"] != null)
                {
                    objSMBO.ID = Convert.ToInt32(Session["ID"]);
                }
                else
                {
                    objSMBO.ID = Convert.ToInt32(0);
                }
                Session["ID"] = null;
               // objSMBO.ID = Convert.ToInt32(0);
                objSMBO.CP_Number = Convert.ToString(dlcpname.SelectedValue);
                objSMBO.CP_Name = Convert.ToString(dlcpname.SelectedItem.Text);
                string customer_names = "", customer_number = "";
                DataTable dtSelectedCustomers = new DataTable();
                dtSelectedCustomers.Columns.Add("Customer_Name");
                dtSelectedCustomers.Columns.Add("Customer_number");
                DataRow dr;

                for (int i = 0; i < lstSelectedCusName.Items.Count; i++)
                {
                    dr = dtSelectedCustomers.NewRow();
                    dr["Customer_Name"] = Convert.ToString(lstSelectedCusName.Items[i].Text);
                    dr["Customer_number"] = Convert.ToString(lstSelectedCusName.Items[i].Value);
                    //customer_number += lstSelectedCusName.Items[i].Value + ",";
                    dtSelectedCustomers.Rows.Add(dr);

                   // dtSelectedCustomers.DefaultView.Sort = "Customer_Name";
                }
                //sort

                if (dtSelectedCustomers.Rows.Count>0)
                {
                    DataView dv = dtSelectedCustomers.DefaultView;
                    dv.Sort = "Customer_Name";
                    dtSelectedCustomers = dv.ToTable();
                }

                //assign to customer_names and customer_number
                for (int i = 0; i < dtSelectedCustomers.Rows.Count; i++)
                {

                    customer_number += Convert.ToString(dtSelectedCustomers.Rows[i]["Customer_number"]) + ",";
                    customer_names += Convert.ToString(dtSelectedCustomers.Rows[i]["Customer_Name"]) + ",";                   
                }

                customer_names = customer_names.TrimEnd(',');
                customer_number = customer_number.TrimEnd(',');
                objSMBO.Customer_Name = Convert.ToString(customer_names);
                objSMBO.Customer_Number = Convert.ToString(customer_number);
                objSMBO.SM_NAME = Convert.ToString(txtusername.Text);
                objSMBO.SM_MAIL_ID = Convert.ToString(txtemail.Text);
                //objSMBO = objSMBL.saveSMDetailsBL(objSMBO);
                SMDetailsBO output = objSMBL.saveSMDetailsBL(objSMBO);
               // if (!string.IsNullOrEmpty(objSMBO.ERR_MESSAGE))
                if (output.ERR_CODE == 0)
                {
                    DataTable dtDistributors = objSMBL.getSM_Mapping_DetailsBL(objSMBO);
                    Session["dtDistributors"] = dtDistributors;
                    if (dtDistributors.Rows.Count > 0)
                    {
                        grdSMDetails.DataSource = dtDistributors;
                        grdSMDetails.DataBind();
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + output.ERR_MESSAGE + "');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
                    lstSelectedCusName.Items.Clear();
                    lstSelectCusName.Items.Clear();
                    populate_listBox();
                    txtemail.Text = string.Empty;
                    txtusername.Text = string.Empty;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error in inserting.');", true);

                }
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void btnAddSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                if (lstSelectCusName.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstSelectCusName.Items.Count; i++)
                    {
                        if (lstSelectCusName.Items[i].Selected)
                        {
                            if (!arraylist1.Contains(lstSelectCusName.Items[i]))
                            {
                                arraylist1.Add(lstSelectCusName.Items[i]);

                            }
                        }
                    }
                    for (int i = 0; i < arraylist1.Count; i++)
                    {
                        if (!lstSelectedCusName.Items.Contains(((ListItem)arraylist1[i])))
                        {
                            lstSelectedCusName.Items.Add(((ListItem)arraylist1[i]));
                        }
                        lstSelectCusName.Items.Remove(((ListItem)arraylist1[i]));
                    }
                    lstSelectedCusName.SelectedIndex = -1;
                }
                else
                {
                    lbltxt.Visible = true;
                    lbltxt.Text = "Please select atleast one customer name in Listbox1 to move";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                while (lstSelectCusName.Items.Count != 0)
                {
                    for (int i = 0; i < lstSelectCusName.Items.Count; i++)
                    {
                        lstSelectedCusName.Items.Add(lstSelectCusName.Items[i]);
                        lstSelectCusName.Items.Remove(lstSelectCusName.Items[i]);
                    }
                  
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }

        protected void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                if (lstSelectedCusName.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstSelectedCusName.Items.Count; i++)
                    {
                        if (lstSelectedCusName.Items[i].Selected)
                        {
                            if (!arraylist2.Contains(lstSelectedCusName.Items[i]))
                            {
                                arraylist2.Add(lstSelectedCusName.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < arraylist2.Count; i++)
                    {
                        if (!lstSelectCusName.Items.Contains(((ListItem)arraylist2[i])))
                        {
                            lstSelectCusName.Items.Add(((ListItem)arraylist2[i]));
                        }
                        lstSelectedCusName.Items.Remove(((ListItem)arraylist2[i]));
                    }
                    lstSelectCusName.SelectedIndex = -1;
                }
                else
                {
                    lbltxt.Visible = true;
                    lbltxt.Text = "Please select atleast one customer name in Listbox2 to move";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                while (lstSelectedCusName.Items.Count != 0)
                {
                    for (int i = 0; i < lstSelectedCusName.Items.Count; i++)
                    {
                        lstSelectCusName.Items.Add(lstSelectedCusName.Items[i]);
                        lstSelectedCusName.Items.Remove(lstSelectedCusName.Items[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtDistributors = new DataTable();           
                objSMBL = new SMDetailsBL();
                objSMBO = new SMDetailsBO();
                objSMBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                dtDistributors = objSMBL.getSM_Mapping_DetailsBL(objSMBO);
                Session["dtDistributors"] = dtDistributors;
                if (dtDistributors.Rows.Count > 0)
                {
                    grdSMDetails.DataSource = dtDistributors;
                    grdSMDetails.DataBind();
                    int id = Convert.ToInt32(dtDistributors.Rows[0]["ID"]);
                  //  Session["Clicked_ID"] = ID;
                    DataRow drfirst = selectedRow(id);
                    LoadEditPanel(drfirst);
                //  lstSelectCusName.Items.Clear();
                    lstSelectedCusName.Items.Clear();
                    lstSelectCusName.Items.Clear();
                    populate_listBox();
                    txtemail.Text = string.Empty;
                    txtusername.Text = string.Empty;
                    Session["ID"] = null;
                }

                else
                {
                    grdSMDetails.DataSource = null;
                    grdSMDetails.DataBind();
                }
               
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView()", true);
        }
            
        public void populate_listBox()
        {
            try
            {
                DataTable dtDistributor = new DataTable();

                objSMBL = new SMDetailsBL();
                objSMBO = new SMDetailsBO();
                objSMBO.CP_Number = Convert.ToString(dlcpname.SelectedValue);
                dtDistributor = objSMBL.getCustDetailsBL(objSMBO);
                Session["SMCustomer"] = dtDistributor;
                if (dtDistributor.Rows.Count > 0)
                {
                    lstSelectCusName.DataSource = dtDistributor;
                    lstSelectCusName.DataValueField = "customernumber";
                    lstSelectCusName.DataTextField = "customername";
                    lstSelectCusName.DataBind();
                    lstSelectCusName.ToolTip = lstSelectCusName.Text;
                    
                }

                else
                {
                    lstSelectCusName.Items.Clear();
                    lstSelectedCusName.Items.Clear();
                    lstSelectCusName.DataBind();
                    lstSelectedCusName.DataBind();
                }
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        public void populate_DropDown()
        {
            try
            {
                DataTable dtDistributors = new DataTable();
                objSMBL = new SMDetailsBL();
                dtDistributors = objSMBL.getSMDetailsBL();

                if (Convert.ToString(Session["UserRole"]) == "Admin")
                {

                   
                    if (dtDistributors.Rows.Count > 0)
                    {
                        dlcpname.DataSource = dtDistributors;
                        dlcpname.DataValueField = "customer_number";
                        dlcpname.DataTextField = "customer_short_name";
                        dlcpname.DataBind();
                    }

                    else
                    {
                        dlcpname.DataSource = null;
                        dlcpname.DataBind();
                    }
                    dlcpname.Attributes.Add("enabled", "enabled");
                }
                else
                 {
                    var customers = from customer in dtDistributors.AsEnumerable()
                        where customer.Field<string>("customer_number") == Convert.ToString(Session["DistributorNumber"])
                                    select new
                                    {
                                        customer_number = customer.Field<string>("customer_number"),
                                        customer_short_name = customer.Field<string>("customer_short_name")
                                    }; ;
                    //dtDistributors.Columns.Add("customer_number");
                    //dtDistributors.Columns.Add("customer_short_name");
                    //DataRow dr = dtDistributors.NewRow();
                    //dr["customer_number"] = Convert.ToString(Session["DistributorNumber"]);
                    //dr["customer_short_name"] = Convert.ToString(Session["DistributorName"]);
                    //dtDistributors.Rows.Add(dr);

                    dlcpname.DataSource = customers;
                    dlcpname.DataValueField = "customer_number";
                    dlcpname.DataTextField = "customer_short_name";
                    dlcpname.DataBind();
                    dlcpname.Attributes.Add("disabled", "disabled");
                }
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
          
        }

        private void LoadEditPanel(DataRow drselect)
           {
               DataTable dt = new DataTable();

               try
               {                  
                   lstSelectCusName.Items.Clear();
                   lstSelectedCusName.Items.Clear();
                   //Session["ClickedCP_ID"] = Convert.ToInt32(drselect["ID"]);
                   // dlcpname.SelectedValue = Convert.ToString(drselect["CP_Name"]);
                   populate_DropDown();                   
                   dlcpname.SelectedValue = Convert.ToString(drselect["CP_Number"]);
                   Session["ID"] = Convert.ToInt32(drselect["ID"]);
                   populate_listBox();
                   dt = (DataTable)Session["SMCustomer"];
                   txtusername.Text = Convert.ToString(drselect["SM_Name"]);
                   txtemail.Text = Convert.ToString(drselect["SM_MAIL_ID"]);
                   string customers = Convert.ToString(drselect["Customer_Name"]);
                   string customer_numbers = Convert.ToString(drselect["Customer_Number"]);
                   //List<string> customer_list = customers.Split(',').ToList();

                   List<string> customer_number_list = customer_numbers.Split(',').ToList();
                   List<string> customer_list = new List<string>();

                   DataRow[] dr;
                   for(int i=0;i<customer_number_list.Count;i++)
                   {
                       //dr = new DataRow();
                       dr = dt.Select("customernumber = " + customer_number_list.ElementAt(i).ToString());
                       customer_list.Add(Convert.ToString(dr[0]["customername"]));
                   }
                   //lstSelectCusName.Items.Clear();
                   for (int i = 0; i < customer_list.Count; i++)
                   {
                       lstSelectedCusName.Items.Add(customer_list.ElementAt(i));
                       lstSelectedCusName.Items[i].Value = customer_number_list.ElementAt(i);
                   }

                   for (int i = 0; i < lstSelectedCusName.Items.Count; i++)
                   {
                       lstSelectedCusName.Items[i].Value = lstSelectedCusName.Items[i].Value.Trim();
                       lstSelectCusName.Items.Remove(lstSelectedCusName.Items[i]);
                       //lstSelectCusName.Items.Remove(customer_list.ElementAt(i));
                   }
                    //lstSelectedCusName.Items.Clear();
               }

               catch (Exception ex)
               {
                   objCom.ErrorLog(ex);
               }

           }

        private DataRow selectedRow(int ID)
       {
           DataTable dtGrid = (DataTable)Session["dtDistributors"];
           DataRow drselect = (from DataRow dr in dtGrid.Rows where (int)dr["ID"] == ID select dr).FirstOrDefault();
           return drselect;
       }
             
        protected void grdSMDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(e.CommandArgument);
                //Session["Clicked_ID"] = ID;
                DataRow drselect = selectedRow(ID);
                LoadEditPanel(drselect);
            }

            catch(Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void lstSelectCusName_PreRender(object sender, EventArgs e)
        {
            foreach (ListItem item in lstSelectCusName.Items)
            {
                string str =item.Text.ToString();
                item.Attributes.Add("title", str);
            }
        }
       
    }
}