﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class PurchaseDashboard : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                LoadDashboardData();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadDashboardData()
        {
            DashboardBO objBO;
            DashboardBL objBL;
            DataSet dsoutput;
            string output = string.Empty;
            try
            {
                dsoutput = new DataSet();
                objBO = new DashboardBO();
                objBL = new DashboardBL();
                objBO.distributor_number = Convert.ToInt32(HttpContext.Current.Session["DistributorNumber"]);
                dsoutput = objBL.GetPuchaseDashboardDataBL(objBO);
                Session["PurchaseDashboardData"] = dsoutput;
                if (dsoutput != null)
                {
                    lblResult.Text = "";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
                }
                else
                {
                    lblResult.Text = "There are no data to show.";
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string LoadChartHistoricalPurchase()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["PurchaseDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[0] != null)
                    if (dsData.Tables[0].Rows.Count > 0)
                        dtoutput = dsData.Tables[0];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        [WebMethod]
        public static string LoadChartConsolidatedPurchaseView()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["PurchaseDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[1] != null)
                    if (dsData.Tables[1].Rows.Count > 0)
                        dtoutput = dsData.Tables[1];
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        [WebMethod]
        public static string LoadChartFamilyView()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataSet dsData;
            string output = string.Empty;
            try
            {
                dsData = new DataSet();
                dsData = (DataSet)HttpContext.Current.Session["PurchaseDashboardData"];
                dtoutput = new DataTable();
                if (dsData.Tables[2] != null)
                    if (dsData.Tables[2].Rows.Count > 0)
                        dtoutput = dsData.Tables[2];
                if (dtoutput.Rows.Count > 0)
                {
                    //dtoutput = GenerateTransposedTable(dtoutput);
                    output = DataTableToJSONWithStringBuilder(dtoutput);

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        private static DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
    }
}