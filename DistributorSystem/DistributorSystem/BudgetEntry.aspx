﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BudgetEntry.aspx.cs" Inherits="DistributorSystem.BudgetEntry" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function () {

            //var head_content = $('#MainContent_grdEntry tr:first').html();
            //$('#MainContent_grdEntry').prepend('<thead></thead>')
            //$('#MainContent_grdEntry thead').html('<tr>' + head_content + '</tr>');
            //$('#MainContent_grdEntry tbody tr:first').hide();
            //$('#MainContent_grdEntry').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            //$('#MainContent_grdEntry').DataTable(
            //    {
            //        
            //    });

        });
        function BindGridView() {
            var head_content = $('#MainContent_grdEntry tr:first').html();
            $('#MainContent_grdEntry').prepend('<thead></thead>')
            $('#MainContent_grdEntry thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdEntry tbody tr:first').hide();
            $('#MainContent_grdEntry').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            $('#MainContent_grdEntry').DataTable(
                {
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "columnDefs": [
            {
                "targets": [4],
                "visible": false,
                "searchable": false
            }]
                });


        }
        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 11) {
                    if (charCode == 46) {
                       return true;
                    }
                    else return false;
                }

            }
            if (charCode == 46) {
                return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            else {
                return true;
            }
        }

        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function CheckExtension(sender) {
            var validExts = new Array(".xlsx", ".xls");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnUpload').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnUpload').attr("style", "display:block");
                return true;
            }
        }
    </script>
    <style>
        .mn_popup
        {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table
        {
            border-color: white!important;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }

        .mn_bott
        {
            margin: 0px 0 10px 0;
        }

        .btn-default.btn-on-2.active
        {
            background-color: #006681;
            color: white;
        }

        .btn-default.btn-off-2.active
        {
            background-color: #006681;
            color: white;
        }

        .btn-group
        {
            margin-bottom: 5px;
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Entry</a>
                        </li>
                        <li class="current">Annual Target</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-12 nopadding">
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lblYear" Text="Year"></asp:Label>
                </div>
                <div class="col-md-3">
                    <div class="controls">
                        <asp:DropDownList Style="margin: 0;" runat="server" ID="ddlYear"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-8">
                    <asp:Button runat="server" ID="btnEntry" CssClass="btnSubmit" Text="Entry" OnClick="btnEntry_Click" />
                    <asp:Button runat="server" CssClass="btnSubmit" Text="Upload Entry" OnClientClick="alert('All values should be in unit.'); $('#addDialog').modal(); return false;" />

                    <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" Text="Save" OnClick="btnSave_Click" />
                </div>
            </div>

            <div id="addDialog" class="modal fade">
                <div class="mn_popup">
                    <div class="modal-content">

                        <div id="Div1" class="modal-body">
                            <div class="filter_panel">
                                <div class="col-md-12 nopadding">
                                    <div class="col-md-3">
                                        <asp:Button runat="server" ID="btntemplate" CssClass="btnSubmit" OnClick="btntemplate_Click" Text="Download Template" />
                                    </div>
                                    <div class="col-md-5">
                                        <asp:FileUpload ID="updFile" runat="server" Style="width: 100%;" onchange="CheckExtension(this);" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label ID="lblExtError" runat="server" ForeColor="Red"></asp:Label>
                                        <asp:Button runat="server" Style="display: none;" CssClass="btnSubmit" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="text-align: left; color: black;">
                            <asp:Label runat="server">
                                <p style="font-size:.8em;"> * Please download the template, fill the data and then upload.</p>
                            <p style="font-size:.8em;"> * Please upload only excel file like .xls, .xlsx.</p>
                             <p style="font-size:.8em;"> * Column name of the excel sheet will be as "Customer_number","Customer_name", "Sales_Year".</p>
                             <p style="font-size:.8em;"> * All the column datatype should be "General".</p>
                             <p style="font-size:.8em;"> * The Sheet name should be renamed as "AnnualTarget".</p>
                            </asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--        <asp:Panel runat="server" ID="pnlUpload" style="display:none;">
        <div class="col-md-12 nopadding">
            <asp:FileUpload runat="server" ID="uplFile" />
            <asp:Button runat="server" ID="btnUpload" CssClass="btnSubmit" Text="Upload"  />
            </div></asp:Panel>--%>
        </asp:Panel>
        <asp:Panel ID="panelvalues" runat="server" Visible="false">
            <div id="Divvalues" class="pull-right">
                <div class="btn-group">
                    <%-- data-toggle="buttons">--%>
                    <asp:Button ID="Btn_Units" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In Units" OnClick="Btn_Units_Click" />
                    <asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In '000" OnClick="Btn_Thousand_Click" />
                    <asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
                </div>
            </div>
        </asp:Panel>
        <asp:Label runat="server" ID="lblmessage"></asp:Label>
        <asp:LinkButton runat="server" ID="lnkErrorExcel" Visible="false" Text="Click here to download error excel." OnClick="lnkErrorExcel_Click"></asp:LinkButton>
        <asp:Label runat="server" CssClass="result" ID="lblResult"></asp:Label>

        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdEntry" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdEntry_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Customer Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("customernumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customername") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblyearHeader" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:TextBox Style="text-align: right; width: 100%;" ID="txtAmount" onkeypress="return isNumberKey(event,this);" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" runat="server" Text='<%#Bind("YearlyAmount") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblyear1Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label Style="text-align: right" ID="lblyear1" runat="server" Text='<%#Bind("last1yearAmount") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblyear2Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label Style="text-align: right" ID="lblyear2" runat="server" Text='<%#Bind("last2yearAmount") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>

