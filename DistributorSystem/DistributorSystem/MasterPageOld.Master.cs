﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (Convert.ToInt32(Session["Quote_Flag"]) == 1)
                {
                    quoteList.Visible = true;
                }
                else
                {
                    quoteList.Visible = false;
                }
                if (Convert.ToInt32(Session["Price_Flag"]) == 1)
                {
                    lstRFQPrice.Visible = true;
                }
                else
                {
                    lstRFQPrice.Visible = false;
                }
                if (String.IsNullOrEmpty(Convert.ToString(Session["UserRole"])))
                {
                    lblUserName.Text = Convert.ToString(Session["DistributorName"]);
                    lnkAdminProfile.Visible = false;
                    fireSale.Visible = true;
                }
                else
                {
                    fireSale.Visible = true;
                    lblUserName.Text = "Admin(Logged In As " + Convert.ToString(Session["DistributorName"])+")";
                    lnkAdminProfile.Visible = true;
                    lstRFQPrice.Visible = true;
                    //Session["DistributorNumber"] = "Admin";
                    //Session["DistributorName"] = "Admin";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Buffer = true;
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Expires = -1000;
                Response.CacheControl = "no-cache";
                Response.Redirect("Login.aspx", true);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void lnkAdminProfile_Click(object sender, EventArgs e)
        {
            lblUserName.Text = "Admin";
            lnkAdminProfile.Visible = false;
            Session["DistributorNumber"] = "Admin";
            Session["DistributorName"] = "Admin";
            //Response.Redirect("UserDetails.aspx");
            Response.Redirect("UserDetailsWithMenu.aspx");
        }
    }
}