﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class UserDetails : System.Web.UI.Page
    {
        UserDetailsBL objUserBL;
        UserDetailsBO objUserBO;
        CommonFunctions objCom = new CommonFunctions();
        string dist_number = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    DataTable dtDistributors = new DataTable();
                    if (!IsPostBack)
                    {
                        objUserBL = new UserDetailsBL();
                        dtDistributors = objUserBL.getUserDetailsBL();
                        Session["dtDistributors"] = dtDistributors;
                        if (dtDistributors.Rows.Count > 0)
                        {
                            grdUserDetails.DataSource = dtDistributors;
                            grdUserDetails.DataBind();
                            dist_number = Convert.ToString(dtDistributors.Rows[0]["Distributor_number"]);
                            Session["ClickedDistributor_number"] = dist_number;
                            DataRow drfirst = selectedRow(dist_number);
                            LoadEditPanel(drfirst);
                            //LoadEditPanel();
                        }
                        else
                        {
                            grdUserDetails.DataSource = null;
                            grdUserDetails.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadEditPanel(DataRow drselect)
        {
            try
            {

                Session["ClickedDistributor_number"] = Convert.ToString(drselect["Distributor_number"]);
                lblDistributorName.Text = Convert.ToString(drselect["Distributor_name"]);
                lblDistributorNumber.Text = Convert.ToString(drselect["Distributor_number"]);
                txtEmailId.Text = Convert.ToString(drselect["Email_id"]);
                txtpwd.Text = Decrypt(Convert.ToString(drselect["Password"]));
                txtpwd.Attributes["type"] = "password";
                txtrepwd.Text = Decrypt(Convert.ToString(drselect["Password"]));
                txtrepwd.Attributes["type"] = "password";
                if (Convert.ToBoolean(drselect["Status"]))
                    chkStatus.Checked = true;
                else
                    chkStatus.Checked = false;
                if (!string.IsNullOrEmpty(Convert.ToString(drselect["Role"])))
                {
                    if (Convert.ToString(drselect["Role"]) == "Admin")
                        chkRole.Checked = true;
                    else
                        chkRole.Checked = false;
                }
                else
                    chkRole.Checked = false;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(string dist_number)
        {
            DataTable dtGrid = (DataTable)Session["dtDistributors"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (string)dr["Distributor_number"] == dist_number
                                select dr).FirstOrDefault();

            return drselect;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objUserBO = new UserDetailsBO();
                objUserBL = new UserDetailsBL();
                objUserBO.userid = Convert.ToString(lblDistributorNumber.Text);
                objUserBO.emailid = Convert.ToString(txtEmailId.Text);
                objUserBO.password = Encrypt(Convert.ToString(txtpwd.Text));
                objUserBO.role = Convert.ToBoolean(chkRole.Checked) ? "Admin" : "";
                objUserBO.status = Convert.ToBoolean(chkStatus.Checked) ? 1 : 0;
                objUserBO = objUserBL.saveUserDetailsBL(objUserBO);
                if (!string.IsNullOrEmpty(objUserBO.err_msg))
                {
                    DataTable dtDistributors = objUserBL.getUserDetailsBL();
                    Session["dtDistributors"] = dtDistributors;
                    if (dtDistributors.Rows.Count > 0)
                    {
                        grdUserDetails.DataSource = dtDistributors;
                        grdUserDetails.DataBind();
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objUserBO.err_msg + "');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView();", true);

                }
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error in saving.Please try again later.');", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void grdUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string user_id = Convert.ToString(e.CommandArgument);
                Session["ClickedDistributor_number"] = user_id;
                DataRow drselect = selectedRow(user_id);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return cipherText;
        }
    }
}