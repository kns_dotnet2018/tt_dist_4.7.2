﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;

namespace DistributorSystem
{
    public partial class AllDistributors : System.Web.UI.Page
    {
        #region Global Declaration
        AllDistributorsBL objAllDist;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                DataTable dtAllDistributors = new DataTable();
                if (!IsPostBack)
                {
                    objAllDist = new AllDistributorsBL();
                    dtAllDistributors = objAllDist.getAllDistributorsBL();
                    Session["dtAllDistributors"] = dtAllDistributors;
                    if (dtAllDistributors.Rows.Count > 0)
                    {
                        grdDistributors.DataSource = dtAllDistributors;
                        grdDistributors.DataBind();
                    }
                    else
                    {
                        grdDistributors.DataSource = null;
                        grdDistributors.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void lblCustomerNumber_Click(object sender, EventArgs e)
        {
            try
            {
                string CustomerNumber = Convert.ToString((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(CustomerNumber);
                Session["DistributorName"] = Convert.ToString(drselect["customer_short_name"]);
                Session["DistributorNumber"] = CustomerNumber;
                Session["cter"] = Convert.ToString(drselect["cter"]);
                Response.Redirect("PurchaseDashboard.aspx");
                //return;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected DataRow selectedRow(string CustomerNumber)
        {
            DataTable dtGrid = (DataTable)Session["dtAllDistributors"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                     where (string)dr["customer_number"] == CustomerNumber
                             select dr).FirstOrDefault();
            return drselect;
        }
    }
}