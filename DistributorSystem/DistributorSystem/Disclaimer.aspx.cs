﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DistributorSystem
{
    public partial class Disclaimer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (Session["DistributorNumber"] != null)
            {

                if (Session["UserRole"].ToString() == "Admin")
                {
                    Response.Redirect("UserDetails.aspx");
                }
                else Response.Redirect("PurchaseDashboard.aspx");

            }
        }
    }
}