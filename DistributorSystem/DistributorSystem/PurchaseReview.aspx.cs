﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Web.Services;
using System.Text;

namespace DistributorSystem
{
    public partial class PurchaseReview : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        ReviewBL objReviewBL;
        ReviewBO objReviewBO;
        public static int gridLoadedStatus;
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                gridLoadedStatus = 0;
                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Btn_Thousand_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Btn_Lakhs_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Btn_Units_Click(null, null); }
                else
                {
                    Btn_Units_Click(null, null);
                }
                Btn_value_Click(null, null);
                //Session["Btn_ThousandWasClicked"] = true;
                //Session["Btn_ValueWasClicked"] = true;

                BindProductGroup();
                BindFamily();
                BindApplication();

                BindData();
            }
        }



        protected void Btn_Units_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = true;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = true;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_value_Click(object sender, EventArgs e)
        {
            Session["Btn_ValueWasClicked"] = true;
            Session["Btn_QtyWasClicked"] = false;
            Btn_value.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_quantity.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";

            if (Convert.ToBoolean(Session["Btn_ValueWasClicked"]) == true)
            {
                BindData();
                divgridchartValues.Visible = true;
                divgridchartQty.Visible = false;
            }
        }

        protected void Btn_quantity_Click(object sender, EventArgs e)
        {
            Session["Btn_ValueWasClicked"] = false;
            Session["Btn_QtyWasClicked"] = true;
            Btn_quantity.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_value.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";

            if (Convert.ToBoolean(Session["Btn_QtyWasClicked"]) == true)
            {
                BindData();
                divgridchartQty.Visible = true;
                divgridchartValues.Visible = false;
            }
        }

        #endregion

        #region Methods
        protected void BindData()
        {
            DataTable dtYtdSales = new DataTable();
            gridLoadedStatus = 1;

            if (Convert.ToBoolean(Session["Btn_ValueWasClicked"]) == true)
            {

                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { dtYtdSales = loadGrid(); }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { dtYtdSales = loadGrid_lakh(); }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { dtYtdSales = loadGrid_Unit(); }

                if (dtYtdSales.Rows.Count != 0)
                {
                    grdPurchaseReviewValues.DataSource = dtYtdSales;
                    grdPurchaseReviewValues.DataBind();
                    divgridchartValues.Visible = true;
                    divgridchartQty.Visible = false;
                }


                //chart pre
                DataTable dtChart = GenerateTransposedTable(dtYtdSales);
                DataTable dtoriginal = dtChart.Copy();
                dtChart.Columns.RemoveAt(0);


                dtChart.Rows.RemoveAt(dtChart.Rows.Count - 1);
                dtChart.Columns.Add("column_color");
                for (int i = 0; i < dtChart.Rows.Count; i++)
                    dtChart.Rows[i]["column_color"] = "#006681";
                Session["Chart"] = dtChart;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadChartPurchaseReview()", true);
                //foreach (DataRow row in dtChart.Rows)
                //{
                //    if (row.ItemArray[0].ToString() == "flag")
                //    {
                //        row.Delete(); dtChart.AcceptChanges();
                //        //LoadChartData(dtChart);
                //        string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
                //        string column2 = dtChart.Columns[5].ColumnName.ToString(); // YTD SALE
                //        string column3 = dtChart.Columns[4].ColumnName.ToString(); // YTD PLAN
                //        string column4 = dtChart.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                //        Chart2.DataSource = dtChart;
                //        //Bar chart
                //        Chart2.Series["YTD SALE"].XValueMember = column1;
                //        Chart2.Series["YTD SALE"].YValueMembers = column2;
                //        //Line chart YTD PLAN 2015
                //        Chart2.Series["YTD PLAN"].XValueMember = column1;
                //        Chart2.Series["YTD PLAN"].YValueMembers = column3;
                //        Chart2.Series["Series4"].XValueMember = column1;
                //        Chart2.Series["Series4"].YValueMembers = column3;
                //        Chart2.Series["Series4"].IsVisibleInLegend = false;
                //        // Line chaert YTD SALE 2014
                //        Chart2.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                //        Chart2.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                //        Chart2.Series["Series5"].XValueMember = column1;
                //        Chart2.Series["Series5"].YValueMembers = column4;

                //        Chart2.Series["Series5"].IsVisibleInLegend = false;
                //        Chart2.DataBind();

                //        // making user friendly
                //        Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                //        Chart2.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                //        Chart2.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                //        Chart2.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                //        Chart2.Visible = true;
                //        //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                //        Random random = new Random();
                //        foreach (var item in Chart2.Series["YTD SALE"].Points)
                //        {
                //            Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                //            item.Color = c;
                //        }
                //        break;
                //    }
                //    divgridchartValues.Visible = true;
                //    divgridchartQty.Visible = false;
                //}

            }

            else if (Convert.ToBoolean(Session["Btn_QtyWasClicked"]) == true)
            {

                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { dtYtdSales = loadGridQty(); }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { dtYtdSales = loadGrid_lakhQty(); }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { dtYtdSales = loadGrid_UnitQty(); }

                if (dtYtdSales.Rows.Count != 0)
                {
                    grdPurchaseReviewQty.DataSource = dtYtdSales;
                    grdPurchaseReviewQty.DataBind();
                    divgridchartValues.Visible = false;
                    divgridchartQty.Visible = true;
                }

                //chart pre
                DataTable dtChart_qty = GenerateTransposedTable(dtYtdSales);
                DataTable dtoriginal_qty = dtChart_qty.Copy();
                Session["Chart"] = dtChart_qty;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadChartPurchaseReview()", true);
                //foreach (DataRow row in dtChart_qty.Rows)
                //{
                //    if (row.ItemArray[0].ToString() == "flag")
                //    {
                //        row.Delete(); dtChart_qty.AcceptChanges();
                //        //LoadChartData(dtChart_qty);
                //        string column1 = dtChart_qty.Columns[1].ColumnName.ToString(); // Months 
                //        string column2 = dtChart_qty.Columns[5].ColumnName.ToString(); // YTD SALE
                //        string column3 = dtChart_qty.Columns[4].ColumnName.ToString(); // YTD PLAN
                //        string column4 = dtChart_qty.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                //        Chart3.DataSource = dtChart_qty;
                //        //Bar chart
                //        Chart3.Series["YTD SALE"].XValueMember = column1;
                //        Chart3.Series["YTD SALE"].YValueMembers = column2;
                //        //Line chart YTD PLAN 2015
                //        Chart3.Series["YTD PLAN"].XValueMember = column1;
                //        Chart3.Series["YTD PLAN"].YValueMembers = column3;
                //        Chart3.Series["Series4"].XValueMember = column1;
                //        Chart3.Series["Series4"].YValueMembers = column3;
                //        Chart3.Series["Series4"].IsVisibleInLegend = false;
                //        // Line chaert YTD SALE 2014
                //        Chart3.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                //        Chart3.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                //        Chart3.Series["Series5"].XValueMember = column1;
                //        Chart3.Series["Series5"].YValueMembers = column4;

                //        Chart3.Series["Series5"].IsVisibleInLegend = false;
                //        Chart3.DataBind();

                //        // making user friendly
                //        Chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                //        Chart3.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                //        Chart3.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                //        Chart3.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                //        Chart3.Visible = true;
                //        //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                //        Random random = new Random();
                //        foreach (var item in Chart3.Series["YTD SALE"].Points)
                //        {
                //            Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                //            item.Color = c;
                //        }

                //        break;
                //    }
                //    divgridchartValues.Visible = false;
                //    divgridchartQty.Visible = true;
                //}               
            }

        }

        private void BindFamily()
        {
            string name_desc = string.Empty, name_code = string.Empty;
            int count = 0;
            try
            {
                objReviewBL = new ReviewBL();
                DataTable dtProductFamilyList = new DataTable();
                dtProductFamilyList = objReviewBL.LoadFamilyId();
                ProductFamilyList.DataSource = dtProductFamilyList;
                ProductFamilyList.DataTextField = "item_family_name";
                ProductFamilyList.DataValueField = "item_family_id";

                ProductFamilyList.DataBind();
                //ddlProductFamliy.Items.Insert(0, "ALL");

                foreach (ListItem val in ProductFamilyList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + ",";
                    }
                }
                //name_code = "'" + name_code;
                name_desc = "'" + name_desc;

                // TxtProductfamily.Text = name_desc;

                string ProductFamilyListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                string ProductFamilyNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                Session["SelectedProductFamilyID"] = ProductFamilyListVal;

                if (count == ProductFamilyList.Items.Count)
                {
                    Session["SelectedProductFamily"] = "ALL";
                }
                else
                {
                    Session["SelectedProductFamily"] = ProductFamilyNameList;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        private void BindProductGroup()
        {
            int count = 0;
            string chakgrp_desc = string.Empty, chkgrp_code = string.Empty;
            try
            {
                 foreach (ListItem val in ProductGrpList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;
                string ProductGrpListVal = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                string ProductGrpNameList = chakgrp_desc.Substring(0, Math.Max(0, chakgrp_desc.Length - 2));

                if (count == ProductGrpList.Items.Count)
                {
                    Session["SelectedProductGroup"] = "ALL";
                }
                else
                {
                    Session["SelectedProductGroup"] = ProductGrpListVal;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindApplication()
        {
            string name_desc = string.Empty, name_code = string.Empty;
            int count = 0;

            try
            {
                objReviewBL = new ReviewBL();
                objReviewBO = new ReviewBO();
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);

                string ProductFamilyId = Convert.ToString(Session["SelectedProductFamilyID"]);
                objReviewBO.Item_familyID = ProductFamilyId == "ALL" ? "0" : ProductFamilyId;
                DataTable dtPL = new DataTable();
                objReviewBO.Productgroup = ProductGroup == "ALL" ? null : ProductGroup;
                dtPL = objReviewBL.getProductsBL(objReviewBO);
                DataTable dtTemp = dtPL.Clone();
                for (int i = 0; i < dtPL.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(dtPL.Rows[i].ItemArray[0], dtPL.Rows[i].ItemArray[0].ToString() + "_" + dtPL.Rows[i].ItemArray[2].ToString(), dtPL.Rows[i].ItemArray[2]);
                }
                if (dtTemp.Rows.Count != 0)
                {
                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                }
                else
                {

                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                }
            
            foreach (ListItem val in ApplicationList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;

            // TxtApplication.Text = name_desc;

            string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ApplicationList.Items.Count)
            {
                Session["SelectedApplications"] = "ALL";
            }
            else
            {
                Session["SelectedApplications"] = ApplicationListVal;
            }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }




        #region load Grid By Values
        protected DataTable getMonthlyvalues(int Year, string flag, int valuein = 1000)
        {
            DataTable dtmonthval = new DataTable();
            try
            {
                objReviewBO = new ReviewBO();
                objReviewBL = new ReviewBL();
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.year = Year;
                objReviewBO.Flag = flag;
                objReviewBO.Valuein = valuein;
                objReviewBO.Cter = null;
                objReviewBO.Branchcode = null;
                objReviewBO.Salesengineer = null;
                objReviewBO.Item_subfamilyname = null;
                objReviewBO.Customer_type = null;

                string ProductFamily = Convert.ToString(Session["SelectedProductFamilyID"]);
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ApplicationListVal = Convert.ToString(Session["SelectedApplications"]);

                objReviewBO.Item_familyname = ProductFamily == "ALL" || ProductFamily == "" ? null : ProductFamily;
                objReviewBO.Productgroup = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
                objReviewBO.Item_code = ApplicationListVal == "ALL" || ApplicationListVal == "" ? null : ApplicationListVal;

                dtmonthval = objReviewBL.getMonthlyVal(objReviewBO);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            return dtmonthval;
        }

        protected DataTable loadGrid()
        {
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyvalues(MTDYear, "MTD Plan");
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyvalues(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyvalues(MTDYear, "MTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchaseTarget CurrentYear");

            }



            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }



            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyvalues(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {

                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyvalues(ActualYear, "YTD Sale");

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
            ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             
            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD PLAN 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        protected DataTable loadGrid_lakh()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1; ;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyvalues(MTDYear, "MTD Plan", 100000);
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyvalues(YTDYear, "YTD Sale", 100000);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyvalues(MTDYear, "MTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchase CurrentYear");

            }


            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }


            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyvalues(YTDYear, "YTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyvalues(ActualYear, "YTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        protected DataTable loadGrid_Unit()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 3, 3 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyvalues(MTDYear, "MTD Plan", 1);
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyvalues(YTDYear, "YTD Sale", 1);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyvalues(MTDYear, "MTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchase CurrentYear");

            }


            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }


            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyvalues(YTDYear, "YTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {

                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt64(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt64(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt64(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt64(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt64(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt64(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt64(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt64(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt64(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt64(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt64(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt64(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyvalues(ActualYear, "YTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );
            }

            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        #endregion

        #region load Grid By Quantites
        protected DataTable getMonthlyquantites(int Year, string flag, int valuein = 1000)
        {
            DataTable dtmonthval = new DataTable();
            try
            {
                objReviewBO = new ReviewBO();
                objReviewBL = new ReviewBL();
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.year = Year;
                objReviewBO.Flag = flag;
                objReviewBO.Valuein = valuein;
                objReviewBO.Cter = null;
                objReviewBO.Branchcode = null;
                objReviewBO.Salesengineer = null;
                objReviewBO.Item_familyname = null;
                objReviewBO.Item_subfamilyname = null;
                objReviewBO.Customer_type = null;
                objReviewBO.Productgroup = null;
                objReviewBO.Item_code = null;

                string ProductFamily = Convert.ToString(Session["SelectedProductFamilyID"]);
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ApplicationListVal = Convert.ToString(Session["SelectedApplications"]);

                objReviewBO.Item_familyname = ProductFamily == "ALL" || ProductFamily == "" ? null : ProductFamily;
                objReviewBO.Productgroup = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
                objReviewBO.Item_code = ApplicationListVal == "ALL" || ApplicationListVal == "" ? null : ApplicationListVal;


                dtmonthval = objReviewBL.getMonthlyQTY(objReviewBO);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            return dtmonthval;
        }

        protected DataTable loadGridQty()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyquantites(MTDYear, "MTD Plan");
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyquantites(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyquantites(MTDYear, "MTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchase CurrentYear");

            }



            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }



            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyquantites(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {

                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyquantites(ActualYear, "YTD Sale");

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
            ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             
            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD PLAN 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        protected DataTable loadGrid_lakhQty()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyquantites(MTDYear, "MTD Plan", 100000);
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyquantites(YTDYear, "YTD Sale", 100000);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyquantites(MTDYear, "MTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchase CurrentYear");

            }


            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }


            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyquantites(YTDYear, "YTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyquantites(ActualYear, "YTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        protected DataTable loadGrid_UnitQty()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 3, 3 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("jan", typeof(string));
            dtYtdSales.Columns.Add("feb", typeof(string));
            dtYtdSales.Columns.Add("mar", typeof(string));
            dtYtdSales.Columns.Add("apr", typeof(string));
            dtYtdSales.Columns.Add("may", typeof(string));
            dtYtdSales.Columns.Add("jun", typeof(string));
            dtYtdSales.Columns.Add("jul", typeof(string));
            dtYtdSales.Columns.Add("aug", typeof(string));
            dtYtdSales.Columns.Add("sep", typeof(string));
            dtYtdSales.Columns.Add("oct", typeof(string));
            dtYtdSales.Columns.Add("nov", typeof(string));
            dtYtdSales.Columns.Add("dec", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = System.DateTime.Now.Year - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int Remain_months = 12 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015 

            temp = getMonthlyquantites(MTDYear, "MTD Plan", 1);
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
            dtSale = getMonthlyquantites(YTDYear, "YTD Sale", 1);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                        val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                        val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                        val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                        val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                        val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                        val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                        val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                        val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                        try
                        {
                            dtYtdSales.Rows.Add("MTD Purchase Target " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4
                                               Math.Round(val5).ToString("N0", culture),//5
                                               Math.Round(val6).ToString("N0", culture),//6
                                               Math.Round(val7).ToString("N0", culture),//7
                                               Math.Round(val8).ToString("N0", culture),//8
                                               Math.Round(val9).ToString("N0", culture),//9
                                               Math.Round(val10).ToString("N0", culture),//10
                                               Math.Round(val11).ToString("N0", culture),//11
                                               Math.Round(val12).ToString("N0", culture),//12
                                               "MTDPurchaseTarget CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015 
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getMonthlyquantites(MTDYear, "MTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                dtYtdSales.Rows.Add("MTD Purchase " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),

                                    "MTDPurchase CurrentYear");

            }


            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                try
                {
                    dtYtdSales.Rows.Add("YTD Purchase Target " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       Math.Round(YTD_val5).ToString("N0", culture),//5
                                       Math.Round(YTD_val6).ToString("N0", culture),//6
                                       Math.Round(YTD_val7).ToString("N0", culture),//7
                                       Math.Round(YTD_val8).ToString("N0", culture),//8
                                       Math.Round(YTD_val9).ToString("N0", culture),//9
                                       Math.Round(YTD_val10).ToString("N0", culture),//10
                                       Math.Round(YTD_val11).ToString("N0", culture),//11
                                       Math.Round(YTD_val12).ToString("N0", culture),//12
                                       "YTDPurchaseTarget CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }


            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015 
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getMonthlyquantites(YTDYear, "YTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {

                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());

                dtYtdSales.Rows.Add("YTD Purchase " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt64(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt64(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt64(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt64(loc4).ToString("N0", culture),
                                   currentmonth < 5 ? "0" : Convert.ToInt64(loc5).ToString("N0", culture),
                                   currentmonth < 6 ? "0" : Convert.ToInt64(loc6).ToString("N0", culture),
                                   currentmonth < 7 ? "0" : Convert.ToInt64(loc7).ToString("N0", culture),
                                   currentmonth < 8 ? "0" : Convert.ToInt64(loc8).ToString("N0", culture),
                                   currentmonth < 9 ? "0" : Convert.ToInt64(loc9).ToString("N0", culture),
                                   currentmonth < 10 ? "0" : Convert.ToInt64(loc10).ToString("N0", culture),
                                   currentmonth < 11 ? "0" : Convert.ToInt64(loc11).ToString("N0", culture),
                                   currentmonth < 12 ? "0" : Convert.ToInt64(loc12).ToString("N0", culture),
                                   "YTDPurchase CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getMonthlyquantites(ActualYear, "YTD Sale", 1);

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                //pending to add all values
                dtYtdSales.Rows.Add("YTD Purchase " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   loc5.ToString("N0", culture),
                                   loc6.ToString("N0", culture),
                                   loc7.ToString("N0", culture),
                                   loc8.ToString("N0", culture),
                                   loc9.ToString("N0", culture),
                                   loc10.ToString("N0", culture),
                                   loc11.ToString("N0", culture),
                                   loc12.ToString("N0", culture),
                                   "YTDPurchase ActualYear"
                                   );
            }

            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                //execute one time
                if (flag == "YTDPurchase ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchaseTarget CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }
                if (flag == "YTDPurchase CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                    ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                    ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                    ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                    ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                    ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                    ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                    ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                    ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
            result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
            result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
            result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
            result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
            result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
            result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
            result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
            result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
            result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
            result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
            result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
            result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
            result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
            result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 Math.Round(result5),
                                 Math.Round(result6),
                                 Math.Round(result7),
                                 Math.Round(result8),
                                 Math.Round(result9),
                                 Math.Round(result10),
                                 Math.Round(result11),
                                 Math.Round(result12),
                                 "Growth"
                                );


            return dtYtdSales;
        }

        #endregion

        //protected void bindchart(DataTable dtChart)
        //{


        //    //LoadChartData(dtChart);
        //    string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
        //    string column2 = dtChart.Columns[5].ColumnName.ToString(); // YTD SALE
        //    string column3 = dtChart.Columns[4].ColumnName.ToString(); // YTD PLAN
        //    string column4 = dtChart.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

        //    Chart2.DataSource = dtChart;
        //    //Bar chart
        //    Chart2.Series["YTD SALE"].XValueMember = column1;
        //    Chart2.Series["YTD SALE"].YValueMembers = column2;
        //    //Line chart YTD PLAN 2015
        //    Chart2.Series["YTD PLAN"].XValueMember = column1;
        //    Chart2.Series["YTD PLAN"].YValueMembers = column3;
        //    Chart2.Series["Series4"].XValueMember = column1;
        //    Chart2.Series["Series4"].YValueMembers = column3;
        //    Chart2.Series["Series4"].IsVisibleInLegend = false;
        //    // Line chaert YTD SALE 2014
        //    Chart2.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
        //    Chart2.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
        //    Chart2.Series["Series5"].XValueMember = column1;
        //    Chart2.Series["Series5"].YValueMembers = column4;

        //    Chart2.Series["Series5"].IsVisibleInLegend = false;
        //    Chart2.DataBind();

        //    // making user friendly
        //    Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
        //    Chart2.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY";
        //    Chart2.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY";
        //    Chart2.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
        //    Chart2.Visible = true;
        //    //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
        //    Random random = new Random();
        //    foreach (var item in Chart2.Series["YTD SALE"].Points)
        //    {
        //        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
        //        item.Color = c;
        //    }
        //}

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = (inRow[0].ToString()).Replace(",", "");
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString().Replace(",", "");
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString().Replace(",", "");
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }

        [WebMethod]
        public static string LoadChartPurchaseReview()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["Chart"];
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
        #endregion


        protected void ProductGrpList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProductFamilyList_SelectedIndexChanged(null, null);
        }

        protected void ProductFamilyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            objReviewBO = new ReviewBO();
            objReviewBL = new ReviewBL();
            try
            {

                string name_desc = "", name_code = "", chakgrp_desc = "", chkgrp_code = "";
                int count = 0, count1 = 0;
                //if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                // For PRODUCT FAMILY
                foreach (ListItem val in ProductFamilyList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + "','";
                        name_code += val.Value + ",";
                    }
                }

                name_desc = "'" + name_desc;

                // TxtProductfamily.Text = name_desc;

                string ProductFamilyListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 1));
                string ProductFamilyNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                Session["SelectedProductFamilyID"] = ProductFamilyListVal;

                if (count == ProductFamilyList.Items.Count)
                {
                    Session["SelectedProductFamily"] = "ALL";
                }
                else
                {
                    Session["SelectedProductFamily"] = ProductFamilyNameList;
                }
                // For PRODUCT GROUP
                foreach (ListItem val in ProductGrpList.Items)
                {
                    if (val.Selected)
                    {
                        count1++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;

                //  TxtProductGrp.Text = chakgrp_desc;

                string ProductGrpListVal = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                string ProductGrpNameList = chakgrp_desc.Substring(0, Math.Max(0, chakgrp_desc.Length - 2));

                if (count1 == ProductGrpList.Items.Count)
                {
                    Session["SelectedProductGroup"] = "ALL";
                }
                else
                {
                    Session["SelectedProductGroup"] = ProductGrpListVal;
                }
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);

                string ProductFamilyId = Convert.ToString(Session["SelectedProductFamilyID"]);
                objReviewBO.Item_familyID = ProductFamilyId == "ALL" ? "0" : ProductFamilyId;
                DataTable dtPL = new DataTable();
                objReviewBO.Productgroup = ProductGroup == "ALL" ? null : ProductGroup;
                dtPL = objReviewBL.getProductsBL(objReviewBO);
                DataTable dtTemp = dtPL.Clone();
                for (int i = 0; i < dtPL.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(dtPL.Rows[i].ItemArray[0], dtPL.Rows[i].ItemArray[0].ToString() + "_" + dtPL.Rows[i].ItemArray[2].ToString(), dtPL.Rows[i].ItemArray[2]);
                }
                if (dtTemp.Rows.Count != 0)
                {
                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                }
                else
                {

                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                }

                count = 0;
                foreach (ListItem val in ApplicationList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;

                // TxtApplication.Text = name_desc;

                string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == ApplicationList.Items.Count)
                {
                    Session["SelectedApplications"] = "ALL";
                }
                else
                {
                    Session["SelectedApplications"] = ApplicationListVal;
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ApplicationList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string name_desc = "", name_code = "";
                int count = 0;
               
                foreach (ListItem val in ApplicationList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;

                // TxtApplication.Text = name_desc;

                string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == ApplicationList.Items.Count)
                {
                    Session["SelectedApplications"] = "ALL";
                }
                else
                {
                    Session["SelectedApplications"] = ApplicationListVal;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnFetch_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}