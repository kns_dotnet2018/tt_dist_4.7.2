﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace DistributorSystem
{
    public partial class PriceSummary : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    QuoteBL objquoteBL = new QuoteBL();

                    DataTable dtCust = new DataTable();
                    QuoteBO objQuoteBO = new QuoteBO();
                    objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                    dtCust = objquoteBL.GetCustomerListBL(objQuoteBO);
                    Session["dtCustForQuote"] = dtCust;

                    DataTable dtQuote = new DataTable();
                    string start_date = string.Empty;
                    string end_date = string.Empty;
                    string selectedDate = txtDateRange.Text;
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                    objQuoteBO.StartDate = start_date;
                    objQuoteBO.EndDate = end_date;
                    dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
                    Session["dtQuote"] = dtQuote;
                    BindGrid();
                    DataTable dt = new DataTable();
                    dt = objquoteBL.GetCompetitorsBL();
                    Session["dtCompetitors"] = dt;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            QuoteBL objquoteBL = new QuoteBL();
            QuoteBO objQuoteBO = new QuoteBO();
            objQuoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
            DataTable dtQuote = new DataTable();
            string start_date = string.Empty;
            string end_date = string.Empty;
            string selectedDate = txtDateRange.Text;
            if (selectedDate.Contains("/"))
            {
                string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
            }
            objQuoteBO.StartDate = start_date;
            objQuoteBO.EndDate = end_date;
            dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
            Session["dtQuote"] = dtQuote;
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "LoadTable()", true);
        }
        protected void grdPriceSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Open"))
            //{
            //    QuoteBL objquoteBL = new QuoteBL();
            //    DataTable dtQuote = new DataTable();
            //    QuoteBO objQuoteBO = new QuoteBO();
            //    objQuoteBO.Ref_Number = Convert.ToString(e.CommandArgument);
            //    dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
            //    if (dtQuote.Rows.Count > 0)
            //    {
            //        grdDetailedPriceSummary.DataSource = dtQuote;
            //    }
            //    else
            //    {
            //        grdDetailedPriceSummary.DataSource = null;
            //    }
            //    grdDetailedPriceSummary.DataBind();
            // }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            DataTable dtoutput = new DataTable();
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }
                QuoteBO objquoteBO = new QuoteBO();
                objquoteBO.StartDate = start_date;
                objquoteBO.EndDate = end_date;
                objquoteBO.CP_Number = Convert.ToString(Session["DistributorNumber"]);
                QuoteBL objQuoteBL = new QuoteBL();
                dtoutput = objQuoteBL.GetQuoteSummaryBL(objquoteBO);
                if (dtoutput.Rows.Count > 0)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        grdPriceSummary.DataSource = dtoutput;
                        grdPriceSummary.DataBind();
                    }
                    else
                    {
                        grdPriceSummary.DataSource = null;
                        grdPriceSummary.DataBind();
                    }

                }
                else
                {
                    grdPriceSummary.DataSource = null;
                    grdPriceSummary.DataBind();
                }
                dtoutput = (DataTable)Session["dtQuote"];
                if (dtoutput.Rows.Count > 0)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        grdItemSummary.DataSource = dtoutput;
                        grdItemSummary.DataBind();
                    }
                    else
                    {
                        grdItemSummary.DataSource = null;
                        grdItemSummary.DataBind();
                    }

                }
                else
                {
                    grdItemSummary.DataSource = null;
                    grdItemSummary.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string LoadDetailedGrid(string ref_no)
        {
            string output = string.Empty;
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["dtQuote"];
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        var rows = from row in dt.AsEnumerable()
                                   where row.Field<string>("Ref_number").Trim() == ref_no
                                   select row;
                        dt = rows.CopyToDataTable();
                        if (dt.Rows.Count > 0)
                        {
                            output = DataTableToJSONWithStringBuilder(dt);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return output;
        }

        [WebMethod]
        public static string PlaceOrder(string ref_num, string ID, string quantity, string item, string order_type, string schedule, string OrderStartDate, string cust_num, string cust_name, string PO_comment)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            PriceSummary objPrice = new PriceSummary();
            try
            {
                QuoteBO objBO = new QuoteBO();
                objBO.Ref_Number = ref_num;
                objBO.Item_Number = item;
                objBO.QuoteID = Convert.ToInt32(ID);
                objBO.OrderType = order_type;
                if (order_type == "schedule")
                    objBO.scheduleType = schedule;
                else
                    objBO.scheduleType = "";
                objBO.quantity = quantity;
                objBO.orderbyId = Convert.ToString(HttpContext.Current.Session["DistributorNumber"]);
                objBO.orderbyName = Convert.ToString(HttpContext.Current.Session["DistributorName"]);
                objBO.Cust_Number = cust_num;
                objBO.Cust_Name = cust_name;
                objBO.OrderStartDate = OrderStartDate;
                objBO.POComment = PO_comment;
                QuoteBL objBL = new QuoteBL();
                objBO = objBL.PlaceOrderBL(objBO);
                if (objBO.Err_code == 200)
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(objBO.to)))
                    {
                        string attachment = objPrice.GenerateQuotePOFormat(objBO.Ref_Number, objBO.Item_Number);
                        EmailDetails objEmail = new EmailDetails();
                        objEmail.toMailId = objBO.to;
                        objEmail.ccMailId = objBO.cc;
                        objEmail.subject = objBO.subject;
                        objEmail.body = objBO.message;
                        objEmail.attachment = attachment;
                       // objCom.SendMail(objEmail);
                        CommonFunctions.SendGridMail(objEmail).Wait();
                    }
                }
                output = "{\"code\":\"" + objBO.Err_code + "\",\"msg\":\"" + objBO.Err_msg + "\"}";
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string RequestForReApproval(string ref_num, string ID, string item, string status, string reason, string comp_name, string comp_desc, string comp_SP, string end_cust_num, string end_cust_name, string qty, string MOQ, string file, string price)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                List<QuoteStatusDB> objList = new List<QuoteStatusDB>();
                QuoteStatusDB objBO = new QuoteStatusDB();
                objBO.RefNumber = ref_num;
                objBO.item = item;
                objBO.ID = Convert.ToInt32(ID);
                objBO.changeby = Convert.ToString(HttpContext.Current.Session["UserId"]);
                objBO.Status = status;
                objBO.Reason = reason;
                objBO.Comp_name = comp_name;
                objBO.Comp_desc = comp_desc;
                objBO.Comp_SP = comp_SP;
                objBO.End_cust_name = end_cust_name;
                objBO.End_cust_num = end_cust_num;
                objBO.OrderQty = qty;
                objBO.MOQ = MOQ;
                objBO.Escalation_file = file;
                objBO.RecommendedPrice = price;
                // objBO.flag = "CP";
                objList.Add(objBO);
                DataTable dtQuote = new DataTable();
                dtQuote = ToDataTable<QuoteStatusDB>(objList);
                QuoteBL objBL = new QuoteBL();
                QuoteBO objOutput = new QuoteBO();

                objOutput = objBL.RequestForReApprovalBL(dtQuote);
                if (objOutput.Err_code == 200)
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(objOutput.to)))
                    {
                        EmailDetails objEmail = new EmailDetails();
                        objEmail.toMailId = objOutput.to;
                        objEmail.ccMailId = objOutput.cc;
                        objEmail.subject = objOutput.subject;
                        objEmail.body = objOutput.message;
                        //objCom.SendMail(objEmail);
                        CommonFunctions.SendGridMail(objEmail).Wait();
                    }
                }
                output = "{\"code\":\"" + objOutput.Err_code + "\",\"msg\":\"" + objOutput.Err_msg + "\"}";
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string GetStatusLog(string ref_num, string item)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                QuoteBO objBO = new QuoteBO();
                objBO.Ref_Number = ref_num;
                objBO.Item_Number = item;
                DataTable dtQuote = new DataTable();
                QuoteBL objBL = new QuoteBL();
                dtQuote = objBL.GetQuoteStatusLogBL(objBO);
                if (dtQuote.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtQuote);
                }
                else
                {
                    output = "{\"code\":\"202\",\"msg\":\"No Record available.\"}";
                }
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string DownloadFile(string file)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(file);

                //if (File.Exists(filePath))
                //{
                byte[] bytes = File.ReadAllBytes(filePath);

                //Convert File to Base64 string and send to Client.
                return Convert.ToBase64String(bytes, 0, bytes.Length);
                //Response.ContentType = "application/octet-stream";
                //byte[] bts = System.IO.File.ReadAllBytes(filePath);
                //MemoryStream ms = new MemoryStream(bts);
                //HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                //HttpContext.Current.Response.TransmitFile(filePath);
                //HttpContext.Current.Response.End();
                //}
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
                return ex.Message;
            }

        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        #endregion

        #region reportGenerate

        private string GenerateQuotePOFormat(string ref_number, string item_code)
        {
            string file = string.Empty;
            try
            {
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;
                QuoteBL objQuoteBL = new QuoteBL();
                QuoteBO objQuoteBO = new QuoteBO();
                DataTable dt = new DataTable();

                html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
                objQuoteBO.Ref_Number = ref_number;
                objQuoteBO.Item_Number = item_code;
                dt = objQuoteBL.getQuotePOFormatBL(objQuoteBO);

                html += Getheading(dt);

                html += "</body></html>";
                filename = "Quote_PO_" + Convert.ToString(ref_number) + ".pdf";
                filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                convertPDF(html, filepath, filename);
                file = String.Concat(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return file;
        }

        private void convertPDF(string html, string filepath, string filename)
        {
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test1.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(html);
                    }
                }
                GeneratePdfFromHtml(filepath, filename, html);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void GeneratePdfFromHtml(string filepath, string filename, string html)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test1.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, html);
            }
        }

        private void CreatePdf(string filepath, string filename, FileStream htmlInput, FileStream pdfOutput, string html)
        {
            try
            {
                using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 30, 30, 30, 30))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);
                    //worker.ParseXHtml(writer, document, new StringReader(html));
                    document.Close();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private string Getheading(DataTable dt)
        {

            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            string Heading = string.Empty;
            string taegutec_add = string.Empty;
            string Customer_Name = string.Empty;
            string Customer_Address = string.Empty;
            string Customer_Number = string.Empty;
            string Quotation_No = string.Empty;
            string Date = string.Empty;
            string remarks = string.Empty;
            string PO_no = string.Empty;
            try
            {
                Heading = Convert.ToString(ConfigurationManager.AppSettings["QuotePO_Heading"]);
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
                        Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
                        Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
                        Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
                        Date = Convert.ToString(dt.Rows[0]["Date"]);
                        PO_no = Convert.ToString(dt.Rows[0]["PO_No"]);
                        taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);
                        remarks = Convert.ToString(dt.Rows[0]["PurchaseOrderComment"]);

                        strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
                        strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
                        strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
                        strHTMLBuilder.Append(Heading);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("'/>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
                        strHTMLBuilder.Append(Customer_Name);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(taegutec_add);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(Customer_Address);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' >");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append("REMARKS:");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
                        strHTMLBuilder.Append("<table width='100%'>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
                        strHTMLBuilder.Append(Customer_Number);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>PURCHASE ORDER NO : ");
                        strHTMLBuilder.Append(PO_no);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QTN REFERENCE NO : ");
                        strHTMLBuilder.Append(Quotation_No);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
                        strHTMLBuilder.Append(Date);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-size: 10px;'>");
                        strHTMLBuilder.Append(remarks);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");

                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SCHEDULE<br/>DATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>W/H</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
                        strHTMLBuilder.Append("</tr>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strHTMLBuilder.Append("<tr>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(i + 1));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Schedule_Date"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["WHS"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("</tr>");
                        }


                        strHTMLBuilder.Append("</table>");
                    }
                }

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        protected void grdItemSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("lnkFile") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
            }
        }

        protected void lnkFile_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (Convert.ToString(e.CommandArgument) != string.Empty)
                {

                    // Response.ContentType = "application/octet-stream";
                    string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(e.CommandArgument);
                    if (File.Exists(filePath))
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        if (file.Exists)
                        {
                            //Response.Clear();
                            //Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                            //Response.AddHeader("Content-Length", file.Length.ToString());
                            //Response.ContentType = "application/octet-stream";
                            // download […]
                            Response.ContentType = "application/octet-stream";
                            byte[] bts = System.IO.File.ReadAllBytes(filePath);
                            MemoryStream ms = new MemoryStream(bts);
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                            Response.TransmitFile(filePath);
                            Response.End();
                        }
                    }
                    //Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                    //Response.TransmitFile(Server.MapPath(filePath));
                    //Response.End();
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        //protected void btnPlace_Click(object sender, EventArgs e)
        //{
        //    string ID = string.Empty;
        //    string Item = string.Empty;
        //    string Ref = string.Empty;
        //    string Ordertype = string.Empty;
        //    string quantity = string.Empty;
        //    string scheduletype = string.Empty;
        //    try
        //    {
        //        ID = Convert.ToString(hdnID.Value);
        //        Item = Convert.ToString(hdnItem.Value);
        //        Ref = Convert.ToString(hdnRef.Value);
        //        Ordertype = Convert.ToString(lblOrderType.Text);
        //        quantity = Convert.ToString(txtQuantity.Text);
        //        if (Ordertype == "schedule")
        //        {
        //            scheduletype = Convert.ToString(ddlSchedule.SelectedValue);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

    }
}