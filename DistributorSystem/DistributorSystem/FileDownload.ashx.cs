﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace DistributorSystem
{
    /// <summary>
    /// Summary description for FileDownload
    /// </summary>
    public class FileDownload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string file = context.Request.QueryString[0];
            string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(file);

            if (File.Exists(filePath))
            {
                context.Response.ContentType = "application/octet-stream";
                byte[] bts = System.IO.File.ReadAllBytes(filePath);
                MemoryStream ms = new MemoryStream(bts);
                context.Response.Clear();
                context.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                context.Response.TransmitFile(filePath);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}