﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RFQPrice.aspx.cs" Inherits="DistributorSystem.RFQPrice"  MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/powerbi.js"></script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="embedDiv" style="height: 750px; width: 100%;" />
            <script>
        debugger;
	// Read embed token
	var embedToken = "<% =this.embedToken %>";

	// Read embed URL
	var embedUrl = "<% = this.embedUrl %>";

	// Read report Id
	var reportId = "<% = this.reportId %>";

	// Get models (models contains enums)
	var models = window['powerbi-client'].models;

	// Embed configuration is used to describe what and how to embed
	// This object is used when calling powerbi.embed
	// It can also includes settings and options such as filters
	var config = {
		type: 'report',
		tokenType: models.TokenType.Embed,
		accessToken: embedToken,
		embedUrl: embedUrl,
		id: reportId,

		settings: {
			filterPaneEnabled: true,
			navContentPaneEnabled: true
		}
	};

        // Embed the report within the div element
	var embedDiv=document.getElementById("embedDiv");
	var report = powerbi.embed(embedDiv, config);
            </script>
    </asp:Content>
