﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllDistributors.aspx.cs" Inherits="DistributorSystem.AllDistributors" MasterPageFile="~/AdminMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdDistributors tr:first').html();
            $('#MainContent_grdDistributors').prepend('<thead></thead>')
            $('#MainContent_grdDistributors thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdDistributors tbody tr:first').hide();
          //  $('#MainContent_grdDistributors').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            $('#MainContent_grdDistributors').DataTable(
                {
                    //"pageLength": 50,
                    // dom: 'frtlip' ,
                    //"aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                    //"iDisplayLength": 25,
                    //"bPaginate": true,
                    //"bLengthChange": true,
                    //"bFilter": true,
                    //"bInfo": true,
                    //  dom: 'lBfrtip',
                    //dom: 'Bfrtip',
                    //buttons: [
                    //    //'copy', 'csv',
                    //    'excel', 'pdf', 'print'
                    //],

                    //    "footerCallback": function (row, data, start, end, display) {
                    //        var api = this.api(), data;

                    //        // Remove the formatting to get integer data for summation
                    //        var intVal = function (i) {
                    //            // console.log(i);

                    //            if ($.isNumeric(i)) {
                    //                return typeof i === 'string' ?
                    //                    i.replace(/[\$,]/g, '') * 1 :
                    //                    typeof i === 'number' ?
                    //                    i : 0;
                    //            } else {
                    //                var obj = $(i);
                    //                var filter = $(obj).filter('input');
                    //                i = filter.val();
                    //                return typeof i === 'string' ?
                    //                    i.replace(/[\$,]/g, '') * 1 :
                    //                    typeof i === 'number' ?
                    //                    i : 0;
                    //            }
                    //        };

                    //        // Total over all pages
                    //        total = api
                    //            .column(2)
                    //            .data()
                    //            .reduce(function (a, b) {

                    //                if ($.isNumeric(a) && $.isNumeric(b)) {
                    //                    return intVal(a) + intVal(b);
                    //                } else return intVal(a);
                    //            }, 0);

                    //        // Total over this page
                    //        pageTotal = api
                    //            .column(2, { page: 'current' })
                    //            .data()
                    //            .reduce(function (a, b) {
                    //                if ($.isNumeric(a) && $.isNumeric(b)) {
                    //                    return intVal(a) + intVal(b);
                    //                } else if (!$.isNumeric(b)) {
                    //                    var obj = $(b);
                    //                    var filter = $(obj).filter('input');
                    //                    b = filter.val();
                    //                    return intVal(a) + intVal(b);
                    //                } else
                    //                    return intVal(a);
                    //            }, 0);

                    //        // Update footer
                    //        $(api.column(2).footer()).html(
                    //            pageTotal
                    //            //+ ' ( ' + total + ' total)'
                    //        );
                    //        //var api = this.api(), data;

                    //        //// Remove the formatting to get integer data for summation
                    //        // var spanID = 0;
                    //        var intVal1 = function (i) {
                    //            //debugger;
                    //            //console.log(i);
                    //            //console.log(typeof i);
                    //            var obj = $(i);
                    //            console.log(obj);
                    //            //var filter = $(obj).filter('span[id^=MainContent_grdEntry_lblyear1_]');
                    //            //console.log(filter);
                    //            //console.log($(obj)[3]);
                    //            //var spanVal = jQuery('span[id^=MainContent_grdEntry_lblyear1_]' + spanID).html();
                    //            //console.log(spanID)
                    //            var filter = $(obj).filter('div');
                    //            console.log(filter);
                    //            console.log('hello');

                    //            return typeof i === 'string' ?
                    //                i.replace(/[\$,]/g, '') * 1 :
                    //                typeof i === 'number' ?
                    //                i : 0;
                    //            //console.log(i);
                    //            //spanID++;
                    //        };

                    //        // Total over all pages
                    //        total = api
                    //            .column(3)
                    //            .data()
                    //             .data()
                    //.reduce(function (a, b) {
                    //    return intVal(a) + intVal(b);
                    //}, 0);

                    //        // Total over this page
                    //        pageTotal = api
                    //            .column(3, { page: 'current' })
                    //            .data()
                    //             .reduce(function (a, b) {
                    //                 return intVal(a) + intVal(b);
                    //             }, 0);
                    //        $(api.column(3).footer()).html(
                    //            pageTotal
                    //            //+ ' ( ' + total + ' total)'
                    //        );

                    //        // Total over all pages
                    //        total = api
                    //            .column(4)
                    //            .data()
                    //            .reduce(function (a, b) {

                    //                if ($.isNumeric(a) && $.isNumeric(b)) {
                    //                    return intVal(a) + intVal(b);
                    //                } else return intVal(a);
                    //            }, 0);

                    //        // Total over this page
                    //        pageTotal = api
                    //            .column(4, { page: 'current' })
                    //            .data()
                    //            .reduce(function (a, b) {
                    //                if ($.isNumeric(a) && $.isNumeric(b)) {
                    //                    return intVal(a) + intVal(b);
                    //                } else if (!$.isNumeric(b)) {
                    //                    var obj = $(b);
                    //                    var filter = $(obj).filter('input');
                    //                    b = filter.val();
                    //                    return intVal(a) + intVal(b);
                    //                } else
                    //                    return intVal(a);
                    //            }, 0);
                    //        $(api.column(4).footer()).html(
                    //            pageTotal
                    //            //+ ' ( ' + total + ' total)'
                    //        );
                    //    }
                });

        });
        function isNumberKey(evt, obj) {

            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 2) {
                    if (charCode == 46) return true;
                    else return false;
                }

            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function CheckExtension(sender) {
            var validExts = new Array(".xlsx", ".xls");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnUpload').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnUpload').attr("style", "display:block");
                return true;
            }
        }
    </script>
    <style>
        .mn_popup
        {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table
        {
            border-color: white!important;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Users</a>
                        </li>
                        <li class="current">Login as Different User</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:Label runat="server">Channel Partner are displayed below. Please click on the CP number to view it's CP Sales application.</asp:Label>

        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdDistributors" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Channel Partner Number">
                        <ItemTemplate>
                            <asp:LinkButton CommandArgument='<%# Bind("customer_number") %>' OnClick="lblCustomerNumber_Click" ID="lblCustomerNumber" runat="server" Text='<%#Bind("customer_number") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Channel Partner Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customer_short_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>
