﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.Web.Services;

namespace DistributorSystem
{
    public partial class FlashSale : System.Web.UI.Page
    {
        #region Global Declaration
        SaleBO objSaleBO;
        SaleBL objSaleBL;
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        #endregion
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindGrid()
        {
            DataTable dtOutput = new DataTable();
            try
            {
                objSaleBO = new SaleBO();
                objSaleBL = new SaleBL();

                dtOutput = objSaleBL.GetSaleItemBL();
                grdItem.DataSource = dtOutput;
                grdItem.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "BindGridView();", true);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        [WebMethod]
        public static string PlaceOrder(string qty, string id)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                SaleBO objBO = new SaleBO();
                objBO.item_available_id = Convert.ToInt32(id);
                objBO.order_qty = Convert.ToInt32(qty);
                objBO.cust_num = Convert.ToInt32(HttpContext.Current.Session["DistributorNumber"]);
                objBO.user_id = Convert.ToInt32(HttpContext.Current.Session["DistributorNumber"]);

                SaleBL objBL = new SaleBL();
                objBO = objBL.PlaceOrderBL(objBO);
                if (objBO.Err_code==200)
                {
                    output = "{\"code\":\"200\",\"msg\":\""+objBO.Err_msg+"\"}";
                }
                
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }


    }
}