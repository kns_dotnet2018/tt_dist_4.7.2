﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="salesitemupload.aspx.cs" Inherits="DistributorSystem.salesitemupload" MasterPageFile="~/AdminMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--%>

    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>--%>
    <%-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>  
     <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--%>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>--%>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>



    <script type="text/javascript" class="init">
        $(document).ready(function () {
            var fname = "";
            var head_content = $('#MainContent_grdfileupload tr:first').html();
            $('#MainContent_grdfileupload').prepend('<thead></thead>')
            $('#MainContent_grdfileupload thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdfileupload tbody tr:first').hide();
            $('#MainContent_grdfileupload').DataTable(
                {
                });

        });
        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_grdfileupload tr:first').html();
            $('#MainContent_grdfileupload').prepend('<thead></thead>')
            $('#MainContent_grdfileupload thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdfileupload tbody tr:first').hide();
            $('#MainContent_grdfileupload').DataTable(
                {
                });
        }
        function callme(oFile) {
            var name = oFile.value;
            fname = name;
            var error = $("#MainContent_lblExtError").val();
            if (error != '' || error == '') {
                $("#MainContent_lblExtError").empty();
                if (name != '' && name != null) {
const fi = document.getElementById('MainContent_updFile');
if (fi.files.length > 0) { 
            for (const i = 0; i <= fi.files.length-1; i++) { 
  
                const fsize = fi.files.item(i).size; 
                const file = Math.round((fsize / 1024)); 
                // The size of the file. 
                if (file >= 4096) { 
                   
$("#MainContent_lblExtError").text("File too Big, please select a file less than 4mb"); 
$("#MainContent_btnUpload").prop('disabled', true);
                } 
else{
$("#MainContent_btnUpload").prop('disabled', false);
}
            } 
        } 
                    var q = name.substring(name.lastIndexOf('.') + 1);
                    if (q == "xlsx" || q == "xls") {
                        $("#btnspan").empty();
                        $("#MainContent_filename").empty();
                    }
                }
            }
        }

        function callmeForUpdate(oFile) {
            var name = oFile.value;
            if (name != '' && name != null) {
                var q = name.substring(name.lastIndexOf('.') + 1);
                if (q == "xlsx" || q == "xls") {
                    $("#MainContent_Label4").empty();
                    $("#btnspan1").empty();
                }
               
            }
        }


        function ShowPopup() {
            $("#product_view").modal("show");
        }

        
    </script>


    <style>
        .fileupload {
            margin-top: 10px;
            margin-bottom: 10px;
            /* text-align-last: center;*/
        }

        .salesname {
            width: 56%;
        }

        table {
            border-color: white !important;
        }

        #pnlData {
            width: 100%;
        }

        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        .newtable {
            width: 100%;
        }

        .uploaddoc {
            margin: 9px 0px 9px 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <input id="sessionInput" type="hidden" value='<%=Session["filename"] %>' />
    <div id="pdfdialog" style="display: none">
        <div style="height: 500px;">
            <iframe style="width: 100%; height: 100%;" id="if_pdf"></iframe>
        </div>
    </div>
    <div class='block-web' style='float: left; width: 100%; height: 36px;'>
        <div class="header">
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <a class="mn_breadcrumb">Sales Item Upload</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-12 filter_panel" style="height: 91px">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel" Style="text-align: center">
            <asp:Label ID="Label7" runat="server" ForeColor="white">Only New files has to be uploaded here And Excel Should Contains Column as ItemCode,ItemDesc,OpeningQuantity,SalePrice,ListPrice And File Size should be less than 4Mb</asp:Label>
            <div class="col-md-12 nopadding">
                <div class="col-md-3 fileupload">
                    <asp:FileUpload ID="updFile" runat="server" Style="width: 100%; color: white" onchange="callme(this)" /><asp:Label ID="filename" runat="server" ForeColor="white"></asp:Label>
                    <span id="btnspan" style="color: white; margin-right: -20px;"></span>

                </div>
                <div class="col-md-2 fileupload">
                    <asp:Label ID="Label8" runat="server" ForeColor="white">Sales Name </asp:Label><asp:TextBox runat="server" ID="txtsalename" CssClass="salesname" Rows="10" MaxLength="100"></asp:TextBox>
                </div>
                <div class="col-md-6 fileupload">
                    <asp:Label ID="Label1" runat="server" ForeColor="white">Sales From </asp:Label><asp:TextBox ID="TextBox1" runat="server" placeholder="From Date" TextMode="DateTimeLocal" ReadOnly="false"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" ForeColor="white">Sales To </asp:Label><asp:TextBox ID="TextBox2" runat="server" placeholder="To Date" TextMode="DateTimeLocal" ReadOnly="false"></asp:TextBox>

                </div>


                <div class="col-md-1 fileupload" style="float: right">
                    <%--<asp:CustomValidator ID="customValidatorUpload" runat="server" ErrorMessage="" ControlToValidate="fileUpload" ClientValidationFunction="setUploadButtonState();" />--%>
                    <asp:Button runat="server" CssClass="btnSubmit" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
                </div>
                <asp:Label ID="lblExtError" runat="server" ForeColor="white"></asp:Label>
            </div>


        </asp:Panel>
    </div>


    <div class="col-md-12 filter_panel">
        <asp:Label ID="Label3" CssClass="uploaddoc" Style="display: none;" runat="server">Uploaded Sales Details : </asp:Label>

    </div>
    <div class="col-md-8 mn_margin newtable">
        <asp:Panel runat="server" ID="pnlpopup">
            <asp:GridView ID="grdfileupload" CssClass="display compact" runat="server" AutoGenerateColumns="false" Width="100%" OnRowCommand="grdfileDetails_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Document Name">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Bind("file_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sales Name">
                        <ItemTemplate>
                            <asp:Label ID="lblsalesName" runat="server" Text='<%#Bind("salesName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Uploaded Date">
                        <ItemTemplate>
                            <asp:Label ID="txtuploadeddate" runat="server" Text='<%#Bind("uploaded_on") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Date">
                        <ItemTemplate>
                            <asp:Label ID="lblfromdate" runat="server" Text='<%#Bind("start_date") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Date">
                        <ItemTemplate>
                            <asp:Label ID="lbltodate" runat="server" Text='<%#Bind("end_date") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <%--<asp:ImageButton runat="server" ToolTip="Edit" Width="20px" data-toggle="modal" data-target="#product_view" ImageUrl="images/edit-icon.png" ID="ImageButton1" CommandArgument='<%# Bind("file_ID") %>' CommandName="editdocument" />--%>
                            <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit-icon.png" ID="ImageButton1" CommandArgument='<%# Bind("file_ID") %>' CommandName="editdocument" />
                            <asp:ImageButton runat="server" ToolTip="Delete" Width="20px" ImageUrl="images/delete.png" ID="imgAction" CommandArgument='<%# Bind("file_ID") %>' CommandName="deletedocument" />

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
    <div class="modal fade product_view" id="product_view" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title">Sales Details Update
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 nopad">
                        <div class="col-md-12 control">
                            <div class="col-md-6 fileupload">
                                <asp:FileUpload ID="FileUpload1" runat="server" Style="width: 100%; color: black" onchange="callmeForUpdate(this)" /><asp:Label ID="Label4" runat="server" ForeColor="black"></asp:Label>
                                <span id="btnspan1" style="color: black; margin-right: -20px;"></span>
                            </div>
                            <div class="col-md-6 fileupload">
                                <asp:Label ID="Label9" runat="server" ForeColor="black">Sales Name </asp:Label><asp:TextBox runat="server" ID="salesname" CssClass="salesname"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6 fileupload">
                                <asp:Label ID="Label5" runat="server" ForeColor="black">Sales From </asp:Label><asp:TextBox ID="salesfrom" runat="server" placeholder="From Date" TextMode="DateTimeLocal" ReadOnly="false"></asp:TextBox>
                            </div>
                            <div class="col-md-6 fileupload">
                                <asp:Label ID="Label6" runat="server" ForeColor="black">Sales To </asp:Label><asp:TextBox ID="salesto" runat="server" placeholder="To Date" TextMode="DateTimeLocal" ReadOnly="false"></asp:TextBox>

                            </div>

                            <div class="col-md-12" style="padding: 9px;">
                                <div class="col-md-6">
                                    <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btnSubmit" OnClick="btnUpdate_Click" Style="padding: 6px 12px;" />

                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close</button>
                    </div>
                    <asp:Label ID="lblExtError1" runat="server" ForeColor="white"></asp:Label>
                </div>
            </div>
        </div>



    </div>
</asp:Content>
