﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="DistributorSystem.ResetPassword" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function validateControls() {
            //debugger
            var err_flag = 0;
            var err_old = 0;
            if ($('#MainContent_txtOld').val() == "") {
                $('#MainContent_txtOld').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtOld').css('border-color', '');
                err_old = checkOldPassword();
            }
            if ($('#MainContent_txtNew').val() == "") {
                $('#MainContent_txtNew').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtNew').css('border-color', '');
            }

            if ($('#MainContent_txtConfirm').val() == "") {
                $('#MainContent_txtConfirm').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtConfirm').css('border-color', '');
            }


            if (err_flag == 0) {
                if (err_old == 0) {
                    $('#MainContent_lblError').text('');
                    return checkPasswordMatch();
                }
                else {
                    $('#MainContent_txtOld').css('border-color', 'red');
                    $('#MainContent_lblError').text('Please enter correct old password.');
                    return false;
                }
            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        function checkPasswordMatch() {
            var pwd = $('#MainContent_txtNew').val();
            var repwd = $('#MainContent_txtConfirm').val();
            if (pwd == repwd) {
                $('#MainContent_lblError').text('');
                return true;

            }
            else {
                $('#MainContent_lblError').text('Password does not match.');
                return false;
            }
        }
        function checkOldPassword() {
            var val = "<%= Session["LoggedInPassword"]%>";
            if ($('#MainContent_txtOld').val() == val) {
                $('#MainContent_txtOld').css('border-color', '');
                $('#MainContent_lblError').text('');
                return 0;
            }
            else {
                $('#MainContent_txtOld').css('border-color', 'red');
                $('#MainContent_lblError').text('Please enter correct old password.');
                return 1;
            }
        }
    </script>
    <style>
        .toppadding
        {
            margin-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">

    <div class="filter_panel form-boxRest" style="width: 60%;    margin-left: 20%;">      
<%--    <form id="Form1" action="#" class="login-form" runat="server">--%>

            <div class="col-md-12 toppadding">
                                                        <div class="col-md-5 nopad">
                                                            <p ><b>Old Password* :</b></p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                           <asp:TextBox runat="server" ID="txtOld" class="form-control1 mn_inp control3" TextMode="Password"></asp:TextBox>
                                                        </div>
                                                    </div>
         

       
                     <div class="col-md-12 toppadding">
                                                        <div class="col-md-5 nopad">
                                                            <p><b>New Password* :</b></p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                           <asp:TextBox runat="server" ID="txtNew" class="form-control1 mn_inp control3" TextMode="Password"></asp:TextBox>
                                                        </div>
                                                    </div>
   
              
                     <div class="col-md-12 toppadding">
                                                        <div class="col-md-5 nopad">
                                                            <p><b>Confirm New Password* :</b></p>
                                                        </div>
                                                        <div class="col-md-7 nopad">
                                                           <asp:TextBox runat="server" ID="txtConfirm" class="form-control1 mn_inp control3" TextMode="Password"></asp:TextBox>
                                                        </div>
                                                    </div>
     
         <div class="col-md-12 toppadding">
             <div class="col-md-5 nopad">
        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label></div>
             <div class="col-md-7 nopad">
        <asp:Button runat="server" ID="btnReset" Text="Change Password" CssClass="btnSubmit" OnClick="btnReset_Click" OnClientClick="return validateControls();" />
             </div></div>
        </div>
   <%-- </form>--%>
    </asp:Content>

