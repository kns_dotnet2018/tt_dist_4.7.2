﻿using DistributorSystemBL;
using DistributorSystemBO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DistributorSystem
{
    public partial class salesitemupload : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string name = Convert.ToString(Session["DistributorNumber"]);
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"])) || Convert.ToString(Session["DistributorNumber"]) != "Admin") { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    Loaddocument();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        private string GetNextFileName(string pathname,string fileName)
        {
            
            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(pathname);
            string onlyfilename = Path.GetFileNameWithoutExtension(fileName);

            int fCount = Directory.GetFiles(pathname, fileName, SearchOption.AllDirectories).Length;
            int fCount1 = Directory.GetFiles(pathname, "test1(*", SearchOption.AllDirectories).Length;
            if (fCount ==0 && fCount1 ==0)
            {
                fileName = fileName;
            }
            else if(fCount ==1 && fCount1 >=0)
            {
                fileName = string.Format("{0}({1}){2}", onlyfilename,fCount1 +1, extension);
            }
            //string[] allFiles = Directory.GetFiles(pathname).ToArray();
            
            //int count = 1;
            //foreach (var item in allFiles)
            //{
            //    string fileName1 = item.Substring(pathname.Length);
                
            //        string fileNameOnly = Path.Combine(pathName, Path.GetFileNameWithoutExtension(fileName));
            //        fileName = string.Format("{0}({1}){2}", onlyfilename, count++, extension);
                
            //}
            
            return fileName;
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (updFile.PostedFile != null && updFile.PostedFile.ContentLength > 0 && txtsalename.Text != "" && TextBox1.Text != "" && TextBox2.Text != "")
                {
                    lblExtError.Text = "";
                    int count = 0;
                    string fileName = Path.GetFileName(updFile.PostedFile.FileName);
                    FileInfo fi = new FileInfo(fileName);
                    string extension = fi.Extension;
                   
                        if (TextBox1.Text == TextBox2.Text)
                    {
                        lblExtError.Text = "Sales From and Sales To Should be same";
                    }
                    
                        lblExtError.Text = "";  

                        string date = DateTime.Now.ToString("yyyy-MM-dd");
                        date = date.Replace("-", "");
                        string folder = Server.MapPath("~/UploadedFiles/" + date + "/");
                        bool exists = System.IO.Directory.Exists(folder);
                        if (!exists)
                            System.IO.Directory.CreateDirectory(folder);
                        string pathname = folder + fileName;

                        FileUploadBO obj = new FileUploadBO();
                        FileUploadBL objfilebl = new FileUploadBL();
                        string scriptString = "";
                        ClientScriptManager script = Page.ClientScript;
                        DataTable dt = null;
                        obj.UploadDate = DateTime.Now.ToString("yyyy-MM-dd");
                        obj.FromDate = TextBox1.Text.Replace('T', ' ');
                        obj.ToDate = TextBox2.Text.Replace('T', ' ');
                        obj.salesName = txtsalename.Text;
                        obj.insertflag = 2;
                        obj.file_id = 0;
                        obj = objfilebl.salesFileUploadDetailsBL(obj, dt);
                        if (obj.err_code == 1)
                        {
                            string filename1 = GetNextFileName(folder, fileName);
                            string CurrentFilePath = Path.Combine(folder, filename1);

                            if (File.Exists(CurrentFilePath))
                            {
                                //File.Delete(CurrentFilePath);
                            }
                            updFile.SaveAs(Path.Combine(folder, filename1));

                        if (extension == ".xlsx" || extension == ".xls")
                        {
                            if (File.Exists(CurrentFilePath))
                            {
                                long size = fi.Length;
                            }
                            dt = ReadFile(CurrentFilePath, extension);
                            DataColumnCollection columns = dt.Columns;
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr.ItemArray[2] == "")
                                {
                                    count = 1;
                                    
                                    break;
                                }
                                
                            }
                            if (count == 0)
                            {
                                if (dt.Rows.Count > 0)
                                {

                                    obj.FileName = filename1;
                                    obj.UploadDate = DateTime.Now.ToString("yyyy-MM-dd");
                                    obj.FromDate = TextBox1.Text.Replace('T', ' ');
                                    obj.ToDate = TextBox2.Text.Replace('T', ' ');
                                    obj.salesName = txtsalename.Text;
                                    obj.insertflag = 1;
                                    obj.file_id = 0;
                                    obj = objfilebl.salesFileUploadDetailsBL(obj, dt);
                                    if (obj.err_code == 0)
                                    {
                                        scriptString = "<script type='text/javascript'> alert('File Uploaded Successfully');</script>";
                                        script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                                        filename.Text = filename1;
                                        Loaddocument();
                                    }
                                }
                                else
                                {
                                    if (File.Exists(CurrentFilePath))
                                    {
                                        File.Delete(CurrentFilePath);
                                    }
                                    scriptString = "<script type='text/javascript'> alert('Required Columns are not present in the excel, Please check the excel and try to upload');</script>";
                                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                                }
                            }
                            else
                            {
                                if (File.Exists(CurrentFilePath))
                                {
                                    File.Delete(CurrentFilePath);
                                }
                                scriptString = "<script type='text/javascript'> alert('Please check the excel and try to upload, Opening Quantity should be integer.');</script>";
                                script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                            }

                        }
                        else
                        {
                            scriptString = "<script type='text/javascript'> alert('" + obj.err_msg + "');</script>";
                            script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                        }
                    }
                    else
                    {
                        lblExtError.Text = "File should accept only .xlsx and .xls extension";
                    }
                }
                else
                {
                    lblExtError.Text = "Please Enter All The Details";
                }
            }
            catch (Exception ex)
            {

                objCom.ErrorLog(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string scriptString = "";
                ClientScriptManager script = Page.ClientScript;
                if (FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength > 0 && salesfrom.Text != "" && salesto.Text != "")
                {
                    string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    FileInfo fi = new FileInfo(fileName);
                    string extension = fi.Extension;

                    if (extension == ".xlsx" || extension == ".xls")
                    {
                        string date = DateTime.Now.ToString("yyyy-MM-dd");
                        date = date.Replace("-", "");
                        string folder = Server.MapPath("~/UploadedFiles/" + date + "/");
                        bool exists = System.IO.Directory.Exists(folder);
                        if (!exists)
                            System.IO.Directory.CreateDirectory(folder);
                        string filename1 = GetNextFileName(folder, fileName);
                        string CurrentFilePath = Path.Combine(folder, filename1);

                        if (File.Exists(CurrentFilePath))
                        {

                            //File.Delete(CurrentFilePath);
                        }
                        FileUpload1.SaveAs(Path.Combine(folder, filename1));

                        FileUploadBO obj = new FileUploadBO();
                        FileUploadBL objfilebl = new FileUploadBL();
                    
                        DataTable dt = ReadFile(CurrentFilePath, extension);
                        obj.FileName = filename1;
                        obj.UploadDate = DateTime.Now.ToString("yyyy-MM-dd");
                        obj.FromDate = salesfrom.Text.Replace('T', ' ');
                        obj.ToDate = salesto.Text.Replace('T', ' ');
                        obj.salesName = salesname.Text;
                        obj.insertflag = 0;
                        obj.file_id = Convert.ToInt32(Session["fileid"]);
                        obj = objfilebl.salesFileUploadDetailsBL(obj, dt);
                        if (obj.err_code == 0)
                        {
                            scriptString = "<script type='text/javascript'> alert('File Uploaded Successfully');</script>";
                            script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                            Loaddocument();
                        }
                        else
                        {
                            scriptString = "<script type='text/javascript'> alert('" + obj.err_msg + "');</script>";
                            script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                        }
                    }
                    else
                    {
                        scriptString = "<script type='text/javascript'> alert('File should accept only .xlsx and .xls extension');</script>";
                        script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                       
                    }

                       
                }
                else
                {
                    FileUploadBO obj = new FileUploadBO();
                    FileUploadBL objfilebl = new FileUploadBL();
                    obj.FileName = Label4.Text;
                    obj.UploadDate = DateTime.Now.ToString("yyyy-MM-dd");
                    obj.FromDate = salesfrom.Text.Replace('T', ' ');
                    obj.ToDate = salesto.Text.Replace('T', ' ');
                    obj.salesName = salesname.Text;
                    obj.insertflag = 0;
                    obj.file_id = Convert.ToInt32(Session["fileid"]);
                    obj = objfilebl.salesFileUploadDetailsBL(obj, null);
                    if (obj.err_code == 0)
                    {
                        scriptString = "<script type='text/javascript'> alert('Item updated Successfully');</script>";
                        script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                        Loaddocument();
                    }
                }
            }
            catch (Exception ex)
            {

                objCom.ErrorLog(ex);
            }
        }

        protected void grdfileDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string name = e.CommandName;
                FileUploadBO obj = new FileUploadBO();
                FileUploadBL objfilebl = new FileUploadBL();
                DataTable dtDistributors = new DataTable();

                string fileid1 = e.CommandArgument.ToString();
                int fileid = Convert.ToInt32(fileid1);
                if (name == "deletedocument")
                {
                    
                        obj = objfilebl.DeleteSaleFileDetailsBL(fileid);
                        if (obj.err_code == 0)
                        {
                            Loaddocument();
                        }
                    
                }
                else if (name == "editdocument")
                {
                 
                    int flag = 0;
                 
                    Session["fileid"] = fileid;

                    dtDistributors = objfilebl.getsaleFileDetailsBL(flag, fileid);
                    DateTime uploadeddate = Convert.ToDateTime(dtDistributors.Rows[0]["uploaded_on"]);
                    DateTime dt = Convert.ToDateTime(dtDistributors.Rows[0]["end_date"]);
                    Label4.Text = Convert.ToString(dtDistributors.Rows[0]["file_name"]);
                    salesfrom.Text = Convert.ToDateTime(dtDistributors.Rows[0]["start_date"]).ToString("yyyy-MM-ddTHH:mm:ss");
                    salesto.Text = dt.ToString("yyyy-MM-ddTHH:mm:ss");
                    salesname.Text = Convert.ToString(dtDistributors.Rows[0]["salesName"]);
                    if(Convert.ToDateTime(dtDistributors.Rows[0]["uploaded_on"]) > Convert.ToDateTime(dtDistributors.Rows[0]["start_date"]) && Convert.ToDateTime(dtDistributors.Rows[0]["uploaded_on"]) > Convert.ToDateTime(dtDistributors.Rows[0]["end_date"]))
                    {
                       string scriptString = "<script type='text/javascript'> alert('Uploaded date should not be greater than sales from and sales to date');</script>";
                        ClientScriptManager script = Page.ClientScript;
                        script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                    }

                    ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup();", true);

                }
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        private DataTable ReadFile(string FilePath, string Extension)
        {
            DataTable dt = new DataTable();
            try
            {
                string conStr = "";
                switch (Extension)

                {
                    case ".xls": //Excel 97-03

                        conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                        FilePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        break;

                    case ".xlsx": //Excel 07

                        conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
                        FilePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        break;

                }
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                cmdExcel.Connection = connExcel;
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                //Read Data from First Sheet

                connExcel.Open();

                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                OleDbDataReader dr = cmdExcel.ExecuteReader();

                if (dr.HasRows)
                {
                    dt.Columns.Add("item_code", typeof(string));
                    dt.Columns.Add("Item_desc", typeof(string));
                    dt.Columns.Add("Opening_Quantity", typeof(string));
                    dt.Columns.Add("Sale_price", typeof(string));
                    dt.Columns.Add("List_price", typeof(string));
                    while (dr.Read())
                    {
                        string d1 = dr[2].ToString();
                        if(d1 =="")
                        {
                            dt.Rows.Add(dr[0].ToString().Trim(), dr[1].ToString().Trim(), dr[2].ToString().Trim(), dr[3].ToString().Trim(), dr[4].ToString().Trim());
                        }

                        else if (dr[0].ToString().Trim() != string.Empty && dr[1].ToString().Trim() != string.Empty && dr[2].ToString().Trim() != string.Empty && dr[3].ToString().Trim() != string.Empty && dr[4].ToString().Trim() != string.Empty && dr[0].ToString().Trim() != " " && dr[1].ToString().Trim() != " " && dr[2].ToString().Trim() != " " && dr[3].ToString().Trim() != " " && dr[4].ToString().Trim() != " ")
                            dt.Rows.Add(dr[0].ToString().Trim(), dr[1].ToString().Trim(), dr[2].ToString().Trim(), dr[3].ToString().Trim(), dr[4].ToString().Trim());

                    }
                }

                dr.Close();
                connExcel.Close();
                
            }
            catch(Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dt;
        }

        protected void Loaddocument()
        {
            FileUploadBL objfilebl = new FileUploadBL();
            DataTable dtDistributors = new DataTable();
            int flag = 1;
            int fileid = 0;

            dtDistributors = objfilebl.getsaleFileDetailsBL(flag,fileid);

            if (dtDistributors.Rows.Count > 0)
            {
                
                grdfileupload.DataSource = dtDistributors;
                grdfileupload.DataBind();
                Label3.Attributes.Add("style", "display:block");
            }
            else
            {
                grdfileupload.DataSource = null;
                grdfileupload.DataBind();
            }
        }
    }
}