﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Data;

namespace DistributorSystem
{
    public partial class Login : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        LoginBL objLoginBL;
        LoginBO objLoginBO;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string password = Decrypt("JAEZ+ZHL/H8kDBva4cH9bvtAM0D05ijp2pq+auP5Vlk=");
                password = Decrypt("dbU36CzHSn2Y8KqY/u0RFUl2EdjFe+SgU+4l5GOhaDY=");
                password = Decrypt("s30QrswOK33F57XfLLAUHLvhPuPxSgTseI3/y6WnMmM=");
                Session["UserId"] = null;
                Session["UserFullName"] = null;
                Session["LoginMailId"] = null;
                Session["DistributorNumber"] = null;
                Session["DistributorName"] = null;
                Session["UserRole"] = null;
                Session["cter"] = null;
            }
        }
        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return cipherText;
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                objLoginBL = new LoginBL();
                objLoginBO = new LoginBO();
                objLoginBO.Username = Convert.ToString(txtusername.Text);
                objLoginBO.Password = Encrypt(Convert.ToString(txtpassword.Text));
                Session["LoggedInPassword"] = Convert.ToString(txtpassword.Text);
                Session["EncryptedLoggedInPassword"] = objLoginBO.Password;
                objLoginBO = objLoginBL.authLoginBL(objLoginBO);
                if (objLoginBO.ErrorNum == 0)
                {
                    Session["UserId"] = objLoginBO.EngineerId;
                    Session["UserFullName"] = objLoginBO.UserFullName;
                    Session["LoginMailId"] = objLoginBO.LoginMailID;
                    Session["DistributorNumber"] = objLoginBO.Distributor_number;
                    Session["DistributorName"] = objLoginBO.Distributor_name;
                    Session["UserRole"] = objLoginBO.Role;
                    Session["Quote_Flag"] = objLoginBO.Quote_Flag;
                    Session["Price_Flag"] = objLoginBO.Price_Flag;
                    Session["cter"] = objLoginBO.cter;
                    if (Session["DistributorNumber"] != null)
                    {

                        if (Session["UserRole"].ToString() == "Admin")
                        {
                            //Response.Redirect("UserDetails.aspx");
                            Response.Redirect("UserDetailsWithMenu.aspx");
                        }
                        else Response.Redirect("PurchaseDashboard.aspx");

                        //Response.Redirect("Test.aspx");
                    }
                }
                else if (objLoginBO.ErrorNum == 1)
                {
                    string scriptString = "<script type='text/javascript'> alert('User name and password does not match');</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else if (objLoginBO.ErrorNum == 2)
                {
                    string scriptString = "<script type='text/javascript'> alert('User name is incorrect. Please provide valid crendential');</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

      
        protected void lnkRequest_Click(object sender, EventArgs e)
        {
            string result = string.Empty;
            try
            {
                objLoginBO = new LoginBO();
                objLoginBL = new LoginBL();
                objLoginBO.Username = Convert.ToString(txtusername.Text);
                //to create a random password
                string password = CreateRandomPassword(8);
                objLoginBO.Password = Encrypt(password);
                objLoginBO.genrated_password = password;
                // save the encrypted password to database
                objLoginBO = objLoginBL.ChangePasswordBL(objLoginBO);
                if (objLoginBO.ErrorNum==0)
                {
                    //
                    if (!String.IsNullOrEmpty(objLoginBO.LoginMailID))
                    {
                        SendMail(password, objLoginBO.LoginMailID, objLoginBO.Username);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been reset and sent to your mail ID.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been reset but your mail ID is not registerred to the system. Please contact administrator for new password.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('"+objLoginBO.ErrorMessege+" Please enter valid distributor number and try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void SendMail(string password, string emailTo, string username)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "Password has been reset for your account. <br/><br/> UserName : "+ username + " <br/><br/>New Password : " + password;
                objEmail.toMailId = emailTo;
                objEmail.subject = "CP Sales : Password Request";//Subject for your request
                CommonFunctions.SendGridMail(objEmail).Wait();
                //objCom.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }


        private string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }


        private string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return clearText;
        }

    }
}