﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExceptionReport.aspx.cs" Inherits="DistributorSystem.ExceptionReport" MasterPageFile="~/AdminMaster.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="js/buttons.flash.min.js"></script>
    <%--<script type="text/javascript" src="js/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>--%>
    <script type="text/javascript" src="js/buttons.colVis.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdReport tr:first').html();
            $('#MainContent_grdReport').prepend('<thead></thead>')
            $('#MainContent_grdReport thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdReport tbody tr:first').hide();
            //$('#MainContent_grdReport').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            $('#MainContent_grdReport').DataTable(
                {
                    bLengthChange: true,
                    bAutoWidth: false,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    dom: 'lBfrtip',
                    buttons: [
                        'colvis'
                    ]
                });
            $('a.toggle-vis').on('click', function (e) {
                var table = $('#MainContent_grdReport').DataTable();
                e.preventDefault();

                // Get the column API object
                var column = table.column($(this).attr('data-column'));

                // Toggle the visibility
                column.visible(!column.visible());
            });
        });

        $('a.toggle-vis').on('click', function (e) {
            var table = $('#MainContent_grdReport').DataTable();
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    </script>
    <style>
      
        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table
        {
            border-color: white!important;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result
        {
            /*margin-left: 45%;*/
            font-weight: bold;
        }
        .dt-button-collection
        {
            width:1000px!important;
        }
        div a .dt-button buttons-columnVisibility active
        {
            width:20%!important;
        }
         .mn_bott {
            margin: 0px 0 10px 0;
        }

        .btn-default.btn-on-2.active {
            background-color: #006681;
            color: white;
        }

        .btn-default.btn-off-2.active {
            background-color: #006681;
            color: white;
        }
         .btn-group
        {
            margin-bottom: 5px;
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Summary</a>
                        </li>
                        <li class="current">CP Sales Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-12 nopadding">
                <div class="col-md-1">
                    <asp:Label runat="server" ID="lblYear" Text="Year"></asp:Label>
                </div>
                <div class="col-md-3">
                    <div class="controls">
                        <asp:DropDownList Style="margin: 0;" runat="server" ID="ddlYear" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-8">
                    <asp:Button runat="server" ID="btnFilter" CssClass="btnSubmit" Text="Filter" OnClick="btnFilter_Click"/>
                    <asp:Button runat="server" ID="btnExport" CssClass="btnSubmit" Text="Export" OnClick="btnExport_Click" Visible="false" />
                </div>
            </div>
        </asp:Panel>

        <asp:Label runat="server" CssClass="result" ID="lblResult"></asp:Label>
         <asp:Panel ID="panelvalues" runat="server" Visible="false">
            <div id="Divvalues" class="pull-right">
                <div class="btn-group">
                    <%-- data-toggle="buttons">--%>
                    <asp:Button ID="Btn_Units" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In Units" OnClick="Btn_Units_Click"/>
                    <asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In '000" OnClick="Btn_Thousand_Click" />
                    <asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdReport" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Channel Partner Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCPNumber" runat="server" Text='<%#Bind("distributor_number") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Channel Partner Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCPName" runat="server" Text='<%#Bind("distributor_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total no of Customers">
                        <ItemTemplate>
                            <asp:Label ID="lbltotCust" runat="server" Text='<%#Bind("total_no_of_customers") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Achieved %">
                        <ItemTemplate>
                            <asp:Label ID="lblachieved" runat="server" Text='<%#Bind("pct_of_target_achieved") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Annual Purchase Target">
                        <ItemTemplate>
                            <asp:Label ID="lblpurTarget" runat="server" Text='<%#Bind("Annual_purchase_target") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Annual Sales Target">
                        <ItemTemplate>
                            <asp:Label ID="lblSalTarget" runat="server" Text='<%#Bind("Annual_Sales_target") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Jan Sales">
                        <ItemTemplate>
                            <asp:Label ID="lbljansale" runat="server" Text='<%#Bind("jan_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer For Jan Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_jan_sales_added" runat="server" Text='<%#Bind("no_of_jan_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feb Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblfebsale" runat="server" Text='<%#Bind("feb_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer For Feb Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Feb_sales_added" runat="server" Text='<%#Bind("no_of_Feb_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mar Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblmarsale" runat="server" Text='<%#Bind("mar_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Customer For Mar Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Mar_sales_added" runat="server" Text='<%#Bind("no_of_Mar_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Apr Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblaprsale" runat="server" Text='<%#Bind("apr_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer For Apr Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Apr_sales_added" runat="server" Text='<%#Bind("no_of_Apr_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="May Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblmaysale" runat="server" Text='<%#Bind("may_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Customer For May Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_May_sales_added" runat="server" Text='<%#Bind("no_of_May_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Jun Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#Bind("jun_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer For Jun Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_jun_sales_added" runat="server" Text='<%#Bind("no_of_jun_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Jul Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label2"  runat="server" Text='<%#Bind("jul_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Customer For Jul Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_jul_sales_added"  runat="server" Text='<%#Bind("no_of_jul_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Aug Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%#Bind("aug_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Customer For Aug Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Aug_sales_added" runat="server" Text='<%#Bind("no_of_Aug_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sep Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%#Bind("sep_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Customer For Sep Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Sep_sales_added" runat="server" Text='<%#Bind("no_of_Sep_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Oct Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%#Bind("oct_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Customer For Oct Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Oct_sales_added" runat="server" Text='<%#Bind("no_of_Oct_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nov Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%#Bind("nov_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Customer For Nov Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Nov_sales_added" runat="server" Text='<%#Bind("no_of_Nov_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dec Sales">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%#Bind("dec_sales_amt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Customer For Dec Sales">
                        <ItemTemplate>
                            <asp:Label ID="lblno_of_Dec_sales_added" runat="server" Text='<%#Bind("no_of_Dec_sales_added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>


