﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Text;
using System.Web.Services;

namespace DistributorSystem
{
    public partial class SalesReview : System.Web.UI.Page
    {

        #region Global Declaration
        EntryBL objEntryBL;
        EntryBO objEntryBO;
        ReviewBL objReviewBL;
        ReviewBO objReviewBO;
        public static int gridLoadedStatus;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                gridLoadedStatus = 0;
                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Btn_Thousand_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Btn_Lakhs_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Btn_Units_Click(null, null); }
                else
                {
                    Btn_Units_Click(null, null); 
                }
                CustomerBind();
                SalesManBind();
                BindData();
            }
        }

        protected void Btn_Units_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = true;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = true;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        #endregion

        #region Methods
        private void SalesManBind()
        {
            DataTable dtBind = new DataTable();
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                dtBind = objEntryBL.GetSalesManBL(objEntryBO);
                ddlSalesman.DataSource = dtBind;
                ddlSalesman.DataTextField = "salemanname";
                ddlSalesman.DataValueField = "salemanid";
                ddlSalesman.DataBind();
                ddlSalesman.Items.Insert(0, "ALL");
            }
        
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void CustomerBind()
        {
            DataTable dtBind = new DataTable();
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                dtBind = objEntryBL.GetCustomersBL(objEntryBO);
                ddlCustomerList.DataSource = dtBind;
                ddlCustomerList.DataTextField = "customername";
                ddlCustomerList.DataValueField = "customernumber";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void BindData()
        {
            DataTable dtYtdSales = new DataTable();
            gridLoadedStatus = 1;

            if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { dtYtdSales = loadGrid(); }
            else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { dtYtdSales = loadGrid_lakh(); }
            else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { dtYtdSales = loadGrid_Unit(); }

            if (dtYtdSales.Rows.Count != 0)
            {
                lblResult.Text = "";
                grdSalesReviewValues.Visible = true;
                grdSalesReviewValues.DataSource = dtYtdSales;
                grdSalesReviewValues.DataBind();
                divgridchartValues.Visible = true;
            }

            else
            {
                lblResult.Text = "There are no records for selected customer.";
                grdSalesReviewValues.Visible = false;
                divgridchartValues.Visible = false;
            }

            //chart pre
            DataTable dtChart = GenerateTransposedTable(dtYtdSales);
            if (dtChart.Columns.Count > 1)
            {
                dtChart.Columns.RemoveAt(1);
                Session["Chart"] = dtChart;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadChartSalesReview()", true);
            }
        }

        #region load Grid By Values
        protected DataTable getMonthlyvalues(int Year, string flag, int valuein = 1000)
        {
            DataTable dtmonthval = new DataTable();
            try
            {
                objReviewBO = new ReviewBO();
                objReviewBL = new ReviewBL();
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.year = Year;
                objReviewBO.Flag = flag;
                objReviewBO.Valuein = valuein;
                objReviewBO.Cter = null;
                objReviewBO.Branchcode = null;
                objReviewBO.Salesengineer = null;
                objReviewBO.Item_familyname = null;
                objReviewBO.Item_subfamilyname = null;
                objReviewBO.Customer_type = null;
                objReviewBO.Productgroup = null;
                objReviewBO.Item_code = null;
                dtmonthval = objReviewBL.getMonthlyVal(objReviewBO);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            return dtmonthval;
        }

        protected DataTable loadGrid()
        {
            DataTable dtoutput = new DataTable();

            try
            {
                objReviewBO = new ReviewBO();
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                objReviewBO.Salesengineer = Convert.ToString(ddlSalesman.SelectedValue);
                objReviewBL = new ReviewBL();
                dtoutput = objReviewBL.getSalesReviewBL(objReviewBO);
                if (dtoutput.Rows.Count > 0)
                {
                    for (int i = 1; i < dtoutput.Rows.Count; i++)
                    {
                        if (Convert.ToString(dtoutput.Rows[i][0]) != "GROWTH%" && Convert.ToString(dtoutput.Rows[i][0]) != "PRO-RATA ACH%")
                        {
                            dtoutput.Rows[i]["JAN"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JAN"])) ? dtoutput.Rows[i]["JAN"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["FEB"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["FEB"])) ? dtoutput.Rows[i]["FEB"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["MAR"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["MAR"])) ? dtoutput.Rows[i]["MAR"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["APR"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["APR"])) ? dtoutput.Rows[i]["APR"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["MAY"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["MAY"])) ? dtoutput.Rows[i]["MAY"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["JUN"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JUN"])) ? dtoutput.Rows[i]["JUN"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["JUL"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JUL"])) ? dtoutput.Rows[i]["JUL"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["AUG"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["AUG"])) ? dtoutput.Rows[i]["AUG"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["SEP"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["SEP"])) ? dtoutput.Rows[i]["SEP"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["OCT"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["OCT"])) ? dtoutput.Rows[i]["OCT"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["NOV"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["NOV"])) ? dtoutput.Rows[i]["NOV"] : 0) / 1000, 2);
                            dtoutput.Rows[i]["DEC"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["DEC"])) ? dtoutput.Rows[i]["DEC"] : 0) / 1000, 2);
                        }
                        else
                        {
                            dtoutput.Rows[i]["JAN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JAN"])), 2);
                            dtoutput.Rows[i]["FEB"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["FEB"])), 2);
                            dtoutput.Rows[i]["MAR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAR"])), 2);
                            dtoutput.Rows[i]["APR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["APR"])), 2);
                            dtoutput.Rows[i]["MAY"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAY"])), 2);
                            dtoutput.Rows[i]["JUN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUN"])), 2);
                            dtoutput.Rows[i]["JUL"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUL"])), 2);
                            dtoutput.Rows[i]["AUG"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["AUG"])), 2);
                            dtoutput.Rows[i]["SEP"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["SEP"])), 2);
                            dtoutput.Rows[i]["OCT"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["OCT"])), 2);
                            dtoutput.Rows[i]["NOV"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["NOV"])), 2);
                            dtoutput.Rows[i]["DEC"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["DEC"])), 2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        protected DataTable loadGrid_lakh()
        {

            DataTable dtoutput = new DataTable();

            try
            {
                objReviewBO = new ReviewBO();
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                objReviewBO.Salesengineer = Convert.ToString(ddlSalesman.SelectedValue);
                objReviewBL = new ReviewBL();
                dtoutput = objReviewBL.getSalesReviewBL(objReviewBO);
                if (dtoutput.Rows.Count > 0)
                {
                    for (int i = 1; i < dtoutput.Rows.Count; i++)
                    {
                        if (Convert.ToString(dtoutput.Rows[i][0]) != "GROWTH%" && Convert.ToString(dtoutput.Rows[i][0]) != "PRO-RATA ACH%")
                        {
                            dtoutput.Rows[i]["JAN"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JAN"])) ? dtoutput.Rows[i]["JAN"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["FEB"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["FEB"])) ? dtoutput.Rows[i]["FEB"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["MAR"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["MAR"])) ? dtoutput.Rows[i]["MAR"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["APR"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["APR"])) ? dtoutput.Rows[i]["APR"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["MAY"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["MAY"])) ? dtoutput.Rows[i]["MAY"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["JUN"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JUN"])) ? dtoutput.Rows[i]["JUN"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["JUL"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["JUL"])) ? dtoutput.Rows[i]["JUL"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["AUG"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["AUG"])) ? dtoutput.Rows[i]["AUG"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["SEP"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["SEP"])) ? dtoutput.Rows[i]["SEP"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["OCT"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["OCT"])) ? dtoutput.Rows[i]["OCT"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["NOV"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["NOV"])) ? dtoutput.Rows[i]["NOV"] : 0) / 100000, 2);
                            dtoutput.Rows[i]["DEC"] = Math.Round(Convert.ToDecimal(!string.IsNullOrEmpty(Convert.ToString(dtoutput.Rows[i]["DEC"])) ? dtoutput.Rows[i]["DEC"] : 0) / 100000, 2);
                        }
                        else
                        {
                            dtoutput.Rows[i]["JAN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JAN"])), 2);
                            dtoutput.Rows[i]["FEB"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["FEB"])), 2);
                            dtoutput.Rows[i]["MAR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAR"])), 2);
                            dtoutput.Rows[i]["APR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["APR"])), 2);
                            dtoutput.Rows[i]["MAY"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAY"])), 2);
                            dtoutput.Rows[i]["JUN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUN"])), 2);
                            dtoutput.Rows[i]["JUL"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUL"])), 2);
                            dtoutput.Rows[i]["AUG"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["AUG"])), 2);
                            dtoutput.Rows[i]["SEP"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["SEP"])), 2);
                            dtoutput.Rows[i]["OCT"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["OCT"])), 2);
                            dtoutput.Rows[i]["NOV"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["NOV"])), 2);
                            dtoutput.Rows[i]["DEC"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["DEC"])), 2);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        protected DataTable loadGrid_Unit()
        {
            DataTable dtoutput = new DataTable();

            try
            {
                objReviewBO = new ReviewBO();
                objReviewBO.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                objReviewBO.Distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objReviewBO.Salesengineer = Convert.ToString(ddlSalesman.SelectedValue);
                objReviewBL = new ReviewBL();
                dtoutput = objReviewBL.getSalesReviewBL(objReviewBO);
                if (dtoutput.Rows.Count > 0)
                {
                    for (int i = 1; i < dtoutput.Rows.Count; i++)
                    {
                        dtoutput.Rows[i]["JAN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JAN"])), 2);
                        dtoutput.Rows[i]["FEB"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["FEB"])), 2);
                        dtoutput.Rows[i]["MAR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAR"])), 2);
                        dtoutput.Rows[i]["APR"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["APR"])), 2);
                        dtoutput.Rows[i]["MAY"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["MAY"])), 2);
                        dtoutput.Rows[i]["JUN"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUN"])), 2);
                        dtoutput.Rows[i]["JUL"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["JUL"])), 2);
                        dtoutput.Rows[i]["AUG"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["AUG"])), 2);
                        dtoutput.Rows[i]["SEP"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["SEP"])), 2);
                        dtoutput.Rows[i]["OCT"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["OCT"])), 2);
                        dtoutput.Rows[i]["NOV"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["NOV"])), 2);
                        dtoutput.Rows[i]["DEC"] = Math.Round(Convert.ToDecimal((dtoutput.Rows[i]["DEC"])), 2);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        #endregion

        [WebMethod]
        public static string LoadChartSalesReview()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["Chart"];
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
        #endregion

        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtSM = new DataTable();
                objEntryBO = new EntryBO();
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                objEntryBO.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                objEntryBL = new EntryBL();
                dtSM = objEntryBL.GetSalesManDetailsBL(objEntryBO);
                if (dtSM.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(dtSM.Rows[0]["salemanid"])))
                    {
                        ddlSalesman.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlSalesman.SelectedValue = Convert.ToString(dtSM.Rows[0]["salemanid"]);
                    }
                }
                else
                {
                    ddlSalesman.SelectedIndex = 0;
                }
                BindData();
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void ddlSalesman_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCust = new DataTable();
                objEntryBO = new EntryBO();
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                objEntryBO.salesman_id = Convert.ToString(ddlSalesman.SelectedValue);
                objEntryBL = new EntryBL();
                dtCust = objEntryBL.GetCustomerBasedOnSalesBL(objEntryBO);
                ddlCustomerList.DataSource = dtCust;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");

                //if (dtCust.Rows.Count > 0)
                //{
                //    if (string.IsNullOrEmpty(Convert.ToString(dtCust.Rows[0]["customer_number"])))
                //    {
                //        ddlCustomerList.SelectedIndex = 0;
                //    }
                //    else
                //    {
                //        ddlCustomerList.SelectedValue = Convert.ToString(dtCust.Rows[0]["customer_number"]);
                //    }
                //}
                //else
                //{
                //    ddlCustomerList.SelectedIndex = 0;
                //}
                BindData();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

    }
}