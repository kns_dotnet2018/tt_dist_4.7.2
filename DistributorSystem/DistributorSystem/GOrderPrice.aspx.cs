﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;

namespace DistributorSystem
{
    public partial class GOrderPrice : System.Web.UI.Page
    {
        public string embedToken;
        public string embedUrl;
        public Guid reportId;
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                    var report_id = "";
                    using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                    {
                        // Get a list of reports
                        var reports = client.Reports.GetReportsInGroup(Configurations.WorkspaceId);

                        //// Populate dropdown list
                        foreach (Report item in reports.Value)
                        {
                            // ddlReport.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                            if (item.Name == "CPList")
                            {
                                report_id = item.Id.ToString();
                                break;
                            }
                        }

                        //// Select first item
                        //ddlReport.SelectedIndex = 2;
                    }
                    // Generate an embed token and populate embed variables
                    using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                    {

                        // Retrieve the selected report
                        var report = client.Reports.GetReportInGroup(Configurations.WorkspaceId, new Guid(report_id));

                        // Generate an embed token to view
                        var generateTokenRequestParameters = new GenerateTokenRequest(TokenAccessLevel.View);
                        var tokenResponse = client.Reports.GenerateTokenInGroup(Configurations.WorkspaceId, report.Id, generateTokenRequestParameters);

                        embedToken = tokenResponse.Token;
                        embedUrl = report.EmbedUrl;
                        reportId = report.Id;
                    }

                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }

            }
        }
    }
}