﻿using DistributorSystemBL;
using DistributorSystemBO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DistributorSystem
{
    public partial class UserDetailsWithMenu : System.Web.UI.Page
    {
        UserDetailsBL objUserBL;
        UserDetailsBO objUserBO;
        CommonFunctions objCom = new CommonFunctions();
        string dist_number = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    DataTable dtDistributors = new DataTable();
                    if (!IsPostBack)
                    {
                        objUserBL = new UserDetailsBL();
                        dtDistributors = objUserBL.getUserDetailsBL();
                        Session["dtDistributors"] = dtDistributors;
                        if (dtDistributors.Rows.Count > 0)
                        {
                            grdUserDetails.DataSource = dtDistributors;
                            grdUserDetails.DataBind();
                            dist_number = Convert.ToString(dtDistributors.Rows[0]["Distributor_number"]);
                            Session["ClickedDistributor_number"] = dist_number;
                            DataRow drfirst = selectedRow(dist_number);
                            LoadEditPanel(drfirst);
                            //LoadEditPanel();
                        }
                        else
                        {
                            grdUserDetails.DataSource = null;
                            grdUserDetails.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                LoginBL loginBL = new LoginBL();
                LoginBO objLoginBO = new LoginBO();
                DataSet ds = new DataSet();
                int count = 0;
                int count1 = 0;
                Session["ClickedDistributor_number"] = Convert.ToString(drselect["Distributor_number"]);
                lblDistributorName.Text = Convert.ToString(drselect["Distributor_name"]);
                lblDistributorNumber.Text = Convert.ToString(drselect["Distributor_number"]);
                txtEmailId.Text = Convert.ToString(drselect["Email_id"]);
                txtpwd.Text = Decrypt(Convert.ToString(drselect["Password"]));
                txtpwd.Attributes["type"] = "password";
                txtrepwd.Text = Decrypt(Convert.ToString(drselect["Password"]));
                txtrepwd.Attributes["type"] = "password";
                objLoginBO.Username = Convert.ToString(drselect["Distributor_number"]);
                ds = loginBL.GetAllMenusBL(objLoginBO);

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.ID = "childdiv";
                div.Attributes.Add("class", "panelclass");
                Panel1.Controls.Add(div);
                CheckBox chk2 = new System.Web.UI.WebControls.CheckBox();
                chk2.ID = "selectAll";
                chk2.Text = "SelectAll";
                if (ds.Tables.Count > 1)
                {
                    int dtcount = ds.Tables[1].Rows.Count;
                    if (dtcount == ds.Tables[0].Rows.Count)
                    {
                        chk2.Checked = true;
                    }
                    else
                    {
                        chk2.Checked = false;
                    }
                }

                div.Controls.Add(chk2);
                div.Controls.Add(new LiteralControl("<br />"));
                var distinctMenus = ds.Tables[0].AsEnumerable().Select(s => new { name = s.Field<string>("Menu") }).Distinct().ToList();
                for (int i = 0; i < distinctMenus.Count; i++)
                {
                    count = 0;
                    CheckBox chk = new System.Web.UI.WebControls.CheckBox();
                    var distinctMenusid = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { id = s.Field<int>("ID") }).ToList();
                    if (ds.Tables.Count > 1)
                    {
                        var distinctMenusid1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { id = s.Field<int>("ID") }).ToList();
                        if (count != distinctMenusid1.Count && distinctMenusid1.Count > 0)
                        {

                            if (distinctMenusid[0].id == distinctMenusid1[0].id)
                            {
                                count++;
                                if (distinctMenus[i].name.Contains(' '))
                                {
                                    var name = distinctMenus[i].name.Replace(" ", "");
                                    chk.ID = name + "_" + distinctMenusid[0].id;
                                    //chk.CssClass = "MenuClass";
                                    chk.CssClass = "MenuClass_" + name;
                                    chk.Text = distinctMenus[i].name;
                                    chk.Checked = true;
                                }

                                else
                                {
                                    chk.ID = distinctMenus[i].name + "_" + distinctMenusid[0].id;
                                    //chk.CssClass = "MenuClass";
                                    chk.CssClass = "MenuClass_" + distinctMenus[i].name;
                                    chk.Text = distinctMenus[i].name;
                                    chk.Checked = true;
                                }
                            }

                        }
                        else
                        {
                            if (distinctMenus[i].name.Contains(' '))
                            {
                                var name = distinctMenus[i].name.Replace(" ", "");
                                chk.ID = name + "_" + distinctMenusid[0].id;
                                //chk.CssClass = "MenuClass";
                                chk.CssClass = "MenuClass_" + name;
                                chk.Text = distinctMenus[i].name;
                                chk.Checked = false;
                            }
                            else
                            {
                                chk.ID = distinctMenus[i].name + "_" + distinctMenusid[0].id;
                                //chk.CssClass = "MenuClass";
                                chk.CssClass = "MenuClass_" + distinctMenus[i].name;
                                chk.Text = distinctMenus[i].name;
                                chk.Checked = false;
                            }
                        }
                        div.Controls.Add(chk);
                        div.Controls.Add(new LiteralControl("<br />"));
                    }
                    else
                    {
                        if (distinctMenus[i].name.Contains(' '))
                        {
                            var name = distinctMenus[i].name.Replace(" ", "");
                            chk.ID = name + "_" + distinctMenusid[0].id;
                            //chk.CssClass = "MenuClass";
                            chk.CssClass = "MenuClass_" + name;
                            chk.Text = distinctMenus[i].name;
                            chk.Checked = false;
                        }
                        else
                        {
                            chk.ID = distinctMenus[i].name + "_" + distinctMenusid[0].id;
                            //chk.CssClass = "MenuClass";
                            chk.CssClass = "MenuClass_" + distinctMenus[i].name;
                            chk.Text = distinctMenus[i].name;
                            chk.Checked = false;
                        }
                    }
                    div.Controls.Add(chk);
                    div.Controls.Add(new LiteralControl("<br />"));

                    var distinctsubMenus = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { name = s.Field<string>("SubMenu_Name"), id = s.Field<int>("ID") }).ToList();
                    if (ds.Tables.Count > 1)
                    {
                        //var distinctsubMenus1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { name = s.Field<string>("SubMenu_Name"), id = s.Field<int>("ID") }).ToList();

                        for (int j = 0; j < distinctsubMenus.Count; j++)
                        {
                            DataRow[] rows = ds.Tables[1].Select("ID='" + distinctsubMenus[j].id + "'");
                            if (rows.Length > 0)

                            {
                                if (distinctsubMenus[j].name != "")
                                {
                                    int idval = distinctsubMenus[j].id;
                                    CheckBox chk1 = new System.Web.UI.WebControls.CheckBox();
                                    if (distinctMenus[i].name.Contains(' '))
                                    {
                                        var name = distinctMenus[i].name.Replace(" ", "");
                                        chk1.ID = name + "_submenuclass_" + idval;
                                        chk1.CssClass = name + "submenuclass";
                                        chk1.Text = distinctsubMenus[j].name;
                                        chk1.Checked = true;
                                    }
                                    else
                                    {
                                        chk1.ID = distinctMenus[i].name + "_submenuclass_" + idval;
                                        chk1.Text = distinctsubMenus[j].name;
                                        chk1.CssClass = distinctMenus[i].name + "submenuclass";
                                        chk1.Checked = true;
                                    }

                                    div.Controls.Add(chk1);
                                    div.Controls.Add(new LiteralControl("<br />"));
                                }


                            }
                            else
                            {
                                if (distinctsubMenus[j].name != "")
                                {
                                    int idval = distinctsubMenus[j].id;
                                    CheckBox chk1 = new System.Web.UI.WebControls.CheckBox();
                                    if (distinctMenus[i].name.Contains(' '))
                                    {
                                        var name = distinctMenus[i].name.Replace(" ", "");
                                        chk1.ID = name + "_submenuclass_" + idval;
                                        chk1.CssClass = name + "submenuclass";
                                        chk1.Text = distinctsubMenus[j].name;
                                        chk1.Checked = false;
                                    }
                                    else
                                    {
                                        chk1.ID = distinctMenus[i].name + "_submenuclass_" + idval;
                                        chk1.Text = distinctsubMenus[j].name;
                                        chk1.CssClass = distinctMenus[i].name + "submenuclass";
                                        chk1.Checked = false;
                                    }
                                    div.Controls.Add(chk1);
                                    div.Controls.Add(new LiteralControl("<br />"));
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < distinctsubMenus.Count; j++)
                        {

                            if (distinctsubMenus[j].name != "")
                            {
                                int idval = distinctsubMenus[j].id;
                                CheckBox chk1 = new System.Web.UI.WebControls.CheckBox();
                                if (distinctMenus[i].name.Contains(' '))
                                {
                                    var name = distinctMenus[i].name.Replace(" ", "");
                                    chk1.ID = name + "_submenuclass_" + idval;
                                    chk1.CssClass = name + "submenuclass";
                                    chk1.Text = distinctsubMenus[j].name;
                                    chk1.Checked = false;
                                }
                                else
                                {
                                    chk1.ID = distinctMenus[i].name + "_submenuclass_" + idval;
                                    chk1.Text = distinctsubMenus[j].name;
                                    chk1.CssClass = distinctMenus[i].name + "submenuclass";
                                    chk1.Checked = false;
                                }
                                div.Controls.Add(chk1);
                                div.Controls.Add(new LiteralControl("<br />"));
                            }

                        }
                    }
                }
                if (Convert.ToBoolean(drselect["Status"]))
                    chkStatus.Checked = true;
                else
                    chkStatus.Checked = false;
                if (!string.IsNullOrEmpty(Convert.ToString(drselect["Role"])))
                {
                    if (Convert.ToString(drselect["Role"]) == "Admin")
                        chkRole.Checked = true;
                    else
                        chkRole.Checked = false;
                }
                else
                    chkRole.Checked = false;


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(string dist_number)
        {
            DataTable dtGrid = (DataTable)Session["dtDistributors"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (string)dr["Distributor_number"] == dist_number
                                select dr).FirstOrDefault();

            return drselect;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                objUserBO = new UserDetailsBO();
                objUserBL = new UserDetailsBL();
                objUserBO.userid = Convert.ToString(lblDistributorNumber.Text);
                objUserBO.emailid = Convert.ToString(txtEmailId.Text);
                objUserBO.password = Encrypt(Convert.ToString(txtpwd.Text));
                DataTable dt = new DataTable();
                dt = Session["menulist"] as DataTable;
                var menuids = "";


                objUserBO.role = Convert.ToBoolean(chkRole.Checked) ? "Admin" : "";
                objUserBO.status = Convert.ToBoolean(chkStatus.Checked) ? 1 : 0;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "getMenuDetails();", true);
                objUserBO = objUserBL.saveUserDetailsBL(objUserBO);
                if (!string.IsNullOrEmpty(objUserBO.err_msg))
                {

                    DataTable dtDistributors = objUserBL.getUserDetailsBL();
                    Session["dtDistributors"] = dtDistributors;
                    if (dtDistributors.Rows.Count > 0)
                    {
                        grdUserDetails.DataSource = dtDistributors;
                        grdUserDetails.DataBind();
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objUserBO.err_msg + "');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CallMyFunction", "bindGridView();", true);

                }
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error in saving.Please try again later.');", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string SaveMenuDetails(string menuids, string lblDistributorNumber, string txtEmailId, string txtpwd, string chkRole, int chkStatus)
        {

            string menu = "";
            string res = "";
            UserDetailsBO objUserBO = new UserDetailsBO();
            UserDetailsBL objUserBL = new UserDetailsBL();
            objUserBO.userid = lblDistributorNumber;
            objUserBO.emailid = txtEmailId;
            objUserBO.password = Encrypt(txtpwd);
            objUserBO.role = chkRole;
            objUserBO.status = chkStatus;
            //string role = HttpContext.Current.Session["RoleId"].ToString();
            var Json3 = JsonConvert.DeserializeObject<List<object>>(menuids);
            string[] menuid = Json3.Select(x => x.ToString()).ToArray();
            for (int i = 0; i < menuid.Length; i++)
            {
                menu += menuid[i] + ',';
            }
            if (menu.Length > 0)
            {
                menu = menu.Remove(menu.Length - 1);
            }
            objUserBO.menuid = menu;
            objUserBO = objUserBL.saveUserDetailsBL(objUserBO);
            if (!string.IsNullOrEmpty(objUserBO.err_msg))
            {

                return res = objUserBO.err_msg;

            }
            else

                return res = "There is some error in saving.Please try again later.";
        }

        protected void grdUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string user_id = Convert.ToString(e.CommandArgument);
                Session["ClickedDistributor_number"] = user_id;
                DataRow drselect = selectedRow(user_id);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private static string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                //objCom.ErrorLog(ex);
            }
            return clearText;
        }

        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return cipherText;
        }
    }
}