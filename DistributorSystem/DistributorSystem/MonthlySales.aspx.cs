﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using System.Globalization;

namespace DistributorSystem
{
    public partial class MonthlySales : System.Web.UI.Page
    {
        #region Global Declaration
        EntryBO objEntryBO;
        EntryBL objEntryBL;
        CommonFunctions objCom = new CommonFunctions();
        int currentMonth = DateTime.Now.Month;
        public static int gridLoadedStatus;
        DataTable dtConfig = new DataTable();
        #endregion
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    gridLoadedStatus = 0;
                    if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Btn_Thousand_Click(null, null); }
                    else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Btn_Lakhs_Click(null, null); }
                    else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Btn_Units_Click(null, null); }
                    else
                    {
                        Btn_Units_Click(null, null);
                    }
                    BindData();
                    dtConfig = objCom.getConfiguration();
                    int day = 31;
                    if (dtConfig != null)
                    {
                        foreach (DataRow dr in dtConfig.Rows)
                        {
                            if (Convert.ToString(dr["MODULE"]) == "MONTHLY SALES UPLOAD")
                            {
                                day = Convert.ToInt32(dr["VALUE"]);
                                break;
                            }
                        }
                    }
                    if (Convert.ToInt32(DateTime.Now.Day) <= day || Convert.ToString(Session["UserRole"]) == "Admin")
                    {
                        Panel1.Visible = true;
                    }
                    else
                        Panel1.Visible = false;
                    
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Btn_Units_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = true;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = true;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                BindData();
            }
        }

        private void BindData()
        {
            DataTable dtBind = new DataTable();
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                gridLoadedStatus = 1;

                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { objEntryBO.value = 1000; }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { objEntryBO.value = 100000; }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { objEntryBO.value = 1; }
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                dtBind = objEntryBL.GetCustomerMonthlySalesBL(objEntryBO);

                Session["MonthlySales"] = dtBind;

                if (dtBind.Rows.Count > 0)
                {
                    Session["month1"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month1"])) || Convert.ToString(dtBind.Rows[0]["month1"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month1"]);
                    Session["month2"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month2"])) || Convert.ToString(dtBind.Rows[0]["month2"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month2"]);
                    Session["month3"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month3"])) || Convert.ToString(dtBind.Rows[0]["month3"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month3"]);
                    Session["month4"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month4"])) || Convert.ToString(dtBind.Rows[0]["month4"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month4"]);
                    Session["month5"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month5"])) || Convert.ToString(dtBind.Rows[0]["month5"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month5"]);
                    Session["month6"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month6"])) || Convert.ToString(dtBind.Rows[0]["month6"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month6"]);
                    Session["month7"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month7"])) || Convert.ToString(dtBind.Rows[0]["month7"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month7"]);
                    Session["month8"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month8"])) || Convert.ToString(dtBind.Rows[0]["month8"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month8"]);
                    Session["month9"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month9"])) || Convert.ToString(dtBind.Rows[0]["month9"]) == "0.00" ? "0" : Convert.ToString(dtBind.Rows[0]["month9"]);
                    Session["month10"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month10"])) ? "0" : Convert.ToString(dtBind.Rows[0]["month10"]);
                    Session["month11"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month11"])) ? "0" : Convert.ToString(dtBind.Rows[0]["month11"]);
                    Session["month12"] = String.IsNullOrEmpty(Convert.ToString(dtBind.Rows[0]["month12"])) ? "0" : Convert.ToString(dtBind.Rows[0]["month12"]);
                    DataRow dr = dtBind.Rows[0];
                    if (Convert.ToString(dr["customername"]) == "TOTAL")
                        dr.Delete();
                    grdMonthlySales.DataSource = dtBind;
                    grdMonthlySales.DataBind();
                    //lblResult.Text = "All values are in thousands.";
                    //lblmessage.Text = "Please divide the actual sales value by 1000 and then save. System will accept value in 999.99 format.";
                }
                else
                {
                    grdMonthlySales.DataSource = null;
                    grdMonthlySales.DataBind();
                    lblResult.Text = "No records found.";
                    lblmessage.Text = "";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet dsOutput;
            int Valuein;
            try
            {
                dsOutput = new DataSet();
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                DataTable Distributors = new DataTable();
                Distributors.Columns.Add("Customer_number");
                Distributors.Columns.Add("Customer_name");
                Distributors.Columns.Add("year");
                Distributors.Columns.Add("month");
                Distributors.Columns.Add("amount");
                //Distributors.Columns.Add("PreviousMonthAmount");
                Distributors.Columns.Add("Distributor");
                if (grdMonthlySales.Rows.Count != 0)
                {
                    DataRow dr;
                    Label custname;
                    Label custnumber;
                    TextBox txtAmount;
                    //TextBox txtmonth2;
                    string id;
                    foreach (GridViewRow row in grdMonthlySales.Rows)
                    {
                        custname = (row.FindControl("lblCustomerName") as Label);
                        custnumber = (row.FindControl("lblCustomerNumber") as Label);
                        txtAmount = (row.FindControl("txtAmount") as TextBox);
                        //txtmonth2 = (row.FindControl("txtmonth2") as TextBox);
                        dr = Distributors.NewRow();
                        dr["Customer_number"] = custnumber.Text;
                        dr["Customer_name"] = custname.Text;
                        dr["amount"] = Convert.ToString(txtAmount.Text);
                        if (Convert.ToInt32(DateTime.Now.Month) == 1)
                        {
                            dr["year"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1);
                            dr["month"] = "12";
                        }
                        else
                        {
                            dr["year"] = Convert.ToString(DateTime.Now.Year);
                            dr["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                        }
                        dr["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        //dr["PreviousMonthAmount"] = Convert.ToString(txtmonth2.Text);
                        Distributors.Rows.Add(dr);
                    }
                    if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Valuein = 1000; }
                    else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Valuein = 100000; }
                    else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Valuein = 1; }
                    else
                    {
                        Valuein = 1;
                    }
                    dsOutput = objEntryBL.SaveBulkMonthlySalesBL(Distributors, Valuein);
                    if (dsOutput.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(dsOutput.Tables[0].Rows[0]["Error_Code"]) == 0 && Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) == "Inserted Successfully")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) + "');", true);

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is some error in saving. Please retry.');", true);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                BindData();
            }
        }

        protected void grdMonthlySales_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //e.Row.Cells[2].Text = DateTime.Now.AddMonths(0).ToString("Y");
                    e.Row.Cells[2].Text = DateTime.Now.AddMonths(-1).ToString("Y");
                    e.Row.Cells[3].Text = DateTime.Now.AddMonths(-2).ToString("Y");
                    e.Row.Cells[4].Text = DateTime.Now.AddMonths(-3).ToString("Y");
                    e.Row.Cells[5].Text = DateTime.Now.AddMonths(-4).ToString("Y");
                    e.Row.Cells[6].Text = DateTime.Now.AddMonths(-5).ToString("Y");
                    e.Row.Cells[7].Text = DateTime.Now.AddMonths(-6).ToString("Y");
                    e.Row.Cells[8].Text = DateTime.Now.AddMonths(-7).ToString("Y");
                    e.Row.Cells[9].Text = DateTime.Now.AddMonths(-8).ToString("Y");
                    e.Row.Cells[10].Text = DateTime.Now.AddMonths(-9).ToString("Y");
                    e.Row.Cells[11].Text = DateTime.Now.AddMonths(-10).ToString("Y");
                    e.Row.Cells[12].Text = DateTime.Now.AddMonths(-11).ToString("Y");
                    e.Row.Cells[13].Text = DateTime.Now.AddMonths(-12).ToString("Y");
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (updFile.PostedFile != null && updFile.PostedFile.ContentLength > 0)
            {

                string fileName = Path.GetFileName(updFile.PostedFile.FileName);
                if (fileName.EndsWith(".xlsx"))
                    fileName = Convert.ToString(Session["DistributorNumber"]) + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                else if (fileName.EndsWith(".xls"))
                    fileName = Convert.ToString(Session["DistributorNumber"]) + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".xls";
                string folder = ConfigurationManager.AppSettings["MonthlySalesFolder"].ToString() + Convert.ToString(Session["DistributorNumber"])+"/";
                //string folder = Server.MapPath("~/Reports/");
                Directory.CreateDirectory(folder);
                string CurrentFilePath = Path.Combine(folder, fileName);
                if (File.Exists(CurrentFilePath))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(CurrentFilePath);
                }
                updFile.PostedFile.SaveAs(Path.Combine(folder, fileName));
                try
                {
                    InsertExcelRecords(CurrentFilePath);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        private void InsertExcelRecords(string CurrentFilePath)
        {
            int correct_value, wrong_value;
            string amount1 = string.Empty;
            string amount = string.Empty;
            int intvalue;
            float floatvalue;
            double doublevalue;
            int column;
            string col_name = string.Empty;
            string date_part = string.Empty;
            int month = 0;
            int year = 0;
            DataSet dsOutput;
            DataRow drValid;
            DataRow drError;
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                DataTable Distributors = new DataTable();
                Distributors.Columns.Add("Customer_number");
                Distributors.Columns.Add("Customer_name");
                Distributors.Columns.Add("year");
                Distributors.Columns.Add("month");
                Distributors.Columns.Add("amount");
                Distributors.Columns.Add("Distributor");

                DataTable ErrorDistributors = new DataTable();
                ErrorDistributors.Columns.Add("Customer_number");
                ErrorDistributors.Columns.Add("Customer_name");
                ErrorDistributors.Columns.Add("year");
                ErrorDistributors.Columns.Add("month");
                ErrorDistributors.Columns.Add("amount");
                ErrorDistributors.Columns.Add("Distributor");

                DataTable dtOutput = new DataTable();
                OleDbConnection Econ;
                string constr = string.Empty;
                if (CurrentFilePath.EndsWith(".xlsx"))
                {
                    constr = string.Format(Convert.ToString(ConfigurationManager.AppSettings["xlsx"]), CurrentFilePath);
                }
                else if (CurrentFilePath.EndsWith(".xls"))
                {
                    constr = string.Format(Convert.ToString(ConfigurationManager.AppSettings["xlsx"]), CurrentFilePath);
                }
                Econ = new OleDbConnection(constr);
                string Query = string.Empty;
                //if (Convert.ToString(Session["UserRole"]) == "Admin")
                //{
                Query = string.Format("Select * FROM [{0}]", "MonthlySales$");
                //}
                //else
                //{
                //    Query = string.Format("Select [Customer_number],[Customer_name],[Sales_" + DateTime.Now.AddMonths(-1).ToString("Y") + "] FROM [{0}]", "MonthlySales$");
                //}
                OleDbCommand Ecom = new OleDbCommand(Query, Econ);
                Econ.Open();

                DataSet ds = new DataSet();
                OleDbDataAdapter oda = new OleDbDataAdapter(Query, Econ);
                oda.FillSchema(ds, SchemaType.Source);

                foreach (DataColumn cl in ds.Tables[0].Columns)
                {
                    if (cl.DataType == typeof(double))
                        cl.DataType = typeof(string);
                }
                oda.Fill(ds);
                Econ.Close();
                DataTable Exceldt = ds.Tables[0];

                //if (Convert.ToString(Session["UserRole"]) == "Admin" )
                //{
                #region converting to table
                column = Exceldt.Columns.Count;
                for (int row = 0; row < Exceldt.Rows.Count; row++)
                {
                    for (int col = 2; col < column; col++)
                    {
                        col_name = Convert.ToString(Exceldt.Columns[col].ColumnName);
                        if (!string.IsNullOrEmpty(col_name))
                        {
                            if (col_name.Contains(','))
                            {
                                date_part = col_name.Substring(6, col_name.Length - 12);
                            }
                            else
                            {
                                date_part = col_name.Substring(6, col_name.Length - 11);
                            }
                            month = GetMonthNum(date_part);
                            year = Convert.ToInt32(col_name.Substring(col_name.Length - 4, 4));

                            amount1 = Convert.ToString(Exceldt.Rows[row][col]);
                            amount = Convert.ToString(Exceldt.Rows[row][col]);

                            if (int.TryParse(amount, out intvalue) || double.TryParse(amount, out doublevalue) || float.TryParse(amount, out floatvalue))
                            {
                                amount = string.Format("{0:0.00}", amount);
                                if (Convert.ToDouble(amount) >= 0.00 && Convert.ToDouble(amount) < 100000000000.00)
                                {
                                    drValid = Distributors.NewRow();
                                    drValid["Customer_number"] = Convert.ToString(Exceldt.Rows[row]["Customer_number"]);
                                    drValid["Customer_name"] = Convert.ToString(Exceldt.Rows[row]["Customer_name"]);
                                    drValid["year"] = year;
                                    drValid["month"] = month;
                                    drValid["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                                    drValid["amount"] = Convert.ToString(Exceldt.Rows[row][col]);
                                    Distributors.Rows.Add(drValid);
                                }
                                else
                                {
                                    drError = ErrorDistributors.NewRow();
                                    drError["Customer_number"] = Convert.ToString(Exceldt.Rows[row]["Customer_number"]);
                                    drError["Customer_name"] = Convert.ToString(Exceldt.Rows[row]["Customer_name"]);
                                    drError["year"] = year;
                                    drError["month"] = month;
                                    drError["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                                    drError["amount"] = amount1;
                                    ErrorDistributors.Rows.Add(drError);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(amount))
                                {
                                    drError = ErrorDistributors.NewRow();
                                    drError["Customer_number"] = Convert.ToString(Exceldt.Rows[row]["Customer_number"]);
                                    drError["Customer_name"] = Convert.ToString(Exceldt.Rows[row]["Customer_name"]);
                                    drError["year"] = year;
                                    drError["month"] = month;
                                    drError["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                                    drError["amount"] = amount1;
                                    ErrorDistributors.Rows.Add(drError);
                                }
                            }
                        }

                        #region commented code

                        //amount1 = Convert.ToString(dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")]);
                        //amount = Convert.ToString(dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")]);

                        //if (int.TryParse(amount, out intvalue) || double.TryParse(amount, out doublevalue) || float.TryParse(amount, out floatvalue))
                        //{
                        //    amount = string.Format("{0:0.00}", amount);
                        //    if (Convert.ToDouble(amount) >= 0.00 && Convert.ToDouble(amount) < 1000.00)
                        //    {
                        //        dr1 = Distributors.NewRow();
                        //        dr1["Customer_number"] = dr["Customer_number"];
                        //        dr1["Customer_name"] = dr["Customer_name"];
                        //        //dr1["amount"] = dr["Sales_" + DateTime.Now.AddMonths(0).ToString("Y")];
                        //        dr1["year"] = Convert.ToString(DateTime.Now.Year);
                        //        dr1["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                        //        dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        //        dr1["amount"] = dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")];
                        //        Distributors.Rows.Add(dr1);
                        //    }
                        //    else
                        //    {
                        //        dr1 = ErrorDistributors.NewRow();
                        //        dr1["Customer_number"] = dr["Customer_number"];
                        //        dr1["Customer_name"] = dr["Customer_name"];
                        //        //dr1["amount"] = dr["Sales_" + DateTime.Now.AddMonths(0).ToString("Y")];
                        //        dr1["year"] = Convert.ToString(DateTime.Now.Year);
                        //        dr1["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                        //        dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        //        dr1["amount"] = amount1;
                        //        ErrorDistributors.Rows.Add(dr1);
                        //    }
                        //}
                        //else
                        //{
                        //    if (!string.IsNullOrEmpty(amount))
                        //    {
                        //        dr1 = ErrorDistributors.NewRow();
                        //        dr1["Customer_number"] = dr["Customer_number"];
                        //        dr1["Customer_name"] = dr["Customer_name"];
                        //        //dr1["amount"] = dr["Sales_" + DateTime.Now.AddMonths(0).ToString("Y")];
                        //        dr1["year"] = Convert.ToString(DateTime.Now.Year);
                        //        dr1["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                        //        dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        //        dr1["amount"] = amount1;
                        //        //dr1["Error"] = "Please check the value format.";
                        //        ErrorDistributors.Rows.Add(dr1);
                        //    }
                        //    else
                        //    {

                        //    }
                        //}



                        #endregion
                    }

                }
                #endregion
                //}
                //else
                //{
                //    foreach (DataRow dr in Exceldt.Rows)
                //    {
                //        #region last month
                //        amount1 = Convert.ToString(dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")]);
                //        amount = Convert.ToString(dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")]);

                //        if (int.TryParse(amount, out intvalue) || double.TryParse(amount, out doublevalue) || float.TryParse(amount, out floatvalue))
                //        {
                //            amount = string.Format("{0:0.00}", amount);
                //            if (Convert.ToDouble(amount) >= 0.00 && Convert.ToDouble(amount) < 100000000000.00)
                //            {
                //                drValid = Distributors.NewRow();
                //                drValid["Customer_number"] = dr["Customer_number"];
                //                drValid["Customer_name"] = dr["Customer_name"];
                //                drValid["year"] = Convert.ToString(DateTime.Now.Year);
                //                drValid["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                //                drValid["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                //                drValid["amount"] = dr["Sales_" + DateTime.Now.AddMonths(-1).ToString("Y")];
                //                Distributors.Rows.Add(drValid);
                //            }
                //            else
                //            {
                //                drError = ErrorDistributors.NewRow();
                //                drError["Customer_number"] = dr["Customer_number"];
                //                drError["Customer_name"] = dr["Customer_name"];
                //                drError["year"] = Convert.ToString(DateTime.Now.Year);
                //                drError["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                //                drError["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                //                drError["amount"] = amount1;
                //                ErrorDistributors.Rows.Add(drError);
                //            }
                //        }
                //        else
                //        {
                //            if (!string.IsNullOrEmpty(amount))
                //            {
                //                drError = ErrorDistributors.NewRow();
                //                drError["Customer_number"] = dr["Customer_number"];
                //                drError["Customer_name"] = dr["Customer_name"];
                //                drError["year"] = Convert.ToString(DateTime.Now.Year);
                //                drError["month"] = Convert.ToString(Convert.ToInt32(DateTime.Now.Month) - 1);
                //                drError["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                //                drError["amount"] = amount1;
                //                ErrorDistributors.Rows.Add(drError);
                //            }
                //            else
                //            {

                //            }
                //        }


                //        #endregion
                //    }
                //}
                Session["ErrorDistributorsMonthly"] = ErrorDistributors;

                //correct_value = Distributors.Rows.Count;
                wrong_value = ErrorDistributors.Rows.Count;
                correct_value = Exceldt.Rows.Count - wrong_value;
                int wrong_customer = 0;
                dsOutput = new DataSet();

                dsOutput = objEntryBL.SaveBulkMonthlySalesBL(Distributors, 1);
                if (dsOutput != null)
                {
                    if (dsOutput.Tables.Count > 0)
                    {
                        if (dsOutput.Tables[0] != null)
                        {
                            if (dsOutput.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(dsOutput.Tables[0].Rows[0]["Error_Code"]) == 0 && Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) == "Inserted Successfully")
                                {
                                    if (dsOutput.Tables[1] != null)
                                    {
                                        if (dsOutput.Tables[1].Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dsOutput.Tables[1].Rows)
                                            {
                                                drError = ErrorDistributors.NewRow();
                                                drError["Customer_number"] = dr["Customer_number"];
                                                drError["Customer_name"] = dr["Customer_name"];
                                                drError["amount"] = dr["amount"];
                                                drError["year"] = dr["year"];
                                                drError["Distributor"] = dr["Distributor"];
                                                //dr1["Error"] = "Customer is not available in system.";
                                                ErrorDistributors.Rows.Add(drError);
                                            }
                                        }
                                    }

                                    BindData();
                                    wrong_customer = ErrorDistributors.Rows.Count - wrong_value;
                                    if (ErrorDistributors.Rows.Count > 0)
                                    {
                                        string errormessage = string.Empty;
                                        if (correct_value > 0 && correct_value > dsOutput.Tables[1].Rows.Count)
                                        {
                                            errormessage = "* Budget for " + correct_value + " customers are updated successfully.<br/>";
                                        }
                                        if (wrong_value > 0)
                                        {
                                            errormessage += "* For " + wrong_value + " customers, values for some months are in not correct format, so values for those months are not updated.<br/>";
                                        }
                                        if (wrong_customer > 0)
                                        {
                                            errormessage += "* Excel file has " + wrong_customer + " invalid customers, so values for invalid customers are not uploaded.<br/>";
                                        }
                                        if (wrong_value > 0 || wrong_customer > 0)
                                        {
                                            errormessage += "Please check the excel.";
                                        }
                                        lblmessage.Text = errormessage;
                                        // lblmessage.Text = "Budget for " + correct_value + " customers are updated successfully. For " + wrong_value + " customers, values are in not correct format. Please check the excel.";
                                        lblExtError.Visible = true;
                                    }
                                    else
                                    {
                                        lblmessage.Text = "* Budget for all customers are updated successfully.";
                                        lblExtError.Visible = false; //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Budget for all customers are updated successfully. ');", true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                BindData();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please check all the columns added in excel sheet and try again.');", true);
            }
        }

        private int GetMonthNum(string date_part)
        {
            int month = 0;
            string month_str = DateTime.Now.AddMonths(-1).ToString("MMMMM");
            switch (date_part)
            {
                case "January":
                    month = 1;
                    break;
                case "February":
                    month = 2;
                    break;
                case "March":
                    month = 3;
                    break;
                case "April":
                    month = 4;
                    break;
                case "May":
                    month = 5;
                    break;
                case "June":
                    month = 6;
                    break;
                case "July":
                    month = 7;
                    break;
                case "August":
                    month = 8;
                    break;
                case "September":
                    month = 9;
                    break;
                case "October":
                    month = 10;
                    break;
                case "November":
                    month = 11;
                    break;
                case "December":
                    month = 12;
                    break;
            }
            return month;
        }

        protected void btntemplate_Click(object sender, EventArgs e)
        {
            DataTable dtOutput = new DataTable();
            try
            {
                //if (Convert.ToString(Session["UserRole"]) == "Admin")
                //{
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                dtOutput = objEntryBL.GetCustomerMonthlySalesForAdminBL(objEntryBO);
                dtOutput.Columns[0].ColumnName = "Customer_number";
                dtOutput.Columns[1].ColumnName = "Customer_name";
                for (int i = 2; i < dtOutput.Columns.Count; i++)
                {
                    dtOutput.Columns[i].ColumnName = "Sales_" + DateTime.Now.AddMonths(-(i - 1)).ToString("Y");
                }
                //}
                //else
                //{
                //    dtOutput = (DataTable)Session["MonthlySales"];
                //    dtOutput.Columns[0].ColumnName = "Customer_number";
                //    dtOutput.Columns[1].ColumnName = "Customer_name";
                //    dtOutput.Columns[2].ColumnName = "Sales_" + DateTime.Now.AddMonths(-1).ToString("Y");
                //    dtOutput.Columns.RemoveAt(13);
                //    dtOutput.Columns.RemoveAt(12);
                //    dtOutput.Columns.RemoveAt(11);
                //    dtOutput.Columns.RemoveAt(10);
                //    dtOutput.Columns.RemoveAt(9);
                //    dtOutput.Columns.RemoveAt(8);
                //    dtOutput.Columns.RemoveAt(7);
                //    dtOutput.Columns.RemoveAt(6);
                //    dtOutput.Columns.RemoveAt(5);
                //    dtOutput.Columns.RemoveAt(4);
                //    dtOutput.Columns.RemoveAt(3);
                //}
                ExporttoExcel(dtOutput, "MonthlySales");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void ExporttoExcel(DataTable table, string filename)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            //HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            var t = table.AsEnumerable().Where(dataRow => dataRow.RowState != DataRowState.Deleted);
            table = t.CopyToDataTable();
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void lnkErrorExcel_Click(object sender, EventArgs e)
        {
            DataTable ErrorDistributors;
            try
            {
                ErrorDistributors = new DataTable();
                ErrorDistributors = (DataTable)Session["ErrorDistributorsMonthly"];

                ExporttoExcel(ErrorDistributors, "MonthlyBudgetErrorFile");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {

                TextBox txt = sender as TextBox;
                GridViewRow currentRow = txt.NamingContainer as GridViewRow;
                Label customernumber = (Label)currentRow.FindControl("lblCustomerNumber");

                DataTable dtOutput = (DataTable)Session["MonthlySales"];
                DataRow dr = (from myRow in dtOutput.AsEnumerable()
                              where myRow.Field<int>("customernumber") == Convert.ToInt32(customernumber.Text)
                              select myRow).SingleOrDefault();
                dr["month1"] = txt.Text;

                grdMonthlySales.DataSource = dtOutput;
                grdMonthlySales.DataBind();
                panelvalues.Visible = true;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

    }
}