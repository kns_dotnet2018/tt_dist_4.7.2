﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace DistributorSystem
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        #region GlobalDeclaration
        LoginBO objLoginBO;
        LoginBL objLoginBL;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                
                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["DistributorNumber"]);
                }
                if (!IsPostBack)
                {
                    txtConfirm.Text = "";
                    txtNew.Text = "";
                    txtOld.Text = "";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                objLoginBO = new LoginBO();
                objLoginBL = new LoginBL();
                objLoginBO.Username = Convert.ToString(Session["DistributorNumber"]);
                objLoginBO.Password = Encrypt(Convert.ToString(txtNew.Text));
                Session["LoggedInPassword"] = Convert.ToString(txtNew.Text);
                objLoginBO = objLoginBL.ChangePasswordBL(objLoginBO);
                if (objLoginBO.ErrorNum == 0)
                {
                    
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been updated successfully.');", true);
                    txtConfirm.Text = "";
                    txtNew.Text = "";
                    txtOld.Text = "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is some error in updating password. Please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        private string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return clearText;
        }
    }
}