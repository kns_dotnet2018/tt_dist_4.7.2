﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Xml;
using System.Text;
using System.Configuration;

namespace DistributorSystem
{
    public partial class BudgetEntry : System.Web.UI.Page
    {
        #region Global Declaration
        EntryBO objEntryBO;
        EntryBL objEntryBL;
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                
                if (!IsPostBack)
                {
                    gridLoadedStatus = 0;
                    if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Btn_Thousand_Click(null, null); }
                    else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Btn_Lakhs_Click(null, null); }
                    else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Btn_Units_Click(null, null); }
                    else
                    {
                        Btn_Units_Click(null, null);
                    }
                    ddlYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                    ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1)));
                
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnEntry_Click(object sender, EventArgs e)
        {
            DataTable dtOutput = new DataTable();
            try
            {
                dtOutput=GetEntryData();
                Session["dtOutput"] = dtOutput;
                if (dtOutput.Rows.Count > 0)
                {
                    Session["YearlyAmount"] = String.IsNullOrEmpty(Convert.ToString(dtOutput.Rows[0]["YearlyAmount"])) ? "0" : Convert.ToString(dtOutput.Rows[0]["YearlyAmount"]);
                    Session["last1yearAmount"] = String.IsNullOrEmpty(Convert.ToString(dtOutput.Rows[0]["last1yearAmount"])) ? "0" : Convert.ToString(dtOutput.Rows[0]["last1yearAmount"]);
                    Session["last2yearAmount"] = String.IsNullOrEmpty(Convert.ToString(dtOutput.Rows[0]["last2yearAmount"])) ? "0" : Convert.ToString(dtOutput.Rows[0]["last2yearAmount"]);
                    DataRow dr = dtOutput.Rows[0];
                    if (Convert.ToString(dr["customername"]) == "TOTAL")
                        dr.Delete();
                    dtOutput.AcceptChanges();
                    grdEntry.DataSource = dtOutput;
                    grdEntry.DataBind();
                    panelvalues.Visible = true;
                    //lblResult.Text = "All values are in lakhs.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "BindGridView();", true);
                }
                else
                {
                    grdEntry.DataSource = null;
                    grdEntry.DataBind();
                    lblResult.Text = "No records found.";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataTable GetEntryData()
        {
            DataTable dtCustomer = new DataTable();
            DataTable dtCustomerValue = new DataTable();
            DataTable dtOutput = new DataTable();
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                gridLoadedStatus = 1;

                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { objEntryBO.value = 1000; }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { objEntryBO.value = 100000; }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { objEntryBO.value = 1; }
                objEntryBO.distributor_name = Convert.ToString(Session["DistributorNumber"]);
                objEntryBO.year = Convert.ToInt32(ddlYear.SelectedValue);
                dtCustomer = objEntryBL.GetCustomersBL(objEntryBO);
                dtCustomerValue = objEntryBL.GetAnualTargetBL(objEntryBO);
                if (dtCustomerValue != null && dtCustomerValue.Rows.Count > 0)
                {
                    dtOutput = dtCustomerValue.Copy();
                    var rowsOnlyInDt1 = dtCustomer.AsEnumerable().Where(r => !dtCustomerValue.AsEnumerable()
                    .Any(r2 => Convert.ToString(r["customernumber"]).Trim().ToLower() == Convert.ToString(r2["customernumber"]).Trim().ToLower() && Convert.ToString(r["customername"]).Trim().ToLower() == Convert.ToString(r2["customername"]).Trim().ToLower()));

                    if (rowsOnlyInDt1.Any())
                        dtOutput.Merge(rowsOnlyInDt1.CopyToDataTable());
                }
                else
                {
                    dtOutput = dtCustomer.Copy();
                    if (!dtOutput.Columns.Contains("YearlyAmount"))
                        dtOutput.Columns.Add("YearlyAmount");
                    if (!dtOutput.Columns.Contains("last1yearAmount"))
                        dtOutput.Columns.Add("last1yearAmount");
                    if (!dtOutput.Columns.Contains("last2yearAmount"))
                        dtOutput.Columns.Add("last2yearAmount");
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataSet dsOutput;
            int valuein;
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                dsOutput = new DataSet();
                DataTable Distributors = new DataTable();
                Distributors.Columns.Add("Customer_number");
                Distributors.Columns.Add("Customer_name");
                Distributors.Columns.Add("year");
                Distributors.Columns.Add("amount");
                Distributors.Columns.Add("Distributor");
                if (grdEntry.Rows.Count != 0)
                {
                    DataRow dr;
                    Label custname;
                    Label custnumber;
                    TextBox txtAmount;
                    string id;
                    foreach (GridViewRow row in grdEntry.Rows)
                    {
                        custname = (row.FindControl("lblCustomerName") as Label);
                        custnumber = (row.FindControl("lblCustomerNumber") as Label);
                        txtAmount = (row.FindControl("txtAmount") as TextBox);
                        dr = Distributors.NewRow();
                        dr["Customer_number"] = custnumber.Text;
                        dr["Customer_name"] = custname.Text;
                        dr["amount"] = Convert.ToString(txtAmount.Text);
                        dr["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                        dr["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        Distributors.Rows.Add(dr);
                    }
                    if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { valuein=1000; }
                    else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { valuein=100000; }
                    else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { valuein=1; }
                    else
                    {
                        valuein = 1;
                    }
                    dsOutput = objEntryBL.SaveBulkBudgetBL(Distributors,valuein);
                    if (dsOutput.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(dsOutput.Tables[0].Rows[0]["Error_Code"]) == 0 && Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) == "Inserted Successfully")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) + "');", true);

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is some error in saving. Please retry.');", true);
                    }
                    btnEntry_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                btnEntry_Click(null, null);
            }
        }

        protected void lnkErrorExcel_Click(object sender, EventArgs e)
        {
            DataTable ErrorDistributors;
            try
            {
                ErrorDistributors = new DataTable();
                ErrorDistributors = (DataTable)Session["ErrorDistributors"];

                ExporttoExcel(ErrorDistributors, "AnnualBudgetErrorFile");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Btn_Units_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = true;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnEntry_Click(null, null);
            }
        }

        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = true;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnEntry_Click(null, null);
            }
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnEntry_Click(null, null);
            }
        }

        protected void grdEntry_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[2].Text = "Target Sales " + Convert.ToString(ddlYear.SelectedValue);
                    e.Row.Cells[3].Text = "Actual Sales " + Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 1);
                    e.Row.Cells[4].Text = "Actual Sales " + Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 2);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (updFile.PostedFile != null && updFile.PostedFile.ContentLength > 0)
            {

                string fileName = Path.GetFileName(updFile.PostedFile.FileName);
                if (fileName.EndsWith(".xlsx"))
                    fileName = Convert.ToString(Session["DistributorNumber"])+"_"+DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss")+".xlsx";
                else if(fileName.EndsWith(".xls"))
                    fileName = Convert.ToString(Session["DistributorNumber"]) + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".xls";
                //string folder = Server.MapPath("~/Reports/AnnualBudget");
                string folder = ConfigurationManager.AppSettings["AnnualBudgetFolder"].ToString() + Convert.ToString(Session["DistributorNumber"]) + "/";
                Directory.CreateDirectory(folder);
                string CurrentFilePath = Path.Combine(folder, fileName);
                if (File.Exists(CurrentFilePath))
                {
                    File.Delete(CurrentFilePath);
                }
                updFile.SaveAs(Path.Combine(folder, fileName));
                try
                {
                    InsertExcelRecords(CurrentFilePath);

                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        protected void btntemplate_Click(object sender, EventArgs e)
        {
            DataTable dtOutput = new DataTable();
            try
            {
                dtOutput = GetEntryData();
                DataRow dr = dtOutput.Rows[0];
                if (Convert.ToString(dr["customername"]) == "TOTAL")
                    dr.Delete();
                if (dtOutput.Columns.Contains("last1yearAmount"))
                    dtOutput.Columns.Remove("last1yearAmount");
                if (dtOutput.Columns.Contains("last2yearAmount"))
                    dtOutput.Columns.Remove("last2yearAmount");
                dtOutput.Columns[0].ColumnName = "Customer_number";
                dtOutput.Columns[1].ColumnName = "Customer_name";
                dtOutput.Columns[2].ColumnName = "Target_Sales_" + Convert.ToString(ddlYear.SelectedValue);
                //dtOutput.Columns[3].ColumnName = "Actual_Sales_" + Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 1);
                //dtOutput.Columns[4].ColumnName = "Actual_Sales_" + Convert.ToString(Convert.ToInt32(ddlYear.SelectedValue) - 2);
                ExporttoExcel(dtOutput, "AnnualTarget");
                // Create(dtOutput, "G:\\Projects\\Distributor System\\DistributorApp_25_09_2017\\DistributorSystem\\DistributorSystem\\Reports\\part_mass_upload.xlsx");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods

        private void InsertExcelRecords(string CurrentFilePath)
        {
            int correct_value, wrong_value;
            DataSet dsOutput;
            try
            {
                objEntryBO = new EntryBO();
                objEntryBL = new EntryBL();
                DataTable dtOutput = new DataTable();
                OleDbConnection Econ;
                string constr = string.Empty;
                if (CurrentFilePath.EndsWith(".xlsx"))
                {
                    constr = string.Format(Convert.ToString(ConfigurationManager.AppSettings["xlsx"]), CurrentFilePath);
                    //constr = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;""", CurrentFilePath);
                }
                else if (CurrentFilePath.EndsWith(".xls"))
                {
                    constr = string.Format(Convert.ToString(ConfigurationManager.AppSettings["xls"]), CurrentFilePath);
                    // constr = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=YES;IMEX=1;""", CurrentFilePath);
                }
                Econ = new OleDbConnection(constr);
                string Query = string.Format("Select [Customer_number],[Customer_name],[Target_Sales_" + Convert.ToString(ddlYear.SelectedValue) + "] FROM [{0}]", "AnnualTarget$");
                OleDbCommand Ecom = new OleDbCommand(Query, Econ);
                Econ.Open();

                DataRow dr1;
                DataSet ds = new DataSet();
                OleDbDataAdapter oda = new OleDbDataAdapter(Query, Econ);
                Econ.Close();
                oda.Fill(ds);
                DataTable Exceldt = ds.Tables[0];
                DataTable ErrorDistributors = new DataTable();
                ErrorDistributors.Columns.Add("Customer_number");
                ErrorDistributors.Columns.Add("Customer_name");
                ErrorDistributors.Columns.Add("year");
                ErrorDistributors.Columns.Add("amount");
                ErrorDistributors.Columns.Add("Distributor");
                ErrorDistributors.Columns.Add("Error");

                DataTable Distributors = new DataTable();
                Distributors.Columns.Add("Customer_number");
                Distributors.Columns.Add("Customer_name");
                Distributors.Columns.Add("year");
                Distributors.Columns.Add("amount");
                Distributors.Columns.Add("Distributor");

                foreach (DataRow dr in Exceldt.Rows)
                {
                    string amount1 = Convert.ToString(dr["Target_Sales_" + Convert.ToString(ddlYear.SelectedValue)]);
                    string amount = Convert.ToString(dr["Target_Sales_" + Convert.ToString(ddlYear.SelectedValue)]);
                    int intvalue;
                    float floatvalue;
                    double doublevalue;
                    if (int.TryParse(amount, out intvalue) || double.TryParse(amount, out doublevalue) || float.TryParse(amount, out floatvalue))
                    {
                        amount = string.Format("{0:0.00}", amount);
                        if (Convert.ToDouble(amount) >= 0.00 && Convert.ToDouble(amount) < 1000000000000.00)
                        {
                            dr1 = Distributors.NewRow();
                            dr1["Customer_number"] = dr["Customer_number"];
                            dr1["Customer_name"] = dr["Customer_name"];
                            dr1["amount"] = amount;
                            dr1["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                            dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                            Distributors.Rows.Add(dr1);
                        }
                        else
                        {
                            dr1 = ErrorDistributors.NewRow();
                            dr1["Customer_number"] = dr["Customer_number"];
                            dr1["Customer_name"] = dr["Customer_name"];
                            dr1["amount"] = amount1;
                            dr1["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                            dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                            ErrorDistributors.Rows.Add(dr1);
                        }
                    }
                    else
                    {
                        dr1 = ErrorDistributors.NewRow();
                        dr1["Customer_number"] = dr["Customer_number"];
                        dr1["Customer_name"] = dr["Customer_name"];
                        dr1["amount"] = amount1;
                        dr1["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                        dr1["Distributor"] = Convert.ToString(Session["DistributorNumber"]);
                        dr1["Error"] = "Please check the value format.";
                        ErrorDistributors.Rows.Add(dr1);
                    }
                }

                Session["ErrorDistributors"] = ErrorDistributors;
                correct_value = Distributors.Rows.Count;
                wrong_value = ErrorDistributors.Rows.Count;
                dsOutput = new DataSet();
                dsOutput = objEntryBL.SaveBulkBudgetBL(Distributors, 1);
                if (dsOutput.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(dsOutput.Tables[0].Rows[0]["Error_Code"]) == 0 && Convert.ToString(dsOutput.Tables[0].Rows[0]["Error_message"]) == "Inserted Successfully")
                    {
                        if (dsOutput.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsOutput.Tables[1].Rows)
                            {
                                dr1 = ErrorDistributors.NewRow();
                                dr1["Customer_number"] = dr["Customer_number"];
                                dr1["Customer_name"] = dr["Customer_name"];
                                dr1["amount"] = dr["amount"];
                                dr1["year"] = dr["year"];
                                dr1["Distributor"] = dr["Distributor"];
                                dr1["Error"] = "Customer is not available in system.";
                                ErrorDistributors.Rows.Add(dr1);
                            }
                        }
                        btnEntry_Click(null, null);
                        if (ErrorDistributors.Rows.Count > 0)
                        {
                            lblmessage.Text = "Budget for " + correct_value + " customers are updated successfully. For " + wrong_value + " customers, values are in not correct format. Please check the excel.";
                            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Budget for " + correct_value + " customers are updated successfully. For " + wrong_value + " customers, values are in not correct format. Please check the excel.');", true);
                            lblExtError.Visible = true;
                        }
                        else
                        {
                            lblmessage.Text = "Budget for all customers are updated successfully.";
                            lblExtError.Visible = false;
                            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Budget for all customers are updated successfully. ');", true);
                        }

                        //ExporttoExcel(ErrorDistributors, "AnnualBudgetErrorFile");
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objEntryBO.error_msg + "');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                btnEntry_Click(null, null);
                lblmessage.Text = "Please check the excel sheet name, all the columns added in excel sheet and try again.";
                lblExtError.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please check all the columns added in excel sheet and try again.');", true);
            }
        }

        private void ExporttoExcel(DataTable table, string filename)
        {
            //string columnname =Convert.ToString(table.Columns[2].ColumnName);
            //table.Columns[2].ColumnName = columnname.Replace("Sales", "Sales_");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 12.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            var t = table.AsEnumerable().Where(dataRow => dataRow.RowState != DataRowState.Deleted);
            table = t.CopyToDataTable();
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write( Convert.ToString(row[i])=="0"?null: row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();

            //Microsoft.Office.Interop.Excel.Application excel;
            //Microsoft.Office.Interop.Excel.Workbook worKbooK;
            //Microsoft.Office.Interop.Excel.Worksheet worKsheeT;
            //Microsoft.Office.Interop.Excel.Range celLrangE;

            //try
            //{
            //    excel = new Microsoft.Office.Interop.Excel.Application();
            //    excel.Visible = false;
            //    excel.DisplayAlerts = false;
            //    worKbooK = excel.Workbooks.Add(Type.Missing);


            //    worKsheeT = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
            //    worKsheeT.Name = "StudentRepoertCard";

            //    worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[1, 8]].Merge();
            //    worKsheeT.Cells[1, 1] = "Student Report Card";
            //    worKsheeT.Cells.Font.Size = 15;


            //    int rowcount = 2;

            //    foreach (DataRow datarow in table.Rows)
            //    {
            //        rowcount += 1;
            //        for (int i = 1; i <= table.Columns.Count; i++)
            //        {

            //            if (rowcount == 3)
            //            {
            //                worKsheeT.Cells[2, i] = table.Columns[i - 1].ColumnName;
            //                worKsheeT.Cells.Font.Color = System.Drawing.Color.Black;

            //            }

            //            worKsheeT.Cells[rowcount, i] = datarow[i - 1].ToString();

            //            if (rowcount > 3)
            //            {
            //                if (i == table.Columns.Count)
            //                {
            //                    if (rowcount % 2 == 0)
            //                    {
            //                        celLrangE = worKsheeT.Range[worKsheeT.Cells[rowcount, 1], worKsheeT.Cells[rowcount, table.Columns.Count]];
            //                    }

            //                }
            //            }

            //        }

            //    }

            //    celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[rowcount, table.Columns.Count]];
            //    celLrangE.EntireColumn.AutoFit();
            //    Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
            //    border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            //    border.Weight = 2d;

            //    celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[2, table.Columns.Count]];

            //    worKbooK.SaveAs("A.xlsx"); ;
            //    worKbooK.Close();
            //    excel.Quit();

            //}
            //catch (Exception ex)
            //{
            //    objCom.ErrorLog(ex);

            //}
            //finally
            //{
            //    worKsheeT = null;
            //    celLrangE = null;
            //    worKbooK = null;
            //}  
        }

        public void Create(DataTable dtSource, string strFileName)
        {
            // Create XMLWriter
            using (XmlTextWriter xtwWriter = new XmlTextWriter(strFileName, Encoding.UTF8))
            {

                //Format the output file for reading easier
                xtwWriter.Formatting = Formatting.Indented;

                // 
                xtwWriter.WriteStartDocument();

                // Adding processing information 
                xtwWriter.WriteProcessingInstruction("mso-application", "progid='Excel.Sheet'");

                // Adding root element
                xtwWriter.WriteStartElement("Workbook");
                xtwWriter.WriteAttributeString("xmlns", "urn:schemas-microsoft-com:office:spreadsheet");
                xtwWriter.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
                xtwWriter.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
                xtwWriter.WriteAttributeString("xmlns", "ss", null, "urn:schemas-microsoft-com:office:spreadsheet");
                xtwWriter.WriteAttributeString("xmlns", "html", null, "http://www.w3.org/TR/REC-html40");

                // 
                xtwWriter.WriteProcessingInstruction("mso-application", "progid=\"Excel.Sheet\"");

                // 
                xtwWriter.WriteStartElement("DocumentProperties", "urn:schemas-microsoft-com:office:office");

                // Write document properties
                xtwWriter.WriteElementString("Author", "Sandeep Prajapati");
                xtwWriter.WriteElementString("LastAuthor", "Sandeep Prajapati");
                xtwWriter.WriteElementString("Created", DateTime.Now.ToString("u") + "Z");
                xtwWriter.WriteElementString("Company", "XXXXXXXXXX");
                xtwWriter.WriteElementString("Version", "12");

                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteStartElement("ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");

                // Write settings of workbook
                xtwWriter.WriteElementString("WindowHeight", "8010");
                xtwWriter.WriteElementString("WindowWidth", "14805");
                xtwWriter.WriteElementString("WindowTopX", "240");
                xtwWriter.WriteElementString("WindowTopY", "105");
                xtwWriter.WriteElementString("ProtectStructure", "False");
                xtwWriter.WriteElementString("ProtectWindows", "False");

                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteStartElement("Styles");

                // 
                xtwWriter.WriteStartElement("Style");
                xtwWriter.WriteAttributeString("ss", "ID", null, "Default");
                xtwWriter.WriteAttributeString("ss", "Name", null, "Normal");

                // 
                xtwWriter.WriteStartElement("Alignment");
                xtwWriter.WriteAttributeString("ss", "Vertical", null, "Bottom");
                xtwWriter.WriteEndElement();

                // Write null on the other properties
                xtwWriter.WriteElementString("Borders", null);
                xtwWriter.WriteElementString("Font", null);
                xtwWriter.WriteElementString("Interior", null);
                xtwWriter.WriteElementString("NumberFormat", null);
                xtwWriter.WriteElementString("Protection", null);


                // 
                xtwWriter.WriteEndElement();

                //
                xtwWriter.WriteStartElement("Style");
                xtwWriter.WriteAttributeString("ss", "ID", null, "s16");
                xtwWriter.WriteStartElement("Font");
                xtwWriter.WriteAttributeString("ss", "Bold", null, "1");
                xtwWriter.WriteAttributeString("ss", "Size", null, "11");
                xtwWriter.WriteAttributeString("ss", "Underline", null, "Single");
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteEndElement();


                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteStartElement("Worksheet");
                xtwWriter.WriteAttributeString("ss", "Name", null, dtSource.TableName);

                // 
                xtwWriter.WriteStartElement("Table");
                xtwWriter.WriteAttributeString("ss", "ExpandedColumnCount", null, dtSource.Columns.Count.ToString());
                xtwWriter.WriteAttributeString("ss", "ExpandedRowCount", null, (dtSource.Rows.Count + 1).ToString());
                xtwWriter.WriteAttributeString("x", "FullColumns", null, "1");
                xtwWriter.WriteAttributeString("x", "FullRows", null, "1");
                //xtwWriter.WriteAttributeString("ss", "DefaultColumnWidth", null, "60");

                // Run through all rows of data source


                // 
                xtwWriter.WriteStartElement("Row");
                foreach (DataColumn Header in dtSource.Columns)
                {
                    // 
                    xtwWriter.WriteStartElement("Cell");
                    xtwWriter.WriteAttributeString("ss", "StyleID", null, "s16");

                    // xxx
                    xtwWriter.WriteStartElement("Data");
                    xtwWriter.WriteAttributeString("ss", "Type", null, "String");
                    // Write content of cell
                    xtwWriter.WriteValue(Header.ColumnName);

                    // 
                    xtwWriter.WriteEndElement();

                    // 
                    xtwWriter.WriteEndElement();
                }

                xtwWriter.WriteEndElement();


                foreach (DataRow row in dtSource.Rows)
                {
                    // 
                    xtwWriter.WriteStartElement("Row");

                    // Run through all cell of current rows
                    foreach (object cellValue in row.ItemArray)
                    {
                        // 
                        xtwWriter.WriteStartElement("Cell");
                        //if (cnt == 0)
                        //    xtwWriter.WriteAttributeString("ss", "StyleID", null, "s16");

                        // 
                        xtwWriter.WriteStartElement("Data");
                        xtwWriter.WriteAttributeString("ss", "Type", null, "String");
                        // Write content of cell
                        string strcellValue = (cellValue == System.DBNull.Value ? string.Empty : Convert.ToString(cellValue));
                        xtwWriter.WriteValue(strcellValue);

                        // 
                        xtwWriter.WriteEndElement();

                        // 
                        xtwWriter.WriteEndElement();

                        xtwWriter.WriteEndElement();
                    }
                    // 
                    xtwWriter.WriteEndElement();
                }

                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteStartElement("WorksheetOptions", "urn:schemas-microsoft-com:office:excel");

                // Write settings of page
                xtwWriter.WriteStartElement("PageSetup");
                xtwWriter.WriteStartElement("Header");
                xtwWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
                xtwWriter.WriteEndElement();
                xtwWriter.WriteStartElement("Footer");
                xtwWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
                xtwWriter.WriteEndElement();
                xtwWriter.WriteStartElement("PageMargins");
                xtwWriter.WriteAttributeString("x", "Bottom", null, "0.984251969");
                xtwWriter.WriteAttributeString("x", "Left", null, "0.78740157499999996");
                xtwWriter.WriteAttributeString("x", "Right", null, "0.78740157499999996");
                xtwWriter.WriteAttributeString("x", "Top", null, "0.984251969");
                xtwWriter.WriteEndElement();
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteElementString("Selected", null);

                // 
                xtwWriter.WriteStartElement("Panes");

                // 
                xtwWriter.WriteStartElement("Pane");

                // Write settings of active field
                xtwWriter.WriteElementString("Number", "1");
                xtwWriter.WriteElementString("ActiveRow", "1");
                xtwWriter.WriteElementString("ActiveCol", "1");

                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteEndElement();

                // False
                xtwWriter.WriteElementString("ProtectObjects", "False");

                // False
                xtwWriter.WriteElementString("ProtectScenarios", "False");

                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteEndElement();

                // 
                xtwWriter.WriteEndElement();

                // Write file on hard disk
                xtwWriter.Flush();
                xtwWriter.Close();
            }
        }


        #endregion

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                
            TextBox txt = sender as TextBox;
            GridViewRow currentRow = txt.NamingContainer as GridViewRow;
            
            Label customernumber = (Label)currentRow.FindControl("lblCustomerNumber");
           
            DataTable dtOutput = (DataTable)Session["dtOutput"];
            DataRow dr=(from myRow in dtOutput.AsEnumerable()
                   where myRow.Field<int>("customernumber") == Convert.ToInt32(customernumber.Text)
                     select myRow).SingleOrDefault();
            dr["YearlyAmount"] = txt.Text;

            grdEntry.DataSource = dtOutput;
            grdEntry.DataBind();
            panelvalues.Visible = true;
            //lblResult.Text = "All values are in lakhs.";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "BindGridView();", true);
            
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

    }
}