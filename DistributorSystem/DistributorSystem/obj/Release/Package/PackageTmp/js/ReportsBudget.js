﻿
$(document).ready(function () {

    triggerPostGridLodedActions();

});
$(window).resize(function () {

    triggerPostGridLodedActions();
});
/**
*
*/
function resizewidth(elementId, parentGroupId) {

    var width1 = 0;
    if ($(elementId).is(':visible')) {
        width1 = $(elementId).width();
    }
    else {
        var tempele = $(elementId).closest(parentGroupId);
        $(tempele).show()
        width1 = $(elementId).width();
        $(tempele).hide();
    }
    return width1;
}

function floatingheader() {
    var $fixedHeader = $("#header_fixed");
    $fixedHeader.html("");
    var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
    var $header1 = $('#MainContent_salesbylinegrid > tbody >tr:first').clone();
    var $header2 = $('#MainContent_salesbyfamilygrid > tbody >tr:first').clone();
    var $header3 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
    var $header4 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();
    var $header5 = $('#MainContent_salesbyapp_qty > tbody >tr:first').clone();

    var width1 = $('#MainContent_fiveyrsproducts').width();
    var width2 = $('#MainContent_salesbylinegrid').width();
    var width3 = $('#MainContent_salesbyfamilygrid').width();
    var width4 = $('#MainContent_salesbyapp').width();
    var width5 = $('#MainContent_salesbycustomer').width();
    var width6 = $('#MainContent_salesbyapp_qty').width();

    $fixedHeader.append($header);
    $fixedHeader.append($header1);
    $fixedHeader.append($header2);
    $fixedHeader.append($header3);
    $fixedHeader.append($header4);
    $fixedHeader.append($header5);

    $fixedHeader.show();
    $header.hide();
    $header1.hide();
    $header2.hide();
    $header3.hide();
    $header4.hide();
    $header5.hide();

    $(window).bind("scroll", function () {
        var tableOffset = 150;

        var tab2ht = $("#tab2").offset().top;
        var tab3ht = $("#tab3").offset().top;
        var tab4ht = $("#tab4").offset().top;
        var tab5ht = $("#tab5").offset().top;

        var offset = $(this).scrollTop() + 1;

        if (offset >= tableOffset && offset < tab2ht && $('#prdgroup').is(":visible")) {
            $('#header_fixed').width(width1);
            $header.show();
        }
        else {
            $header.hide();
        }
        if (offset >= tab2ht && offset < tab3ht && $('#linegrid').is(":visible")) {
            $('#header_fixed').width(width2);
            $header1.show();
        }
        else {
            $header1.hide();
        }
        if (offset >= tab3ht && offset < tab4ht && $('#familygrid').is(":visible")) {
            $('#header_fixed').width(width3);
            $header2.show();
        }
        else {
            $header2.hide();
        }
        if (offset > tab4ht && offset < tab5ht && $('#salesappgrid').is(":visible")) {

            if ($("#MainContent_rbtn_Value").is(':checked')) {
                $('#header_fixed').width(width4);
                $header3.show();
            }
            if ($("#MainContent_rbtn_Quantity").is(':checked')) {
                $('#header_fixed').width(width6);
                $header5.show();
            }
        }
        else {
            $header3.hide();
            $header5.hide();
        }
        if (offset > tab5ht && $('#slsbycustomergrid').is(":visible")) {
            $('#header_fixed').width(width5);
            $header4.show();
        }
        else {
            $header4.hide();
        }


    });
}

function value_or_qty_Change() {
    if ($("#MainContent_rbtn_Value").is(':checked')) {
        $("#MainContent_salesbyapp").show();
        $("#MainContent_salesbyapp_qty").hide();
    }
    if ($("#MainContent_rbtn_Quantity").is(':checked')) {
        $("#MainContent_salesbyapp").hide();
        $("#MainContent_salesbyapp_qty").show();
    }
}

function byValueorunitsorlacks_CheckedChanged() {
    jQuery('.panel-heading4').trigger('click');
}

function triggerPostGridLodedActions() {
    value_or_qty_Change();
    var $fixedHeader = $("#header_fixed");
    $fixedHeader.html("");
    var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
    var $header1 = $('#MainContent_salesbylinegrid > tbody >tr:first').clone();
    var $header2 = $('#MainContent_salesbyfamilygrid > tbody >tr:first').clone();
    var $header3 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
    var $header4 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();
    var $header5 = $('#MainContent_salesbyapp_qty > tbody >tr:first').clone();


    var width1 = $('#MainContent_fiveyrsproducts').width();
    var width2 = $('#MainContent_salesbylinegrid').width();
    var width3 = $('#MainContent_salesbyfamilygrid').width();
    var width4 = $('#MainContent_salesbyapp').width();
    var width5 = $('#MainContent_salesbycustomer').width();
    var width6 = $('#MainContent_salesbyapp_qty').width();


    $fixedHeader.append($header);
    $fixedHeader.append($header1);
    $fixedHeader.append($header2);
    $fixedHeader.append($header3);
    $fixedHeader.append($header4);
    $fixedHeader.append($header5);

    $fixedHeader.show();
    $header.hide();
    $header1.hide();
    $header2.hide();
    $header3.hide();
    $header4.hide();
    $header5.hide();

    $(window).bind("scroll", function () {
        var tableOffset = 150;

        var tab2ht = $("#tab2").offset().top;
        var tab3ht = $("#tab3").offset().top;
        var tab4ht = $("#tab4").offset().top;
        var tab5ht = $("#tab5").offset().top;

        var offset = $(this).scrollTop() + 1;


        if (offset >= tableOffset && offset < tab2ht && $('#prdgroup').is(":visible")) {
            $('#header_fixed').width(width1);
            $header.show();
        }
        else {
            $header.hide();
        }
        if (offset >= tab2ht && offset < tab3ht && $('#linegrid').is(":visible")) {
            $('#header_fixed').width(width2);
            $header1.show();
        }
        else {
            $header1.hide();
        }
        if (offset >= tab3ht && offset < tab4ht && $('#familygrid').is(":visible")) {
            $('#header_fixed').width(width3);
            $header2.show();
        }
        else {
            $header2.hide();
        }
        if (offset > tab4ht && offset < tab5ht && $('#salesappgrid').is(":visible")) {
            //$('#header_fixed').width(width4);
            //$header3.show();
            if ($("#MainContent_rbtn_Value").is(':checked')) {
                $('#header_fixed').width(width4);
                $header3.show();
            }
            if ($("#MainContent_rbtn_Quantity").is(':checked')) {
                $('#header_fixed').width(width6);
                $header5.show();
            }


        }
        else {
            $header3.hide();
            $header5.hide();
        }
        if (offset > tab5ht && $('#slsbycustomergrid').is(":visible")) {
            $('#header_fixed').width(width5);
            $header4.show();
        }
        else {
            $header4.hide();
        }

    });
    $('#collapsebtn').unbind('click').bind('click', function (e) {


        var attr = $('#product_image').attr('src');
        //var imgsrc=images/button_plus.gif;
        $("#prdgroup").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#product_image").attr("src", "images/button_plus.gif");
        } else {
            $("#product_image").attr("src", "images/button_minus.gif");
        }
        floatingheader();

    });
    $('#collapseline').unbind('click').bind('click', function (e) {

        var attr = $('#linegrid_image').attr('src');
        $("#linegrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#linegrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#linegrid_image").attr("src", "images/button_minus.gif");
        }
        floatingheader();
    });
    $('#collapsefamily').unbind('click').bind('click', function (e) {

        var attr = $('#familygrid_image').attr('src');
        $("#familygrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#familygrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#familygrid_image").attr("src", "images/button_minus.gif");
        }
        floatingheader();
    });
    $('#collapse_app').unbind('click').bind('click', function (e) {

        var attr = $('#salesapp_img').attr('src');
        $("#salesappgrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#salesapp_img").attr("src", "images/button_plus.gif");
        } else {
            $("#salesapp_img").attr("src", "images/button_minus.gif");
        }
        floatingheader();
    });
    $('#cust_app').unbind('click').bind('click', function (e) {


        var attr = $('#slsbycust_image').attr('src');
        $("#slsbycustomergrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#slsbycust_image").attr("src", "images/button_plus.gif");
        } else {
            $("#slsbycust_image").attr("src", "images/button_minus.gif");
        }
        floatingheader();
    });

    $('#MainContent_salesbycustomer tr').each(function () {

        $(this).find("td:eq(1)").each(function () {
            if ($(this).find('span').text() == "CUSTOMER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:eq(1)").each(function () {
            if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:eq(1)").each(function () {
            if ($(this).find('span').text() == "BRANCH TOTAL") {
                $(this).closest("tr").find("td").addClass("grand_total");
            }
        });
        $(this).find("td:eq(1)").each(function () {
            if ($(this).find('span').text() == "GRAND TOTAL") {
                $(this).closest("tr").find("td").addClass("grand_total");
            }
        });
    });
    $('#MainContent_ddlCustomerList').change(function () {

        var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
        $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
    });
    $('#MainContent_ddlCustomerNumber').change(function () {

        var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
        $("#MainContent_ddlCustomerList").val(ddlslectedText);
    });
    $("#familygrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });
    $("#linegrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });
    $("#MainContent_salesbyapp tr:last td").each(function () { $(this).removeClass(); $(this).addClass("sum_total") });
    $("#linegrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });

    $("#slsbycustomergrid tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") })
    $("#MainContent_salesbyapp tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total"); })
    $('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
        var attr = $('#Imgcollapsedropdwns').attr('src');
        $("#MainContent_reportdrpdwns").slideToggle();
        if (attr == "images/up_arrow.png") {
            $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
        } else {
            $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
        }
    });
}

function headergrid() {
    var $fixedHeader = $("#header_fixed");
    $fixedHeader.html("");
    var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
    var $header1 = $('#MainContent_salesbylinegrid > tbody >tr:first').clone();
    var $header2 = $('#MainContent_salesbyfamilygrid > tbody >tr:first').clone();
    var $header3 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
    var $header4 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();
    var width1 = $('#MainContent_fiveyrsproducts').width();
    var width2 = $('#MainContent_salesbylinegrid').width();
    var width3 = $('#MainContent_salesbyfamilygrid').width();
    var width4 = $('#MainContent_salesbyapp').width();
    var width5 = $('#MainContent_salesbycustomer').width();
    $fixedHeader.append($header);
    $fixedHeader.append($header1);
    $fixedHeader.append($header2);
    $fixedHeader.append($header3);
    $fixedHeader.append($header4);

    $fixedHeader.show();
    $header.hide();
    $header1.hide();
    $header2.hide();
    $header3.hide();
    $header4.hide();
    $(window).bind("scroll", function () {
        var tableOffset = 150;

        var tab2ht = $("#tab2").offset().top;
        var tab3ht = $("#tab3").offset().top;
        var tab4ht = $("#tab4").offset().top;
        var tab5ht = $("#tab5").offset().top;

        var offset = $(this).scrollTop() + 1;


        if (offset >= tableOffset && offset < tab2ht) {
            $('#header_fixed').width(width1);
            $header.show();
        }
        else {
            $header.hide();
        }
        if (offset >= tab2ht && offset < tab3ht) {
            $('#header_fixed').width(width2);
            $header1.show();
        }
        else {
            $header1.hide();
        }
        if (offset >= tab3ht && offset < tab4ht) {
            $('#header_fixed').width(width3);
            $header2.show();
        }
        else {
            $header2.hide();
        }
        if (offset > tab4ht && offset < tab5ht) {
            $('#header_fixed').width(width4);
            $header3.show();
        }
        else {
            $header3.hide();
        }
        if (offset > tab5ht) {
            $('#header_fixed').width(width5);
            $header4.show();
        }
        else {
            $header4.hide();
        }


    });
}
