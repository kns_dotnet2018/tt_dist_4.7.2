﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PriceSummary.aspx.cs" Inherits="DistributorSystem.PriceSummary" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="css/Tabs.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script>
    <%--    <script src="https://cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js" type="text/javascript"></script>--%>
    <style>
        button[disabled], html input[disabled] {
            cursor: not-allowed;
            background: grey;
        }


        .popupControl {
            margin: 5px;
            float: right;
        }

        th,
        td {
            white-space: nowrap;
        }

        div.dataTables_wrapper {
            /*width: 800px;*/
            margin: 0 auto;
        }

        .loader_div {
            position: absolute;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../tt_dist/images/loader.gif') center center no-repeat;
        }

        .link {
            cursor: pointer;
        }

        .modal a.close-modal {
            top: 0;
            right: 0;
        }

        .blocker {
            z-index: 99;
        }
    </style>
    <script type="text/javascript">

        var table1;
        $(document).ready(function () {
            debugger;

            LoadTable();

            //$('#MainContent_txtOrderDate').daterangepicker({
            //    singleDatePicker: true,
            //    showDropdowns: true,
            //    minYear: 2019,
            //    maxYear: parseInt(moment().format('YYYY'),10)
            //}, function(start, end, label) {
            //});

        });

        function LoadData(msg) {
            debugger;
            console.log(msg);
            console.log(msg.d);
            msg = JSON.parse(msg.d);
            $('#grdDetailedPriceSummary1 tbody').html("");
            for (var i = 0; i < msg.length; i++) {

                var qty = '';
                if (msg[i].Order_type == "schedule") {
                    qty = msg[i].QTY_perOrder;
                }
                else
                    qty = msg[i].Total_QTY;
                qty = msg[i].Approved_OrderQty;
                var button = '';
                if (msg[i].Status == "Approved") {
                    if (msg[i].EscalateFLag == 1) {
                        button = "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" title=\"Escalate Option will not be there For Rejected Quote\" id=\"btnEscalate1\" value=\"Escalate\" class=\"btnSubmit\" onclick=\"return EscalateWithQty('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated', '" + qty + "', '" + msg[i].New_OfferPrice + "', '" + msg[i].MOQ_Escalate + "');\" /></a>"


                        if (msg[i].Order_Flag == "1")
                            button +=
                                //Disabled Place order
                                //    "<a href=\"#ex1\" rel=\"modal:open\"><input type=\"button\"  class=\"btnSubmit\"  id=\"imgbtnPlace\" value=\"Place Order\"  onclick=\"return PlaceOrder('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + msg[i].Order_type + "', '" + msg[i].Cust_number + "', '" + msg[i].Cust_Name + "', '" + msg[i].MOQ + "', '" + msg[i].QTY_perOrder + "', '" + msg[i].Order_frequency + "');\" ></a>"
                                //+
                                //  +
                                "<a href=\"#mdRejectReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnRejectRFQ\" value=\"Reject\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'RejectedRFQ');\" /></a>"
                                +
                                "<input type=\"button\" id=\"btnAccept\" value=\"Accept\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Accepted');\" />";
                        else
                            button +=
                                //       "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate\" value=\"Escalate\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a>"
                                //+
                                "<a href=\"#mdRejectReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnRejectRFQ\" value=\"Reject\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'RejectedRFQ');\" /></a>"
                                +
                                "<input type=\"button\" id=\"btnAccept\" value=\"Accept\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Accepted');\" />";


                        console.log(button);
                        // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                        //
                    }
                    else {
                        button = "<label>Any action is denied for this quote,<br/>please raise a fresh RFQ.</label>";
                    }
                }
                else if (msg[i].Status == "Escalated & Approved") {
                    if (msg[i].EscalateFLag == 1) {
                        if (msg[i].Order_Flag == "1")
                            button =

                                "<a href=\"#mdRejectReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnRejectRFQ\" value=\"Reject\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'RejectedRFQ');\" /></a>"
                                +
                                "<input type=\"button\" id=\"btnAccept\" value=\"Accept\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Accepted');\" />";
                        else
                            button =
                                "<a href=\"#mdRejectReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnRejectRFQ\" value=\"Reject\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'RejectedRFQ');\" /></a>"
                                +
                                "<input type=\"button\" id=\"btnAccept\" value=\"Accept\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Accepted');\" />";


                        console.log(button);
                        // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                        //
                    }
                }
                else if (msg[i].Status == "Expired" && msg[i].EscalateFLag == 1) {
                    button = "<input type=\"button\" id=\"btnRequest\" value=\"Request for approval\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Request For ReApproval');\" /></a>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else if (msg[i].Status == "Rejected" && msg[i].EscalateFLag == 1) {
                    button = "<a href=\"#mdReason\" rel=\"modal:open\"><input disabled type=\"button\" title=\"Escalate Option will not be available for Rejected quote\" id=\"btnEscalate\" value=\"Escalate\" class=\"btnSubmit\" style=\"disabled:true;\" onclick=\"return EscalateWithQty('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated', '" + qty + "', '" + msg[i].New_OfferPrice + "', '" + msg[i].MOQ_Escalate + "');\" /></a>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else {
                    button = "<label>Any action is denied for this quote,<br/>please raise a fresh RFQ.</label>";
                }
                button += '<img src="images/info.jpg" style="height:25px;" title="View Status Log" id="imgView_' + i + '" onclick="OpenStatusLog(\'' + msg[i].Item_code + '\', \'' + msg[i].Item_Desc + '\', \'' + msg[i].Ref_number + '\');" /><input type="hidden" id="hdnID_' + i + '" value="' + msg[i].ID + '">';
                debugger;
                var file = '<a onclick= "DownloadFile(\'' + msg[i].Escalation_file + '\', \'' + msg[i].Escalation_file_name+ '\');">' + msg[i].Escalation_file_name + '</a>';
                $("#grdDetailedPriceSummary1 tbody ").append(" <tr>  <td>" +
                    button + "</td>  <td>" +
                    msg[i].Item_code + "</td>  <td>" +
                    msg[i].Item_Desc + "</td>  <td>" +
                    msg[i].WHS + "</td>  <td>" +
                  /*  msg[i].Order_type + "</td>  <td>" +*/

                    //msg[i].Order_frequency + "</td>  <td>" +
                    msg[i].Approved_OrderQty + "</td>  <td>" +
                    //msg[i].QTY_perOrder + "</td>  <td>" +
                    //msg[i].Expected_price + "</td>  <td>" +
                    msg[i].New_OfferPrice + "</td>  <td>" +
                    msg[i].Order_Validity + "</td>  <td>" +
                    //msg[i].List_Price + "</td>  <td>" +

                    //msg[i].DC_rate + "</td>  <td>" +
                    file + "</td>  <td>" +
                    msg[i].StatusName +
                    "</td>  </tr>");
                // alert(JSON.stringify(msg));
            }
            debugger;
            if ($.fn.dataTable.isDataTable('#grdDetailedPriceSummary1')) {
                //$('#grdDetailedPriceSummary1').DataTable().destroy();
                //table1.destroy();
                //table1 = $('#grdDetailedPriceSummary1').DataTable({
                //    //destroy: true,
                //});
            }
            else {
                //table1.destroy();
                table1 = $('#grdDetailedPriceSummary1').DataTable({
                    // destroy: true
                });
            }

        }
        function DownloadFile(Escalation_file, filename) {
            $.ajax({
                type: "POST",
                url: "PriceSummary.aspx/DownloadFile",
                data: '{file: "' + Escalation_file + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    debugger;
                    //Convert Base64 string to Byte Array.
                    var bytes = Base64ToBytes(r.d);

                    //Convert Byte Array to BLOB.
                    var blob = new Blob([bytes], { type: "application/octetstream" });

                    //Check the Browser type and download the File.
                    var isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, fileName);
                    } else {
                        var url = window.URL || window.webkitURL;
                        link = url.createObjectURL(blob);
                        var a = $("<a />");
                        a.attr("download", filename);
                        a.attr("href", link);
                        $("body").append(a);
                        a[0].click();
                        //$("body").remove(a);
                    }
                }
            });
            //$.ajax({
            //    url: 'FileDownload.ashx?filepath=' + Escalation_file,
            //    type: 'POST',
            //    cache: false,
            //    contentType: false,
            //    processData: false,
            //    success: function (file) {
            //        console.log('success');
            //    },
            //    error: function (err) {
            //        alert(err.statusText);
            //    }
            //});

        }
        function Base64ToBytes(base64) {
            var s = window.atob(base64);
            var bytes = new Uint8Array(s.length);
            for (var i = 0; i < s.length; i++) {
                bytes[i] = s.charCodeAt(i);
            }
            return bytes;
        };
        function LoadTable() {
            var SelectedStart = sessionStorage.getItem("selectedStart");
            var SelectedEnd = sessionStorage.getItem("selectedEnd");
            var start = (SelectedStart == null ? moment().subtract(29, 'days') : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#MainContent_txtDateRange').daterangepicker({
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedStart', start);
                sessionStorage.setItem('selectedEnd', end);
            });

            var head_content = $('#MainContent_grdPriceSummary tr:first').html();
            $('#MainContent_grdPriceSummary').prepend('<thead></thead>')
            $('#MainContent_grdPriceSummary thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdPriceSummary tbody tr:first').hide();
            var table = $('#MainContent_grdPriceSummary').dataTable({
               // "order": [[1, 'desc']]
            });

            var table = $('#MainContent_grdPriceSummary').DataTable();
            var divdetail = document.getElementById("divdetail");
            $('#MainContent_grdPriceSummary tbody').on('click', 'td:first-child .link', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    divdetail.style.display = "none";
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    //var rows = $('#MainContent_grdPriceSummary tr').length;
                    //var dr;
                    //var drrow;
                    //var drchild;w
                    //for (var x = rows; x >= rows / 2; x--) {
                    //    dr = $(this).closest('tr').first();
                    //    drrow = table.row(dr);
                    //    drchild = drrow.child;
                    //    drchild.hide();
                    //}

                    divdetail.style.display = "block";
                    $('#grdDetailedPriceSummary1').DataTable().destroy();
                    var param = tr.context.innerText;
                    console.log(param);
                    $.ajax({
                        url: 'PriceSummary.aspx/LoadDetailedGrid',
                        method: 'post',
                        datatype: 'json',
                        data: '{ref_no:"' + param + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            LoadData(msg);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                        }
                    });

                    // Open row.
                    row.child(divdetail).show();
                    //tr.addClass('shown');
                }
            });


            var divplace = document.getElementById("divplace");

            var head_content = $('#MainContent_grdItemSummary tr:first').html();
            $('#MainContent_grdItemSummary').prepend('<thead></thead>')
            $('#MainContent_grdItemSummary thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdItemSummary tbody tr:first').hide();
            var otable = $('#MainContent_grdItemSummary').dataTable({
                //columnDefs: [{ type: 'date', 'targets': [10] }],
                //"order": [[10, 'desc']]

                "order": [[8, "desc"]], //or asc 
                // "columnDefs": [{ "targets": 11, "type": "date-eu" }],
            });

        }
        document.addEventListener("DOMContentLoaded", function () {

            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {

                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }

                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });

        function tabchange(e) {

            if (e.id == "MainContent_rfqList") {
                $('#MainContent_divRFQSummary').css("display", "block");
                $('#MainContent_divItemSummary').css("display", "none");
                $('#MainContent_divItemSummary').removeClass("active");
                $('#MainContent_divRFQSummary').addClass("active");
            }
            else {
                $('#MainContent_divRFQSummary').css("display", "none");
                $('#MainContent_divItemSummary').css("display", "block");
                $('#MainContent_divItemSummary').addClass("active");
                $('#MainContent_divRFQSummary').removeClass("active");
            }

        }
        function LoadPlace(table) {
            $('#grdDetailedPriceSummary1 tbody').on('click', 'td:first-child .place', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        }


        function format(d) {
            console.log(d);
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                '<td>Full name:</td>' +
                '<td></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extension number:</td>' +
                '<td></td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extra info:</td>' +
                '<td>And any further details here (images etc)...</td>' +
                '</tr>' +
                '</table>';
        }
        function PlaceOrder(ID, item, ref_num, order_type, cust_num, cust_name, MOQ, qty, frequency) {
            debugger;
            console.log('Hi');
            console.log(item);
            console.log(ref_num);
            var divSchedule = document.getElementById('divSchedule');
            if (order_type == "schedule") {
                divSchedule.style.display = "block";
                $('#MainContent_txtQuantity').val(qty);
            }
            else {
                divSchedule.style.display = "none";
                $('#MainContent_txtQuantity').val(qty);
            }
            $('#MainContent_txtFrequency').val(frequency);
            $('#MainContent_lblOrderType').val(order_type);
            $('#MainContent_hdnRef').val(ref_num);
            $('#MainContent_hdnItem').val(item);
            $('#MainContent_hdnID').val(ID);
            $('#MainContent_txtMOQ').val(MOQ);
            $('#MainContent_hdnCustNum').val(cust_num);
            $('#MainContent_hdnCustName').val(cust_name);
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function Order() {
            debugger;
            var order_type = $('#MainContent_lblOrderType').val();
            var ref_num = $('#MainContent_hdnRef').val();
            var item = $('#MainContent_hdnItem').val();
            var ID = $('#MainContent_hdnID').val();
            var quantity = $('#MainContent_txtQuantity').val();
            var schedule = $('#MainContent_txtFrequency').val();
            //var NumOrder = $('#MainContent_txtNumOrder').val();
            var cust_num = $('#MainContent_hdnCustNum').val();
            var cust_name = $('#MainContent_hdnCustName').val();
            var orderstart_date = $('#MainContent_txtOrderDate').val();
            var POComment = $('#MainContent_txtPOComment').val();
            var flag = 0;
            if (order_type == "schedule") {
                if (IsnullOrEmpty(quantity)) {
                    $("#MainContent_txtQuantity").css("border", "1px solid red");
                    flag++;
                }
                else {
                    $("#MainContent_txtQuantity").css("border", "");
                }
            }
            else {
                if (IsnullOrEmpty(quantity)) {
                    quantity = $("#MainContent_txtMOQ").val();
                }
            }

            //if (order_type == 'schedule') {
            //    if (IsnullOrEmpty(NumOrder)) {
            //        $("#MainContent_txtNumOrder").css("border", "1px solid red");
            //        flag++;
            //    }
            //    else {
            //        $("#MainContent_txtNumOrder").css("border", "");
            //    }
            //}
            if (flag == 0) {
                $.ajax({
                    url: 'PriceSummary.aspx/PlaceOrder',
                    method: 'post',
                    datatype: 'json',
                    data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", quantity:"' + quantity + '", item:"' + item + '", order_type:"' + order_type + '", schedule:"' + schedule + '", OrderStartDate:"' + orderstart_date + '", cust_num:"' + cust_num + '", cust_name:"' + cust_name + '", PO_comment:"' + POComment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        location.reload(true);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }

        }

        function Escalate() {
            debugger;
            jQuery(".loader_div").show();
            
            var ref_num = $('#MainContent_hdnRef1').val();
            var item = $('#MainContent_hdnItem1').val();
            var ID = $('#MainContent_hdnID1').val();
            var status = $('#MainContent_hdnStatus1').val();
            var reason = $('#MainContent_txtReason').val();
            var comp_name = $('#MainContent_ddlCompanyName').val();
            var comp_desc = $('#MainContent_txtDescription').val();
            var comp_sp = $('#MainContent_txtCompanySP').val();
            var end_cust = $('#MainContent_ddlEndCustomer').val();
            var end_cust_name = $("#MainContent_ddlEndCustomer").select2('data')[0].text;
            var approved_price = $('#MainContent_hdnApprovedPrice1').val();
            var MOQ = $('#MainContent_hdnMOQ1').val();
            var req_qty = $('#MainContent_txtReqQty').val();

            var req_price = $('#MainContent_txtExpPrice').val();
            
            /*      var tot = req_qty * approved_price;*/
            if (parseInt(req_qty) < parseInt(MOQ)) {
                $('#MainContent_txtReqQty').css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }

            //else {
            //    || tot < 100000)
            //}
            else if (IsnullOrEmpty(reason)) {
                $("#MainContent_txtReason").css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else if (IsnullOrEmpty(end_cust) || end_cust == "0") {
                $("#select2-MainContent_ddlEndCustomer-container").parent().css("border", "1px solid red")
                //$("#MainContent_ddlEndCustomer").css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else {
                var fileUpload = $("#MainContent_fileUpload").get(0);
                var files = fileUpload.files;
                if (files.length > 0) {
                    var fileData = new FormData();

                    // Looping over all files and add it to FormData object  
                    for (var i = 0; i < files.length; i++) {
                        fileData.append(files[i].name, files[i]);
                    }
                    fileData.append('ref_num', ref_num);
                    fileData.append('item', item);

                    $.ajax({
                        url: 'FileUpload.ashx?ref_num=' + ref_num + '&item=' + item,
                        type: 'POST',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (file) {
                            //alert(result);
                            debugger;
                            var input = '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", comp_name:"' + comp_name + '", comp_desc:"' + comp_desc + '", comp_SP:"' + comp_sp + '",  end_cust_num:"' + end_cust + '", end_cust_name:"' + end_cust_name + '", qty:"' + req_qty + '", MOQ:"' + MOQ + '", file:"' + file.name + '", price:"' + req_price+'"}';
                            console.log(input);
                            $.modal.close();
                            $("#MainContent_txtReason").css("border", "");
                            $("#MainContent_ddlEndCustomer").css("border", "");
                            $.ajax({
                                url: 'PriceSummary.aspx/RequestForReApproval',
                                method: 'post',
                                datatype: 'json',
                                data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", comp_name:"' + comp_name + '", comp_desc:"' + comp_desc + '", comp_SP:"' + comp_sp + '",  end_cust_num:"' + end_cust + '", end_cust_name:"' + end_cust_name + '", qty:"' + req_qty + '", MOQ:"' + MOQ + '", file:"' + file.name + '", price:"' + req_price + '"}',
                                contentType: "application/json; charset=utf-8",
                                success: function (msg) {
                                    msg = JSON.parse(msg.d);
                                    alert(msg.msg);
                                    location.reload(true);
                                    jQuery(".loader_div").hide();
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(xhr.responseText);
                                    jQuery(".loader_div").hide();
                                }
                            });
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });

                }
                // Create FormData object  
                else {
                    var input = '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", comp_name:"' + comp_name + '", comp_desc:"' + comp_desc + '", comp_SP:"' + comp_sp + '",  end_cust_num:"' + end_cust + '", end_cust_name:"' + end_cust_name + '", qty:"' + req_qty + '", MOQ:"' + MOQ + '", file:"", price:"' + req_price + '"}';
                    console.log(input);
                    $.modal.close();
                    $("#MainContent_txtReason").css("border", "");
                    $("#MainContent_ddlEndCustomer").css("border", "");
                    $.ajax({
                        url: 'PriceSummary.aspx/RequestForReApproval',
                        method: 'post',
                        datatype: 'json',
                        data: input,
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            msg = JSON.parse(msg.d);
                            alert(msg.msg);
                            location.reload(true);
                            jQuery(".loader_div").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                            jQuery(".loader_div").hide();
                        }
                    });
                }
                
            }
        }

        function EscalateWithQty(ID, item, ref_num, status, qty, approved_price, MOQ_Escalate) {
            console.log(approved_price);
            if (parseInt(qty) >= parseInt(MOQ_Escalate)) {
                var tot = qty * approved_price;
                if (tot < 100000) {
                    var req_qty = Math.ceil(100000 / approved_price);
                    req_qty = Math.ceil(req_qty / 10) * 10;
                    $('#MainContent_txtReqQty').val(req_qty);
                    $('#MainContent_hdnMOQ1').val(req_qty);
                    alert("Please increase the requested quanity to " + req_qty + " for escalation.");
                }
                else {
                    $('#MainContent_hdnMOQ1').val(qty);
                    $('#MainContent_txtReqQty').val(qty);
                }
                $('.ExpPriceDiv').attr("style", "display:block;");
            }
            else {
                $('#MainContent_hdnMOQ1').val(MOQ_Escalate);
                $('#MainContent_txtReqQty').val(MOQ_Escalate);
                $('.ExpPriceDiv').attr("style", "display:none;");
                alert("Please increase the requested quanity to " + MOQ_Escalate + " for escalation.");
            }
            $('#MainContent_hdnApprovedPrice1').val(approved_price);

            debugger;
            Request(ID, item, ref_num, status);
        }


        function Request(ID, item, ref_num, status) {
            debugger;
            if (status == "Escalated") {


                jQuery(".loader_div").show();
                $('#MainContent_hdnRef1').val(ref_num);
                $('#MainContent_hdnItem1').val(item);
                $('#MainContent_hdnID1').val(ID);
                $('#MainContent_hdnStatus1').val(status);
                $.ajax({
                    url: 'QuoteRequest.aspx/LoadCompetitors',
                    method: 'post',
                    datatype: 'json',
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        if ($("#MainContent_ddlCompanyName") != undefined)
                            $("#MainContent_ddlCompanyName").select2({ data: msg.d });
                        $.ajax({
                            url: 'QuoteRequest.aspx/LoadCustomers',
                            method: 'post',
                            datatype: 'json',
                            data: '',
                            contentType: "application/json; charset=utf-8",
                            success: function (msg1) {
                                // sessionStorage.setItem("customerdata", JSON.stringify(msg.d));
                                if ($("#MainContent_ddlEndCustomer") != undefined)
                                    var newOption = new Option('--Select--', 0, false, false);
                                $("#MainContent_ddlEndCustomer").append(newOption).trigger('change');
                                $("#MainContent_ddlEndCustomer").select2({ data: msg1.d });
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.responseText);
                            }
                        });
                        jQuery(".loader_div").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                        jQuery(".loader_div").hide();
                    }
                });

            }
            else if (status == "RejectedRFQ") {
                jQuery(".loader_div").show();
                $('#MainContent_hdnRejectRef').val(ref_num);
                $('#MainContent_hdnRejectItem').val(item);
                $('#MainContent_hdnRejectID').val(ID);
                $('#MainContent_hdnRejectStatus').val(status);
                jQuery(".loader_div").hide();
            }
            else {
                jQuery(".loader_div").show();
                $.ajax({
                    url: 'PriceSummary.aspx/RequestForReApproval',
                    method: 'post',
                    datatype: 'json',
                    data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"" , comp_name:"", comp_desc:"", comp_SP:"",  end_cust_num:"", end_cust_name:"", qty:"", MOQ:"", file:"", price:"" }',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        location.reload(true);
                        jQuery(".loader_div").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                        jQuery(".loader_div").hide();
                    }
                });
            }
        }
        function DownloadItemFile(obj) {
            var filename = $("#" + obj.id).text();
            var hdnpath = obj.id.replace("lnkFile", "hdnFilePath");
            var filepath = $("#" + hdnpath).val();
            //DownloadFile(filepath, filename);
            $.ajax({
                type: "POST",
                url: "PriceSummary.aspx/DownloadFile",
                data: '{file: "' + filepath + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    debugger;
                    //Convert Base64 string to Byte Array.
                    var bytes = Base64ToBytes(r.d);

                    //Convert Byte Array to BLOB.
                    var blob = new Blob([bytes], { type: "application/octetstream" });

                    //Check the Browser type and download the File.
                    var isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, fileName);
                    } else {
                        var url = window.URL || window.webkitURL;
                        link = url.createObjectURL(blob);
                        var a = $("<a />");
                        a.attr("download", filename);
                        a.attr("href", link);
                        $("body").append(a);
                        a[0].click();
                        //$("body").remove(a);
                    }
                    if (!$.fn.DataTable.isDataTable('#MainContent_grdPriceSummary')) {
                        var head_content = $('#MainContent_grdPriceSummary tr:first').html();
                        $('#MainContent_grdPriceSummary').prepend('<thead></thead>')
                        $('#MainContent_grdPriceSummary thead').html('<tr>' + head_content + '</tr>');
                        $('#MainContent_grdPriceSummary tbody tr:first').hide();

                        var table = $('#MainContent_grdPriceSummary').dataTable({
                            // "order": [[1, 'desc']]
                        });
                    }
                    if (!$.fn.DataTable.isDataTable('#MainContent_grdItemSummary')) {
                        var head_content = $('#MainContent_grdItemSummary tr:first').html();
                        $('#MainContent_grdItemSummary').prepend('<thead></thead>')
                        $('#MainContent_grdItemSummary thead').html('<tr>' + head_content + '</tr>');
                        $('#MainContent_grdItemSummary tbody tr:first').hide();
                        var otable = $('#MainContent_grdItemSummary').dataTable({
                            //columnDefs: [{ type: 'date', 'targets': [10] }],
                            //"order": [[10, 'desc']]

                            "order": [[8, "desc"]], //or asc 
                            // "columnDefs": [{ "targets": 11, "type": "date-eu" }],
                        });
                    }
                }
            });
            
        }
        function ItemStatusLog(obj) {
            debugger;
            var item_id = obj.id.replace("imgView", "lblItem1");
            var item = $("#" + item_id).text();
            var itemdesc_id = obj.id.replace("imgView", "lblItemDesc1");
            var item_desc = $("#" + itemdesc_id).text();
            var ref_id = obj.id.replace("imgView", "lblRef");
            var ref_number = $("#" + ref_id).text();
            OpenStatusLog(item, item_desc, ref_number);

        }

        function ItemAction(obj, status) {
            debugger;
            var qty;
            var searchid = obj.id.substring(27, obj.id.lastIndexOf('_'))
            var item_id = obj.id.replace(searchid, "lblItem1");
            var item = $("#" + item_id).text();
            var itemdesc_id = obj.id.replace(searchid, "lblItemDesc1");
            var item_desc = $("#" + itemdesc_id).text();
            var ref_id = obj.id.replace(searchid, "lblRef");
            var ref_number = $("#" + ref_id).text();
            var Quote_id = obj.id.replace(searchid, "hdnitemID");
            var ID = $("#" + Quote_id).val();
            var orderType = $("#" + obj.id.replace(searchid, "lblOrderType1")).text();
           // if (orderType == "onetime")
                qty = $("#" + obj.id.replace(searchid, "lblQty1")).text();
            //else
              //  qty = $("#" + obj.id.replace(searchid, "lblQtyPerOrder1")).text();
            var approved_price = $("#" + obj.id.replace(searchid, "lblAppPrice1")).text();
            var MOQ_Escalate = $("#" + obj.id.replace(searchid, "hdnMOQ")).val();
            if (status == "Escalated") {
                return EscalateWithQty(ID, item, ref_number, status, qty, approved_price, MOQ_Escalate);
            }
            else {
                return Request(ID, item, ref_number, status);
            }
        }
        function RejectRFQ() {
            debugger;
            jQuery(".loader_div").show();
            var ref_num = $('#MainContent_hdnRejectRef').val();
            var item = $('#MainContent_hdnRejectItem').val();
            var ID = $('#MainContent_hdnRejectID').val();
            var status = $('#MainContent_hdnRejectStatus').val();
            var reason = $('#MainContent_txtRejectReason').val();
            if (IsnullOrEmpty(reason)) {
                $("#MainContent_txtRejectReason").css("border", "1px solid red");
            }
            else {
                $("#MainContent_txtRejectReason").css("border", "");
                $.ajax({
                    url: 'PriceSummary.aspx/RequestForReApproval',
                    method: 'post',
                    datatype: 'json',
                    data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", comp_name:"", comp_desc:"", comp_SP:"", end_cust_num:"", end_cust_name:"", qty:"",MOQ:"", file:"", price:""}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        location.reload(true);
                        jQuery(".loader_div").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                        jQuery(".loader_div").hide();
                    }
                });
            }
        }
        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                    // Number of digits right of decimal point.
                    (match[1] ? match[1].length : 0)
                    // Adjust for scientific notation.
                    - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function OpenStatusLog(item, item_desc, ref_number) {
            debugger;
            //var id = obj.id;
            //var quote_id = id.replace("imgView", "hdnID");
            //var quote = $("#" + quote_id).val();
            var uri = "QuoteStatusDetails.aspx?item=" + item + "&desc=" + item_desc + "&ref=" + ref_number;
            win = window.open(encodeURI(uri), "_blank", "WIDTH=1000,HEIGHT=350,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loader_div" class="loader_div"></div>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Price Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <asp:Panel ID="panelref" runat="server" CssClass="filter_panel">
                    <div class="col-md-12 nopadding">

                        <div class="col-md-3">
                            <asp:Label ID="lblRef" Style="float: right;" runat="server" Text="Requested Date Range"></asp:Label>
                        </div>
                        <div class="col-md-6">
                            <div class="controls">
                                <asp:TextBox ID="txtDateRange" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btnSubmit" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Label runat="server" ID="lblmessage"></asp:Label>
                <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                    <ul>
                        <li id="rfqList" runat="server" class="active" onclick="tabchange(this);">RFQ Wise</li>
                        <li id="itemList" runat="server" onclick="tabchange(this);">Item Wise</li>
                    </ul>
                </div>
                <div class="col-md-12 nopad" id="divRFQSummary" runat="server" style="display: block">
                    <asp:GridView ID="grdPriceSummary" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Reference Number">
                                <ItemTemplate>
                                    <asp:Label ID="lnkRef" CssClass="link" Text='<%# Bind("Ref_number")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Requested Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" Text='<%# Bind("Requested_date")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" Text='<%# Bind("Status")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
                <div class="col-md-12 nopad" id="divItemSummary" runat="server" style="display: none">


                    <asp:GridView ID="grdItemSummary" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdItemSummary_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href="#mdReason" rel="modal:open">
                                        <asp:Button ID="btnItemEscalate1" Visible='<%# Eval("EscalateFlag").ToString()=="1" ?Eval("Status").ToString() == "Escalated" || Eval("Status").ToString() == "Escalated By BM" || Eval("Status").ToString() == "Added In PA" || Eval("Status").ToString() == "Escalated & Approved" || Eval("Status").ToString() == "Approved By BM" || Eval("Status").ToString() == "Approved By SE" || Eval("Status").ToString() == "Expired" || Eval("Status").ToString() == "Sent For Approval"  || Eval("Status").ToString() == "Accepted" || Eval("Status").ToString() == "RejectedRFQ" || Eval("EscalateFlag").ToString()=="0" ? false : true : false %>' runat="server" CssClass="btnSubmit" Text="Escalate" OnClientClick="ItemAction(this,'Escalated');" Enabled='<%# Eval("Status").ToString() == "Rejected"? false : true %>' ToolTip='<%# Eval("Status").ToString() == "Rejected"? "Escalate Option will not be available for Rejected quote" : "" %>' /></a>
                                    <a href="#mdRejectReason\" rel="modal:open">
                                        <asp:Button runat="server" ID="btnItemAccept" Visible='<%# Eval("EscalateFlag").ToString()=="1" ? Eval("Status").ToString() == "Escalated & Approved" || Eval("Status").ToString() == "Approved"  ? true : false : false %>' CssClass="btnSubmit" Text="Accept" OnClientClick="ItemAction(this,'Accepted');" />
                                        <asp:Button runat="server" ID="btnRequest" Visible='<%# Eval("EscalateFlag").ToString()=="1"? !(Eval("Status").ToString() == "Expired" || Eval("Status").ToString() == "RejectedRFQ") ? false : true : false %>' CssClass="btnSubmit" Text="Request for approval" OnClientClick="ItemAction(this,'Request For ReApproval');" />
                                        <asp:Button ID="btnItemRejectRFQ" Visible='<%# Eval("EscalateFlag").ToString()=="1"? Eval("Status").ToString() == "Expired" || Eval("Status").ToString() == "Escalated By BM" || Eval("Status").ToString() == "Escalated" || Eval("Status").ToString() == "Added In PA" || Eval("Status").ToString() == "Rejected" ||Eval("Status").ToString() == "Price Quoted" || Eval("Status").ToString() == "Approved By BM" || Eval("Status").ToString() == "Approved By SE" || Eval("Status").ToString() == "Sent For Approval" || Eval("Status").ToString() == "Accepted" || Eval("Status").ToString() == "RejectedRFQ" ? false : true : false %>' runat="server" CssClass="btnSubmit" Text="Reject" OnClientClick="ItemAction(this,'RejectedRFQ');" /></a>

                                    <%--  "<input type=\"button\" id=\"btnRequest\" value=\"Request for approval\" class=\"btnSubmit\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Request For ReApproval');\" /></a>";
                                    --%>
                                    <asp:Image runat="server" ImageUrl="images/info.jpg" ID="imgView" onclick="ItemStatusLog(this);" title="View Status Log" Style="height: 25px;" />
                                    <%--<img src="images/info.jpg" style="height:25px;" title="View Status Log" id="imgView" onclick="ItemStatusLog(this);" />--%>
                                    <br />
                                <%--     <asp:HiddenField ID="hdnFilePath" runat="server" Value='<%# Bind("Escalation_file") %>' />--%>
<%--                                    <asp:LinkButton runat="server" ID="lnkFile" Text='<%# Bind("Escalation_file_name") %>' OnClientClick="DownloadItemFile(this);"></asp:LinkButton>--%>
               <asp:LinkButton ID="lnkFile" runat="server" CommandArgument='<%# Eval("Escalation_file") %>' Text='<%# Eval("Escalation_file_name") %>' OnCommand="lnkFile_Command"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Reference Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblRef" CssClass="link" Text='<%# Bind("Ref_number")%>' runat="server"></asp:Label>
                                    <asp:HiddenField ID="hdnitemID" Value='<%# Bind("ID")%>' runat="server" />
                                    <asp:HiddenField ID="hdnMOQ" Value='<%# Bind("MOQ_Escalate")%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblItem1" Text='<%# Bind("Item_code")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemDesc1" Text='<%# Bind("Item_Desc")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="WHS">
                                <ItemTemplate>
                                    <asp:Label ID="lblWHS1" Text='<%# Bind("WHS")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <%--   <asp:TemplateField HeaderText="Order Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderType1" Text='<%# Bind("Order_type")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Approved Qty">
                                <ItemTemplate>
                                    <asp:Label ID="lblQty1" Text='<%# Bind("Approved_OrderQty")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <%--  <asp:TemplateField HeaderText="Qty per Order">
                                <ItemTemplate>
                                    <asp:Label ID="lblQtyPerOrder1" Text='<%# Bind("QTY_perOrder")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Requested Price">
                                <ItemTemplate>
                                    <asp:Label ID="lblExpPrice1" Text='<%# Bind("Expected_price")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Approved Price">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppPrice1" Text='<%# Bind("New_OfferPrice")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Validity(in Days)">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderValidity1" Text='<%# Bind("Order_Validity")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Requested Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblReqDate1" Text='<%# Bind("Requested_date")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus1" Text='<%# Bind("StatusName")%>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <%--      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                <ContentTemplate>--%>
            <div id="divdetail" style="display: none;">
                <table id="grdDetailedPriceSummary1" class="display responsive nowrap" cellpadding="0" cellspacing="0">
                    <thead style="background-color: #DC5807; color: White; font-weight: bold">
                        <tr style="border: solid 1px #000000">
                            <td>Action</td>
                            <td>Item_code</td>
                            <td>Item_Desc</td>
                            <td>WHS</td>
                          <%--  <td>Order Type</td>--%>
                            <%--<td>Order Frequency</td>--%>

                            <td>Approved Qty</td>
                         <%--   <td>QTY Per Order</td>--%>
                            <%--<td>Requested Price</td>--%>
                            <td>Approved Price</td>
                            <td>Validity(in Days)</td>
                            <%--<td>List Price</td>
                            
                            <td>Discount Rate(%)</td>--%>
                            <td>Uploaded File</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <div id="divplace" style="display: none;">fdgdsgfdg</div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">End Customer : </p>
            </div>
            <div class="col-md-7">
                <asp:DropDownList ID="ddlEndCustomer" runat="server" CssClass="ddl" Style="width: 200px;">
                </asp:DropDownList>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Existing Company : </p>
            </div>
            <div class="col-md-7">
                <asp:DropDownList ID="ddlCompanyName" runat="server" CssClass="ddl" Style="width: 200px;">
                </asp:DropDownList>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Existing Product : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtDescription" Style="width: 200px;" CssClass="ddl" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Sales Price : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtCompanySP" onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>

            </div>
            <div class="col-md-5">
                <p class="popupControl">Quantity : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtReqQty" onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>

            </div>
            <div class="ExpPriceDiv">
            <div class="col-md-5">
                <p class="popupControl">Expected Price : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtExpPrice" onkeypress="return isNumberKey(event,this);" CssClass="ddl" Style="width: 80px;" runat="server"></asp:TextBox>

            </div></div>
            <div class="col-md-5">
                <p class="popupControl">Reason For Escalation : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtReason" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-12">
                <asp:FileUpload runat="server" ID="fileUpload" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <asp:HiddenField ID="hdnID1" runat="server" />
                <asp:HiddenField ID="hdnRef1" runat="server" />
                <asp:HiddenField ID="hdnItem1" runat="server" />
                <asp:HiddenField ID="hdnStatus1" runat="server" />
                <asp:HiddenField ID="hdnApprovedPrice1" runat="server" />
                <asp:HiddenField ID="hdnMOQ1" runat="server" />
            </div>
            <div class="col-md-7">
                <input type="button" id="btnSubmitReason" class="btnSubmit" onclick="Escalate();" title="Escalate" value="Escalate" />
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>
    <div id="mdRejectReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls">

            <div class="col-md-5">
                <p class="popupControl">Reason For Rejection : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtRejectReason" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <asp:HiddenField ID="hdnRejectID" runat="server" />
                <asp:HiddenField ID="hdnRejectRef" runat="server" />
                <asp:HiddenField ID="hdnRejectItem" runat="server" />
                <asp:HiddenField ID="hdnRejectStatus" runat="server" />
            </div>
            <div class="col-md-7">
                <input type="button" id="btnRejectRFQ1" class="btnSubmit" onclick="RejectRFQ();" title="Reject" value="Reject" />
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>
    <div id="ex1" class="modal" style="border: solid 1px #008a8a;">

        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">MOQ : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtMOQ" ReadOnly="true" runat="server"></asp:TextBox>
            </div>
        </div>

        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Order Type : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ReadOnly="true" ID="lblOrderType" runat="server"></asp:TextBox>
            </div>

        </div>
        <div class="col-md-12 controls" id="divSchedule" style="display: none;">

            <div class="col-md-5">
                <p class="popupControl">Order Frequency : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ReadOnly="true" ID="txtFrequency" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-5">
                <p class="popupControl">Quantity Per Shipment : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls">
        </div>
        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Request date: </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtOrderDate" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Purchase Order Comment: </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtPOComment" TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <asp:HiddenField ID="hdnCustNum" runat="server" />
                <asp:HiddenField ID="hdnCustName" runat="server" />
                <asp:HiddenField ID="hdnID" runat="server" />
                <asp:HiddenField ID="hdnRef" runat="server" />
                <asp:HiddenField ID="hdnItem" runat="server" />
            </div>
            <div class="col-md-7">
                <%--CausesValidation="false" --%>
                <input type="button" id="btnOrder" class="btnSubmit" onclick="Order();" title="Place Order" value="Place Order" />
                <%--<asp:Button ID="btnPlace"  Text="Place Order" runat="server" CssClass="btnSubmit" OnClick="btnPlace_Click" />--%>
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>


    <%--             <div id="myModal" style="display:none;" >
        <div>
            <span class="close">&times;</span>
            <p>Quantity : </p>
            <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
            <asp:HiddenField ID="hdnRef" runat="server" />
            <asp:HiddenField ID="hdnItem" runat="server" />
            <asp:HiddenField ID="hdnType" runat="server" />
            <asp:Button ID="btnPlace" Text="Place Order" runat="server"  CssClass="btnSubmit" />
           
        </div>
    </div>--%>
    <%-- </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPlace" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
