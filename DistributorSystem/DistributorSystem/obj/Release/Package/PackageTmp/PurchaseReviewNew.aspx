﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseReviewNew.aspx.cs" MasterPageFile="~/MasterPage.Master" Inherits="DistributorSystem.PurchaseReviewNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="js/app.js"></script>
    <script src="js/gridscroll.js"></script>
    <link href="css/GridviewScroll.css" rel="stylesheet" />
    <style type="text/css">
        td
        {
            height: 48px !important;
        }
        /*body{
        overflow:hidden !important;
        }*/
        .noclose .ui-dialog-titlebar-close
        {
            display: none;
        }

        .ui-dialog .ui-dialog-titlebar
        {
            padding-left: 45px;
            text-align: center !important;
        }
        /*.col-md-4 {
         width: 33.3333% !important;
        }*/

        #MainContent_grdviewAllValues_Image1_0
        {
            display: none;
        }

        #MainContent_grdviewAllValues td
        {
            text-align: right;
        }

        #MainContent_rbtnlistComp td
        {
            height: 0px !important;
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Summary</a>
                        </li>
                        <li class="current">Purchase Budget Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
           
            <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <%--     CssClass="table table-bordered table-hover responsive" --%>
                <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" AutoGenerateColumns="False" OnRowDataBound="grdviewAllValues_RowDataBound" ShowHeader="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/button_plus.gif" Visible='<%# (Eval("sumFlag").ToString() == "SubFamilyHeading") || (Eval("sumFlag").ToString() == "FamilyHeading") || (Eval("sumFlag").ToString() == "HidingHeading") %>' ImageAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                      <%--  <asp:TemplateField HeaderText="GOLD">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblgold" Text='<%#( Eval("gold_flag").ToString()=="" ?"" :"GOLD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TOP">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbltop" Text='<%# (Eval("top_flag").ToString()=="" ?"" :"TOP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="5yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl5yrs" Text='<%# (Eval("five_years_flag").ToString()=="" ?"" :"5yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BB">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblbb" Text='<%# (Eval("bb_flag").ToString()=="" ?"" :"BB") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SPC">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblspc" Text='<%#( Eval("SPC_flag").ToString()=="" ?"" :"SPC") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="1" ItemStyle-Width="35px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ins" Text="Ins." Visible='<%# (Eval("insert_or_tool_flag").ToString() =="I" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="lbl_tool" Text="P" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="P" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label1" Text="S" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="S" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label2" Text="Tools" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="T" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label3" Text="X" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="X" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label4" Text="" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label5" Text='<%# (Eval("item_sub_family_id").ToString()) %>' Visible='<%# (Eval("insert_or_tool_flag").ToString() =="SubFamHeading") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductcode" Text='<%# Eval("item_code".ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T_CLAMP_PARTING_OFF ffffffffffffffffffffffff" ItemStyle-Width="140px"><%--HeaderStyle-CssClass="greendark">--%>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblFmilyName" Text='<%# (Eval("item_family_name").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "FamilyHeading" || Eval("sumFlag").ToString() == "HidingHeading"  %>' CssClass="productLabel"></asp:Label>
                                <asp:Label runat="server" ID="lblProductName" Text='<%# (Eval("item_description").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSumFlag" runat="server" Text='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="color_3" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_item_code" Text='<%# (Eval("item_code").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Actual Quality--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_1" Text='<%# (Eval("sales_qty_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_0" Text='<%# (Eval("sales_qty_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualQuantity_sales_qty_year_0" Value='<%# (Eval("sales_qty_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_ActualQuantity_sales_qty_year_P"  MaxLength="6" Text='<%# Eval("estimate_qty_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" &&Eval("sumFlag").ToString() != "HidingHeading" && Eval("sumFlag").ToString() != ""%>' Width="90px"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_P" Text='<%# Eval("estimate_qty_next_year") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="checkbox_ActualQuantity_sales_qty_year_P" Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading"  && Eval("sumFlag").ToString() != "HidingHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "typeSum" && Eval("sumFlag").ToString() != "SubFamilySum" && Eval("sumFlag").ToString() != "FamilySum"%>'
                                    Checked='<%# Eval("review_flag").ToString() == "Q" || Eval("review_flag").ToString() == "QV"%>' Enabled="true" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%-- Inserts per tool --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_1" Text='<%# (Eval("insertpertool_year_1").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_1" Value='<%# (Eval("insertpertool_year_1").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_0" Text='<%# (Eval("insertpertool_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_0" Value='<%# (Eval("insertpertool_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_next_year" Text='<%# (Eval("insertpertool_next_year").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_next_year" Value='<%# (Eval("insertpertool_next_year").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Variance of Quality--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_1" Text='<%# Eval("QuantityVariance_sales_qty_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_0" Text='<%# Eval("QuantityVariance_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Percentage of Quantiity--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_1" Text='<%# Eval("QuantityPercentage_sales_qty_year_1") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_0" Text='<%# Eval("QuantityPercentage_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Actual Value--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_1" Text='<%# (Eval("sales_value_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_0" Text='<%# (Eval("sales_value_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualValue_sales_value_year_0" Value='<%# (Eval("sales_value_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P_New" Text='<%# Eval("estimate_value_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"></asp:Label>
                                <asp:TextBox runat="server" ID="txt_ActualValue_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("estimate_value_next_year") %>' Style="display: none"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P" Text='<%# (Eval("estimate_value_next_year").ToString()) %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Percentage Value--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_1" Text='<%# Eval("ValuePercentage_sales_value_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_0" Text='<%# Eval("ValuePercentage_sales_value_year_0")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price per Unit --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_1" Text='<%#Eval("PricePerUnit_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_0" Text='<%#Eval("PricePerUnit_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_PricePerUnit_sales_value_year_0" Value='<%#Eval("PricePerUnit_sales_value_year_0") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_PricePerUnit_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("estimate_rate_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_P" Text='<%#Eval("PricePerUnit_sales_value_year_P") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price Changes --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_2" Text='<%#Eval("priceschange_sales_value_year_2") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_1" Text='<%#Eval("priceschange_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_0" Text='<%#Eval("priceschange_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_priceschange_sales_value_year_0" Value='<%#Eval("priceschange_sales_value_year_0") %>' />
                                <%--Hidden values--%>
                                <asp:Label runat="server" ID="lbl_Family_Id" Text='<%# Eval("item_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_Sub_Family_Id" Text='<%# Eval("item_sub_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_customer_number" Text='<%# Eval("customer_number").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblItemCode" Text='<%# (Eval("item_code").ToString()) %>' Style="display: none"> </asp:Label>
                                <asp:Label runat="server" ID="lbl_SumFlag" Text='<%# Eval("sumFlag").ToString()%>' Style="display: none" />
                                <asp:Label runat="server" ID="lbl_BudgetId" Text='<%# Eval("budget_id").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_SumFlag" Value='<%# Eval("sumFlag").ToString()%>' />
                                <asp:Label runat="server" ID="lbl_ReviewFlag" Text='<%# Eval("review_flag").ToString()%>' Style="display: none" />
                                <asp:Label runat="server" ID="lbl_BM_flag" Text='<%# Eval("status_ho").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_BM_flag" Value='<%# Eval("status_ho").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>


            <div id="dvResultPopUp" style="width: 100%; display: none; text-align: center !important;">
                <br />

                <asp:Label runat="server" ID="lblResult" Style="font-weight: 800;"></asp:Label>

                <br />
                <br />
            </div>

        </ContentTemplate>
        <Triggers>
        </Triggers>



    </asp:UpdatePanel>



    <%--  <a style="display: inline;" class="scrollup" href="javascript:void(0);">Scroll</a>--%>


    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>



    <script>
        $(document).ready(function () {
            triggerPostGridLodedActions();
            $(window).resize(function () {
                triggerPostGridLodedActions();
            });
         
        });
        var divPopUp;
        function DivPopUpOpen() {
            dclg = $("#dvResultPopUp").dialog(
                       {
                           resizable: false,
                           draggable: true,
                           modal: true,
                           title: "Select to Proceed",
                           width: "500",
                           //height: "150",
                           closeOnEscape: false,
                           //beforeClose: function (event, ui) { return false; },
                           dialogClass: "noclose",

                       });
            divPopUp = dclg;
            dclg.parent().appendTo(jQuery("form:first"));
        }

       
        function triggerPostGridLodedActions() {
            $('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
                var attr = $('#Imgcollapsedropdwns').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
                } else {
                    $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
                }
            });
            //$("#MainContent_ddlCustomerList").searchable();
            //$("#MainContent_ddlCustomerNumber").searchable();

            //$('#MainContent_ddlCustomerNumber').change(function () {

            //    var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
            //    $("#MainContent_ddlCustomerList").val(ddlslectedText);
            //});
            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });
            gridviewScroll();
            bindToogleForRowIndex();
            bindToogleForParentRowIndex();

          
            //Quantity changes Start
            jQuery(".product_type").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type");
                    var grandTotal = 0;
                    jQuery(".product_type_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_sub_total_" + currentIndex).val(grandTotal).change();
                });
            });

            jQuery(".product_type_sub_total").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type_sub_total_index");
                    var grandTotal = 0;
                    jQuery(".product_type_sub_total_row_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_SubFamilySum_" + currentIndex).val(grandTotal).change();
                });
            });
    

            $("[id*=txtBudgetInc]").bind("change", function () {
                debugger;
                var incBudget = $("[id*=txtBudgetInc]").val();
                var rowsCount = $('#MainContent_grdviewAllValues tr').length;
                var gridViewCtl = document.getElementById('MainContent_grdviewAllValues');
                if (incBudget == "") {
                    for (var i = 0; i < rowsCount; i++) {
                        $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val("");
                    }
                    return;
                }

                for (var i = 0; i < rowsCount; i++) {
                    var result = 0;
                    var qty_P = $('#MainContent_grdviewAllValues_hdn_ActualQuantity_sales_qty_year_0_' + i).val(); //2015 Next Year qty                   
                    if (!isNaN(qty_P) && (qty_P != "") && (qty_P != 0) && (qty_P != null)) {

                        result = (1 + (incBudget / 100)) * qty_P;
                        result = Math.round(result);
                        if (i == 1 || i == rowsCount - 1) {
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').attr('disabled', false);
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').val(result);
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').attr('disabled', true);
                        }
                        $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val(result).change();
                    }

                }

                //for (var i = 0; i < rowsCount; i++) { }


                trigger_grand_total_manupulation(false);

            });

            var td = $("#MainContent_grdviewAllValuesCopy").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
            td = $("#MainContent_grdviewAllValuesCopyFreeze").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
        }

        function alertforRquired() { alert('Quantity/Value should not be Zero or Empty '); }

        function gridviewScroll() {

            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 70,
                height: findminofscreenheight($(window).height(), 460),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 9,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 2,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }
        function findminofscreenheight(a, b) {
            //a = a - $("#MainContent_ddlCustomerList").offset().top;
            return a < b ? a : b;
        }

        $('#MainContent_grdView_T_Clamp_Partingoff tr:last').addClass("last_row");


        //jQuery(".parent_row_index td div img").click();
        function bindToogleForRowIndex() {
            jQuery(".row_index_image").click(function () {

                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        minimised = true;
                    }
                    else {
                        jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                }
                else {
                    jQuery(this).attr("src", "images/button_minus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
            });

            //jQuery(".row_index_image").click();
        }


        function bindToogleForParentRowIndex() {
            jQuery(".parent_row_index td div img").click(function () {

                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideUp();
                    }
                    else {
                        jQuery(this).slideDown();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideDown();
                        minimised = true;
                    }
                });
                if (minimised)
                    jQuery(this).attr("src", "images/button_minus.gif");
                else
                    jQuery(this).attr("src", "images/button_plus.gif");
            });
            //jQuery(".parent_row_index td div img").click();
        }

        //toogle row hide and show

        jQuery(".row_index").click(function () {

            var currentIndex = jQuery(this).data("index");

            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none")
                    jQuery(this).slideUp();
                else
                    jQuery(this).slideDown();
            });
        });


        function isNumberKey(e) {

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }

            return true;
        }



    </script>




</asp:Content>
