﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UserDetailsWithMenu.aspx.cs" Inherits="DistributorSystem.UserDetailsWithMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function () {

            //$('.MenuClass').find(':checkbox').each(function () {

            //    this.checked = true;

            //});
            //$("[class*='submenuclass']").find(':checkbox').each(function () {

            //    this.checked = true;

            //});
            //$("input[id='MainContent_selectAll']").prop('checked', true);
            var head_content = $('#MainContent_grdUserDetails tr:first').html();
            $('#MainContent_grdUserDetails').prepend('<thead></thead>')
            $('#MainContent_grdUserDetails thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdUserDetails tbody tr:first').hide();
            $('#MainContent_grdUserDetails').DataTable(
                {
                });
            var $RowSelected = $("#MainContent_Panel1");

            if ($RowSelected.length > 0) {
                $("[class*=submenuclass]").css({ "margin-left": "50px" });
            }
        });
        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_grdUserDetails tr:first').html();
            $('#MainContent_grdUserDetails').prepend('<thead></thead>')
            $('#MainContent_grdUserDetails thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdUserDetails tbody tr:first').hide();
            $('#MainContent_grdUserDetails').DataTable(
                {
                });

        }

        //$(document).on("click", ".MenuClass", function (e) {
        //    var menuclass = $(this).text();
        //    var id2 = this.id;
        //    var j = 1;
        //    menuclass = menuclass + "submenuclass";
        //    var totaltrCount = $("[class*=" + menuclass + "]").length;
        //    for (var i = 1; i <= totaltrCount; i++) {
        //        var prop = $(this).attr("checked");
        //        if ($(this).is(":checked") == true) {
        //            var val = parseInt(j) + parseInt(i);
        //            var idval = "MainContent_" + menuclass + val;
        //            $("#" + idval).attr("checked", false);
        //        }
        //        else {
        //            var val = parseInt(j) + parseInt(i);
        //            var idval = "MainContent_" + menuclass + val;
        //            $("#" + idval).attr("checked", true);
        //        }

        //    }

        //});



        $(document).on("change", 'input[type="checkbox"]', function (e) {
            var id2 = this.id;
           
            if (id2.indexOf("submenuclass") != -1) {
                var class2 = id2.split('_');
                var count = 0;
                var submenuclass = class2[1] + "submenuclass";
                var menuclass = "MainContent_" + class2[1] + "_submenuclass_";
                if ($(this).is(":checked") == true) {
                    $("[class*='MenuClass_" + class2[1] + "']").find(':checkbox').each(function () {

                        this.checked = true;

                    });
                    $("input[id=" + id2 + "]").prop('checked', true);
                }
                else {
                    $("input[id=" + id2 + "]").prop('checked', false);
                    var len = $("[class*='" + submenuclass + "']").find(":checkbox").length;
                    $("[class*='" + submenuclass + "']").find(":checkbox").each(function () {

                        if ($(this).prop("checked") == false) {
                            count++;
                        }
                    })

                    if (count == len) {
                        $("[class*='MenuClass_" + class2[1] + "']").find(':checkbox').each(function () {

                            this.checked = false;

                        });
                    }
                    else {
                        $("[class*='MenuClass_" + class2[1] + "']").find(':checkbox').each(function () {

                            this.checked = true;

                        });
                    }
                   
                }
            }

            else if (id2.indexOf("selectAll") != -1) {
                if ($(this).is(":checked") == true) {

                    //$('.MenuClass').find(':checkbox').each(function () {

                    //    this.checked = true;

                    //});
                    $("[class*='MenuClass']").find(':checkbox').each(function () {

                        this.checked = true;

                    });
                    $("[class*='submenuclass']").find(':checkbox').each(function () {

                        this.checked = true;

                    });

                }
                else {
                    //$('.MenuClass').find(':checkbox').each(function () {

                    //    this.checked = false;

                    //});
                    $("[class*='MenuClass']").find(':checkbox').each(function () {

                        this.checked = false;

                    });
                    $("[class*='submenuclass']").find(':checkbox').each(function () {

                        this.checked = false;

                    });
                }

            }
            else {
                var class2 = id2.split('_');
                var menuclass = "MainContent_" + class2[1] + "_submenuclass_";

                var submenuclass = class2[1];
                var totaltrCount = $("[class*=" + submenuclass + "]").length;
                if ($(this).is(":checked") == true) {
                    $("[class*=" + submenuclass + "]").find(':checkbox').each(function () {

                        this.checked = true;

                    });
                }
 else {
                    $("[class*=" + submenuclass + "]").find(':checkbox').each(function () {

                        this.checked = false;

                    });
                }

            }

        });

        function isNumberKey(evt, obj) {

            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                    // Number of digits right of decimal point.
                    (match[1] ? match[1].length : 0)
                    // Adjust for scientific notation.
                    - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 2) {
                    if (charCode == 46) return true;
                    else return false;
                }

            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function checkPasswordMatch() {
            var pwd = $('#MainContent_txtpwd').val();
            var repwd = $('#MainContent_txtrepwd').val();
            if (pwd == repwd) {
                $('#MainContent_lblpwd').text('');
                return true;

            }
            else {
                $('#MainContent_lblpwd').text('Password does not match.');
                return false;
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validateControls() {
            //debugger
            var err_flag = 0;

            if ($('#MainContent_txtpwd').val() == "") {
                $('#MainContent_txtpwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtpwd').css('border-color', '');
            }

            if ($('#MainContent_txtrepwd').val() == "") {
                $('#MainContent_txtrepwd').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtrepwd').css('border-color', '');
            }

            if ($('#MainContent_txtEmailId').val() == "") {
                $('#MainContent_txtEmailId').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                if (!validateEmail($('#MainContent_txtEmailId').val())) {
                    $('#MainContent_txtEmailId').css('border-color', 'red');

                    err_flag = 1;
                }
                else
                    $('#MainContent_txtEmailId').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return checkPasswordMatch();
            }

            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields/valid data.');
                return false;
            }
        }
        function getMenuDetails() {
            var output = validateControls();
            if (output == true) {
                var menuids = [];
                var lblDistributorNumber = $("#MainContent_lblDistributorNumber").text();
                var txtEmailId = $("#MainContent_txtEmailId").val();
                var txtpwd = $("#MainContent_txtpwd").val();
                if ($("input[id='MainContent_chkRole']").prop("checked") == true) {
                    var chkRole = "Admin";
                }
                else {
                    chkRole = "";
                }
                if ($("input[id='MainContent_chkStatus']").prop("checked") == true) {
                    var chkStatus = 1;
                }
                else {
                    chkStatus = 0;
                }
                //$('.MenuClass').find(':checkbox').each(function () {
                //    if ($(this).prop("checked") == true) {
                //        var menuid = this.id;
                //        menuid = menuid.split('_');
                //        menuids.push(menuid[menuid.length - 1]);
                //    }

                //});
                $("[class*='MenuClass']").find(':checkbox').each(function () {
                    if ($(this).prop("checked") == true) {
                        var menuid = this.id;
                        menuid = menuid.split('_');
                        menuids.push(menuid[menuid.length - 1]);
                    }

                });
                $("[class*='submenuclass']").find(':checkbox').each(function () {

                    if ($(this).prop("checked") == true) {
                        var menuid = this.id;
                        menuid = menuid.split('_');
                        menuids.push(menuid[menuid.length - 1]);
                    }

                });
                $.ajax({
                    url: 'UserDetailsWithMenu.aspx/SaveMenuDetails',
                    method: 'post',
                    datatype: 'json',
                    //data: "{'custnum':'" + JSON.stringify(custnumaarry) + "','quantity':'" + JSON.stringify(quantityarry) + "','value':'" + JSON.stringify(valuearry) + "','month':'" + JSON.stringify(montharry) + "'}",
                    data: "{'menuids':'" + JSON.stringify(menuids) + "','lblDistributorNumber':'" + lblDistributorNumber + "','txtEmailId':'" + txtEmailId + "','txtpwd':'" + txtpwd + "','chkRole':'" + chkRole + "','chkStatus':'" + chkStatus + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        alert(data.d);
                        window.location.href ="UserDetailsWithMenu.aspx";
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields/valid data.');
                return false;
                //window.location.reload();
            }
        }
    </script>
    <style>
        .mn_popup {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table {
            border-color: white !important;
        }

        .mn_margin {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result {
            margin-left: 45%;
            font-weight: bold;
        }

        #pnlData {
            width: 70%;
        }

        .control {
            padding: 9px;
            border-bottom: solid 1px #ddd;
            /*background-color: #eaeaea;*/
        }

        .panelclass {
            margin-top: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Users</a>
                        </li>
                        <li class="current">Users and Menu Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="updpanel">
        <ContentTemplate>

            <div class="col-md-12 filter_panel">
                <asp:Label ID="Label1" Style="font-weight: bolder;" runat="server">User Details : </asp:Label>
                <%-- <div class="col-md-3">
                    <asp:Button runat="server" ID="btnEdit" CssClass="btnSubmit" OnClick="btnEdit_Click" Text="Edit" />

                    <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" OnClick="btnSave_Click" Text="Save" />
                </div>--%>
            </div>
            <div class="col-md-6 mn_margin">


                <asp:Panel runat="server" ID="pnlData" Style="height: 504px; border: solid 1px #ddd; padding: 10px 15px; margin-top: 50px;">
                    <asp:GridView ID="grdUserDetails" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowCommand="grdUserDetails_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="User Id">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("Distributor_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("Distributor_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Id">
                                <ItemTemplate>
                                    <asp:Label ID="txtEmail" runat="server" Text='<%#Bind("Email_id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%#Bind("Role") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit-icon.png" ID="imgAction" CommandArgument='<%# Bind("Distributor_number") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div class="col-md-3 mn_margin" style="margin-top: 4%; border: solid 1px #ddd; padding: 10px 15px; height: 504px;">

                <asp:Panel runat="server" ID="pnlEdit">

                    <div class="col-md-12 filter_panel">
                        <asp:Label runat="server" Style="font-weight: bolder;" Text="User Details Update"></asp:Label>
                    </div>
                    <div class="col-md-12 nopad">
                        <div class="col-md-12 control">
                            <div class="col-md-4">
                                <asp:Label runat="server" Text="User Id : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:Label ID="lblDistributorNumber" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-4">
                                <asp:Label runat="server" Text="User Name : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="lblDistributorName"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="Label3" Text="Email Id : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtEmailId"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-5">
                                <asp:Label runat="server" Text="Password : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtpwd" Style="margin-left: -24px;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-5">
                                <asp:Label runat="server" Text="Re-Type Password : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtrepwd" Style="margin-left: -24px;"></asp:TextBox>
                                <asp:Label runat="server" ID="lblpwd" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="Label4" Text="Status : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:CheckBox runat="server" ID="chkStatus" Text="Active" />
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-4">
                                <asp:Label runat="server" ID="Label5" Text="Role : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:CheckBox runat="server" ID="chkRole" Text="Admin" />
                            </div>
                        </div>

                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-3 mn_margin" style="margin-top: 4%;">

                <asp:Panel runat="server" ID="Panel1" Style="height: 504px; overflow: auto; border: solid 1px #ddd; padding: 10px 15px;">
                    <div class="col-md-12 filter_panel" id="dfs" style="position: absolute; width: 285px;">
                                <asp:Label runat="server" Style="font-weight: bolder;" Text="Menu and Sub Menu Details Update"></asp:Label>
                             
                            </div>
                </asp:Panel>
            </div>
            <div class="col-md-8" style="padding: 9px;float: right;">
                <div class="col-md-4">
                    <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                </div>
                <div class="col-md-4">
                    <%--<asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btnSubmit" OnClick="btnUpdate_Click" OnClientClick="return validateControls();" />--%>
                    <asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btnSubmit" OnClientClick="return getMenuDetails();" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
            <asp:PostBackTrigger ControlID="grdUserDetails" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; visibility:visible" >
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff" >Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
