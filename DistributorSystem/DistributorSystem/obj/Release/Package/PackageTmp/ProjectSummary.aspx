﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectSummary.aspx.cs" Inherits="DistributorSystem.SalesBudgetSummary" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Summary</a>
                        </li>
                        <li class="current">Project Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
   <div class="mn_sales">
  <%--<div class="mn_header"> <img src="images/head.png" alt=""> </div>
  --%>
  <div class="col-md-12 nopadding">
    <div class="mn_box">
      <div class="col-md-3">
        <div class="form-horizontal">
          <div>
          <h2>CUSTOMER</h2>
          <div class="control-group">

            <label class="control-label">Customer Name</label>
            <div class="controls">
                <asp:DropDownList runat="server" ID="ddlCustomer" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">

                </asp:DropDownList>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Customer Class </label>
            <div class="controls">
                <asp:TextBox runat="server" ID="txtCustClass" Enabled="false"></asp:TextBox>            
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Industry </label>
            <div class="controls">
                <asp:TextBox runat="server" ID="txtIndustry" Enabled="false"></asp:TextBox>
            </div>
          </div>
          </div>
        </div>
      </div>
      
      <div class="col-md-6">
        <div class="form-horizontal">
          <h2> PROJECT DETAILS</h2>
        </div>
        <div class="form-horizontal">
          <div class="col-md-6 nopadding">
            <div>
            <div class="control-group">
              <label class="control-label">Project Title </label>
              <div class="controls">
                <asp:DropDownList runat="server" ID="ddlProject" AutoPostBack="true" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged"></asp:DropDownList>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Project TYPE</label>
              <div class="controls">
                <asp:TextBox runat="server" ID="txtProjType" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Existing Brand</label>
              <div class="controls">
                <asp:TextBox runat="server" ID="txtExistingBrand" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Component </label>
              <div class="controls">
                <asp:TextBox runat="server" ID="txtComponent" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Competition Spec </label>
              <div class="controls">
                <asp:TextBox runat="server" ID="txtCompetitionSpec" Enabled="false"></asp:TextBox>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-6 nopadding">
            
            <div class="clearfix"></div>
            <div class="control-group">
              <label class="control-label">No.of Stages </label>
              <div class="controls">
               <asp:TextBox runat="server" ID="txtStages" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Target Date</label>
              <div class="controls">
               <asp:TextBox runat="server" ID="txtTargetDate" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Owner</label>
              <div class="controls">
               <asp:TextBox runat="server" ID="txtOwner" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Reviewer</label>
              <div class="controls">
               <asp:TextBox runat="server" ID="txtReviewer" Enabled="false"></asp:TextBox>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Escalate to</label>
              <div class="controls">
                <asp:TextBox runat="server" ID="txtExcaleteTo" Enabled="false"></asp:TextBox>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-horizontal">
          <div>
          <!-- Address form -->
          <h2>ANNUAL BUSINESS VALUE (LAKHS)</h2>
          <!-- address-line1 input-->
          <div class="control-group">
            <label class="control-label">Customer Potential</label>
            <div class="controls">
              <asp:TextBox runat="server" ID="txtCustPotential" Enabled="false"></asp:TextBox>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Project Potential</label>
            <div class="controls">
              <asp:TextBox runat="server" ID="txtProjPotential" Enabled="false"></asp:TextBox>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Business Expected</label>
            <div class="controls">
              <asp:TextBox runat="server" ID="txtBusinessExpected" Enabled="false"></asp:TextBox>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- price & service -->
  <div class="col-md-12 nopadding">
  <div class="mn_box_new">
    <h2 id="details">Stages</h2>
    <br/>
    <!-- Pack 1-->
      <asp:Panel runat="server" ID="stage1" Visible="false">
    <div class="col-md-3" id="home-box" >
      <div class="pricing_header">
        <h2>Stage 1</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
         <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i><b> GOAL :</b><asp:Label runat="server" ID="lblGoal1"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget1"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp1"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS :</b><asp:Label runat="server" ID="lblremarks1"></asp:Label></li>
     </ul>
    </div>
          </asp:Panel><asp:Panel runat="server" ID="stage2" Visible="false">
    <!-- Pack 2-->
    <div class="col-md-3" id="Div1">
      <div class="pricing_header">
        <h2>Stage 2</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
        <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal2"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget2"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp2"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks2"></asp:Label></li>
      </ul>
    </div>
               </asp:Panel><asp:Panel runat="server" ID="stage3" Visible="false">
    <!-- Pack 3-->
    <div class="col-md-3" id="Div2">
      <div class="pricing_header">
        <h2>Stage 3</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
        <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal3"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget3"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp3"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks3"></asp:Label></li>
      </ul>
    </div>
                    </asp:Panel><asp:Panel runat="server" ID="stage4" Visible="false">
    <!-- Pack 4-->
    <div class="col-md-3" id="Div3">
      <div class="pricing_header">
        <h2>Stage 4</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
      <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal4"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget4"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp4"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks4"></asp:Label></li>
       </ul>
    </div>
                         </asp:Panel><asp:Panel runat="server" ID="stage5" Visible="false">
    <!-- Pack 5-->
    <div class="col-md-3" id="Div4">
      <div class="pricing_header">
        <h2>Stage 5</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
      <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal5"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget5"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp5"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks5"></asp:Label></li>
      </ul>
    </div>
                              </asp:Panel><asp:Panel runat="server" ID="stage6" Visible="false">
    <!-- Pack 6-->
    <div class="col-md-3" id="Div5">
      <div class="pricing_header">
        <h2>Stage 6</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
      <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal6"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget6"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp6"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks6"></asp:Label></li>
       </ul>
    </div>
                                   </asp:Panel><asp:Panel runat="server" ID="stage7" Visible="false">
    <!-- Pack 7-->
    <div class="col-md-3" id="Div6">
      <div class="pricing_header">
        <h2>Stage 7</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
       <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal7"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget7"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp7"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks7"></asp:Label></li>
       </ul>
    </div>
                                        </asp:Panel><asp:Panel runat="server" ID="stage8" Visible="false">
    <!-- Pack 8-->
    <div class="col-md-3" id="Div7">
      <div class="pricing_header">
        <h2>Stage 8</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
       <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal8"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget8"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp8"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks8"></asp:Label></li>
      </ul>
    </div>
                                             </asp:Panel><asp:Panel runat="server" ID="stage9" Visible="false">
    <!-- Pack 9-->
    <div class="col-md-3 col-xs-offset-3" id="Div8">
      <div class="pricing_header">
        <h2>Stage 9</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
       <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal9"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget9"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp9"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks9"></asp:Label></li>
      </ul>
    </div>
                                                  </asp:Panel><asp:Panel runat="server" ID="stage10" Visible="false">
    <!-- Pack10-->
    <div class="col-md-3" id="Div9">
      <div class="pricing_header">
        <h2>Stage 10</h2>
        <div class="space"></div>
      </div>
      <ul class="list-group">
       <li class="list-group-item"><i class="fa fa-bullseye" aria-hidden="true"></i> <b>GOAL : </b><asp:Label runat="server" ID="lblGoal10"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Target Date : </b><asp:Label runat="server" ID="lblTarget10"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> <b>Actual Completion Date : </b><asp:Label runat="server" ID="lblcomp10"></asp:Label></li>
        <li class="list-group-item"><i class="fa fa-comment" aria-hidden="true"></i> <b>REMARKS : </b><asp:Label runat="server" ID="lblremarks10"></asp:Label></li>
       </ul>
    </div>
                                                      </asp:Panel>
  </div>
  </div>
</div>
    </asp:Content>