﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FlashSale.aspx.cs" Inherits="DistributorSystem.FlashSale" MasterPageFile="~/MasterPage.Master" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script src="Scripts/jquery.modal.min.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" class="init">

        $(document).ready(function () {
            //setTimeout(function () {
            //    debugger;
            //    var flag = sessionStorage.getItem("refresh_flag");
            //    if (flag != "false") {
            //        location.reload();
            //    }
            //}, 60000);

            setTimeout("countdown()", 1000);
            //$("input").focus(function () {
            //    console.log('in');
            //    //sessionStorage.setItem("refresh_flag", "1");
            //}).blur(function () {
            //    console.log('out');

            //    //if (value.length > 0) {
            //    //sessionStorage.setItem("refresh_flag", "1");
            //    //}
            //    //else {
            //    //    sessionStorage.setItem("refresh_flag", "0");
            //    //}
            //})

        });


        function Request(obj) {
            debugger;
            sessionStorage.setItem("refresh_flag", "false");
            var id = $('#' + obj.id.replace("btnOrder", "hdn_id")).val();
            //var count = $(obj).closest('tr').index();
            //var id = $('#MainContent_grdItem_hdn_id_' + count).val();
            $('#MainContent_hdnID').val(id);
            return false;
        }

        function countdown() {
            
            seconds = document.getElementById("MainContent_timerLabel").innerHTML;
            var flag = sessionStorage.getItem("refresh_flag");
            if (seconds > 0) {
                if (flag != "false") {
                    document.getElementById("MainContent_timerLabel").innerHTML = seconds - 1;
                    if (seconds == 1) {
                     
                        location.reload();
                    }
                    
                    
                }
                setTimeout("countdown()", 1000);
            }
        }

        
        function BindGridView() {
            var head_content = $('#MainContent_grdItem tr:first').html();
            $('#MainContent_grdItem').prepend('<thead></thead>')
            $('#MainContent_grdItem thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdItem tbody tr:first').hide();
            //$('#MainContent_grdItem').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">' +<%= Session["YearlyAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last1yearAmount"] %> +'</th><th style="text-align: right">' +<%= Session["last2yearAmount"] %> +'</th></tr></tfoot>');
            $('#MainContent_grdItem').DataTable(
                {
                    "paging": false
                    // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    //        "columnDefs": [
                    //{
                    //    "targets": [4],
                    //    "visible": false,
                    //    "searchable": false
                    //}]
                });

            sessionStorage.setItem("refresh_flag", "true");
        }
        function isNumberKey(evt, obj) {
            
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            //if (value.length > 0) {
            //    sessionStorage.setItem("refresh_flag", "1");
            //}
            //else {
            //    sessionStorage.setItem("refresh_flag", "0");
            //}
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 11) {
                    if (charCode == 46) {
                        return true;
                    }
                    else return false;
                }

            }
            if (charCode == 46) {
                return true;
            }
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            else {
                return true;
            }
        }

        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function CheckExtension(sender) {
            var validExts = new Array(".xlsx", ".xls");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnUpload').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnUpload').attr("style", "display:block");
                return true;
            }
        }

        function PlaceOrder() {
  
            var id = $('#MainContent_hdnID').val();
            var qty = $('#MainContent_txtAmount').val();
            if (qty > 0) {
                $('#err_msg').text("");
                $.ajax({
                    url: 'FlashSale.aspx/PlaceOrder',
                    method: 'post',
                    datatype: 'json',
                    data: '{qty:"' + qty + '", id:"' + id + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        debugger;
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        location.reload(true);
                        sessionStorage.setItem("refresh_flag", "true");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        debugger;
                        alert(xhr.responseText);
                        sessionStorage.setItem("refresh_flag", "true");
                    }
                });
            }
            else {
                $('#err_msg').text("Quantity should be greater than zero.");
            }
            return false;
        }
    </script>
    <style>
        .mn_popup
        {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table
        {
            border-color: white!important;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }
        /*th { white-space: nowrap; }*/
        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }

        .mn_bott
        {
            margin: 0px 0 10px 0;
        }

        .btn-default.btn-on-2.active
        {
            background-color: #006681;
            color: white;
        }

        .btn-default.btn-off-2.active
        {
            background-color: #006681;
            color: white;
        }

        .btn-group
        {
            margin-bottom: 5px;
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <%--<div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Entry</a>
                        </li>
                        <li class="current">Annual Target</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>--%>
    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-12 nopadding">
                Sale items will be refreshed in 
                <span id="timerLabel" runat="server">60</span> Seconds.
            </div>

            <div id="addDialog" class="modal fade">
                <div class="mn_popup">
                    <div class="modal-content">

                        <%-- <div id="Div1" class="modal-body">
                            <div class="filter_panel">
                                <div class="col-md-12 nopadding">
                                    <div class="col-md-3">
                                        <asp:Button runat="server" ID="btntemplate" CssClass="btnSubmit" OnClick="btntemplate_Click" Text="Download Template" />
                                    </div>
                                    <div class="col-md-5">
                                        <asp:FileUpload ID="updFile" runat="server" Style="width: 100%;" onchange="CheckExtension(this);" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label ID="lblExtError" runat="server" ForeColor="Red"></asp:Label>
                                        <asp:Button runat="server" Style="display: none;" CssClass="btnSubmit" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>--%>
                        <div class="modal-footer" style="text-align: left; color: black;">
                            <asp:Label runat="server">
                                <p style="font-size:.8em;"> * Please download the template, fill the data and then upload.</p>
                            <p style="font-size:.8em;"> * Please upload only excel file like .xls, .xlsx.</p>
                             <p style="font-size:.8em;"> * Column name of the excel sheet will be as "Customer_number","Customer_name", "Sales_Year".</p>
                             <p style="font-size:.8em;"> * All the column datatype should be "General".</p>
                             <p style="font-size:.8em;"> * The Sheet name should be renamed as "AnnualTarget".</p>
                            </asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Label runat="server" ID="lblmessage" Text="Order are subject to availability." style="align-content:center; color:black; font-weight:bold;"></asp:Label>
        <asp:Label runat="server" CssClass="result" ID="lblResult"></asp:Label>

        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdItem" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Item Code">
                        <ItemTemplate>
                            <asp:Label ID="lblitemCode" runat="server" Text='<%#Bind("item_code") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Desc">
                        <ItemTemplate>
                            <asp:Label ID="lblitemDesc" runat="server" Text='<%#Bind("Item_desc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Opening Quantity">
                        <ItemTemplate>
                            <div style="text-align: right">
                            <asp:Label ID="lblopening_qty" Style="text-align: right"  runat="server" Text='<%#Bind("Opening_Quantity") %>'></asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sale Price">
                        <ItemTemplate>
                            <div style="text-align: right">
                            <asp:Label ID="lblsale_price" Style="text-align: right"  runat="server" Text='<%#Bind("Sale_price") %>'></asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="List Price">
                        <ItemTemplate>
                            <div style="text-align: right">
                            <asp:Label ID="lbllist_price" Style="text-align: right"  runat="server" Text='<%#Bind("List_price") %>'></asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Quantity">
                        <ItemTemplate>
                            <div style="text-align: right">
                            <asp:Label ID="lblcurrent_qty" Style="text-align: right"  runat="server" Text='<%#Bind("current_quantity") %>'></asp:Label>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Order Quantity">
                        <ItemTemplate>
                            <asp:TextBox Style="text-align: right; width: 100%;" ID="txtAmount" onkeypress="return isNumberKey(event,this);" AutoPostBack="true" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <a href="#mdReason" rel="modal:open">

                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%#Bind("item_available_id") %>' />
                              <%--  <input type="button" id="btnOrder" class="btnSubmit" value="Place Order" onclick="return Request(this);" />--%>

                                                            <asp:Button ID="btnOrder" CssClass="btnSubmit" runat="server" Text="Place Order" OnClientClick="return Request(this);" />

                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
                <div class="col-md-12 controls">
                    <div class="col-md-5">
                        <p class="popupControl">Order Quantity : </p>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox Style="text-align: right; width: 100%;" ID="txtAmount" onkeypress="return isNumberKey(event,this);" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <asp:HiddenField ID="hdnID" runat="server" />
                    </div>
                    <div class="col-md-7">
                        <label id="err_msg" style="color: red;"></label>
                        <%-- <input type="button" id="btnOrder" class="btnSubmit"  value="Place Order"  onclick=" Request(this);" />--%>
                        <%--<asp:Button runat="server" ID="btnplaceorder" CssClass="btnSubmit" CausesValidation="false" OnClientClick="return PlaceOrder(); return false;" value="Place Order" />--%>
                        <input type="button" id="btnOrder" class="btnSubmit" onclick="return PlaceOrder(); return false;" value="Place Order" />
                        <a href="#" rel="modal:close">Close</a>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>


