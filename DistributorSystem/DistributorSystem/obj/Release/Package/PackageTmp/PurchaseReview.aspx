﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseReview.aspx.cs" Inherits="DistributorSystem.PurchaseReview" MasterPageFile="~/MasterPage.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
		<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<%--    <script src="js/amcharts.js"></script>
	<script src="js/serial.js"></script>
	<script src="js/light.js"></script>--%>
	<script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
	<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

	<script src="js/jquery.sumoselect.min.js"></script>
	<link href="css/sumoselect.css" rel="stylesheet" />

	<style>
		body {
			font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
		}

		.mn_margin {
			margin: 10px 0 0 0;
		}
		 .amcharts-chart-div a{
			display:none!important
		}
		.mn_bott {
			margin: 0px 0 10px 0;
		}

		.btn-default.btn-on-2.active {
			background-color: #006681;
			color: white;
		}

		.btn-default.btn-off-2.active {
			background-color: #006681;
			color: white;
		}

		#MainContent_Panel1 {
			margin: 0 0 12px 0;
			width: 100%;
			float: left;
		}
		/*.btn-default.btn-off-2.active {
			background-color: #00677f;
			color: white;
			border-color: #adadad;
		}*/
		#ChartPurchaseReview
		{
			height:500px;
			width:100%;
		}
		.filter_panel li
		{
			color:black;
		}
		.filter_panel .select-all
		{
			color: black;
		}
		.filter_panel .MultiControls
		 {
			color: black;
		}
		.filter_panel .SumoSelect .CaptionCont
		 {
			color: black;
		}

		.form-group .optWrapper .okCancelInMulti .selall .multiple
		{
			width:300px;
		}
		
	</style>


	<script type="text/javascript" class="init">
		$(document).ready(function () {
			var head_contentvalues = $('#MainContent_grdPurchaseReviewValues tr:first').html();
			$('#MainContent_grdPurchaseReviewValues').prepend('<thead></thead>')
			$('#MainContent_grdPurchaseReviewValues thead').html('<tr>' + head_contentvalues + '</tr>');
			$('#MainContent_grdPurchaseReviewValues tbody tr:first').hide();
			$('#MainContent_grdPurchaseReviewValues').DataTable({
				"paging": false,
				"ordering": false,
				"searching": false,
				"bInfo": false
			});
			var head_contentqty = $('#MainContent_grdPurchaseReviewQty tr:first').html();
			$('#MainContent_grdPurchaseReviewQty').prepend('<thead></thead>')
			$('#MainContent_grdPurchaseReviewQty thead').html('<tr>' + head_contentqty + '</tr>');
			$('#MainContent_grdPurchaseReviewQty tbody tr:first').hide();
			$('#MainContent_grdPurchaseReviewQty').DataTable({
				"paging": false,
				"ordering": false,
				"searching": false,
				"bInfo": false
			});

			LoadDropDown();
		  
		});
		function LoadDropDown() {
			$(<%=ProductGrpList.ClientID%>).SumoSelect({ selectAll: true, search: true, okCancelInMulti: true });
			$(<%=ProductFamilyList.ClientID%>).SumoSelect({ selectAll: true, search: true, okCancelInMulti: true });
			$(<%=ApplicationList.ClientID%>).SumoSelect({ selectAll: true, search: true, okCancelInMulti: true });  
		}
		function LoadChartPurchaseReview() {
			debugger;
			var tmp = null;
			$.ajax({
				type: "POST",
				url: 'PurchaseReview.aspx/LoadChartPurchaseReview',
				data: "",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (msg) {
					console.log("msg : " + msg);
					tmp = msg.d;
					console.log("tmp1 : " + tmp);
					var d = new Date();
					var n = d.getFullYear();
					var chart = AmCharts.makeChart("ChartPurchaseReview", {
						"type": "serial",
						"theme": "light",
						"dataProvider": tmp,
						"valueAxes": [{
							"gridColor": "#FFFFFF",
							"gridAlpha": 0.2,
							"dashLength": 0
						}],
						"gridAboveGraphs": true,
						"startDuration": 1,
						"graphs": [{
							"title": "YTD Purchase Target "+d.getFullYear(),
							"balloonText": "[[title]]: <b>[[value]]</b>",
							"bullet": "round",
							"bulletSize": 10,
							"bulletBorderColor": "#ffffff",
							"bulletBorderAlpha": 1,
							"bulletBorderThickness": 2,
							"valueField": "YTD Purchase Target " + d.getFullYear()
						},
						{
							"type": "column",
						   
							"title": "YTD Purchase "+d.getFullYear(),
							"balloonText": "[[title]]: <b>[[value]]</b>",
							"bullet": "round",
							"bulletSize": 10,
							"bulletBorderColor": "#ffffff",
							"bulletBorderAlpha": 1,
							"bulletBorderThickness": 2,
							"fillColorsField": "column_color",
							"valueField": "YTD Purchase " + d.getFullYear()
						}, {
							"title": "YTD Purchase "+(d.getFullYear()-1),
							"balloonText": "[[title]]: <b>[[value]]</b>",
							"bullet": "round",
							"bulletSize": 10,
							"bulletBorderColor": "#ffffff",
							"bulletBorderAlpha": 1,
							"bulletBorderThickness": 2,
							"valueField": "YTD Purchase " + (d.getFullYear() - 1)
						}],
						"chartCursor": {
							"categoryBalloonEnabled": false,
							"cursorAlpha": 0,
							"zoomable": false
						},
						"categoryField": "Column1",
						"categoryAxis": {
							"gridPosition": "start",
							"gridAlpha": 0
						},
						"legend": {}
					});
					chart.dataProvider = AmCharts.parseJSON(tmp);
					chart.validateData();
				},
				error: function (e) {
					//console(e);
				}
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<div class='block-web' style='float: left; width: 100%; height: 36px;'>
			<div class="header">
				<div class="crumbs">
					<!-- Start : Breadcrumbs -->
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<a class="mn_breadcrumb">Review</a>
						</li>
						<li class="current">Purchase Review</li>

					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 mn_margin">

		   <asp:Panel ID="Panel1" runat="server">
				<div>
				<div id="Divvalues" class="pull-left mn_bott">
				   <div class="btn-group" id="Div5">
					   <%--  data-toggle="buttons">--%>
						<asp:Button ID="Btn_Units" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Units" OnClick="Btn_Units_Click"  />
						<asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In '000" OnClick="Btn_Thousand_Click" />
						<asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
					</div>
				</div>

				<div id="DivValueOrQty" class="pull-right">
				   <div class="btn-group" id="Div2"> 
						<%--data-toggle="buttons">--%>
						<asp:Button ID="Btn_value" runat="server" class="btn btn-default btn-on-2 btn-sm active" Text="Value" OnClick="Btn_value_Click" />
						<asp:Button ID="Btn_quantity" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Quantity" OnClick="Btn_quantity_Click" />
					</div>
				</div>
			</div>
			   </asp:Panel>
		<asp:Panel runat="server"  CssClass="filter_panel">
			<div class="col-md-12 nopadding">
				 <div class="col-md-2">
						<div class="form-group">
							<label class="control-label ">Product Group</label>
							<asp:ListBox runat="server" ID="ProductGrpList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductGrpList_SelectedIndexChanged" AutoPostBack="true" class="search-txt" CssClass="form-control select2">
								<asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
								<asp:ListItem Value="BB" Text="BB"></asp:ListItem>
								<asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
								<asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
								<asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
							</asp:ListBox>
						</div>
					</div>

				<div class="col-md-2">
						<div class="form-group">
							<label class="control-label ">Product Family</label>
							<asp:ListBox runat="server" ID="ProductFamilyList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductFamilyList_SelectedIndexChanged" AutoPostBack="true" class="search-txt" CssClass="form-control select2"></asp:ListBox>
						</div>
					</div>

				  <div class="col-md-2">
						<div class="form-group">
							<label class="control-label">Application</label>
							<asp:ListBox runat="server" ID="ApplicationList" SelectionMode="Multiple" OnSelectedIndexChanged="ApplicationList_SelectedIndexChanged" AutoPostBack="true" class="search-txt" CssClass="form-control select2"></asp:ListBox>
						</div>
					</div>
				<div class="col-md-6" style="top:25px;">
					<asp:Button runat="server" ID="btnFetch" CssClass="btnSubmit" Text="Fetch" OnClick="btnFetch_Click" />
				</div>
			</div>


	   
		   
		</asp:Panel>

	</div>


	<asp:Panel runat="server" ID="pnlData">
		<div runat="server" id="divgridchartValues" visible="false">
			<asp:GridView ID="grdPurchaseReviewValues" runat="server" ViewStateMode="Enabled" CssClass="display compact" AutoGenerateColumns="False" ShowHeader="false">
				<Columns>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
							<asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>

			<%--<div style="float: left; padding: 10px; width: 100%;" class="col-md-12">

				<asp:Chart ID="Chart2" runat="server" Width="1307px" Visible="False" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
					<Titles>
						<asp:Title Text="Summary of Sales Budget Monthly "></asp:Title>
					</Titles>
					<Legends>
						<asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
					</Legends>
					<Series>
						<asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
							CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column">
						</asp:Series>
						<asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
						<asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
						<asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
						<asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
					</Series>

					<ChartAreas>
						<asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
							<AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
								<MajorGrid LineWidth="0" />
								<LabelStyle Font="Verdana, 8.25pt" />
							</AxisX>
							<AxisY>
								<MajorGrid LineWidth="0" />
							</AxisY>
						</asp:ChartArea>
					</ChartAreas>
					<BorderSkin BackColor="Transparent" PageColor="Transparent"
						SkinStyle="Emboss" />
				</asp:Chart>

			</div>--%>

		</div>

		<div runat="server" id="divgridchartQty" visible="false">
			<asp:GridView ID="grdPurchaseReviewQty" runat="server" ViewStateMode="Enabled" CssClass="display compact" AutoGenerateColumns="False" ShowHeader="false">
				<Columns>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
						<ItemTemplate>
							<asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
							<asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>

			<%--         <div style="float: left; padding: 10px; width: 100%;" class="col-md-12">

				<asp:Chart ID="Chart3" runat="server" Width="1307px" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
					<Titles>
						<asp:Title Text="Summary of Sales Budget Monthly "></asp:Title>
					</Titles>
					<Legends>
						<asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
					</Legends>
					<Series>
						<asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
							CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column">
						</asp:Series>
						<asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
						<asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
						<asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
						<asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
					</Series>

					<ChartAreas>
						<asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
							<AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
								<MajorGrid LineWidth="0" />
								<LabelStyle Font="Verdana, 8.25pt" />
							</AxisX>
							<AxisY>
								<MajorGrid LineWidth="0" />
							</AxisY>
						</asp:ChartArea>
					</ChartAreas>
					<BorderSkin BackColor="Transparent" PageColor="Transparent"
						SkinStyle="Emboss" />
				</asp:Chart>

			</div>--%>

		</div>

		
	<div id="ChartPurchaseReview"></div>
	</asp:Panel>
</asp:Content>
