﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlySales.aspx.cs" Inherits="DistributorSystem.MonthlySales" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
     <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
   

    <script type="text/javascript" class="init">
        $(document).ready(function () {
            var head_content = $('#MainContent_grdMonthlySales tr:first').html();
            $('#MainContent_grdMonthlySales').prepend('<thead></thead>')
            $('#MainContent_grdMonthlySales thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdMonthlySales tbody tr:first').hide();
            $('#MainContent_grdMonthlySales').append('<tfoot><tr> <th colspan="2" style="text-align:right">Total:</th><th style="text-align: right">'+<%= Session["month1"] %> +'</th><th style="text-align: right">'+<%= Session["month2"] %> +'</th><th style="text-align: right">'+<%= Session["month3"] %> +'</th><th style="text-align: right">'+<%= Session["month4"] %> +'</th><th style="text-align: right">'+<%= Session["month5"] %> +'</th><th style="text-align: right">'+<%= Session["month6"] %> +'</th><th style="text-align: right">'+<%= Session["month7"] %> +'</th><th style="text-align: right">'+<%= Session["month8"] %> +'</th><th style="text-align: right">'+<%= Session["month9"] %> +'</th><th style="text-align: right">'+<%= Session["month10"] %> +'</th><th style="text-align: right">'+<%= Session["month11"] %> +'</th><th style="text-align: right">'+<%= Session["month12"] %> +'</th></tr></tfoot>');
            var table = $('#MainContent_grdMonthlySales').dataTable({
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'lBfrtip',
                //dom: 'Bfrtip',
                buttons: [
                    //'copy', 'csv',
                    //'excel', 'pdf', 'print'
                ],

            });
        });
        function openwindow() {
            $("#dialogwindow").dialog("open");
        }

        function CheckExtension(sender) {
            var validExts = new Array(".xlsx", ".xls");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnUpload').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnUpload').attr("style", "display:block");
                return true;
            }
        }

        function isNumberKey(evt, obj) {
            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


        function DisplayModal(id) {
            document.getElementById("overlay-" + id).style.height = document.body.clientHeight + 'px';
            document.getElementById("overlay-" + id).className = "OverlayEffect";
            document.getElementById("modalMsg-" + id).className = "ShowModal";
        }

        function RemoveModal(id) {
            document.getElementById("overlay-" + id).style.height = "0px";
            document.getElementById("overlay-" + id).className = "";
            document.getElementById("modalMsg-" + id).className = "HideModal";
        }
        
      

    </script>
    <style>
        .mn_popup
        {
            width: 60%;
            align-content: center;
            margin: 15%;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }

        table
        {
            border-color: white!important;
        }

        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }
          .mn_bott {
            margin: 0px 0 10px 0;
        }
        .btn-default.btn-on-2.active {
            background-color: #006681;
            color: white;
        }
        .btn-default.btn-off-2.active {
            background-color: #006681;
            color: white;
        }
          .btn-group
        {
            margin-bottom: 5px;
            margin-top: 5px;
        }
           .OverlayEffect {  
            background-color: black;  
            filter: alpha(opacity=70);  
            opacity: 0.7;  
            width: 100%;  
            height: 100%;  
            z-index: 400;  
            position: absolute;  
            top: 0;  
            left: 0;  
        }  
        .HideModal {  
            display: none;  
        }  
  
        .modalPopup {  
            z-index: 1 !important;  
        }  
  
        .ShowModal {  
            top: 200px;  
            z-index: 1000;  
            position: absolute;  
            background-color: lightblue;  
            text-align: center;  
            width: 300px;  
            height: 200px;  
        }  
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Entry</a>
                        </li>
                        <li class="current">Customer Monthly Sales</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mn_margin">
        <asp:Panel ID="Panel1" runat="server" CssClass="filter_panel">
            <div class="col-md-4">
                <%--<asp:Button runat="server" ID="btnEntry" Text="Entry" OnClick="btnEntry_Click" />--%>
                <asp:Button runat="server" CssClass="btnSubmit" ID="btnSave" Text="Save" OnClick="btnSave_Click" />
                <asp:Button ID="Button1" runat="server" CssClass="btnSubmit" Text="Upload Entry" OnClientClick="alert('All values should be in unit.'); $('#addDialog').modal(); return false;" />
              
                 </div>
        </asp:Panel>
         <asp:Panel ID="panelvalues" runat="server">
            <div id="Divvalues" class="pull-right">
                <div class="btn-group">
                    <%-- data-toggle="buttons">--%>
                    <asp:Button ID="Btn_Units" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In Units" OnClick="Btn_Units_Click"/>
                    <asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In '000" OnClick="Btn_Thousand_Click" />
                    <asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
                </div>
            </div>
        </asp:Panel>
        <div id="addDialog" class="modal fade">
            <div class="mn_popup">
                <div class="modal-content">
                    <%-- <asp:Button ID="Button2" runat="server" CssClass="btnSubmit" Text="Instruction" OnClientClick="alert('please save.'); $('#addDialog').modal(); return false;" />--%>
                    <div id="Div1" class="modal-body">
                        <div class="filter_panel">
                            <div class="col-md-12 nopadding">
                                <div class="col-md-3">
                                    <asp:Button runat="server" ID="btntemplate" CssClass="btnSubmit" OnClick="btntemplate_Click" Text="Download Template" OnClientClick="alert('Please make the changes in template and Save As excel sheet.'); $('#addDialog').modal();"  />                           
                                </div>
                                <div class="col-md-5">
                                    <asp:FileUpload ID="updFile" runat="server" Style="width: 100%;" onchange="CheckExtension(this);" />
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lblExtError" runat="server" ForeColor="Red"></asp:Label>
                                    <asp:Button runat="server" Style="display: none;" CssClass="btnSubmit" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer" style="text-align: left; color: black;">
                        <asp:Label runat="server">
                             <p style="font-size:.8em;"> * System will accept value in 9999999999.99 format.</p>
                             <p style="font-size:.8em;"> * Please download the template, fill the data and then upload.</p>
                             <p style="font-size:.8em;"> * Please upload only excel file like .xls, .xlsx.</p>
                             <p style="font-size:.8em;"> * Column name of the excel sheet will be as "Customer_number","Customer_name", "Sales_Month Year", "Sales_LastMonth Year".</p>
                             <p style="font-size:.8em;"> * All the column datatype should be "General".</p>
                             <p style="font-size:.8em;"> * The Sheet name should be renamed as "MonthlySales".</p>
                        </asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <asp:Label ID="lblResult" CssClass="result" runat="server"></asp:Label><br />
         <asp:Label runat="server" ID="lblmessage"></asp:Label>
        <asp:LinkButton runat="server" ID="lnkErrorExcel" Visible="false" Text="Click here to download error excel." OnClick="lnkErrorExcel_Click"></asp:LinkButton>   
        <asp:Panel runat="server" ID="pnlData">
            <asp:GridView ID="grdMonthlySales" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdMonthlySales_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Customer Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("customernumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name" HeaderStyle-Width="200px">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customername") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotal" Text="Total : " runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox Style="text-align: right" ID="txtAmount" OnTextChanged="txtAmount_TextChanged" Enabled='<%# (Eval("Active").ToString() == "Y") %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month1") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth1Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right" id="divmonth1">
                                <asp:Label ID="lblmonth1" runat="server" Text='<%#Bind("month2") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth2Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth2" runat="server" Text='<%#Bind("month3") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth4Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth4" runat="server" Text='<%#Bind("month4") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth5Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth5" runat="server" Text='<%#Bind("month5") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth6Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth6" runat="server" Text='<%#Bind("month6") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth7Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth7" runat="server" Text='<%#Bind("month7") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth8Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth8" runat="server" Text='<%#Bind("month8") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth9Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth9" runat="server" Text='<%#Bind("month9") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth10Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth10" runat="server" Text='<%#Bind("month10") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth11Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth11" runat="server" Text='<%#Bind("month11") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblmonth12Header" runat="server"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="text-align: right">
                                <asp:Label ID="lblmonth12" runat="server" Text='<%#Bind("month12") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>



</asp:Content>
