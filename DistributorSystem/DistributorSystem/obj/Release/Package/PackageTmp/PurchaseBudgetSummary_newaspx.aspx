﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseBudgetSummary_newaspx.aspx.cs" MasterPageFile="~/MasterPage.Master" Inherits="DistributorSystem.PurchaseBudgetSummary_newaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
<%--    <script src="Scripts/jszip.js"></script>
    <script src="Scripts/jszip.min.js"></script>
    <script src="Scripts/globalize.js"></script>
    <script src="Scripts/jquery-2.2.3.js"></script>
    <script src="Scripts/dx.viz.js"></script>
    <link href="Content/dx.light.css" rel="stylesheet" />
     <script type="text/javascript" class="init">
         $(function () {
             var salesPivotGrid = $("#sales").dxPivotGrid({
                 dataFieldArea: "column",
                 rowHeaderLayout: "tree",
                 wordWrapEnabled: false,
                 dataSource: {
                     fields: [{
                         caption: "Region",
                         dataField: "region",
                         expanded: true,
                         area: "row"
                     }, {
                         caption: "Country",
                         dataField: "country",
                         expanded: true,
                         area: "row"
                     }, {
                         caption: "City",
                         dataField: "city",
                         area: "row"
                     }, {
                         dataField: "date",
                         dataType: "date",
                         area: "column"
                     }, {
                         caption: "Sales",
                         dataField: "amount",
                         dataType: "number",
                         summaryType: "sum",
                         format: "currency",
                         area: "data"
                     }, {
                         caption: "Percent",
                         dataField: "amount",
                         dataType: "number",
                         summaryType: "sum",
                         summaryDisplayMode: "percentOfRowGrandTotal",
                         area: "data"
                     }],
                     store: sales
                 },
                 showBorders: true,
                 height: 440
             }).dxPivotGrid("instance");


             $("#show-totals-prior").dxCheckBox({
                 text: "Show Totals Prior",
                 value: false,
                 onValueChanged: function (data) {
                     salesPivotGrid.option("showTotalsPrior", data.value ? "both" : "none");
                 }
             });

             $("#data-field-area").dxCheckBox({
                 text: "Data Field Headers in Rows",
                 value: false,
                 onValueChanged: function (data) {
                     salesPivotGrid.option("dataFieldArea", data.value ? "row" : "column");
                 }
             });

             $("#row-header-layout").dxCheckBox({
                 text: "Tree Row Header Layout",
                 value: true,
                 onValueChanged: function (data) {
                     salesPivotGrid.option("rowHeaderLayout", data.value ? "tree" : "standard");
                 }
             });
         });
         </script>
     <style>

         .options {
    padding: 20px;
    margin-top: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}

.caption {
    font-size: 18px;
    font-weight: 500;
}

.option {
    width: 33%;
    display: inline-block;
    margin-top: 10px;
}
     </style>--%>

       <script type="text/javascript" src="js/jquery-ui.min.js"></script>
   <script type="text/javascript" src="js/ReportsBudget.js"></script>   
  
   <style type="text/css">
       @media (min-width: 768px) and (max-width: 900px) {
    th,td {
        font-size:9px;
    }
    }
      #header_fixed {
      display:none;
      position:fixed;
      top:50px;
      background-color:#006780;
      margin-left:15px;
      color:#fff;
      }
      th,td {
      border: 1px solid #ddd;
      }
      th {
      padding:13px;
      text-align:center;
      }
      td {
      padding:4px;
      text-align:right;
      }
      .greendark, .color_4, .color_5 {
      text-align:left !important;
      font-size:12px !important;
      }
      #familygrid table tr:last-child {
      background-color:#999;
      }
      #linegrid table tr:last-child {
      background-color:#999;
      }
      #familygrid table tr:last-child td:first-child {
      text-align:center;
      }
      #linegrid table tr:last-child td:first-child {
      text-align:center;
      }
      #slsbycustomergrid table  td:first-child {
      text-align:left;
      }
      .color_total { 
      font-size:14px;
      background-color:#999!important;
      }
      .sum_total {
      font-size:14px;
      background-color:#999!important;
      }
       #slsbycustomergrid,#linegrid,#familygrid,#prdgroup,#salesappgrid {
           display:none;
       }
      .panel-group .panel {
      border-radius: 4px;
      margin-bottom: 0;
      overflow: hidden;
      margin-top: 20px;
      margin-bottom: 20px;
      }
      .panel-heading1 { background:#333; color:#29AAe1 !important; padding: 15px; color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading2 { background:#999; color:#333 !important; padding: 15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading3 { background:#333; color:#8ac340 !important;  padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading4 { background:#999; color:#91298E !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading5 { background:#333; color:#F05A26   !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading6 { background:#999; color:#002238  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading7 { background:#333; color:#fff  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading8 { background:#333; color:#F05A26   !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}

        .btn-default.btn-on-2.active {
            background-color: #006681!important;
            color: white!important;
        }

        .btn-default.btn-off-2.active {
            background-color: #006681!important;
            color: white!important;
        }

        
.color_1 {background:#E8D0A9;
color: #333;}
.color_2 {background:	#BFBFBF;/*#B7AFA3;*/
color: #333;}
.color_3 {background:#C1DAD6;
color: #333;}
.color_4 {background:	#F5FAFA;
color: #333;}
.color_5 {background:	#ACD1E9;
color: #333;}
.color_6 {background:#6D929B;
color: #333;}
.color_7 {background:#7D9C9F;
color: #333;}
.color_8 {background:#BDD8DA;
color: #333;}
.color_9 {background:#DFEFF0;
color: #333;}
.color_10 {background:#ECECEC;
color: #333;}
.color_11 {background:	#B1B1B1;
color: #333;}

.color_Product1 {background:	#e1e1e1;}
.color_Product2 {background:	#ffffff;}
.rbtn_panel {
       list-style: none;
    width: 198px;
    padding: 0px 2px 0px 9px;     margin-bottom: 0px;
}
   </style>

   <script type="text/javascript" src="js/select2.min.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<div class="demo-container">
        <div id="sales"></div>
        <div class="options">
            <div class="caption">Options</div>
            <div class="option">
                <div id="show-totals-prior"></div>
            </div>
            <div class="option">
                <div id="data-field-area"></div>
            </div>
            <div class="option">
                <div id="row-header-layout"></div>
            </div>
        </div>
    </div>--%>

       <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360"></asp:ScriptManager>
  
   <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
      <ContentTemplate>
             <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Summary</a>
                        </li>
                        <li class="current">Purchase Budget Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
          <div>
        
             <asp:Panel ID="panelvalues" runat="server">
            <div id="Divvalues" class="pull-right">
                <div class="btn-group">
                    <asp:Button ID="Btn_Thousand" runat="server" class="btn btn-default btn-off-2 btn-sm active" Text="Val In '000" OnClick="Btn_Thousand_Click" />
                    <asp:Button ID="Btn_Lakhs" runat="server" class="btn btn-default btn-off-2 btn-sm" Text="Val In Lakhs" OnClick="Btn_Lakhs_Click" />
                </div>
            </div>
        </asp:Panel>
                                    
   </div>

     
         <div class="row" id="reportsgrid" runat="server">
            <div class="col-md-12">
               <div class="portlet-body">
                  <div id="accordion" class="panel-group">
                     <div class="panel panel-default">
                        <div id="collapsebtn"  class=" panel-heading1 " >
                           <h4 class="panel-title">
                              <img id="product_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; Product Group
                           </h4>
                        </div>
                        <div id="prdgroup">
                           <div style="height: 0px;" >
                              <div  style="background:#fbfbfb;">
                                 <div>
                                    <%--class="col-md-5">--%>
                                    <asp:GridView ID="goldproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound">
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div >
                                 <div>
                                    <asp:GridView ID="fiveyrsproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div style="background:#fbfbfb;">
                                 <div>
                                    <asp:GridView ID="spcproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div >
                                 <div>
                                    <asp:GridView ID="topproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="topproductslbl" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div  style="background:#fbfbfb;">
                                 <div >
                                    <asp:GridView style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" ID="bbproducts" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="lblbbproducts" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab2">
                        <div id="collapseline" class="panel-heading2">
                           <h4 class="panel-title">
                              <img id="linegrid_image" src="images/button_plus.gif" align="left"/> &nbsp;&nbsp;&nbsp;&nbsp; SALES BY LINE
                           </h4>
                        </div>
                        <div >
                           <div id="linegrid"  style="background:#fbfbfb;">
                             
                                 <asp:GridView ID="salesbylinegrid"  style="margin-top: 25px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                    <columns>
                                       <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                          <ItemTemplate>
                                             <asp:Label  ID="lbl_linegrid" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                          <ItemTemplate>  
                                              <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                          </ItemTemplate>  
                                          </asp:TemplateField> --%> 
                                       <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                    </columns>
                                 </asp:GridView>
                             
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab3">
                        <div  id="collapsefamily" class="panel-heading3">
                           <h4 class="panel-title">
                              <img id="familygrid_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY FAMILY
                           </h4>
                        </div>
                        <div  >
                           <div id="familygrid"  style="background:#fbfbfb;">
                              <asp:GridView ID="salesbyfamilygrid" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                 <columns>
                                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                       <ItemTemplate>
                                          <asp:Label  ID="lblfamilygrid" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                       <ItemTemplate>  
                                           <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                       </ItemTemplate>  
                                       </asp:TemplateField> --%> 
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                              <div class="col-md-2">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab4">
                        <div  id="collapse_app" class="panel-heading4">

                           <h4 class="panel-title">
                              <img id="salesapp_img" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY APPLICATION                                                              
                           </h4>
                        </div>
                        <div>                            
                           <div id="salesappgrid"  style="background:#fbfbfb;">                            
                              <div id="div1" runat="server">
                                  <ul class="btn-info rbtn_panel" >
                                      <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">VALUE</span>                       
                                        <asp:RadioButton ID="rbtn_Value"  Checked="true"  GroupName="byValueorQty" runat="server" onclick="value_or_qty_Change();" />
                                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">QTY</span>
                                        <asp:RadioButton ID="rbtn_Quantity"  GroupName="byValueorQty" runat="server" onclick="value_or_qty_Change();"/>
                                     </li>
                                  </ul>
                               </div>

                               <div id="div2" runat="server">
                                  <ul style="float:right;list-style:none;   margin-top: -22px; width: 300px; margin-right: -5px; "  class="alert-danger fade in" >
                                      <li> 
                                          <span style="margin-right:-1px; margin-left:-35px;vertical-align:text-bottom;">Val In Units</span>
                                        <asp:RadioButton ID="Rdbtnunits" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" GroupName="byunitsorvalueorlacks" runat="server"/>
                                          <span style="margin-right:-1px; margin-left:5px; vertical-align:text-bottom;  ">Val In '000</span>                       
                                        <asp:RadioButton ID="Rdbtnvalue" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" Checked="true"  GroupName="byunitsorvalueorlacks" runat="server"/>
                                         <span style="margin-right:0px; margin-left:6px;vertical-align:text-bottom;">Val In Lakhs</span>
                                        <asp:RadioButton ID="Rdbtnlacks" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" GroupName="byunitsorvalueorlacks" runat="server"/>                                         
                                     </li>
                                  </ul>
                               </div>

                              <asp:GridView ID="salesbyapp" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_RowDataBound" >
                                 <columns>
                                      <asp:TemplateField HeaderText="APPLICATION CODE" HeaderStyle-CssClass="HeadergridAll"  HeaderStyle-Width="95px" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela1"  runat="server" Text='<%# Eval("app_Code") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPLICATION DESCRIPTION" HeaderStyle-CssClass="HeadergridAll"   HeaderStyle-Width="240px" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela0" runat="server" Text='<%# Eval("app_Desc") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>                                  
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela4" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela5" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela6" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela7" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela8" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>

                              <asp:GridView ID="salesbyapp_qty" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_qty_RowDataBound" >
                                 <columns>
                                      <asp:TemplateField HeaderText="APPLICATION CODE" HeaderStyle-CssClass="HeadergridAll"  HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center">
                                       <ItemTemplate>
                                          <asp:Label ID="Labela1"  runat="server" Text='<%# Eval("app_Code") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPLICATION DESCRIPTION" HeaderStyle-CssClass="HeadergridAll"   HeaderStyle-Width="240px" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela0" runat="server" Text='<%# Eval("app_Desc") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>                                  
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela4" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela5" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela6" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela7" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela8" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                                                                 
                              <div class="col-md-2">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab5">
                        <div  id="cust_app" class="panel-heading5">
                           <h4 class="panel-title">
                              <img id="slsbycust_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY CHANNEL PARTNER
                           </h4>
                        </div>
                        <div  >
                           <div id="slsbycustomergrid"  style="background:#fbfbfb;">
                              <asp:GridView ID="salesbycustomer" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_cust_RowDataBound" >
                                 <columns>
                                     <asp:TemplateField HeaderText="CUSTOMER NUMBER" HeaderStyle-CssClass="HeadergridAll" visible="true" HeaderStyle-Width="95px">
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc0" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CUSTOMER NAME" HeaderStyle-CssClass="HeadergridAll" visible="true" HeaderStyle-Width="240px">
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelcust_name" runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelc3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label  ID="lbl_Budget" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc4" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="lbl_YtdSale" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc5" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc6" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                           </div>
                        </div>
                     </div>
                     <table id="header_fixed"   ></table>
                  </div>
               </div>
            </div>
            <!-- End : Inner Page container -->
            <a style="display: none;" class="scrollup" href="javascript:void(0);">Scroll</a>
         </div>
         <!-- End : Inner Page container -->
         <a href="javascript:void(0);" class="scrollup" style="display: none;">Scroll</a>
         </div>  <!-- End : Inner Page Content -->
         </div>
         </div>  <!-- End : container -->
      </ContentTemplate>
      <Triggers>
         <%--<asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />--%>
    <%--    <asp:AsyncPostBackTrigger ControlID="ValInThsnd" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="ValInLakhs" EventName="CheckedChanged" />--%>
       
<%--          <asp:PostBackTrigger ControlID="exportbtn" />--%>
      </Triggers>
   </asp:UpdatePanel>
   <asp:UpdateProgress ID="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
</asp:Content>
