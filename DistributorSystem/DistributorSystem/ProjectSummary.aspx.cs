﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class SalesBudgetSummary : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        SummaryBL objBL;
        SummaryBO objBO;
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                
                if (!IsPostBack)
                    LoadCustomer();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadCustomer()
        {
            DataSet dtcust = new DataSet();
            try
            {
                objBL = new SummaryBL();
                objBO = new SummaryBO();
                objBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objBO.customer_number=null;
                dtcust = objBL.GetCustomerAndProjectsBL(objBO);
                if (dtcust != null)
                {
                    if (dtcust.Tables[0].Rows.Count > 0)
                    {
                        ddlCustomer.DataSource = dtcust.Tables[0];
                        ddlCustomer.DataTextField = "customer_name";
                        ddlCustomer.DataValueField = "customer_number";
                        ddlCustomer.DataBind();
                        ddlCustomer_SelectedIndexChanged(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dtcust = new DataSet();
            try
            {
                objBL = new SummaryBL();
                objBO = new SummaryBO();
                objBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objBO.customer_number = Convert.ToString(ddlCustomer.SelectedValue);
                dtcust = objBL.GetCustomerAndProjectsBL(objBO);
                if (dtcust != null)
                {
                    if (dtcust.Tables[1].Rows.Count > 0)
                    {
                        ddlProject.DataSource = dtcust.Tables[1];
                        ddlProject.DataTextField = "Project_title";
                        ddlProject.DataValueField = "Project_Number";
                        ddlProject.DataBind();
                        ddlProject_SelectedIndexChanged(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dsProject = new DataSet();
            try
            {
                objBO = new SummaryBO();
                objBL = new SummaryBL();
                objBO.customer_number = Convert.ToString(ddlCustomer.SelectedValue);
                objBO.project_number = Convert.ToString(ddlProject.SelectedValue);
                dsProject = objBL.GetProjectDetailsBL(objBO);
                if (dsProject != null)
                {
                    if (dsProject.Tables[0] != null)
                    {
                        if (dsProject.Tables[0].Rows.Count > 0)
                        {
                            txtBusinessExpected.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Business_Expected"]);
                            txtCompetitionSpec.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Competition"]);
                            txtComponent.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Component"]);
                            txtCustClass.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["customer_class"]);
                            txtCustPotential.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["OverAll_Potential"]);
                            txtExcaleteTo.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["EscalatedTo"]);
                            txtExistingBrand.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Existing_Product"]);
                            txtIndustry.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["sub_industry"]);
                            txtOwner.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Owner"]);
                            txtProjPotential.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Potential_Lakhs"]);
                            txtProjType.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["project_type"]);
                            txtReviewer.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Reviewer"]);
                            txtStages.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["NoOfStages"]);
                            txtTargetDate.Text = Convert.ToString(dsProject.Tables[0].Rows[0]["Main_Target_Date"]);
                           
                        }
                        if (dsProject.Tables[1].Rows.Count > 0)
                        {
                           

                            for (int i = 0; i <= dsProject.Tables[1].Rows.Count; i++)
                            {
                                if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 1)
                                {
                                    stage1.Visible = true;
                                    lblGoal1.Text= Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp1.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks1.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget1.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage2.Visible = false;
                                    stage3.Visible = false;
                                    stage4.Visible = false;
                                    stage5.Visible = false;
                                    stage6.Visible = false;
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 2)
                                {
                                    stage2.Visible = true;
                                    lblGoal2.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp2.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks2.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget2.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage3.Visible = false;
                                    stage4.Visible = false;
                                    stage5.Visible = false;
                                    stage6.Visible = false;
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 3)
                                {
                                    stage3.Visible = true;
                                    lblGoal3.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp3.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks3.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget3.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage4.Visible = false;
                                    stage5.Visible = false;
                                    stage6.Visible = false;
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 4)
                                {
                                    stage4.Visible = true;
                                    lblGoal4.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp4.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks4.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget4.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage5.Visible = false;
                                    stage6.Visible = false;
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 5)
                                {
                                    stage5.Visible = true;
                                    lblGoal5.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp5.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks5.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget5.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage6.Visible = false;
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 6)
                                {
                                    stage6.Visible = true;
                                    lblGoal6.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp6.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks6.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget6.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage7.Visible = false;
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 7)
                                {
                                    stage7.Visible = true;
                                    lblGoal7.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp7.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks7.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget7.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage8.Visible = false;
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 8)
                                {
                                    stage8.Visible = true;
                                    lblGoal8.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp8.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks8.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget8.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage9.Visible = false;
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 9)
                                {
                                    stage9.Visible = true;
                                    lblGoal9.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp9.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks9.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget9.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                    stage10.Visible = false;
                                }
                                else if (Convert.ToInt32(dsProject.Tables[1].Rows[i]["Stage_Number"]) == 10)
                                {
                                    stage10.Visible = true;
                                    lblGoal10.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Goal"]);
                                    lblcomp10.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Completion_Date"]);
                                    lblremarks10.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Remarks"]);
                                    lblTarget10.Text = Convert.ToString(dsProject.Tables[1].Rows[i]["Target_Date"]);
                                }
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
    }
}