﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMDetails.aspx.cs" Inherits="DistributorSystem.SMDetails" MasterPageFile="~/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--%>
    <%--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
    <script type="text/javascript" class="init">
  

        $(document).ready(function () {
            bindGridView();
        });

        function bindGridView() {
            var head_content = $('#MainContent_grdSMDetails tr:first').html();
            $('#MainContent_grdSMDetails').prepend('<thead></thead>')
            $('#MainContent_grdSMDetails thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdSMDetails tbody tr:first').hide();
            $('#MainContent_grdSMDetails').DataTable(
                {
                    "info": false
                });
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validateControls() {
            var err_flag = 0;
           
          
            if ($('#MainContent_lstSelectedCusName').has('option').length == 0) {
                $('#MainContent_lstSelectedCusName').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_lstSelectedCusName').css('border-color', '');
                //err_flag = 0;
            }


            if ($('#MainContent_txtusername').val() == "") {
                $('#MainContent_txtusername').css('border-color', 'red');
                err_flag = 1;
            }

            else {
                if (!$('#MainContent_txtusername').val().match(/^[a-zA-Z]+$/)) {
                    $('#MainContent_txtusername').css('border-color', 'red');

                    err_flag = 2;

                }
                else {
                    $('#MainContent_txtusername').css('border-color', '');
                    //err_flag = 0;
                }
            }

            if ($('#MainContent_txtemail').val() == "") {
                $('#MainContent_txtemail').css('border-color', 'red');
                err_flag = 1;
            }

            else {
                if (!validateEmail($('#MainContent_txtemail').val())) {
                    $('#MainContent_txtemail').css('border-color', 'red');
                    // $('#MainContent_lblError').text('Please enter valid email id.');
                    // return false;
                    err_flag = 1;
                }

                else
                    $('#MainContent_txtemail').css('border-color', '');
                //err_flag = 0;
            }

            

            if (err_flag == 0) {
                $('#MainContent_lblerror').text('');
                return true;
            }

            else {

                if (err_flag == 1) {
                    $('#MainContent_lblerror').text('Please enter all the mandatory fields/valid data.');
                }
                else {
                    $('#MainContent_lblerror').text('Only alphabets are allowed in User Name field');
                }
                return false;
            }
        }

        //function getMouseCursor() {
        //    document.getElementById('lstSelectCusName').value
        //    document.getElementById('lstSelectedCusName').value
        //    return true;
        //}

    </script>
    <style>
        .mn_popup {
            width: 60%;
            /*align-content: center;*/
            margin: 15%;
        }

        body {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        table {
            border-color: white!important;
        }

        .mn_margin {
            margin: 10px 0 0 0;
        }

        .result {
            margin-left: 45%;
            font-weight: bold;
        }

        #pnlData {
            width: 70%;
        }

        .control {
            padding: 9px;
            border-bottom: solid 1px #ddd;
            /*background-color: #eaeaea;*/
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">SALES</a>
                        </li>
                        <li class="current">Sales Management</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12 filter_panel">
                <asp:Label ID="Label1" Style="font-weight: bolder;" runat="server">Sales Management </asp:Label>
            </div>
            <div class="col-md-8 mn_margin">
                <asp:Panel runat="server" ID="panel1">
                    <asp:GridView ID="grdSMDetails" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowCommand="grdSMDetails_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="CP Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblCPNumber"  runat="server" Text='<%#Bind("CP_Number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CP Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCPName" runat="server" Text='<%#Bind("CP_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("Customer_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("Customer_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User Name">
                                <ItemTemplate>
                                    <asp:Label ID="txtusername" runat="server" Text='<%#Bind("SM_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email id">
                                <ItemTemplate>
                                    <asp:Label ID="txtEmail" runat="server" Text='<%#Bind("SM_MAIL_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit-icon.png" ID="imgAction" CommandArgument='<%# Bind("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div class="col-md-4 mn_margin" style="margin-top: 4%;">
                <asp:Panel runat="server" ID="pnlEdit">
                    <div class="col-md-12 filter_panel">
                        <asp:Label ID="Label4" runat="server" Style="font-weight: bolder;" Text="Save CP Details "></asp:Label>
                    </div>
                    <div class="col-md-12 nopad">
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label ID="Label2" runat="server" Text="CP Name : "></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList ID="dlcpname" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dlcpname_SelectedIndexChanged">
                                    <%--<asp:ListItem>Select</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:Label ID="Label5" runat="server" Text="Customer Name  :"></asp:Label>
                        </div>
                        <%-- <div class="clearfix"></div>--%>
                        <div class="row-info">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 150px;">
                                        <asp:ListBox runat="server" ID="lstSelectCusName" OnPreRender="lstSelectCusName_PreRender" Height="150px" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                    <td style="width: 45px;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnAddSelected" runat="server" Text=">" Width="45px" OnClick="btnAddSelected_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnAddAll" runat="server" Text=">>" Width="45px" OnClick="btnAddAll_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnRemoveSelected" runat="server" Text="<" Width="45px" OnClick="btnRemoveSelected_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnRemoveAll" runat="server" Text="<<" Width="45px" OnClick="btnRemoveAll_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 150px;">
                                        <asp:ListBox ID="lstSelectedCusName" runat="server" Height="150px" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="lbltxt" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%--  <div class="col-md-12 control">
                            <div class="col-md-5">
                                <div class="col-md-12">
                                    <asp:Label runat="server" Text="Customer Name  :"></asp:Label>                               
                                </div>
                                <div>
                                      <asp:Label Text="select" ID="select" runat="server"></asp:Label>
                                </div>
                                <div>                     
                                    <asp:ListBox ID="custname" runat="server" Height="120px" Width="100px" SelectionMode="Multiple"></asp:ListBox>                                 
                                </div>
                            </div>                      
                        <div class="col-md-2">
                            <div class="col-md-12">
                                <asp:Button ID="btnAdd" Text=">" runat="server" Width="40px" />
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnAddAll" Text=">>" runat="server" Width="40px"/>
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnRemove" Text="<" runat="server" Width="40px"/>
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnRemoveAll" Text="<<" runat="server" Width="40px"/>
                            </div>
                        </div>
                        <div>
                            <asp:Label ID="selected" runat="server" Text="Selected"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:ListBox ID="custname2" runat="server" Height="120px" Width="121px" SelectionMode="Multiple"></asp:ListBox>
                        </div>

                    </div>--%>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label ID="label3" runat="server" Text="User Name :"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtusername"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" Text="User Mail id  :"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtemail"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12 control">
                            <div class="col-md-6">
                                <asp:Label runat="server" ID="lblerror" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnsave" runat="server" BackColor="DarkCyan"  Text=" Save" OnClick="btnsave_Click" OnClientClick="return validateControls();" />
                                <asp:Button ID="btnclear" runat="server" Style="margin-left: 4px" BackColor="DarkCyan" Text="Clear" OnClick="btnclear_Click" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnsave" EventName="Click" />
            <asp:PostBackTrigger ControlID="grdsmdetails" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>



