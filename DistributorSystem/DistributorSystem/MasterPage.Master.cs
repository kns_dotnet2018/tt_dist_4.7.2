﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Web.Security;

namespace DistributorSystem
{
    public partial class DynamicSite : System.Web.UI.MasterPage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
        
                LoginBO objLoginBO = new LoginBO();
                LoginBL loginBL = new LoginBL();
                DataSet dt = new DataSet();
             //   DataTable dtSubMenu;
                objLoginBO.Username = Convert.ToString(Session["DistributorNumber"]);
                dt = loginBL.GetAllMenusBL(objLoginBO);
            if (dt.Tables.Count > 1)
            {
                var MenuList = dt.Tables[1].AsEnumerable()

                        .Select(row => new
                        {
                            Menu_ID = row.Field<int>("Menu_ID"),
                            Menu = row.Field<string>("Menu"),
                        })
                        .Distinct().AsEnumerable();
                foreach (var drMenu in MenuList)
                //for(int i=0;i<MenuList.Count;i++)
                {
                    var SubMenuList = dt.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") > 0)
                      .Select(row => new
                      {
                          SubMenu_ID = row.Field<int>("SubMenu_ID"),
                          SubMenu = row.Field<string>("SubMenu_Name"),
                          URL = row.Field<string>("URL")
                      })
                       .Distinct().AsEnumerable();
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.Attributes.Add("class", " dropdown");
                    tabs.Controls.Add(li);
                    HtmlGenericControl anchor = new HtmlGenericControl("a");

                    anchor.InnerText = Convert.ToString(drMenu.Menu);
                    if (SubMenuList.ToList().Count > 0)
                    {

                        anchor.Attributes.Add("href", "#");
                        anchor.Attributes.Add("class", "dropdown-toggle ");
                        anchor.Attributes.Add("data-toggle", "dropdown");
                        anchor.Attributes.Add("role", "button");
                        anchor.Attributes.Add("aria-haspopup", "true");
                        anchor.Attributes.Add("aria-expanded", "false");
                        HtmlGenericControl span = new HtmlGenericControl("span");
                        span.Attributes.Add("class", "caret");
                        anchor.Controls.Add(span);
                    }
                    else
                    {
                        var MenuURL = dt.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") == 0)
                      .Select(row => new
                      {
                          URL = row.Field<string>("URL")
                      })
                       .Distinct().AsEnumerable();
                        foreach (var url in MenuURL)
                        {
                            anchor.Attributes.Add("href", Convert.ToString(url.URL));
                        }
                    }
                    li.Controls.Add(anchor);
                    if (SubMenuList.ToList().Count > 0)
                    {
                        HtmlGenericControl ul = new HtmlGenericControl("ul");
                        ul.Attributes.Add("class", "dropdown-menu mn_drop");


                        foreach (var subMenu in SubMenuList)
                        {
                            HtmlGenericControl ili = new HtmlGenericControl("li");
                            ul.Controls.Add(ili);
                            HtmlGenericControl ianchor = new HtmlGenericControl("a");
                            ianchor.Attributes.Add("href", Convert.ToString(subMenu.URL));
                            ianchor.InnerText = Convert.ToString(subMenu.SubMenu);
                            ili.Controls.Add(ianchor);
                        }

                        li.Controls.Add(ul);
                    }
                }
            }
                if (String.IsNullOrEmpty(Convert.ToString(Session["UserRole"])))
                {
                    lblUserName.Text = Convert.ToString(Session["DistributorName"]);
                    lnkAdminProfile.Visible = false;
                    
                }
                else
                {
                    lblUserName.Text = "Admin(Logged In As " + Convert.ToString(Session["DistributorName"]) + ")";
                    lnkAdminProfile.Visible = true;
                    //Session["DistributorNumber"] = "Admin";
                    //Session["DistributorName"] = "Admin";
                }
                //HtmlGenericControl ili = new HtmlGenericControl("li");
                //ul.Controls.Add(ili);
                //HtmlGenericControl ianchor = new HtmlGenericControl("a");
                //ianchor.Attributes.Add("href", "BudgetEntry.aspx");
                //ianchor.InnerText = Convert.ToString("Annual Target");
                //ili.Controls.Add(ianchor);

                //HtmlGenericControl ili1 = new HtmlGenericControl("li");
                //ul.Controls.Add(ili1);
                //HtmlGenericControl ianchor1 = new HtmlGenericControl("a");
                //ianchor1.Attributes.Add("href", "MonthlySales.aspx");
                //ianchor1.InnerText = Convert.ToString("Customer Monthly Sales");
                //ili1.Controls.Add(ianchor1);

                //HtmlGenericControl ili2 = new HtmlGenericControl("li");
                //ul.Controls.Add(ili2);
                //HtmlGenericControl ianchor2 = new HtmlGenericControl("a");
                //ianchor2.Attributes.Add("href", "SMDetails.aspx");
                //ianchor2.InnerText = Convert.ToString("Sales Management");
                //ili2.Controls.Add(ianchor2);



                //DatTable dtOutputList = Generix.getData("Get another set of Data");

                //foreach (DataRow drOutputList in dtOutputList.Rows)
                //{
                //    HtmlGenericControl ili = new HtmlGenericControl("li");
                //    ul.Controls.Add(ili);
                //    HtmlGenericControl ianchor = new HtmlGenericControl("a");
                //    foreach (DataColumn dcOutputList in dtOutputList.Columns)
                //    {
                //        ianchor.Attributes.Add("href", Convert.ToString(drOutputList["ModuleFileName"]));
                //    }
                //    ianchor.InnerText = Convert.ToString(drOutputList["ModuleName"]);
                //    ili.Controls.Add(ianchor);
                //}

                //DataTable dt = this.BindMenuData(0);
                //DynamicMenuControlPopulation(dt, 0, null);
            
        }



        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Buffer = true;
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Expires = -1000;
                Response.CacheControl = "no-cache";
                Response.Redirect("Login.aspx", true);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void lnkAdminProfile_Click(object sender, EventArgs e)
        {
            lblUserName.Text = "Admin";
            lnkAdminProfile.Visible = false;
            Session["DistributorNumber"] = "Admin";
            Session["DistributorName"] = "Admin";
            //Response.Redirect("UserDetails.aspx");
            Response.Redirect("UserDetailsWithMenu.aspx");
        }
    }
}