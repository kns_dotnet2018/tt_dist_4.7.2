﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBO;
using DistributorSystemBL;
using System.Data;
using System.IO;
using System.Drawing;

namespace DistributorSystem
{
    public partial class ExceptionReport : System.Web.UI.Page
    {
        #region Global Declaration
        SummaryBL objSummaryBL;
        SummaryBO objSummaryBO;
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : On page load, dropdown value for year has been initialized.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
               
                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { Btn_Thousand_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { Btn_Lakhs_Click(null, null); }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { Btn_Units_Click(null, null); }
                else
                {
                    Btn_Units_Click(null, null);
                }
                if (!IsPostBack)
                {
                    gridLoadedStatus = 0;
                    ddlYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                    ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1)));

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Btn_Units_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = true;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnFilter_Click(null, null);
            }
        }


        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = true;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnFilter_Click(null, null);
            }
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {
            Session["Btn_ThousandWasClicked"] = false;
            Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            if (gridLoadedStatus == 1)
            {
                btnFilter_Click(null, null);
            }
        }


        /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : It calls getExceptionReportBL method by passing year value and retriving data and it binds gridview with the output 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            DataTable dtReport;
            try
            {
                objSummaryBO = new SummaryBO();
                objSummaryBL = new SummaryBL();
                dtReport = new DataTable();
                objSummaryBO.year = Convert.ToInt32(ddlYear.SelectedValue);
                if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]) == true) { objSummaryBO.value = 1000; }
                else if (Convert.ToBoolean(Session["Btn_LakhsWasClicked"]) == true) { objSummaryBO.value = 100000; }
                else if (Convert.ToBoolean(Session["Btn_UnitsWasClicked"]) == true) { objSummaryBO.value = 1; }
                dtReport = objSummaryBL.getExceptionReportBL(objSummaryBO);

                if (dtReport != null && dtReport.Rows.Count > 0)
                {
                    Session["dtReport"] = dtReport;
                    btnExport.Visible = true;
                    grdReport.DataSource = dtReport;
                    lblResult.Text = "";
                    panelvalues.Visible = true;
                    gridLoadedStatus = 1;
                }
                else
                {
                    btnExport.Visible = false;
                    grdReport.DataSource = null;
                    lblResult.Text = "No records found.";
                    panelvalues.Visible = false;
                    gridLoadedStatus = 0;
                }
                grdReport.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : On click on Export button the event is called, It fetches session value of dtReport and call ExporttoExcel function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dtReport;
            try
            {
                dtReport = new DataTable();
                dtReport = (DataTable)Session["dtReport"];
                ExporttoExcel(dtReport, "CP Sales Summary");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : Event is called whenever the value of year has been changed and it calls filter function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnFilter_Click(null, null);
        }
        #endregion

        #region Methods
       /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : function is called for fetching data from gridview and format according and create an excel for the same
       /// </summary>
       /// <param name="dttable">data need to be exported</param>
       /// <param name="filename">file name of the excel</param>
        private void ExporttoExcel(DataTable dttable, string filename)
        {
            try
            {

                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filename + ".xls"));
                Response.ContentType = "application/ms-excel";

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdReport.GridLines;
                        if (grdReport.HeaderRow != null)
                        {

                            PrepareControlForExport(grdReport.HeaderRow);
                            table.Rows.Add(grdReport.HeaderRow);
                        }
                        foreach (GridViewRow row in grdReport.Rows)
                        {
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        //for (int i = 0; i < dtNew.Rows.Count; i++)
                        //{
                        //    if (Convert.ToString(dtNew.Rows[i][2]).Contains("TOTAL"))
                        //    {
                        //        table.Rows[i+1].BackColor = Color.Gray;
                        //        table.Rows[i+1].Font.Bold = true;
                        //    }
                        //}
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>Exception Report</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");


                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(ddlYear.SelectedValue) + "</td></tr>");


                        sw.WriteLine("</table><br/>");
                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());

                    Response.End();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 18,2018
        /// Desc : this method is being called to fetch data for export to excel from gridview
        /// </summary>
        /// <param name="control"></param>
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }

        #endregion

        
    }
}