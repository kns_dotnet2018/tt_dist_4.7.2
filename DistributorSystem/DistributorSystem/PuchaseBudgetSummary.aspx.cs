﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class PuchaseBudgetSummary : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        SummaryBL objBL;
        SummaryBO objBO;
        int CurrentYear = DateTime.Now.Year;
        int LastYear = DateTime.Now.Year - 1;
        int NextYear = DateTime.Now.Year + 1;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                
                if (!IsPostBack)
                   LoadData();
                    
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadData()
        {
            List<BudgetSummaryBO> list = new List<BudgetSummaryBO>();
            DataTable dtSummary = new DataTable();
            try
            {
                objBL = new SummaryBL();
                objBO = new SummaryBO();
                objBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
                objBO.customer_number=null;
                dtSummary = objBL.GetPurchaseSummaryBL(objBO);
                //foreach (BudgetSummaryBO events in objBL.GetPurchaseSummaryBL(objBO))
                //{
                //    list.Add(new BudgetSummaryBO
                //    {
                //        FAMILY_NAME = events.FAMILY_NAME,
                //        SUBFAMILY_NAME = events.SUBFAMILY_NAME,
                //        APPLICATION_DESC = events.APPLICATION_DESC,
                //        Value = events.Value,
                //        Quantity = events.Quantity
                //    });
                //}
                //System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //oSerializer.MaxJsonLength = int.MaxValue;
                Session["PurchaseSummary"] = dtSummary;
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
                if (dtSummary != null)
                {
                    if (dtSummary.Rows.Count > 0)
                    {
                        grdBudSummary.DataSource = dtSummary;
                        grdBudSummary.DataBind();
                    }
                    else
                    {
                        grdBudSummary.DataSource = null;
                        grdBudSummary.DataBind();
                    }
                }
                else
                {
                    grdBudSummary.DataSource = null;
                    grdBudSummary.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string LoadSummaryTable()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = (DataTable)HttpContext.Current.Session["PurchaseSummary"];

                if (dtoutput.Rows.Count > 0)
                {
                    DataView view = new DataView(dtoutput);
                    DataTable distinctValues = view.ToTable(true, "FAMILY_NAME", "Quantity","Value");
                    output = DataTableToJSONWithStringBuilder(distinctValues);
                    //System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //oSerializer.MaxJsonLength = int.MaxValue;
                    //output = oSerializer.Serialize(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadSummarySubTable(string familyname)
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = (DataTable)HttpContext.Current.Session["PurchaseSummary"];

                if (dtoutput.Rows.Count > 0)
                {
                    var rows = from row in dtoutput.AsEnumerable()
                               where row.Field<string>("FAMILY_NAME").Trim() == familyname
                               select row;
                    dtoutput = rows.CopyToDataTable();
                    DataView view = new DataView(dtoutput);
                    DataTable distinctValues = view.ToTable(true, "SUBFAMILY_NAME", "SubQuantity", "SubValue");
                    output = DataTableToJSONWithStringBuilder(distinctValues);
                    //System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //oSerializer.MaxJsonLength = int.MaxValue;
                    //output = oSerializer.Serialize(dtoutput);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        protected void grdBudSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[3].Text = Convert.ToString(CurrentYear-1)+" Pieces";
                    e.Row.Cells[4].Text = Convert.ToString(CurrentYear) + "P Pieces";
                    e.Row.Cells[5].Text = Convert.ToString(CurrentYear + 1) + "B Pieces";
                    e.Row.Cells[6].Text = Convert.ToString(CurrentYear - 1) + " Rupee";
                    e.Row.Cells[7].Text = Convert.ToString(CurrentYear) + "P Rupee";
                    e.Row.Cells[8].Text = Convert.ToString(CurrentYear + 1) + "B Rupee";
                    e.Row.Cells[9].Text = Convert.ToString(CurrentYear - 1) + " Inserts Per Tool";
                    e.Row.Cells[10].Text = Convert.ToString(CurrentYear) + "P Inserts Per Tool";
                    e.Row.Cells[11].Text = Convert.ToString(CurrentYear + 1) + "B Inserts Per Tool";
                    e.Row.Cells[12].Text = Convert.ToString(CurrentYear + 1) + "B - " + Convert.ToString(CurrentYear) + "P Incr.PCS";
                    e.Row.Cells[13].Text = Convert.ToString(CurrentYear) + "P - " + Convert.ToString(CurrentYear-1) + " Incr.PCS";
                    e.Row.Cells[14].Text = Convert.ToString(CurrentYear + 1) + "B / " + Convert.ToString(CurrentYear) + "P Incr.PCS(%)";
                    e.Row.Cells[15].Text = Convert.ToString(CurrentYear) + "P / " + Convert.ToString(CurrentYear - 1) + " Incr.PCS(%)";
                    e.Row.Cells[16].Text = Convert.ToString(CurrentYear + 1) + "B / " + Convert.ToString(CurrentYear) + "P Incr.Rupee(%)";
                    e.Row.Cells[17].Text = Convert.ToString(CurrentYear) + "P / " + Convert.ToString(CurrentYear - 1) + " Incr.Rupee(%)";
                    e.Row.Cells[18].Text = Convert.ToString(CurrentYear - 1) + " RupeePerPcs";
                    e.Row.Cells[19].Text = Convert.ToString(CurrentYear) + "P RupeePerPcs";
                    e.Row.Cells[20].Text = Convert.ToString(CurrentYear + 1) + "B RupeePerPcs";
                    e.Row.Cells[21].Text = Convert.ToString(CurrentYear + 1) + "B / " + Convert.ToString(CurrentYear) + "P PriceChange(%)";
                    e.Row.Cells[22].Text = Convert.ToString(CurrentYear) + "P / " + Convert.ToString(CurrentYear - 1) + " PriceChange(%)";
                    e.Row.Cells[23].Text = Convert.ToString(CurrentYear-1) + " / " + Convert.ToString(CurrentYear - 2) + " PriceChange(%)";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
    }
}