﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DistributorSystemBL;
using DistributorSystemBO;

namespace DistributorSystem
{
    public partial class PurchaseBudgetSummary_newaspx : System.Web.UI.Page
    {

        string customernumber = string.Empty;
        SummaryBL objSummaryBL;
        SummaryBO objSummaryBO;
        DataTable dtTotals = new DataTable();
        DataTable dtfamilytotals = new DataTable();
        int actual_mnth;
        int byValueIn;
        int tablesLoadedStatus;
        List<string> cssList = new List<string>();
        // DataTable dtfamilytotals = new DataTable();
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["cter"]) == "DUR")
                this.MasterPageFile = "~/MasterPageDUR.Master";
            else
                this.MasterPageFile = "~/MasterPage.Master";
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (string.IsNullOrEmpty(Convert.ToString(Session["DistributorNumber"]))) { Response.Redirect("Login.aspx"); return; }
                

            objSummaryBL = new SummaryBL();
            if (!IsPostBack)
            {
                Session["Btn_ThousandWasClicked"] = true;
                Session["Btn_ValueWasClicked"] = true;
                BindData();
            }
        }

        //protected void Btn_Units_Click(object sender, EventArgs e)
        //{
        //    Session["Btn_ThousandWasClicked"] = false;
        //    Session["Btn_UnitsWasClicked"] = true;
        //    Session["Btn_LakhsWasClicked"] = false;
        //    Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
        //    Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
        //    Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
        //    byValueIn = 1;
        //   BindData();
           
        //}

        private void BindData()
        {
            int BudgetYear = objSummaryBL.getBudgetYearBL();
           // byValueIn = 1000;
            tablesLoadedStatus = 0;
            if (Convert.ToBoolean(Session["Btn_ThousandWasClicked"]))
                byValueIn = 1000;
            else
                byValueIn = 100000;

            actual_mnth = 12 - (objSummaryBL.getActualMonthBL());

            
            tablesLoadedStatus = 1;
            bindgoldproductsGrid();
            bind5yrsproductsGrid();
            bindspecialsproductsGrid();
            bindtopproductsGrid();
            bindbbproductsGrid();
            bindgridsalesbyline();
            bindgridsalesbyfamily();
            bindgridsalesbyapp();
            bindgridsalesbyappQty();

            bindgridsalesbycustomer();
            LoadCSS();
            bindgridColor();
            // exportbtn.Visible = true;

            //LogFile("Start On Click Proceed", "End", "", "----------------------------------------------------------------------------------------------------------");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }

        protected void Btn_Thousand_Click(object sender, EventArgs e)
        {
            byValueIn = 1000;
            Session["Btn_ThousandWasClicked"] = true;
            //Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = false;
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
          //  Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
                BindData();
        }

        protected void Btn_Lakhs_Click(object sender, EventArgs e)
        {

            byValueIn = 100000;
            Session["Btn_ThousandWasClicked"] = false;
           // Session["Btn_UnitsWasClicked"] = false;
            Session["Btn_LakhsWasClicked"] = true;
            Btn_Lakhs.Attributes["class"] = "btn btn-default btn-on-2 btn-sm active";
            //Btn_Units.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
            Btn_Thousand.Attributes["class"] = "btn btn-default btn-on-2 btn-sm";
                BindData();
        }


        protected void bindgoldproductsGrid()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = "GOLD";
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtTopsum = objSummaryBL.getSpecialGroupValueSumBL(objSummaryBO);
            
            dtTotals = objSummaryBL.getValueSumBL(objSummaryBO);

            DataTable dtGold = new DataTable();
            dtGold.Columns.Add("FixedRow", typeof(string));
            dtGold.Columns.Add("sales_value_year_1", typeof(string));
            dtGold.Columns.Add("sales_value_year_0", typeof(string));
            dtGold.Columns.Add("estimate_value_next_year", typeof(string));
            dtGold.Columns.Add("p_sales_value_year_1", typeof(string));
            dtGold.Columns.Add("ytd", typeof(string));
            dtGold.Columns.Add("ach", typeof(string));
            dtGold.Columns.Add("arate", typeof(string));

            DataTable dtGoldReport = new DataTable();
            dtGoldReport.Columns.Add("FixedRow", typeof(string));
            dtGoldReport.Columns.Add("sales_value_year_1", typeof(string));
            dtGoldReport.Columns.Add("sales_value_year_0", typeof(string));
            dtGoldReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtGoldReport.Columns.Add("p_sales_value_year_1", typeof(string));
            dtGoldReport.Columns.Add("ytd", typeof(string));
            dtGoldReport.Columns.Add("ach", typeof(string));
            dtGoldReport.Columns.Add("arate", typeof(string));

            for (int i = 0; i < dtTopsum.Rows.Count; i++)
            {
                for (int j = 0; j < dtTotals.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        float top0 = dtTopsum.Rows[i].ItemArray[0].ToString() == "" || dtTopsum.Rows[i].ItemArray[0].ToString() == null ? 0    //2013
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[0].ToString());

                        float sum0 = dtTotals.Rows[i].ItemArray[0].ToString() == "" || dtTotals.Rows[i].ItemArray[0].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[0].ToString());
                        float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                        float top1 = dtTopsum.Rows[i].ItemArray[1].ToString() == "" || dtTopsum.Rows[i].ItemArray[1].ToString() == null ? 0    //2014
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[1].ToString());
                        float sum1 = dtTotals.Rows[i].ItemArray[1].ToString() == "" || dtTotals.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[1].ToString());
                        float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                        float top2 = dtTopsum.Rows[i].ItemArray[2].ToString() == "" || dtTopsum.Rows[i].ItemArray[2].ToString() == null ? 0   //2015
                           : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[2].ToString());
                        float sum2 = dtTotals.Rows[i].ItemArray[2].ToString() == "" || dtTotals.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[2].ToString());
                        float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                        float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                        float top4 = dtTopsum.Rows[i].ItemArray[3].ToString() == "" || dtTopsum.Rows[i].ItemArray[3].ToString() == null ? 0   //ytd
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[3].ToString());
                        float sum4 = dtTotals.Rows[i].ItemArray[3].ToString() == "" || dtTotals.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[3].ToString());
                        float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                        //float top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;     //ytd/2015
                        float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                        float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;


                        top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                        top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                        top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                        top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                        top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                        dtGold.Rows.Add("GOLD PRODUCTS", Math.Round(top0, 0).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtGold.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtGold.Rows.Add("CONTRIBUTION TO GROWTH");
                        dtGoldReport.Rows.Add("GOLD PRODUCTS", Math.Round(top0, 0).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtGoldReport.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtGoldReport.Rows.Add("CONTRIBUTION TO GROWTH");
                    }
                }
            }
            Session["goldproducts"] = dtGoldReport;
            goldproducts.DataSource = dtGold;
            goldproducts.DataBind();
        }
        protected void bind5yrsproductsGrid()
        {

            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = "5yrs";
            objSummaryBO.Flag_Desc = "5 YEARS PRODUCTS";

            //dtfamilyname = objSummaryBL.LoadFamilyIdBL();
            //for (int k = 0; k < dtfamilyname.Rows.Count; k++)
            //{
            //    familylist.Add(dtfamilyname.Rows[k].ItemArray[1].ToString());
            //}
            //string branchcode = ddlBranchList.SelectedItem.Value;
            //string seId = ddlSalesEngineerList.SelectedItem.Value;
            //string CustType = ddlcustomertype.SelectedItem.Value;
            //string custNum = ddlCustomerNumber.SelectedItem.Value;
            dtfamilytotals = objSummaryBL.salesbyfamilyBL(objSummaryBO);
            DataTable dt5yrs = productgroup("5yrs", "5 YEARS PRODUCTS");
            fiveyrsproducts.DataSource = dt5yrs;
            fiveyrsproducts.DataBind();
        }


        //specials region
        protected void bindspecialsproductsGrid()
        {
            DataTable dtspc = productgroup("SPC", "SPECIALS");
            spcproducts.DataSource = dtspc;
            spcproducts.DataBind();
        }
        //region  top products
        protected void bindtopproductsGrid()
        {
            DataTable dttop = productgroup("TOP", "TOP PRODUCTS");
            topproducts.DataSource = dttop;
            topproducts.DataBind();
        }

        //region BBproducts
        protected void bindbbproductsGrid()
        {
            DataTable dtbb = productgroup("BB", "BB PRODUCTS");

            bbproducts.DataSource = dtbb;
            bbproducts.DataBind();
        }

        public DataTable productgroup(string flag_short_name, string flag_full_name)
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = flag_short_name;
            objSummaryBO.Flag_Desc = flag_full_name;
            DataTable dtTopsum = objSummaryBL.getSpecialGroupValueSumBL(objSummaryBO);

            DataTable dtproduct = new DataTable();
            dtproduct.Columns.Add("FixedRow", typeof(string));
            dtproduct.Columns.Add("sales_value_year_1", typeof(string));
            dtproduct.Columns.Add("sales_value_year_0", typeof(string));
            dtproduct.Columns.Add("estimate_value_next_year", typeof(string));
            dtproduct.Columns.Add("p_sales_value_year_1", typeof(string));
            dtproduct.Columns.Add("ytd", typeof(string));
            dtproduct.Columns.Add("ach", typeof(string));
            dtproduct.Columns.Add("arate", typeof(string));

            DataTable dtproductReport = new DataTable();
            dtproductReport.Columns.Add("FixedRow", typeof(string));
            dtproductReport.Columns.Add("sales_value_year_1", typeof(string));
            dtproductReport.Columns.Add("sales_value_year_0", typeof(string));
            dtproductReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtproductReport.Columns.Add("p_sales_value_year_1", typeof(string));
            dtproductReport.Columns.Add("ytd", typeof(string));
            dtproductReport.Columns.Add("ach", typeof(string));
            dtproductReport.Columns.Add("arate", typeof(string));
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            for (int i = 0; i < dtTopsum.Rows.Count; i++)
            {
                for (int j = 0; j < dtTotals.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        float top0 = dtTopsum.Rows[i].ItemArray[0].ToString() == "" || dtTopsum.Rows[i].ItemArray[0].ToString() == null ? 0    //2013
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[0].ToString());

                        float sum0 = dtTotals.Rows[i].ItemArray[0].ToString() == "" || dtTotals.Rows[i].ItemArray[0].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[0].ToString());
                        float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                        float top1 = dtTopsum.Rows[i].ItemArray[1].ToString() == "" || dtTopsum.Rows[i].ItemArray[1].ToString() == null ? 0    //2014
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[1].ToString());
                        float sum1 = dtTotals.Rows[i].ItemArray[1].ToString() == "" || dtTotals.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[1].ToString());
                        float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                        float top2 = dtTopsum.Rows[i].ItemArray[2].ToString() == "" || dtTopsum.Rows[i].ItemArray[2].ToString() == null ? 0   //2015
                           : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[2].ToString());
                        float sum2 = dtTotals.Rows[i].ItemArray[2].ToString() == "" || dtTotals.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[2].ToString());
                        float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                        float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                        float top4 = dtTopsum.Rows[i].ItemArray[3].ToString() == "" || dtTopsum.Rows[i].ItemArray[3].ToString() == null ? 0   //ytd
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[3].ToString());
                        float sum4 = dtTotals.Rows[i].ItemArray[3].ToString() == "" || dtTotals.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[3].ToString());
                        float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                        //ytd/2015
                        //float top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;
                        float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                        float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;

                        top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                        top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                        top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                        top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                        top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                        dtproduct.Rows.Add(flag_full_name, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtproduct.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproduct.Rows.Add("CONTRIBUTION TO GROWTH");
                        dtproductReport.Rows.Add(flag_full_name, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtproductReport.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproductReport.Rows.Add("CONTRIBUTION TO GROWTH");
                        //decimal percentage = 
                    }
                }
            }
            //byfamily

            DataTable dtfamilyBelowSum = dtfamilytotals;
            for (int j = 0; j < dtfamilyBelowSum.Rows.Count; j++)
            {
                string fmname = dtfamilyBelowSum.Rows[j].ItemArray[1].ToString();
                objSummaryBO.fname = dtfamilyBelowSum.Rows[j].ItemArray[1].ToString();
                DataTable dtfamilyTopsum = objSummaryBL.getSpecialGroupValueSumBL(objSummaryBO);

                for (int i = 0; i < dtfamilyTopsum.Rows.Count; i++)
                {
                    float top0 = dtfamilyTopsum.Rows[i].ItemArray[0].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[0].ToString() == null ? 0
                                 : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[0].ToString());

                    float sum0 = dtfamilyBelowSum.Rows[j].ItemArray[2].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[2].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[2].ToString());
                    float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                    float top1 = dtfamilyTopsum.Rows[i].ItemArray[1].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[1].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[1].ToString());
                    float sum1 = dtfamilyBelowSum.Rows[j].ItemArray[3].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[3].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[3].ToString());
                    float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                    float top2 = dtfamilyTopsum.Rows[i].ItemArray[2].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[2].ToString() == null ? 0
                       : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[2].ToString());
                    float sum2 = dtfamilyBelowSum.Rows[j].ItemArray[4].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[4].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[4].ToString());
                    float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                    float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;

                    float top4 = dtfamilyTopsum.Rows[i].ItemArray[3].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[3].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[3].ToString());
                    float sum4 = dtfamilyBelowSum.Rows[j].ItemArray[5].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[5].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[5].ToString());
                    float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                    float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                    float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;
                    top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                    top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                    top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                    top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                    top6 = top0 == 0 ? 0 : (top6 / byValueIn);
                    bool allCoulmnszero = objSummaryBL.DeleteEmptyRowsBL(top0, top1, top2, top3, top4, top5, top6);
                    if (allCoulmnszero == false)
                    {
                        dtproduct.Rows.Add(flag_full_name + " - " + fmname, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtproduct.Rows.Add("as % of TOTAL" + " " + fmname, Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproductReport.Rows.Add(flag_full_name + " - " + fmname, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtproductReport.Rows.Add("as % of TOTAL" + " " + fmname, Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');

                    }
                }

            }
            if (flag_short_name == "5yrs")
            {
                Session["fiveyrsproducts"] = dtproductReport;
            }
            if (flag_short_name == "SPC")
            {
                Session["spcproducts"] = dtproductReport;
            }
            if (flag_short_name == "TOP")
            {
                Session["topproducts"] = dtproductReport;
            }
            if (flag_short_name == "BB")
            {
                Session["bbproducts"] = dtproductReport;
            }
            return dtproduct;
        }



        /// <summary>
        /// Modified By : Anamika
        /// Date : 8th Feb 2017
        /// Desc : Loaded data without comma in sessions
        /// </summary>
        protected void bindgridsalesbyline()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = "SF";
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataSet dsTables = (DataSet)getSalesByLineSum(objSummaryBO);
            DataTable dtsubfamilyvalue = new DataTable();
            DataTable dtsubfamilyvalueReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsubfamilyvalue = dsTables.Tables[0];
                dtsubfamilyvalueReport = dsTables.Tables[1];
            }

            DataTable dtsalesbytotal = dtTotals;
            for (int i = 0; i < dtsalesbytotal.Rows.Count; i++)
            {
                float top0 = dtsalesbytotal.Rows[i].ItemArray[0].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[0].ToString() == null ? 0
                           : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[0].ToString());
                float top1 = dtsalesbytotal.Rows[i].ItemArray[1].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[1].ToString());
                float top2 = dtsalesbytotal.Rows[i].ItemArray[2].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[2].ToString());
                float top3 = dtsalesbytotal.Rows[i].ItemArray[3].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[3].ToString());
                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top3 = top3 == 0 ? 0 : (top3 / byValueIn);
                double top4 = 0, top5 = 0, top6 = 0;
                top4 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;
                top6 = actual_mnth == 0 ? 0 : (top2 - top3) / actual_mnth;
                top5 = top2 == 0 ? 0 : (top3 / top2) * 100;
                dtsubfamilyvalue.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                dtsubfamilyvalueReport.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());

            }
            Session["salesbylinegrid"] = dtsubfamilyvalueReport;
            salesbylinegrid.DataSource = dtsubfamilyvalue;
            salesbylinegrid.DataBind();


        }
        //region salesbyfamily
        /// <summary>
        /// Modified By : Anamika
        /// Date : 8th Feb 2017
        /// Desc : Loaded data without comma in sessions
        /// </summary>
        protected void bindgridsalesbyfamily()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = "F";
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataSet dsTables = (DataSet)getSalesByLineSum(objSummaryBO);
            DataTable dtsales = new DataTable();
            DataTable dtsalesReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsales = dsTables.Tables[0];
                dtsalesReport = dsTables.Tables[1];
            }
            DataTable dtsalesbytotal = dtTotals;
            //DataTable dtsalesbytotal = objReports.getSalesByLineSum(customernumber);
            for (int i = 0; i < dtsalesbytotal.Rows.Count; i++)
            {
                float top0 = dtsalesbytotal.Rows[i].ItemArray[0].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[0].ToString() == null ? 0
                           : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[0].ToString());
                float top1 = dtsalesbytotal.Rows[i].ItemArray[1].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[1].ToString());
                float top2 = dtsalesbytotal.Rows[i].ItemArray[2].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[2].ToString());
                float top3 = dtsalesbytotal.Rows[i].ItemArray[3].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[3].ToString());
                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top3 = top3 == 0 ? 0 : (top3 / byValueIn);
                double top4 = 0, top5 = 0, top6 = 0;
                //top4 = top0 == 0 ? 0 : ((double)top1 / top0 - 1) * 100;
                top4 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;
                top6 = actual_mnth == 0 ? 0 : (top2 - top3) / actual_mnth;
                // top5 = top2 == 0 ? 0 : (top3 / top2 - 1) * 100;
                top5 = top2 == 0 ? 0 : (top3 / top2) * 100;
                dtsales.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                dtsalesReport.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
            }
            Session["salesbyfamilygrid"] = dtsalesReport;
            salesbyfamilygrid.DataSource = dtsales;
            salesbyfamilygrid.DataBind();
        }
        /// <summary>
        /// Modified By : Anamika
        /// Date : 8th Feb 2017
        /// Desc : Loaded data without comma in sessions
        /// </summary>
        protected void bindgridsalesbyapp()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = null;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            DataSet dsTables = (DataSet)getsalesbyapp(objSummaryBO);
            DataTable dtsalesbyapp = new DataTable();
            DataTable dtsalesbyappReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsalesbyapp = dsTables.Tables[0];
                dtsalesbyappReport = dsTables.Tables[1];
            }
            if (dtsalesbyapp.Rows.Count != 0)
            {
                decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
                {
                    sv_1 += dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                    sv_0 += dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                    ev += dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                    ytd += dtsalesbyapp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[6].ToString());
                }
                change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                dtsalesbyapp.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                dtsalesbyappReport.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
            }
            Session["salesbyapp"] = dtsalesbyappReport;
            salesbyapp.DataSource = dtsalesbyapp;
            salesbyapp.DataBind();
        }

        private DataSet getsalesbyapp(SummaryBO objSummaryBO)
        {
            objSummaryBL = new SummaryBL();
            DataSet dsTables = new DataSet();
            DataTable dtapp = new DataTable();
            dtapp.Columns.Add("app_Desc", typeof(string));
            dtapp.Columns.Add("app_Code", typeof(string));
            dtapp.Columns.Add("sales_value_year_1", typeof(string));
            dtapp.Columns.Add("sales_value_year_0", typeof(string));
            dtapp.Columns.Add("estimate_value_next_year", typeof(string));
            dtapp.Columns.Add("change", typeof(string));
            dtapp.Columns.Add("ytd", typeof(string));
            dtapp.Columns.Add("acvmnt", typeof(string));
            dtapp.Columns.Add("askrate", typeof(string));

            DataTable dtappReport = new DataTable();
            dtappReport.Columns.Add("app_Desc", typeof(string));
            dtappReport.Columns.Add("app_Code", typeof(string));
            dtappReport.Columns.Add("sales_value_year_1", typeof(string));
            dtappReport.Columns.Add("sales_value_year_0", typeof(string));
            dtappReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtappReport.Columns.Add("change", typeof(string));
            dtappReport.Columns.Add("ytd", typeof(string));
            dtappReport.Columns.Add("acvmnt", typeof(string));
            dtappReport.Columns.Add("askrate", typeof(string));

            int actual_mnth = 12 - (objSummaryBL.getActualMonthBL());
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtsalesbyapp = objSummaryBL.getSalesbyApplicationBL(objSummaryBO);
            for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
            {
                float pre_actlyear = dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[2].ToString() == null ? 0    //13
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                float actlyear = dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[3].ToString() == null ? 0      //14
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[4].ToString() == null ? 0     //15
                           : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                float ytd = dtsalesbyapp.Rows[i].ItemArray[5].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[5].ToString());

                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);
                string app_desc = dtsalesbyapp.Rows[i].ItemArray[0].ToString();
                string app_code = dtsalesbyapp.Rows[i].ItemArray[1].ToString();

                //15/14

                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                //ytd/2015B
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100;
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100;
                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = objSummaryBL.DeleteEmptyRowsBL(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dtapp.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString("N0", culture)), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next, 0)).ToString("N0", culture), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString("N0", culture));
                    dtappReport.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString()), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next, 0)).ToString(), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString(), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString());
                }


            }
            dsTables.Tables.Add(dtapp);
            dsTables.Tables.Add(dtappReport);
            return dsTables;
        }

        protected void bindgridsalesbyappQty()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = null;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            DataSet dsTables = getsalesbyappQty(objSummaryBO);
            DataTable dtsalesbyapp = new DataTable();
            DataTable dtsalesbyappReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsalesbyapp = dsTables.Tables[0];
                dtsalesbyappReport = dsTables.Tables[1];
            }
            if (dtsalesbyapp.Rows.Count != 0)
            {
                decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
                {
                    sv_1 += dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                    sv_0 += dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                    ev += dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                    ytd += dtsalesbyapp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[6].ToString());
                }
                change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                dtsalesbyapp.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                dtsalesbyappReport.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
            }
            Session["salesbyapp_qty"] = dtsalesbyappReport;
            salesbyapp_qty.DataSource = dtsalesbyapp;
            salesbyapp_qty.DataBind();
        }
 
        /// <summary>
        /// Modified By : Anamika
        /// Date : 8th Feb 2017
        /// Desc : Loaded data without comma in sessions
        /// </summary>
        protected void bindgridsalesbycustomer()
        {
            objSummaryBL = new SummaryBL();
            objSummaryBO = new SummaryBO();
            objSummaryBO.distributor_number = Convert.ToString(Session["DistributorNumber"]);
            objSummaryBO.Flag = "C";
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            //if (cust_type == null)
            //{
                DataTable dtcust = new DataTable();
                DataTable dtcustReport = new DataTable();
                /*Case:Customer Toatl*/
                DataSet dsTables = (DataSet)cust_total(objSummaryBO);
                DataTable dtsalesbycust = new DataTable();
                DataTable dtsalesbycustReport = new DataTable();
                if (dsTables.Tables.Count > 0)
                {
                    dtsalesbycust = dsTables.Tables[0];
                    dtsalesbycustReport = dsTables.Tables[1];
                }
                //if (dtsalesbycust.Rows.Count != 0)
                //{
                //    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                //    for (int i = 0; i < dtsalesbycust.Rows.Count; i++)
                //    {
                //        sv_1 += dtsalesbycust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[2].ToString());
                //        sv_0 += dtsalesbycust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[3].ToString());
                //        ev += dtsalesbycust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[4].ToString());
                //        ytd += dtsalesbycust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[6].ToString());
                //    }
                //    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                //    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                //    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                //    dtsalesbycust.Rows.Add("CUSTOMER TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                //    dtsalesbycustReport.Rows.Add("CUSTOMER TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                //}


                //dtcust.Merge(dtsalesbycust);
                //dtcustReport.Merge(dtsalesbycustReport);
                dsTables = (DataSet)cust_total(objSummaryBO);
                DataTable dtsalesbydlr = new DataTable();
                DataTable dtsalesbydlrReport = new DataTable();
                if (dsTables.Tables.Count > 0)
                {
                    dtsalesbydlr = dsTables.Tables[0];
                    dtsalesbydlrReport = dsTables.Tables[1];
                }
                if (dtsalesbydlr.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtsalesbydlr.Rows.Count; i++)
                    {
                        sv_1 += dtsalesbydlr.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[2].ToString());
                        sv_0 += dtsalesbydlr.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[3].ToString());
                        ev += dtsalesbydlr.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[4].ToString());
                        ytd += dtsalesbydlr.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[6].ToString());
                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtsalesbydlr.Rows.Add("CHANNEL PARTNER TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtsalesbydlrReport.Rows.Add("CHANNEL PARTNER TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                dtcust.Merge(dtsalesbydlr);
                dtcustReport.Merge(dtsalesbydlrReport);

                if (dtcust.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtcust.Rows.Count; i++)
                    {
                        if (dtcust.Rows[i].ItemArray[0].ToString() != "CUSTOMER TOTAL" && dtcust.Rows[i].ItemArray[0].ToString() != "CHANNEL PARTNER TOTAL")
                        {
                            sv_1 += dtcust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[2].ToString());
                            sv_0 += dtcust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[3].ToString());
                            ev += dtcust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[4].ToString());
                            ytd += dtcust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[6].ToString());
                        }

                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtcustReport.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                Session["salesbycustomer"] = dtcustReport;
                salesbycustomer.DataSource = dtcust;
                salesbycustomer.DataBind();
            //}
            //else
            //{
            //    string totaltxt = null;
            //    if (cust_type == "C")
            //    {
            //        totaltxt = "CUSTOMER TOTAL";
            //    }
            //    else
            //    {
            //        totaltxt = "CHANNEL PARTNER TOTAL";
            //    }
            //    DataTable dtcust = new DataTable();
            //    DataTable dtcustReport = new DataTable();
            //    /*Case:Customer Toatl*/
            //    DataSet dsTables = (DataSet)objReports.cust_total(branch_Code, se_id, cust_type, cust_num, byValueIn, cter);
            //    DataTable dtsalesbycust = new DataTable();
            //    DataTable dtsalesbycustReport = new DataTable();
            //    if (dsTables.Tables.Count > 0)
            //    {
            //        dtsalesbycustReport = dsTables.Tables[1];
            //        dtsalesbycust = dsTables.Tables[0];
            //    }
            //    if (dtsalesbycust.Rows.Count != 0)
            //    {
            //        decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
            //        for (int i = 0; i < dtsalesbycust.Rows.Count; i++)
            //        {
            //            sv_1 += dtsalesbycust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[2].ToString());
            //            sv_0 += dtsalesbycust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[3].ToString());
            //            ev += dtsalesbycust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[4].ToString());
            //            ytd += dtsalesbycust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[6].ToString());
            //        }
            //        change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
            //        acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
            //        askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
            //        dtsalesbycust.Rows.Add(totaltxt, "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt).ToString("N0", culture) + "%", Math.Round(askrate));
            //        dtsalesbycustReport.Rows.Add(totaltxt, "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt).ToString() + "%", Math.Round(askrate));
            //    }


            //    dtcust.Merge(dtsalesbycust);
            //    dtcustReport.Merge(dtsalesbycustReport);
            //    if (dtcust.Rows.Count != 0)
            //    {
            //        decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
            //        for (int i = 0; i < dtcust.Rows.Count; i++)
            //        {
            //            if (dtcust.Rows[i].ItemArray[0].ToString() != "CUSTOMER TOTAL" && dtcust.Rows[i].ItemArray[0].ToString() != "CHANNEL PARTNER TOTAL")
            //            {
            //                sv_1 += dtcust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[2].ToString());
            //                sv_0 += dtcust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[3].ToString());
            //                ev += dtcust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[4].ToString());
            //                ytd += dtcust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[6].ToString());
            //            }

            //        }
            //        change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
            //        acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
            //        askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
            //        dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
            //        dtcustReport.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
            //    }
            //    Session["salesbycustomer"] = dtcustReport;
            //    salesbycustomer.DataSource = dtcust;
            //    salesbycustomer.DataBind();
            //}


        }

        private DataSet cust_total(SummaryBO objSummaryBO)
        {
            objSummaryBL = new SummaryBL();
            int actual_mnth = 12 - (objSummaryBL.getActualMonthBL());
            DataSet dsTables = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("cust_name");
            dt.Columns.Add("cust_number");
            dt.Columns.Add("sales_value_year_1");
            dt.Columns.Add("sales_value_year_0");
            dt.Columns.Add("estimate_value_next_year");

            dt.Columns.Add("change", typeof(string));
            dt.Columns.Add("ytd", typeof(string));
            dt.Columns.Add("acvmnt", typeof(string));
            dt.Columns.Add("askrate", typeof(string));

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("cust_name");
            dtReport.Columns.Add("cust_number");
            dtReport.Columns.Add("sales_value_year_1");
            dtReport.Columns.Add("sales_value_year_0");
            dtReport.Columns.Add("estimate_value_next_year");
            dtReport.Columns.Add("change", typeof(string));
            dtReport.Columns.Add("ytd", typeof(string));
            dtReport.Columns.Add("acvmnt", typeof(string));
            dtReport.Columns.Add("askrate", typeof(string));

            DataTable dtCTotals;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            dtCTotals = objSummaryBL.getSalesbycustomersBL(objSummaryBO);

            for (int i = 0; i < dtCTotals.Rows.Count; i++)
            {
                float pre_actlyear = dtCTotals.Rows[i].ItemArray[2].ToString() == "" || dtCTotals.Rows[i].ItemArray[2].ToString() == null ? 0 //13
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[2].ToString());
                float actlyear = dtCTotals.Rows[i].ItemArray[3].ToString() == "" || dtCTotals.Rows[i].ItemArray[3].ToString() == null ? 0   //14
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtCTotals.Rows[i].ItemArray[4].ToString() == "" || dtCTotals.Rows[i].ItemArray[4].ToString() == null ? 0   //15
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[4].ToString());
                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);


                string cust_name = dtCTotals.Rows[i].ItemArray[0].ToString();
                string cust_number = dtCTotals.Rows[i].ItemArray[1].ToString();
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100; //15/14
                float ytd = dtCTotals.Rows[i].ItemArray[5].ToString() == "" || dtCTotals.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[5].ToString());
                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100; //ytd/2015B

                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = objSummaryBL.DeleteEmptyRowsBL(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dt.Rows.Add(cust_name, cust_number, (Math.Round(pre_actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next)).ToString("N0", culture), Convert.ToString(Math.Round(change)) + "%", (Math.Round(ytd)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt)) + "%", (Math.Round(askrate)).ToString("N0", culture));
                    dtReport.Rows.Add(cust_name, cust_number, (Math.Round(pre_actlyear, 0)).ToString(), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next)).ToString(), Convert.ToString(Math.Round(change)) + "%", (Math.Round(ytd)).ToString(), Convert.ToString(Math.Round(acvmnt)) + "%", (Math.Round(askrate)).ToString());
                }
            }
            dsTables.Tables.Add(dt);
            dsTables.Tables.Add(dtReport);
            return dsTables;
        }
        protected void bindgridColor()
        {
            if (goldproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in goldproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 3)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (spcproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in spcproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (fiveyrsproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in fiveyrsproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (topproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in topproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (bbproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in bbproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbylinegrid.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbylinegrid.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyfamilygrid.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyfamilygrid.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyapp.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyapp.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyapp_qty.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyapp_qty.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbycustomer.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbycustomer.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }

        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_5");
            cssList.Add("color_4");
            cssList.Add("color_2");
            cssList.Add("greendark");
        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssList.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(5); }

        }


        protected void goldproducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            objSummaryBL = new SummaryBL();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objSummaryBL.getBudgetYearBL();
                int ActualYear = objSummaryBL.getActualYearBL() - 1;
                int ActualMonth = objSummaryBL.getActualMonthBL();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }


                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }
                e.Row.Cells[1].Text = ActualValueyear1 + "<br/> " + valueIn; //2013
                // e.Row.Cells[2].Text = ActualValueyear1 + "<br/> Rupee(000)";
                e.Row.Cells[2].Text = Year + "<br/> " + valueIn; //2014
                e.Row.Cells[3].Text = BudgetYear + "B" + "<br/>" + valueIn; //2015
                e.Row.Cells[4].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE"; //2015/2014
                e.Row.Cells[5].Text = BudgetYear + " " + "YTD " + "<br/> " + valueIn;



                e.Row.Cells[6].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[7].Text = BudgetYear + "B" + " " + " Ask.Rate" + " <br/>" + valueIn;

            }
        }


        protected void salesbyapp_cust_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            objSummaryBL = new SummaryBL();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objSummaryBL.getBudgetYearBL();
                int ActualYear = objSummaryBL.getActualYearBL() - 1;
                int ActualMonth = objSummaryBL.getActualMonthBL();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }
                e.Row.Cells[2].Text = ActualValueyear1 + "<br/>" + valueIn;
                e.Row.Cells[3].Text = Year + "<br/> " + valueIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + valueIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + valueIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate " + " <br/>" + valueIn;

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblYtdPlan = e.Row.FindControl("lbl_Budget") as Label;
                var lblYTDSale = e.Row.FindControl("lbl_YtdSale") as Label;
                var lbl_customername = e.Row.FindControl("Labelcust_name") as Label;
                if (lbl_customername != null)
                    if (!lbl_customername.Text.ToString().Contains("TOTAL"))
                        if (lblYtdPlan != null)
                        {
                            if (lblYtdPlan.Text == "0" && (lblYTDSale.Text != "0"))
                            {
                                e.Row.Cells[6].CssClass = "tdHighlight";
                            }
                        }


            }

        }

        protected void salesbyapp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            objSummaryBL = new SummaryBL();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objSummaryBL.getBudgetYearBL();
                int ActualYear = objSummaryBL.getActualYearBL() - 1;
                int ActualMonth = objSummaryBL.getActualMonthBL();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }

                else if (byValueIn == 1)
                {
                    valueIn = " VAL";
                }

                e.Row.Cells[2].Text = ActualValueyear1 + "<br/> " + valueIn;
                e.Row.Cells[3].Text = Year + "<br/> " + valueIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + valueIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + valueIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate" + "<br/> " + valueIn;
            }

        }

        protected void salesbyapp_qty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            objSummaryBL = new SummaryBL();
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objSummaryBL.getBudgetYearBL();
                int ActualYear = objSummaryBL.getActualYearBL() - 1;
                int ActualMonth = objSummaryBL.getActualMonthBL();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string QtyIn = " QTY(000)";
                if (byValueIn == 100000)
                {
                    QtyIn = " QTY(00,000)";
                }

                else if (byValueIn == 1)
                {
                    QtyIn = " QTY";
                }


                e.Row.Cells[2].Text = ActualValueyear1 + "<br/> " + QtyIn;
                e.Row.Cells[3].Text = Year + "<br/> " + QtyIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + QtyIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + QtyIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate" + "<br/> " + QtyIn;
            }

        }


        public DataSet getSalesByLineSum(SummaryBO objSummaryBO)
        {
            objSummaryBL = new SummaryBL();
            DataSet dsTables = new DataSet();

            DataTable dtsales = new DataTable();
            dtsales.Columns.Add("FixedRow", typeof(string));
            dtsales.Columns.Add("sales_value_year_1", typeof(string));
            dtsales.Columns.Add("sales_value_year_0", typeof(string));
            dtsales.Columns.Add("estimate_value_next_year", typeof(string));
            dtsales.Columns.Add("p_sales_value_year_1", typeof(string));
            dtsales.Columns.Add("ytd", typeof(string));
            dtsales.Columns.Add("ach", typeof(string));
            dtsales.Columns.Add("arate", typeof(string));

            DataTable dtsalesReports = new DataTable();
            dtsalesReports.Columns.Add("FixedRow", typeof(string));
            dtsalesReports.Columns.Add("sales_value_year_1", typeof(string));
            dtsalesReports.Columns.Add("sales_value_year_0", typeof(string));
            dtsalesReports.Columns.Add("estimate_value_next_year", typeof(string));
            dtsalesReports.Columns.Add("p_sales_value_year_1", typeof(string));
            dtsalesReports.Columns.Add("ytd", typeof(string));
            dtsalesReports.Columns.Add("ach", typeof(string));
            dtsalesReports.Columns.Add("arate", typeof(string));

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtSalesbyLine = new DataTable();
            int actual_mnth = 12 - (objSummaryBL.getActualMonthBL());
            if (objSummaryBO.Flag == "F")
            {
                dtSalesbyLine = dtfamilytotals;
            }
            else
            {
                dtSalesbyLine = objSummaryBL.getValueSum_SFBL(objSummaryBO);
            }
            for (int i = 0; i < dtSalesbyLine.Rows.Count; i++)
            {
                string subfmname = dtSalesbyLine.Rows[i].ItemArray[1].ToString();
                float top0 = dtSalesbyLine.Rows[i].ItemArray[2].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[2].ToString() == null ? 0    //2013
                  : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[2].ToString());


                float top1 = dtSalesbyLine.Rows[i].ItemArray[3].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[3].ToString() == null ? 0    //2014
                    : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[3].ToString());


                float top2 = dtSalesbyLine.Rows[i].ItemArray[4].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[4].ToString() == null ? 0   //2015 budget
                   : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[4].ToString());



                float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                float top4 = dtSalesbyLine.Rows[i].ItemArray[5].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                    : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[5].ToString());



                //ytd/2015
                // decimal top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;
                float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;


                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                bool ckforZeros = objSummaryBL.DeleteEmptyRowsBL(top0, top1, top2, top3, top4, top5, top6);
                if (ckforZeros == false)
                {
                    dtsales.Rows.Add(subfmname, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                    dtsalesReports.Rows.Add(subfmname, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                }
            }
            dsTables.Tables.Add(dtsales);
            dsTables.Tables.Add(dtsalesReports);

            return dsTables;
        }


        protected void byValueorunitsorlacks_CheckedChanged(Object sender, EventArgs e)
        {
            
            if (Rdbtnunits.Checked)
            {
                byValueIn = 1;
            }

            if (Rdbtnvalue.Checked)
            {
                byValueIn = 1000;
            }

            if (Rdbtnlacks.Checked)
            {
                byValueIn = 100000;
            }

            tablesLoadedStatus = 1;

            bindgridsalesbyapp();
            bindgridsalesbyappQty();

            LoadCSS();
            bindgridColor();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "byValueorunitsorlacks_CheckedChanged", "<script>byValueorunitsorlacks_CheckedChanged()</script>", false);
        }

        public DataSet getsalesbyappQty(SummaryBO objSummaryBO)
        {
            objSummaryBL = new SummaryBL();
            DataSet dsTables = new DataSet();
            DataTable dtapp = new DataTable();

            dtapp.Columns.Add("app_Desc", typeof(string));
            dtapp.Columns.Add("app_Code", typeof(string));
            dtapp.Columns.Add("sales_value_year_1", typeof(string));
            dtapp.Columns.Add("sales_value_year_0", typeof(string));
            dtapp.Columns.Add("estimate_value_next_year", typeof(string));

            dtapp.Columns.Add("change", typeof(string));
            dtapp.Columns.Add("ytd", typeof(string));
            dtapp.Columns.Add("acvmnt", typeof(string));
            dtapp.Columns.Add("askrate", typeof(string));


            DataTable dtappReport = new DataTable();

            dtappReport.Columns.Add("app_Desc", typeof(string));
            dtappReport.Columns.Add("app_Code", typeof(string));
            dtappReport.Columns.Add("sales_value_year_1", typeof(string));
            dtappReport.Columns.Add("sales_value_year_0", typeof(string));
            dtappReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtappReport.Columns.Add("change", typeof(string));
            dtappReport.Columns.Add("ytd", typeof(string));
            dtappReport.Columns.Add("acvmnt", typeof(string));
            dtappReport.Columns.Add("askrate", typeof(string));


            int actual_mnth = 12 - (objSummaryBL.getActualMonthBL());
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };



            DataTable dtsalesbyapp = objSummaryBL.getSalesbyApplicationQtyBL(objSummaryBO);
            for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
            {
                float pre_actlyear = dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[2].ToString() == null ? 0    //13
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                float actlyear = dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[3].ToString() == null ? 0      //14
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[4].ToString() == null ? 0     //15
                           : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                float ytd = dtsalesbyapp.Rows[i].ItemArray[5].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[5].ToString());

                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);
                string app_desc = dtsalesbyapp.Rows[i].ItemArray[0].ToString();
                string app_code = dtsalesbyapp.Rows[i].ItemArray[1].ToString();

                //15/14

                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                //ytd/2015B
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100;
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100;
                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = objSummaryBL.DeleteEmptyRowsBL(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dtapp.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString("N0", culture)), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next, 0)).ToString("N0", culture), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString("N0", culture));
                    dtappReport.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString()), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next, 0)).ToString(), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString(), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString());
                }


            }
            dsTables.Tables.Add(dtapp);
            dsTables.Tables.Add(dtappReport);
            return dsTables;
        }


    }
}