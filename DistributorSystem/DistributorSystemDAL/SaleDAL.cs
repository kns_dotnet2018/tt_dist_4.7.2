﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    
    public class SaleDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetSaleItemDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.Get_All_Items_on_Sale, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetItemsSubmittedonSaleDAL(SaleBO objSaleBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getItemSubmittedOnSale, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar,100).Value = objSaleBO.cust_num;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public SaleBO PlaceOrderDAL(SaleBO objBO)
        {
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                objCom.TestLog(String.Concat(Convert.ToString(objBO.item_available_id)," - ",objBO.cust_num," - ", objBO.order_qty, " - ", objBO.user_id));
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.place_order, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_item_available_id, SqlDbType.Int).Value = objBO.item_available_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_customer_id, SqlDbType.VarChar, 200).Value = objBO.cust_num;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_order_qty, SqlDbType.VarChar, 100).Value = objBO.order_qty;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_user_id, SqlDbType.VarChar, 100).Value = objBO.user_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar,-1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objBO;
        }
    }
}
