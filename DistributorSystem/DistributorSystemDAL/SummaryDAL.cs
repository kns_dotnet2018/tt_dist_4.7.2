﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class SummaryDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataSet GetCustomerAndProjectsDAL(SummaryBO objBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getCustomers_Projects, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Distributor_number, SqlDbType.VarChar, 50).Value = objBO.distributor_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, 50).Value = objBO.customer_number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataSet GetProjectDetailsDAL(SummaryBO objBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getProjectDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, 100).Value = objBO.customer_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.project_number, SqlDbType.VarChar, 100).Value = objBO.project_number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataTable GetPurchaseSummaryDAL(SummaryBO objBO)
        {
            DataTable dsOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            //List<BudgetSummaryBO> events = new List<BudgetSummaryBO>();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getBudgetSummary, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_customer_number, SqlDbType.VarChar, 100).Value = objBO.distributor_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_effective_date, SqlDbType.Date).Value = null;
                //SqlDataReader reader = sqlcmd.ExecuteReader();
                //while (reader.Read())
                //{
                //    BudgetSummaryBO cevent = new BudgetSummaryBO();
                //    cevent.FAMILY_NAME = Convert.ToString(reader["FAMILY_NAME"]);
                //    cevent.SUBFAMILY_NAME = Convert.ToString(reader["SUBFAMILY_NAME"]);
                //    cevent.APPLICATION_DESC = Convert.ToString(reader["APPLICATION_DESC"]);
                //    cevent.Value = Convert.ToString(reader["Value"]);
                //    cevent.Quantity = Convert.ToString(reader["Quantity"]);
                //    events.Add(cevent);
                //}

                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataTable getEntryMain(SummaryBO objSummaryBO)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_get_entry_final", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        public DataTable AddInsertPerToolDAL(DataTable Dtbudget)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_AddInsertPerTool", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tblBudget", Dtbudget);

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                //return dtBudget;
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        public DataTable RemoveEmptyRowsDAL(DataTable source)
        {
            int colCount = source.Columns.Count;
            int count = 0;
            DataRow currentRow = source.Rows[0];
            foreach (var colValue in currentRow.ItemArray)
            {
                if (!string.IsNullOrEmpty(colValue.ToString()))
                {
                    count++;
                }
            }
            if (count == 0)
            {
                // If we get here, all the columns are empty
                source.Rows[0].Delete();
                source.AcceptChanges();
            }
            return source;
        }

        public DataTable getSpecialGroupValueSumDAL(SummaryBO objSummaryBO)
        {
            DataTable dsgoldprod = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50).Value = objSummaryBO.Flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50).Value = objSummaryBO.fname;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dsgoldprod);

                return dsgoldprod;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dsgoldprod;
        }

        public int getBudgetYearDAL()
        {
            int year = 0;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("GetProfile", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

                parameters[0].Value = "BUDGET_YEAR";
                parameters[1].Direction = ParameterDirection.Output;
                parameters[2].Direction = ParameterDirection.Output;
                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    string profile_value = command.Parameters["@profile_value"].Value.ToString();
                    year = Convert.ToInt32(profile_value);
                }

                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return year;
        }

        public int getActualMonthDAL()
        {
            int Month = 0;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };



            parameters[0].Value = "ACTUAL_MONTH";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                Month = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return Month;
        }

        public int getActualYearDAL()
        {
            int year = 0;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("GetProfile", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

                parameters[0].Value = "ACTUAL_YEAR";
                parameters[1].Direction = ParameterDirection.Output;
                parameters[2].Direction = ParameterDirection.Output;
                Int32 j;
                for (j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }

                try
                {
                    command.ExecuteNonQuery();
                    string profile_value = command.Parameters["@profile_value"].Value.ToString();
                    year = Convert.ToInt32(profile_value);
                }

                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return year;
        }

        public DataTable getSalesbycustomersDAL(SummaryBO objSummaryBO)
        {
            DataTable dssalesbycust = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbycustomer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = null;
                //command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = bmid;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbycust);

                return dssalesbycust;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbycust;
        }

        
        public DataTable salesbyfamilyDAL(SummaryBO objSummaryBO)
        {
            DataTable dtfamily = new DataTable();
            dtfamily = getValueSum_SFDAL(objSummaryBO);
            return dtfamily;
        }

        public DataTable getValueSum_SFDAL(SummaryBO objSummaryBO)
        {
            DataTable dssalesbyline = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum_SF", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 10).Value = "ALL";
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 10).Value = "ALL";
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 10).Value = "ALL";
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 10).Value = objSummaryBO.Flag;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyline);

                return dssalesbyline;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyline;
        }

        public DataTable getValueSumDAL(SummaryBO objSummaryBO)
        {
            DataTable dstotals = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50).Value = objSummaryBO.Flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dstotals);

                return dstotals;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dstotals;
        }

        public DataTable getsalesbyApplicationDAL(SummaryBO objSummaryBO)
        {
            DataTable dssalesbyapp = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbyapp", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyapp);
                command.CommandTimeout = 100000;
                return dssalesbyapp;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyapp;
        }

        public DataTable getSalesbyApplicationQtyDAL(SummaryBO objSummaryBO)
        {
            DataTable dssalesbyapp = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbyappQty", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = objSummaryBO.distributor_number;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = null;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = null;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyapp);

                return dssalesbyapp;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyapp;
        }

        public DataTable getExceptionReportDAL(SummaryBO objSummaryBO)
        {
            DataTable dtReport = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand(ResourceFileDAL.sp_getExceptionReportForCP, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceFileDAL.Year, SqlDbType.Int).Value = objSummaryBO.year;
                command.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = objSummaryBO.value;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtReport);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtReport;
        }

        public DataTable SendMailDAL(SummaryBO objSummaryBO)
        {
            DataTable dtMailContent = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand(ResourceFileDAL.sp_get_MailIdsForCP, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceFileDAL.Year, SqlDbType.Int).Value = objSummaryBO.year;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtMailContent);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtMailContent;
        }
    }
}
