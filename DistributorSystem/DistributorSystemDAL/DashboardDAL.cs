﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class DashboardDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataSet GetSalesDashboardDataDAL(DashboardBO objBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getSalesDashboardForDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Distributor_number, SqlDbType.Int).Value = objBO.distributor_number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataSet GetPuchaseDashboardDataDAL(DashboardBO objBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getPurchaseDashboardForDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Distributor_number, SqlDbType.Int).Value = objBO.distributor_number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }
    }
}
