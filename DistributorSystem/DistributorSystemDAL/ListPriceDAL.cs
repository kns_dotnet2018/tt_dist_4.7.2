﻿using DistributorSystemBO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DistributorSystemDAL
{

   public class ListPriceDAL
    {
        CommonFunctions objCom = new CommonFunctions();

        public DataTable GetListPriceDetails()
        {
            DataTable dt = new DataTable();
            SqlConnection sqlconnection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd = null;
            SqlDataAdapter sda = null;
            try
            {
                sqlconnection.Open();
                cmd = new SqlCommand(ResourceFileDAL.GetListPriceDetails, sqlconnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 180;
                sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            catch(Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconnection.Close();
            }
            return dt;
        }

        public DataTable GetCustomersBasedOnDistributor(string distNum)
        {
            DataTable dt = new DataTable();
            SqlConnection sqlconnection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd = null;
            SqlDataAdapter sda = null;
            try
            {
                sqlconnection.Open();
                cmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, 50).Value = distNum;
                sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconnection.Close();
            }
            return dt;
        }
    }
}
