﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class UserDetailsDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getUserDetailsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDistributorDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public UserDetailsBO saveUserDetailsDAL(UserDetailsBO objUserBO)
        {
            UserDetailsBO objOutput = new UserDetailsBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_updateUserDetails, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.UserID, SqlDbType.VarChar, 20).Value = objUserBO.userid;
                sqlcmd.Parameters.Add(ResourceFileDAL.EMAILID, SqlDbType.VarChar, -1).Value = objUserBO.emailid;
                sqlcmd.Parameters.Add(ResourceFileDAL.STATUS, SqlDbType.Int).Value = objUserBO.status;
                sqlcmd.Parameters.Add(ResourceFileDAL.Role, SqlDbType.VarChar, 100).Value = objUserBO.role;
                sqlcmd.Parameters.Add(ResourceFileDAL.Password, SqlDbType.VarChar, -1).Value = objUserBO.password; 
                sqlcmd.Parameters.Add(ResourceFileDAL.Menuid, SqlDbType.VarChar, -1).Value = objUserBO.menuid;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objOutput.err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
