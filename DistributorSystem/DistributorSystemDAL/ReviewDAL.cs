﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DistributorSystemBO;
using System.Data.SqlClient;
using System.Configuration;

namespace DistributorSystemDAL
{
    public class ReviewDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getMonthlyQTYDAL(ReviewBO objReviewBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.getMonthlyQuantitySum, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, -1).Value = objReviewBO.Distributor_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Year, SqlDbType.Int).Value = objReviewBO.year;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 10).Value = objReviewBO.Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Cter, SqlDbType.VarChar, 10).Value = objReviewBO.Cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = objReviewBO.Valuein;
                sqlcmd.Parameters.Add(ResourceFileDAL.BranchCodeNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Branchcode;
                sqlcmd.Parameters.Add(ResourceFileDAL.SalesEngineerIdNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Salesengineer;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemFamilyNameNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_familyname;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemSubFamilynameNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_subfamilyname;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomertypeNotRequired, SqlDbType.VarChar, 10).Value = objReviewBO.Customer_type;
                sqlcmd.Parameters.Add(ResourceFileDAL.ProductGroupNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Productgroup;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemCodeNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_code;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getMonthlyValDAL(ReviewBO objReviewBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.getMonthlyValueSum, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, -1).Value = objReviewBO.Distributor_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Year, SqlDbType.Int).Value = objReviewBO.year;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 10).Value = objReviewBO.Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Cter, SqlDbType.VarChar, 10).Value = objReviewBO.Cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = objReviewBO.Valuein;
                sqlcmd.Parameters.Add(ResourceFileDAL.BranchCodeNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Branchcode;
                sqlcmd.Parameters.Add(ResourceFileDAL.SalesEngineerIdNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Salesengineer;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemFamilyNameNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_familyname;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemSubFamilynameNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_subfamilyname;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomertypeNotRequired, SqlDbType.VarChar, 10).Value = objReviewBO.Customer_type;
                sqlcmd.Parameters.Add(ResourceFileDAL.ProductGroupNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Productgroup;
                sqlcmd.Parameters.Add(ResourceFileDAL.ItemCodeNotRequired, SqlDbType.VarChar, -1).Value = objReviewBO.Item_code;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getSalesReviewDAL(ReviewBO objReviewBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getSalesReviewData, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, 10).Value = objReviewBO.customer_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Distributor_number, SqlDbType.VarChar, 10).Value = objReviewBO.Distributor_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Salesman_id, SqlDbType.VarChar, -1).Value = objReviewBO.Salesengineer;
                 sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getFamilyDAL()
        {
            DataTable dtSubFam = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getFamily", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtSubFam);
                return dtSubFam;
            }

            catch (Exception ex) {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtSubFam;
        }

        public DataTable getProductDAL(ReviewBO objReviewBO)
        {
            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getApplicationByFam", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@familyId", SqlDbType.VarChar, 50000, null).Value = objReviewBO.Item_familyID;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 50000, null).Value = objReviewBO.Productgroup;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }
    }
}
