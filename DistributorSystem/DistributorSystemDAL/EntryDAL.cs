﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class EntryDAL
    {
        CommonFunctions objCom = new CommonFunctions();

        public DataTable GetCustomerDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataSet SaveBulkBudgetBL(DataTable Distributors, int valuein)
        {
            EntryBO objEntryBO = new EntryBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlAd;
            DataSet ds = new DataSet();
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_SaveDistributorBudget, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tableType, Distributors);
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = valuein;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlAd = new SqlDataAdapter(sqlcmd);
                //objEntryBO.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                //objEntryBO.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
                sqlAd.Fill(ds);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return ds;
        }

        public DataTable GetCustomerMonthlySalesDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();

            DataTable dtCust1 = new DataTable();
            DataTable dtCust2 = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csDistributor"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            int currentmonth;
            try
            {
                //conn.Open();
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtCust1);

               
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomerMonthlySalessBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = objEntryBO.value;
                sqlcmd.CommandTimeout = 1000000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtCust2);

                currentmonth = DateTime.Now.Month;
                if (dtCust2 != null)
                {
                    dtOutput = dtCust2.Copy();
                    if (!dtOutput.Columns.Contains("month1"))
                        dtOutput.Columns.Add("month1");
                    if (!dtOutput.Columns.Contains("month2"))
                        dtOutput.Columns.Add("month2");
                    if (!dtOutput.Columns.Contains("month3"))
                        dtOutput.Columns.Add("month3");
                    if (!dtOutput.Columns.Contains("month4"))
                        dtOutput.Columns.Add("month4");
                    if (!dtOutput.Columns.Contains("month5"))
                        dtOutput.Columns.Add("month5");
                    if (!dtOutput.Columns.Contains("month6"))
                        dtOutput.Columns.Add("month6");
                    if (!dtOutput.Columns.Contains("month7"))
                        dtOutput.Columns.Add("month7");
                    if (!dtOutput.Columns.Contains("month8"))
                        dtOutput.Columns.Add("month8");
                    if (!dtOutput.Columns.Contains("month9"))
                        dtOutput.Columns.Add("month9");
                    if (!dtOutput.Columns.Contains("month10"))
                        dtOutput.Columns.Add("month10");
                    if (!dtOutput.Columns.Contains("month11"))
                        dtOutput.Columns.Add("month11");
                    if (!dtOutput.Columns.Contains("month12"))
                        dtOutput.Columns.Add("month12");
                    var rowsOnlyInDt1 = from r in dtCust1.AsEnumerable()
                                        where !dtCust2.AsEnumerable().Any(r2 => r["customernumber"] != r2["customernumber"])
                                        select r;
                    if (rowsOnlyInDt1.Any())
                        dtOutput.Merge(rowsOnlyInDt1.CopyToDataTable());
                }
                else
                {
                    dtOutput = dtCust1.Copy();
                    if (!dtOutput.Columns.Contains("month1"))
                        dtOutput.Columns.Add("month1");
                    if (!dtOutput.Columns.Contains("month2"))
                        dtOutput.Columns.Add("month2");
                    if (!dtOutput.Columns.Contains("month3"))
                        dtOutput.Columns.Add("month3");
                    if (!dtOutput.Columns.Contains("month4"))
                        dtOutput.Columns.Add("month4");
                    if (!dtOutput.Columns.Contains("month5"))
                        dtOutput.Columns.Add("month5");
                    if (!dtOutput.Columns.Contains("month6"))
                        dtOutput.Columns.Add("month6");
                    if (!dtOutput.Columns.Contains("month7"))
                        dtOutput.Columns.Add("month7");
                    if (!dtOutput.Columns.Contains("month8"))
                        dtOutput.Columns.Add("month8");
                    if (!dtOutput.Columns.Contains("month9"))
                        dtOutput.Columns.Add("month9");
                    if (!dtOutput.Columns.Contains("month10"))
                        dtOutput.Columns.Add("month10");
                    if (!dtOutput.Columns.Contains("month11"))
                        dtOutput.Columns.Add("month11");
                    if (!dtOutput.Columns.Contains("month12"))
                        dtOutput.Columns.Add("month12");
                }
                var rowsCommon = from table1 in dtCust2.AsEnumerable()
                                 join table2 in dtCust1.AsEnumerable() on
                                 table1.Field<Int32>("customernumber") equals table2.Field<Int32>("customernumber")
                                 where table1.Field<Int32>("customernumber") == table2.Field<Int32>("customernumber")
                                 select table2;
                if (!dtOutput.Columns.Contains("Active"))
                dtOutput.Columns.Add("Active");

                dtOutput.AsEnumerable().Join(dtCust1.AsEnumerable(),
                     dt1_Row => dt1_Row.ItemArray[0],
                     dt2_Row => dt2_Row.ItemArray[0],
                     (dt1_Row, dt2_Row) => new { dt1_Row, dt2_Row }).ToList()
                     .ForEach(o => o.dt1_Row.SetField(14, "Y"));

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataSet SaveBulkMonthlySalesDAL(DataTable Distributors, int Valuein)
        {
            EntryBO objEntryBO = new EntryBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            DataSet ds = new DataSet();
            SqlDataAdapter sqlAd;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_SaveDistributorMonthlySales, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tableType, Distributors);
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = Valuein;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //sqlcmd.ExecuteNonQuery();
                //objEntryBO.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                //objEntryBO.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
                sqlAd = new SqlDataAdapter(sqlcmd);
                sqlAd.Fill(ds);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return ds;
        }

        public DataTable GetAnualTargetDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAnnualTargetForDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.Year, SqlDbType.Int).Value = objEntryBO.year;
                sqlcmd.Parameters.Add(ResourceFileDAL.Valuein, SqlDbType.Int).Value = objEntryBO.value;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetSalesManDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_get_SM_For_Customer, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_customer_number, SqlDbType.VarChar, -1).Value = objEntryBO.customer_number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetSalesManDetailsDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter da = null;
            SqlCommand cmd = null;
            try
            {
                conn.Open();
                cmd = new SqlCommand(ResourceFileDAL.sp_get_SM_For_Customer,conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                cmd.Parameters.Add(ResourceFileDAL.p_customer_number, SqlDbType.VarChar, -1).Value = objEntryBO.customer_number;
                da = new SqlDataAdapter(cmd);
                da.Fill(dtOutput);     
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

            return dtOutput;
        }

        public DataTable GetCustomerBasedOnSalesDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter da = null;
            SqlCommand cmd = null;
            try
            {
                conn.Open();
                cmd = new SqlCommand(ResourceFileDAL.sp_get_Customer_For_SM, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                cmd.Parameters.Add(ResourceFileDAL.Salesman_id, SqlDbType.VarChar, -1).Value = objEntryBO.salesman_id;
                da = new SqlDataAdapter(cmd);
                da.Fill(dtOutput);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }
            return dtOutput;
        }

        public DataSet SaveBulkMonthlySalesAdminBL(DataTable Distributors)
        {
            EntryBO objEntryBO = new EntryBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            DataSet ds = new DataSet();
            SqlDataAdapter sqlAd;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_SaveDistributorMonthlySalesByAdmin, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tableType, Distributors);
                sqlAd = new SqlDataAdapter(sqlcmd);
                sqlAd.Fill(ds);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return ds;
        }

        public DataTable GetCustomerMonthlySalesForAdminDAL(EntryBO objEntryBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomerMonthlySalessBasedOnDistributorForAdmin, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objEntryBO.distributor_name;
                sqlcmd.CommandTimeout = 100000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
