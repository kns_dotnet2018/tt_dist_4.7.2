﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DistributorSystemBO;

namespace DistributorSystemDAL
{
    public class QuoteDAL
    {
        CommonFunctions objCom = new CommonFunctions();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objQuoteBO"></param>
        /// <returns></returns>
        public DataTable GetItemDescDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getItemDesc, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetFrequentItemsDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getFrequentlyOrderedItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Requested_By, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCustomerListDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, -1).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }



        public DataTable GetRecentItemsDAL(QuoteBO objQuoteBO)
        {

            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getRecentlyOrderedItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Requested_By, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetItemDetailsDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetItemDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO SaveQuotesDAL(DataTable dt)
        {
            QuoteBO objQuoteBO = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_saveQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tblQuote, dt);
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quotation_no, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objQuoteBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
                objQuoteBO.Ref_Number = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.RefNumber].Value);
                objQuoteBO.Quotation_no = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.Quotation_no].Value);
                if (!string.IsNullOrEmpty(Convert.ToString(objQuoteBO.Quotation_no)))
                {
                    if (Convert.ToString(objQuoteBO.Quotation_no).Contains("/C"))
                    {
                        sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                        sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "SubmitQuote";
                        sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                        sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                        sqlda = new SqlDataAdapter(sqlcmd);
                        sqlda.Fill(dtoutput);
                    }
                    else
                    {
                        sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                        sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "SubmitQuote";
                        sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                        sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                        sqlda = new SqlDataAdapter(sqlcmd);
                        sqlda.Fill(dtoutput);
                    }
                }
                else
                {
                    sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                    sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "SubmitQuote";
                    sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                    sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                    sqlda = new SqlDataAdapter(sqlcmd);
                    sqlda.Fill(dtoutput);
                }
                if (dtoutput != null)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        objQuoteBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                        objQuoteBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                        objQuoteBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                        objQuoteBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                    }
                }

            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.ErrorLog(ex);
            }
            return objQuoteBO;
        }

        public QuoteBO getGPforItemDAL(QuoteBO objItemBO)
        {
            QuoteBO objQuoteBO = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getGPForItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Customer_Number, SqlDbType.VarChar, 100).Value = Convert.ToString(objItemBO.Cust_Number);
                sqlcmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 100).Value = Convert.ToString(objItemBO.CP_Number);
                sqlcmd.Parameters.Add(ResourceFileDAL.itemCode, SqlDbType.VarChar, 100).Value = Convert.ToString(objItemBO.Item_Number);
                sqlcmd.Parameters.Add(ResourceFileDAL.Quantity, SqlDbType.VarChar, 100).Value = Convert.ToString(objItemBO.quantity);
                sqlcmd.Parameters.Add(ResourceFileDAL.GP, SqlDbType.Float).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.SystemPrice, SqlDbType.Float).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.GP = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.GP].Value);
                objQuoteBO.SystemPrice = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.SystemPrice].Value);
            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.ErrorLog(ex);
            }
            return objQuoteBO;
        }
        public string DeleteQuotesDAL(string item)
        {
            string Output = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda;
            DataTable dtoutput = new DataTable();
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(ResourceFileDAL.DeleteQuotes, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceFileDAL.itemCode, SqlDbType.VarChar, 50).Value = item;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

                if (dtoutput.Rows.Count > 0)
                {
                    Output = Convert.ToString(dtoutput.Rows[0]["err_code"]);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }
        public DataTable GetQuoteDetailsDAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objquoteBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Status1, SqlDbType.VarChar, 100).Value = "";
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, 100).Value = objquoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.CommandTimeout = 10000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetQuoteDetailsForPADAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objquoteBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Status1, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Status;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustomerNumberAsDistributor, SqlDbType.VarChar, 100).Value = objquoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlcmd.CommandTimeout = 10000000;
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable GetQuoteSummaryforPADAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteSummaryForPA, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objquoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.roleId, SqlDbType.VarChar, 100).Value = null;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_id, SqlDbType.VarChar, 100).Value = null;
                sqlcmd.CommandTimeout = 10000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetQuoteSummaryDAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteSummary, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objquoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.roleId, SqlDbType.VarChar, 100).Value = null;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_id, SqlDbType.VarChar, 100).Value = null;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCompetitorsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getCompetitorsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO PlaceOrderDAL(QuoteBO objBO)
        {

            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_PlaceOrderForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.QuoteId, SqlDbType.Int).Value = objBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderType, SqlDbType.VarChar, 100).Value = objBO.OrderType;
                sqlcmd.Parameters.Add(ResourceFileDAL.ScheduleType, SqlDbType.VarChar, 100).Value = objBO.scheduleType;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderByID, SqlDbType.VarChar, 100).Value = objBO.orderbyId;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderByName, SqlDbType.VarChar, 100).Value = objBO.orderbyName;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustName, SqlDbType.VarChar, -1).Value = objBO.Cust_Name;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quantity, SqlDbType.VarChar, 100).Value = objBO.quantity;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderStartDate, SqlDbType.VarChar, 100).Value = objBO.OrderStartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Reason, SqlDbType.VarChar, -1).Value = objBO.POComment;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "PlaceOrder";
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
                if (dtoutput != null)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        objBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                        objBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                        objBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                        objBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                    }
                }


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objBO;
        }

        public string SubmitStatusDAL(int ID, string status, string ocNumber, int ocflag, string comment, string changedDate, string loggedBy)
        {
            string result = "";
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.insertUpdateQuotestatus, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.quoteId1, SqlDbType.Int).Value = ID;
                sqlcmd.Parameters.Add(ResourceFileDAL.ocflag, SqlDbType.Int, 200).Value = ocflag;
                sqlcmd.Parameters.Add(ResourceFileDAL.quotestatus, SqlDbType.VarChar, 100).Value = status;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag1, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.userId1, SqlDbType.VarChar, 100).Value = loggedBy;
                sqlcmd.Parameters.Add(ResourceFileDAL.StatusChangeDate, SqlDbType.DateTime, 100).Value = changedDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.OcNumber, SqlDbType.VarChar, 100).Value = ocNumber;
                sqlcmd.Parameters.Add(ResourceFileDAL.Comment1, SqlDbType.VarChar, 100).Value = comment;

                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                result = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return result;
        }

        public QuoteBO RequestForReApprovalDAL(DataTable dtQuote)
        {
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            DataSet ds = new DataSet();
            QuoteBO obj = new QuoteBO();
            try
            {
                sqlconn.Open();

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_quoteStatusChange, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tblQuote, dtQuote);
                //sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objBO.QuoteID;
                //sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                //sqlcmd.Parameters.Add(ResourceFileDAL.changeby, SqlDbType.VarChar, -1).Value = objBO.orderbyId;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Status1, SqlDbType.VarChar, -1).Value = objBO.status;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Reason, SqlDbType.VarChar, -1).Value = objBO.Reason;
                //sqlcmd.Parameters.Add(ResourceFileDAL.MOQ, SqlDbType.VarChar, 100).Value = objBO.MOQ;
                //sqlcmd.Parameters.Add(ResourceFileDAL.OfferPrice, SqlDbType.VarChar, 100).Value = objBO.Offer_Price;
                //sqlcmd.Parameters.Add(ResourceFileDAL.MultiOrderFlag, SqlDbType.Int).Value = objBO.MultiOrderFlag;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;

                //sqlcmd.ExecuteNonQuery();
                //objBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                //objBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(ds);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Columns.Remove("Item");
                            ds.Tables[0].AcceptChanges();
                            DataView view = new DataView(ds.Tables[0]);
                            string[] param = { "Ref_Number", "Quotation_no", "MailTo", "MAilCC", "Subject", "Body", "error_code", "error_msg", "PDF_Flag" };
                            DataTable distinctValues = view.ToTable(true, param);

                            var row = distinctValues.AsEnumerable()
                                .Where(o => o.Field<string>("MailTo") != null || o.Field<string>("MailTo") != "")
                                .Select(o => new QuoteBO
                                {
                                    Ref_Number = o.Field<string>("Ref_Number")
                                    ,
                                    to = o.Field<string>("MailTo")
                                    ,
                                    cc = o.Field<string>("MAilCC")
                                    ,
                                    subject = o.Field<string>("Subject")
                                    ,
                                    message = o.Field<string>("Body")
                                    ,
                                    Err_code = o.Field<Int32>("error_code")
                                    ,
                                    Err_msg = o.Field<string>("error_msg")
                                }).FirstOrDefault();
                            obj = row;
                        }
                    }
                }

                //sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                //sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "ReSubmitQuote";
                //sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                //sqlda = new SqlDataAdapter(sqlcmd);
                //sqlda.Fill(dtoutput);
                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        objBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                //        objBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                //        objBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                //        objBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                //    }
                //}

            }
            catch (Exception ex)
            {
                obj.Err_code = 1;
                obj.Err_msg = "There is some error in status update. Please try again.";
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return obj;
        }

        public DataTable getQuoteFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteFormat, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quotation_no, SqlDbType.VarChar, -1).Value = objQuoteBO.Quotation_no;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getQuotePOFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuotePO, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, -1).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAgreementPriceDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAgreementPriceByQTY, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quantity, SqlDbType.VarChar, 100).Value = objQuoteBO.quantity;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable GetAPListDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAgreementPrice, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO DraftQuotesDAL(DataTable dt)
        {
            QuoteBO objQuoteBO = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_draftQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tblQuote, dt);
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objQuoteBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);


            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.ErrorLog(ex);
            }
            return objQuoteBO;
        }

        public DataTable GetDraftedQuoteDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDraftedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 100).Value = objQuoteBO.CP_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetQuoteStatusLogDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getStatusLog, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
