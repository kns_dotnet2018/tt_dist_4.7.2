﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemBO;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    public class LoginBL
    {
        LoginDAL objLoginDAL = new LoginDAL();


        public LoginBO authLoginBL(LoginBO objLoginBO)
        {
            return objLoginDAL.authLoginDAL(objLoginBO);
        }

        public LoginBO ChangePasswordBL(LoginBO objLoginBO)
        {
            return objLoginDAL.ChangePasswordDAL(objLoginBO);
        }

        public DataSet GetAllMenusBL(LoginBO objLoginBO)
        {
            return objLoginDAL.GetAllMenusDAL(objLoginBO);
        }
    }
}
