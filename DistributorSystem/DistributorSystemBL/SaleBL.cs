﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemDAL;
using DistributorSystemBO;

namespace DistributorSystemBL
{
    public class SaleBL
    {
        SaleDAL objSaleDAL = new SaleDAL();
        public DataTable GetSaleItemBL()
        {
            return objSaleDAL.GetSaleItemDAL();
        }

        public SaleBO PlaceOrderBL(SaleBO objBO)
        {
            return objSaleDAL.PlaceOrderDAL(objBO);
        }

        public DataTable GetItemsSubmittedonSaleBL(SaleBO objSaleBO)
        {
            return objSaleDAL.GetItemsSubmittedonSaleDAL(objSaleBO);
        }
    }
}
