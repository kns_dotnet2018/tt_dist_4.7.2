﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DistributorSystemBO;
using System.Data;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    public class ReviewBL
    {
        ReviewDAL objReviewDAL = new ReviewDAL();
        public DataTable getMonthlyQTY(ReviewBO objReviewBO)
        {
            return objReviewDAL.getMonthlyQTYDAL(objReviewBO);
        }

        public DataTable getMonthlyVal(ReviewBO objReviewBO)
        {
            return objReviewDAL.getMonthlyValDAL(objReviewBO);
        }

        public DataTable getSalesReviewBL(ReviewBO objReviewBO)
        {
            return objReviewDAL.getSalesReviewDAL(objReviewBO);
        }

        public DataTable LoadFamilyId()
        {
            return objReviewDAL.getFamilyDAL();
        }

        public DataTable getProductsBL(ReviewBO objReviewBO)
        {
            return objReviewDAL.getProductDAL(objReviewBO);
        }
    }
}
