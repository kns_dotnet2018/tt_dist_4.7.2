﻿using DistributorSystemBO;
using DistributorSystemDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DistributorSystemBL
{
   public class FileUploadBL
    {
        FileUplaodDAL objfileDAL = new FileUplaodDAL();
        public DataTable getFileDetailsBL()
        {
            return objfileDAL.getFileDetailsDAL();
        }

        public DataTable getsaleFileDetailsBL(int flag,int fileid)
        {
            return objfileDAL.getsaleFileDetailsDAL(flag, fileid);
        }
        public DataSet GetdocType()
        {
            return objfileDAL.getdocumenttypeDAL();
        }
        public FileUploadBO FileUploadDetailsBL(FileUploadBO objfileBO)
        {
            return objfileDAL.UploadFileDetailsDAL(objfileBO);
        }

        public FileUploadBO salesFileUploadDetailsBL(FileUploadBO objfileBO,DataTable dt)
        {
            return objfileDAL.UploadSalesFileDetailsDAL(objfileBO, dt);
        }
        public FileUploadBO UpdateIsUploadColumnDetailsBL(string distributornumber)
        {
            return objfileDAL.UpdateIsUploadColumnDetails(distributornumber);
        }
        public FileUploadBO DeleteFileDetailsBL(string fileName)
        {
            return objfileDAL.DeleteFileDAL(fileName);
        }
        public FileUploadBO DeleteSaleFileDetailsBL(int fileid1)
        {
            return objfileDAL.DeleteSaleFileDAL(fileid1);
        }
    }
}
