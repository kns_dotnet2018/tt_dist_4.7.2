﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemBO;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    public class DashboardBL
    {
        DashboardDAL objDAL = new DashboardDAL();
        public DataSet GetSalesDashboardDataBL(DashboardBO objBO)
        {
            return objDAL.GetSalesDashboardDataDAL(objBO);
        }

        public DataSet GetPuchaseDashboardDataBL(DashboardBO objBO)
        {
            return objDAL.GetPuchaseDashboardDataDAL(objBO);
        }
    }
}
