﻿using DistributorSystemDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DistributorSystemBL
{
   public class ListPriceBL
    {
        ListPriceDAL objlistprice = new ListPriceDAL();
        public DataTable getListPriceDetailsBL()
        {
            return objlistprice.GetListPriceDetails();
        }

        public DataTable getCustomerDetailsBL(string distNum)
        {
            return objlistprice.GetCustomersBasedOnDistributor(distNum);
        }
    }
}
