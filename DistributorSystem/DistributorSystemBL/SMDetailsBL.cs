﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DistributorSystemDAL;
using DistributorSystemBO;
namespace DistributorSystemBL
{
    public class SMDetailsBL
    {

        SMDetailsDAL objSMDAL = new SMDetailsDAL();

        public DataTable getSMDetailsBL()
        {
            return objSMDAL.getSMDetailsDAL();
        }

        public SMDetailsBO saveSMDetailsBL(SMDetailsBO objSMBO)
        {
            return objSMDAL.saveSMDetailsDAL(objSMBO);
        }
        public DataTable getCustDetailsBL(SMDetailsBO objSMBO)
        {
            return objSMDAL.getCustDetailsDAL(objSMBO);
        }
        public DataTable getSM_Mapping_DetailsBL(SMDetailsBO objSMBO)
        {
            return objSMDAL.getSM_Mapping_DetailsDAL(objSMBO);
        }
    }
}