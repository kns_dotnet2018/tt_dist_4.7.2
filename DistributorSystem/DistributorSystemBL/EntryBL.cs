﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemDAL;
using DistributorSystemBO;

namespace DistributorSystemBL
{
    public class EntryBL
    {
        EntryDAL objEntryDAL = new EntryDAL();
        public DataTable GetCustomersBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetCustomerDAL(objEntryBO);
        }

        public DataSet SaveBulkBudgetBL(DataTable Distributors, int valuein)
        {
            return objEntryDAL.SaveBulkBudgetBL(Distributors, valuein);
        }

        public DataTable GetCustomerMonthlySalesBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetCustomerMonthlySalesDAL(objEntryBO);
        }

        public DataSet SaveBulkMonthlySalesBL(DataTable Distributors, int Valuein)
        {
            return objEntryDAL.SaveBulkMonthlySalesDAL(Distributors, Valuein);
        }

        public DataTable GetAnualTargetBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetAnualTargetDAL(objEntryBO);
        }

        public DataTable GetSalesManBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetSalesManDAL(objEntryBO);
        }

        public DataTable GetSalesManDetailsBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetSalesManDetailsDAL(objEntryBO);
        }

        public DataTable GetCustomerBasedOnSalesBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetCustomerBasedOnSalesDAL(objEntryBO);
        }

        public DataSet SaveBulkMonthlySalesAdminBL(DataTable Distributors)
        {
            return objEntryDAL.SaveBulkMonthlySalesAdminBL(Distributors);
        }

        public DataTable GetCustomerMonthlySalesForAdminBL(EntryBO objEntryBO)
        {
            return objEntryDAL.GetCustomerMonthlySalesForAdminDAL(objEntryBO);
        }
    }
}
