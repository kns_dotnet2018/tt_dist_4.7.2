﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemBO;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    
    public class UserDetailsBL
    {
        UserDetailsDAL objUserDAL = new UserDetailsDAL();

        public DataTable getUserDetailsBL()
        {
            return objUserDAL.getUserDetailsDAL();
        }

        public UserDetailsBO saveUserDetailsBL(UserDetailsBO objUserBO)
        {
            return objUserDAL.saveUserDetailsDAL(objUserBO);
        }
    }
}
