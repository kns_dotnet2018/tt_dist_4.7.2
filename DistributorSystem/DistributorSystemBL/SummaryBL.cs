﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DistributorSystemBO;
using DistributorSystemDAL;

namespace DistributorSystemBL
{
    public class SummaryBL
    {
        SummaryDAL objDAL = new SummaryDAL();
        public DataSet GetCustomerAndProjectsBL(SummaryBO objBO)
        {
            return objDAL.GetCustomerAndProjectsDAL(objBO);
        }

        public DataSet GetProjectDetailsBL(SummaryBO objBO)
        {
            return objDAL.GetProjectDetailsDAL(objBO);
        }

        public DataTable GetPurchaseSummaryBL(SummaryBO objBO)
        {
            return objDAL.GetPurchaseSummaryDAL(objBO);
        }

        public DataTable LoadBudgetSummaryBL(SummaryBO objSummaryBO)
        {
            return objDAL.getEntryMain(objSummaryBO);
        }

        public DataTable GrandTotalforEntryBL(DataTable dtMain)
        {
            DataTable Dtbudget = new DataTable();

            Dtbudget.Columns.Add("gold_flag", typeof(string));        //1
            Dtbudget.Columns.Add("top_flag", typeof(string));         //2
            Dtbudget.Columns.Add("five_years_flag", typeof(string));   //3
            Dtbudget.Columns.Add("bb_flag", typeof(string));            //4
            Dtbudget.Columns.Add("SPC_flag", typeof(string));           //5

            Dtbudget.Columns.Add("item_id", typeof(string));               //6
            Dtbudget.Columns.Add("item_family_id", typeof(string));         //7
            Dtbudget.Columns.Add("item_family_name", typeof(string));           //8
            Dtbudget.Columns.Add("item_sub_family_id", typeof(string));        //9
            Dtbudget.Columns.Add("item_sub_family_name", typeof(string));      //10

            Dtbudget.Columns.Add("item_group_code", typeof(string));           //11
            Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string));       //12
            Dtbudget.Columns.Add("item_code", typeof(string));               //13
            Dtbudget.Columns.Add("item_short_name", typeof(string));         //14
            Dtbudget.Columns.Add("item_description", typeof(string));          //15
            Dtbudget.Columns.Add("customer_number", typeof(string));             //16

            Dtbudget.Columns.Add("sales_qty_year_2", typeof(string));        //17
            Dtbudget.Columns.Add("sales_qty_year_1", typeof(string));         //18
            Dtbudget.Columns.Add("sales_qty_year_0", typeof(string));            //19
            Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string));     //20

            Dtbudget.Columns.Add("sales_value_year_2", typeof(string));     //21
            Dtbudget.Columns.Add("sales_value_year_1", typeof(string));  //22
            Dtbudget.Columns.Add("sales_value_year_0", typeof(string));     //23
            Dtbudget.Columns.Add("estimate_value_next_year", typeof(string));    //24

            Dtbudget.Columns.Add("review_flag", typeof(string));    //25
            Dtbudget.Columns.Add("salesengineer_id", typeof(string));     //26
            Dtbudget.Columns.Add("status_sales_engineer", typeof(string));      //27
            Dtbudget.Columns.Add("status_branch_manager", typeof(string));    //28
            Dtbudget.Columns.Add("status_ho", typeof(string));                 //29

            Dtbudget.Columns.Add("sumFlag", typeof(string));      //30

            //quality variance
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string));     //31
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string));       //32
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string));      //33

            //Quality Percentage
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string));    //34
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string));     //35
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string));    //36

            //Value Percentage
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));   //37
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));    //38
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));    //39

            //price per unit
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));    //40
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));    //41
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));    //42
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));     //43

            //price changes per year
            Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string));     //44
            Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string));     //45
            Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string));     //46

            Dtbudget.Columns.Add("budget_id", typeof(string));    //47
            Dtbudget.Columns.Add("estimate_rate_next_year", typeof(string));    //48
            Dtbudget.Rows.Add(
                                   dtMain.Rows[0].ItemArray[0].ToString(),//gold_flag
                                   dtMain.Rows[0].ItemArray[1].ToString(),//top_flag
                                   dtMain.Rows[0].ItemArray[2].ToString(),//five_years_flag
                                   dtMain.Rows[0].ItemArray[3].ToString(),//bb_flag
                                   dtMain.Rows[0].ItemArray[4].ToString(),//SPC_flag
                                   dtMain.Rows[0].ItemArray[5].ToString(),
                                   dtMain.Rows[0].ItemArray[6].ToString(),
                                   dtMain.Rows[0].ItemArray[7].ToString(),
                                   dtMain.Rows[0].ItemArray[8].ToString(),
                                   dtMain.Rows[0].ItemArray[9].ToString(),
                                   dtMain.Rows[0].ItemArray[10].ToString(),
                                   dtMain.Rows[0].ItemArray[11].ToString(),
                                   dtMain.Rows[0].ItemArray[12].ToString(),
                                   dtMain.Rows[0].ItemArray[13].ToString(),
                                   dtMain.Rows[0].ItemArray[14].ToString(),
                                   dtMain.Rows[0].ItemArray[15].ToString(),
                                   dtMain.Rows[0].ItemArray[16].ToString(),
                                   dtMain.Rows[0].ItemArray[17].ToString(),
                                   dtMain.Rows[0].ItemArray[18].ToString(),
                                   dtMain.Rows[0].ItemArray[19].ToString(),
                                   dtMain.Rows[0].ItemArray[20].ToString(),
                                   dtMain.Rows[0].ItemArray[21].ToString(),
                                   dtMain.Rows[0].ItemArray[22].ToString(),
                                   dtMain.Rows[0].ItemArray[23].ToString(),
                                   dtMain.Rows[0].ItemArray[24].ToString(),
                                   dtMain.Rows[0].ItemArray[25].ToString(),
                                   dtMain.Rows[0].ItemArray[26].ToString(),
                                   dtMain.Rows[0].ItemArray[27].ToString(),
                                   dtMain.Rows[0].ItemArray[28].ToString(),
                                   dtMain.Rows[0].ItemArray[29].ToString(),
                                   dtMain.Rows[0].ItemArray[30].ToString(),
                                   dtMain.Rows[0].ItemArray[31].ToString(),
                                   dtMain.Rows[0].ItemArray[32].ToString(),
                                   dtMain.Rows[0].ItemArray[33].ToString(),
                                   dtMain.Rows[0].ItemArray[34].ToString(),
                                   dtMain.Rows[0].ItemArray[35].ToString(),
                                   dtMain.Rows[0].ItemArray[36].ToString(),
                                   dtMain.Rows[0].ItemArray[37].ToString(),
                                   dtMain.Rows[0].ItemArray[38].ToString(),
                                   dtMain.Rows[0].ItemArray[39].ToString(),
                                   dtMain.Rows[0].ItemArray[40].ToString(),
                                   dtMain.Rows[0].ItemArray[41].ToString(),
                                   dtMain.Rows[0].ItemArray[42].ToString(),
                                   dtMain.Rows[0].ItemArray[43].ToString(),
                                   dtMain.Rows[0].ItemArray[44].ToString(),
                                   dtMain.Rows[0].ItemArray[45].ToString(),
                                   dtMain.Rows[0].ItemArray[46].ToString(),
                                   dtMain.Rows[0].ItemArray[47].ToString()
                                   );

            int ji = dtMain.Rows.Count;

            Dtbudget.Rows.Add(
                                  dtMain.Rows[ji - 1].ItemArray[0].ToString(),//gold_flag
                                  dtMain.Rows[ji - 1].ItemArray[1].ToString(),//top_flag
                                  dtMain.Rows[ji - 1].ItemArray[2].ToString(),//five_years_flag
                                  dtMain.Rows[ji - 1].ItemArray[3].ToString(),//bb_flag
                                  dtMain.Rows[ji - 1].ItemArray[4].ToString(),//SPC_flag
                                  dtMain.Rows[ji - 1].ItemArray[5].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[6].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[7].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[8].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[9].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[10].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[11].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[12].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[13].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[14].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[15].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[16].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[17].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[18].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[19].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[20].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[21].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[22].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[23].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[24].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[25].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[26].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[27].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[28].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[29].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[30].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[31].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[32].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[33].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[34].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[35].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[36].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[37].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[38].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[39].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[40].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[41].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[42].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[43].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[44].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[45].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[46].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[47].ToString()
                                  );
            for (int j = 1; j < dtMain.Rows.Count; j++)
            {
                Dtbudget.Rows.Add(
                                 dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                                 dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                                 dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                                 dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                                 dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag
                                 dtMain.Rows[j].ItemArray[5].ToString(),
                                 dtMain.Rows[j].ItemArray[6].ToString(),
                                 dtMain.Rows[j].ItemArray[7].ToString(),
                                 dtMain.Rows[j].ItemArray[8].ToString(),
                                 dtMain.Rows[j].ItemArray[9].ToString(),
                                 dtMain.Rows[j].ItemArray[10].ToString(),
                                 dtMain.Rows[j].ItemArray[11].ToString(),
                                 dtMain.Rows[j].ItemArray[12].ToString(),
                                 dtMain.Rows[j].ItemArray[13].ToString(),
                                 dtMain.Rows[j].ItemArray[14].ToString(),
                                 dtMain.Rows[j].ItemArray[15].ToString(),
                                 dtMain.Rows[j].ItemArray[16].ToString(),
                                 dtMain.Rows[j].ItemArray[17].ToString(),
                                 dtMain.Rows[j].ItemArray[18].ToString(),
                                 dtMain.Rows[j].ItemArray[19].ToString(),
                                 dtMain.Rows[j].ItemArray[20].ToString(),
                                 dtMain.Rows[j].ItemArray[21].ToString(),
                                 dtMain.Rows[j].ItemArray[22].ToString(),
                                 dtMain.Rows[j].ItemArray[23].ToString(),
                                 dtMain.Rows[j].ItemArray[24].ToString(),
                                 dtMain.Rows[j].ItemArray[25].ToString(),
                                 dtMain.Rows[j].ItemArray[26].ToString(),
                                 dtMain.Rows[j].ItemArray[27].ToString(),
                                 dtMain.Rows[j].ItemArray[28].ToString(),
                                 dtMain.Rows[j].ItemArray[29].ToString(),
                                 dtMain.Rows[j].ItemArray[30].ToString(),
                                 dtMain.Rows[j].ItemArray[31].ToString(),
                                 dtMain.Rows[j].ItemArray[32].ToString(),
                                 dtMain.Rows[j].ItemArray[33].ToString(),
                                 dtMain.Rows[j].ItemArray[34].ToString(),
                                 dtMain.Rows[j].ItemArray[35].ToString(),
                                 dtMain.Rows[j].ItemArray[36].ToString(),
                                 dtMain.Rows[j].ItemArray[37].ToString(),
                                 dtMain.Rows[j].ItemArray[38].ToString(),
                                 dtMain.Rows[j].ItemArray[39].ToString(),
                                 dtMain.Rows[j].ItemArray[40].ToString(),
                                 dtMain.Rows[j].ItemArray[41].ToString(),
                                 dtMain.Rows[j].ItemArray[42].ToString(),
                                 dtMain.Rows[j].ItemArray[43].ToString(),
                                 dtMain.Rows[j].ItemArray[44].ToString(),
                                 dtMain.Rows[j].ItemArray[45].ToString(),
                                 dtMain.Rows[j].ItemArray[46].ToString(),
                                 dtMain.Rows[j].ItemArray[47].ToString()
                                 );
            }
            return Dtbudget;
        }

        public DataTable AddInsertPerToolsBL(DataTable dtMain)
        {
            try
            {
                DataTable Dtbudget = new DataTable();

                Dtbudget.Columns.Add("gold_flag", typeof(string)); //1
                Dtbudget.Columns.Add("top_flag", typeof(string)); //2
                Dtbudget.Columns.Add("five_years_flag", typeof(string)); //3
                Dtbudget.Columns.Add("bb_flag", typeof(string)); //4
                Dtbudget.Columns.Add("SPC_flag", typeof(string));//5

                Dtbudget.Columns.Add("item_id", typeof(string)); //6
                Dtbudget.Columns.Add("item_family_id", typeof(string));//7
                Dtbudget.Columns.Add("item_family_name", typeof(string)); //8
                Dtbudget.Columns.Add("item_sub_family_id", typeof(string)); //9
                Dtbudget.Columns.Add("item_sub_family_name", typeof(string)); //10

                Dtbudget.Columns.Add("item_group_code", typeof(string)); //11
                Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string)); //12
                Dtbudget.Columns.Add("item_code", typeof(string)); //13
                Dtbudget.Columns.Add("item_short_name", typeof(string)); //14
                Dtbudget.Columns.Add("item_description", typeof(string)); //15
                Dtbudget.Columns.Add("Customer_region", typeof(string)); //16

                Dtbudget.Columns.Add("sales_qty_year_2", typeof(string)); //17
                Dtbudget.Columns.Add("sales_qty_year_1", typeof(string)); //18
                Dtbudget.Columns.Add("sales_qty_year_0", typeof(string)); //19
                Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string)); //20

                Dtbudget.Columns.Add("insertpertool_year_2", typeof(string)); //17
                Dtbudget.Columns.Add("insertpertool_year_1", typeof(string)); //18
                Dtbudget.Columns.Add("insertpertool_year_0", typeof(string)); //19
                Dtbudget.Columns.Add("insertpertool_next_year", typeof(string)); //20

                Dtbudget.Columns.Add("sales_value_year_2", typeof(string)); //21
                Dtbudget.Columns.Add("sales_value_year_1", typeof(string)); //22
                Dtbudget.Columns.Add("sales_value_year_0", typeof(string)); //23
                Dtbudget.Columns.Add("estimate_value_next_year", typeof(string)); //24

                Dtbudget.Columns.Add("sumFlag", typeof(string)); //25

                //quality variance
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string)); //26
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string)); //27
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string)); //28

                //Quality Percentage
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string)); //29
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string)); //30
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string)); //31

                //Value Percentage
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));//32
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));//33
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));//34

                //price per unit
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));//35
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));//36
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));//37
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));//38

                //price changes per year
                Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string)); //39
                Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string)); //40
                Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string)); //41
                for (int j = 0; j < dtMain.Rows.Count; j++)
                {
                    Dtbudget.Rows.Add(
                                     dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                                     dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                                     dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                                     dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                                     dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag

                                     dtMain.Rows[j].ItemArray[5].ToString(),
                                     dtMain.Rows[j].ItemArray[6].ToString(),
                                     dtMain.Rows[j].ItemArray[7].ToString(),
                                     dtMain.Rows[j].ItemArray[8].ToString(),
                                     dtMain.Rows[j].ItemArray[9].ToString(),

                                     dtMain.Rows[j].ItemArray[10].ToString(),
                                     dtMain.Rows[j].ItemArray[11].ToString(),
                                     dtMain.Rows[j].ItemArray[12].ToString(),
                                     dtMain.Rows[j].ItemArray[13].ToString(),
                                     dtMain.Rows[j].ItemArray[14].ToString(),
                                     dtMain.Rows[j].ItemArray[15].ToString(),

                                     dtMain.Rows[j].ItemArray[16].ToString(),
                                     dtMain.Rows[j].ItemArray[17].ToString(),
                                     dtMain.Rows[j].ItemArray[18].ToString(),
                                     dtMain.Rows[j].ItemArray[19].ToString(),
                                     "", "", "", "",
                                     dtMain.Rows[j].ItemArray[20].ToString(),
                                     dtMain.Rows[j].ItemArray[21].ToString(),
                                     dtMain.Rows[j].ItemArray[22].ToString(),
                                     dtMain.Rows[j].ItemArray[23].ToString(),

                                     dtMain.Rows[j].ItemArray[24].ToString(),//sum flag
                                     dtMain.Rows[j].ItemArray[25].ToString(),
                                     dtMain.Rows[j].ItemArray[26].ToString(),
                                     dtMain.Rows[j].ItemArray[27].ToString(),
                                     dtMain.Rows[j].ItemArray[28].ToString(),
                                     dtMain.Rows[j].ItemArray[29].ToString(),
                                     dtMain.Rows[j].ItemArray[30].ToString(),
                                     dtMain.Rows[j].ItemArray[31].ToString(),
                                     dtMain.Rows[j].ItemArray[32].ToString(),
                                     dtMain.Rows[j].ItemArray[33].ToString(),
                                     dtMain.Rows[j].ItemArray[34].ToString(),
                                     dtMain.Rows[j].ItemArray[35].ToString(),
                                     dtMain.Rows[j].ItemArray[36].ToString(),
                                     dtMain.Rows[j].ItemArray[37].ToString(),
                                     dtMain.Rows[j].ItemArray[38].ToString(),
                                     dtMain.Rows[j].ItemArray[39].ToString(),
                                     dtMain.Rows[j].ItemArray[40].ToString()

                                     );
                }

                DataTable dtreturn = objDAL.AddInsertPerToolDAL(Dtbudget);
                //dtMain.Merge(dtreturn);
                dtMain.Columns.Add("insertpertool_year_2", typeof(string)); //17
                dtMain.Columns.Add("insertpertool_year_1", typeof(string)); //18
                dtMain.Columns.Add("insertpertool_year_0", typeof(string)); //19
                dtMain.Columns.Add("insertpertool_next_year", typeof(string)); //20
                dtMain.Columns.Add("ID", typeof(int)); //20
                dtMain = objDAL.RemoveEmptyRowsDAL(dtMain);
                int ji = 0;
                foreach (DataRow row in dtMain.Rows)
                {
                    if (dtreturn.Rows.Count > ji && row["insert_or_tool_flag"] != "FamilyHeading" && row["insert_or_tool_flag"] != "SubFamHeading")
                    {
                        //row["ID"] = ji;
                        row["insertpertool_year_2"] = dtreturn.Rows[ji].ItemArray[20].ToString();
                        row["insertpertool_year_1"] = dtreturn.Rows[ji].ItemArray[21].ToString();
                        row["insertpertool_year_0"] = dtreturn.Rows[ji].ItemArray[22].ToString();
                        row["insertpertool_next_year"] = dtreturn.Rows[ji].ItemArray[23].ToString();
                    }
                    ji++;
                }


            }
            catch (Exception ex)
            {
                //LogError(ex);
            }
            return dtMain;
        }

        public DataTable getSpecialGroupValueSumBL(SummaryBO objSummaryBO)
        {
            return objDAL.getSpecialGroupValueSumDAL(objSummaryBO);
        }

        public DataTable getValueSumBL(SummaryBO objSummaryBO)
        {
            objSummaryBO.Flag = null;
            return objDAL.getValueSumDAL(objSummaryBO);
        }

        public DataTable salesbyfamilyBL(SummaryBO objSummaryBO)
        {
            return objDAL.salesbyfamilyDAL(objSummaryBO);
        }

        public bool DeleteEmptyRowsBL(float top0, float top1, float top2, float top3, float top4, float top5, float top6)
        {
            DataTable dt1 = new DataTable();

            dt1.Columns.Add("sales_value_year_1", typeof(string));
            dt1.Columns.Add("sales_value_year_0", typeof(string));
            dt1.Columns.Add("estimate_value_next_year", typeof(string));
            dt1.Columns.Add("change", typeof(string));
            dt1.Columns.Add("ytd", typeof(string));
            dt1.Columns.Add("ach", typeof(string));
            dt1.Columns.Add("arate", typeof(string));
            dt1.Rows.Add(top0, top1, top2, top3, top4, top5, top6);
            DataRow dr = dt1.Rows[0];
            return  AreAllColumnsEmpty(dr);


        }

        bool AreAllColumnsEmpty(DataRow dr)
        {
            dr.ItemArray = dr.ItemArray.Select(i => i == null ? string.Empty : i.ToString()).ToArray();
            if (dr == null)
            {
                return true;
            }
            else
            {
                foreach (string value in dr.ItemArray)
                {
                    if (value != "0" && value != "" && value != "0.00000")
                    {
                        return false;
                    }
                }
                return true;
            }
        }


        public int getBudgetYearBL()
        {
           return objDAL.getBudgetYearDAL();
        }

        public int getActualYearBL()
        {
            return objDAL.getActualYearDAL();
        }

        public int getActualMonthBL()
        {
            return objDAL.getActualMonthDAL();
        }

        public DataTable getValueSum_SFBL(SummaryBO objSummaryBO)
        {
            return objDAL.getValueSum_SFDAL(objSummaryBO);
        }



        public DataTable getSalesbyApplicationBL(SummaryBO objSummaryBO)
        {
            return objDAL.getsalesbyApplicationDAL(objSummaryBO);
        }

        public DataTable getSalesbycustomersBL(SummaryBO objSummaryBO)
        {
            return objDAL.getSalesbycustomersDAL(objSummaryBO);
        }

        public DataTable getSalesbyApplicationQtyBL(SummaryBO objSummaryBO)
        {
            return objDAL.getSalesbyApplicationQtyDAL(objSummaryBO);
        }

        public DataTable getExceptionReportBL(SummaryBO objSummaryBO)
        {
            return objDAL.getExceptionReportDAL(objSummaryBO);
        }

        public DataTable SendMailBL(SummaryBO objSummaryBO)
        {
            return objDAL.SendMailDAL(objSummaryBO);
        }
    }
}
