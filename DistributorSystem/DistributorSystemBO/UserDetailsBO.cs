﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class UserDetailsBO
    {
        public string userid { get; set; }
        public string username { get; set; }
        public string emailid { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        public string role { get; set; }
        public string menuid { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
    }
}
