﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class EntryBO
    {
        public int year { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public string distributor_name { get; set; }
        public string customer_number { get; set; }
        public string salesman_id { get; set; }
        public int value { get; set; }
    }
}
