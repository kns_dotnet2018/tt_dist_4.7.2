﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class SummaryBO
    {
        public string distributor_number { get; set; }
        public string customer_number { get; set; }
        public string project_number { get; set; }
        public string Flag { get; set; }
        public string Flag_Desc { get; set; }
        public string fname { get; set; }
        public int year { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public int value { get; set; }
    }
    public class BudgetSummaryBO
    {
        public string FAMILY_NAME { get; set; }
        public string SUBFAMILY_NAME { get; set; }
        public string APPLICATION_DESC { get; set; }
        public string Value { get; set; }
        public string Quantity { get; set; }
    }
}
