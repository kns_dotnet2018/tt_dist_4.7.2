﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class LoginBO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserFullName { get; set; }
        public string LoginMailID { get; set; }
        public string EngineerId { get; set; }
        public string ErrorMessege { get; set; }
        public int ErrorNum { get; set; }
        public string Distributor_number { get; set; }
        public string Distributor_name { get; set; }

        public string Role { get; set; }
        public int Quote_Flag { get; set; }
        public string genrated_password { get; set; }
        public int Price_Flag { get; set; }
        public string cter { get; set; }
    }
}
