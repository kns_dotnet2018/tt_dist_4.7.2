﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DistributorSystemBO
{
    public class SaleBO
    {
        public int item_available_id { get; set; }
        public int cust_num { get; set; }
        public int order_qty { get; set; }
        public int user_id { get; set; }
        public int Err_code { get; set; }
        public string Err_msg { get; set; }
    }
}
